%
%  Autor(s)      : Arman Elibol & Jordi Ferrer Plana
%  e-mail        : aelibol@eia.udg.edu, jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute where the intersection between two lines is.
%
%  File          : LineIntersect.m
%  Date          : 27/02/2006 - 27/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  LineIntersect Given the beginin and the end coordinates of two line segements,
%                compute if the crossing point is inside the area defined
%                by that points.
%
%      Ok = LineIntersect ( p, k )
%
%     Input Parameters:
%      p: 1x4 vector containing [x1, y1, x2, y2] as the first line segement.
%      k: 1x4 vector containing [x1, y1, x2, y2] as the second line segement.
%
%     Output Parameters:
%      Ok: Boolean saying whether the two segments are a crossing or not.
%

function Ok = LineIntersect ( p, k )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Slopes for the segments
  mp = (p(1,4)-p(1,2)) / (p(1,3)-p(1,1));
  mk = (k(1,4)-k(1,2)) / (k(1,3)-k(1,1));

  % The whether the lines cross or not
  if abs ( mp - mk ) < eps;
    Ok = false;
  else
    % x, y Intersection Coordinates
    xi = (mp*p(1,1)-mk*k(1,1)-p(1,2)+k(1,2))/(mp-mk);
    yi = mk*(xi-k(1,1))+k(1,2);

    % Check if intersection makes crossing
    % Bounding box coordinates
    y1 = min ( max ( p(1,2), p(1,4) ), max ( k(1,2), k(1,4) ) );
    y2 = max ( min ( p(1,2), p(1,4) ), min ( k(1,2), k(1,4) ) );
    x1 = min ( max ( p(1,1), p(1,3) ), max ( k(1,1), k(1,3) ) );
    x2 = max ( min ( p(1,1), p(1,3) ), min ( k(1,1), k(1,3) ) );
    % Is the crossing point inside the bounding box?
    Ok = yi >= y2 && yi <= y1 && xi >= x2 && xi <= x1;
  end
end
