%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Extract the submosaic given a GMML structure and a list of nodes.
%
%  File          : SubMosaicSegments.m
%  Date          : 07/03/2006 - 09/03/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  SubMosaicSegments Extract the submosaic given a GMML structure and a list of nodes.
%
%      [MR NodeMap, Segments] = SubMosaicSegments ( M, List )
%
%     Input Parameters:
%      M: GMML Mosaic Structure.
%      List: 1xn list indicating the nodes that will be kept in the
%            resulting mosaic.
%
%     Output Parameters:
%      Mn: Resulting cropped GMML Mosaic Structure.
%      NodeMap: 1xn vector mapping the old nodes to the new ones.
%

function [Mn, NodeMap] = SubMosaicSegments ( M, Segments )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 2, 2, nargout ) );

  % Number of Nodes
  N = size ( M.nodes, 2 );

  NumSegments = size ( Segments, 2 );
  if isempty ( Segments ) || size ( Segments, 1 ) ~= 2 || NumSegments < 1;
    error ( 'MATLAB:SubMosaicSegments:Input', 'The Segments must be a non-empty 2xn matrix!' );
  end

  if ~isempty ( find ( ( Segments(1,:) - Segments(2,:) ) < 1 ) );
    error ( 'MATLAB:SubMosaicSegments:Input', [ 'Each element in the first row of ' ...
            'Segments must be bigger than it''s correspondence in the second row!' ] );
  end
      
  % Number of nodes for all segments.
  NumNodes = sum ( Segments(1,:) - Segments(2,:) + 1 );

  % Detect Errors
  EmptyNodes = 0;
  MultipleChild = 0;
  ParentError = 0;

  % Init the Header
  Mn.init = M.init;
  % I don't know if it is worth to change:
  %   M.init.mosaic_size
  %   M.init.mosaic_origin

  % Init the Nodes
  Mn.nodes = repmat ( M.nodes(1), 1, NumNodes );
  l = 1;
  NodeMap = zeros ( 1, N );
  for i = 1:NumSegments;
    % Compute the nodes in the current segment
    Nodes = Segments(2,i):Segments(1,i);
    n = length ( Nodes );
    % Scan all the nodes in the current segment
    for j = 1:n
      % Node Index
      Idx = Nodes(j);
      Mn.nodes(l) = M.nodes(Idx);
%      Mn.nodes(l).index = l;
      
      NumEdges = length ( M.nodes(Idx).edges );
      if NumEdges == 1;
         if M.nodes(Idx).edges(1).index == (Idx - 1);
           Mn.nodes(l).edges(1).index = l - 1;
         else
           % Edge is not pointing to the previous node
           ParentError = ParentError + 1;
         end
      elseif NumEdges == 0;
        % No edges
        EmptyNodes = EmptyNodes + 1;
      else
        % More than one edge
        MultipleChild = MultipleChild + 1;
      end

      % Compute the mapping
      NodeMap(Idx) = l;
      l = l + 1;
    end
  end

  if EmptyNodes > 0;
    warning ( 'MATLAB:SubMosaicSegments:Input', sprintf ( 'There are %d nodes without any child!', EmptyNodes ) );
  end
  if MultipleChild > 0;
    warning ( 'MATLAB:SubMosaicSegments:Input', sprintf ( 'There are %d nodes with multiple children!', MultipleChild ) );
  end
  if ParentError > 0;
    warning ( 'MATLAB:SubMosaicSegments:Input', sprintf ( 'There are %d nodes that the node parent n is not the node n-1!', ParentError ) );
  end
end

