%
%  Autor(s)      : Jordi Ferrer Plana & Olivier Delaunoy
%  e-mail        : jferrerp@eia.udg.edu, delaunoy@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute vehicle UTM pose from vehicle wrt. 3D Mosaic Frame.
%
%  File          : mosaicCalcPoseFromMosaic.m
%  Date          : 03/04/2006 - 07/08/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%
%  mosaicCalcPoseFromMosaic Compute the Vehicle Pose in UTM world coordinates
%                           according to the input parameter RTs that is the
%                           Camera Pose (rotation and translation) with respecte
%                           to the 3D Mosaic Frame (please see WARNING Notes).
%
%    - WARNING: The pose information that is read in the file (x, y, z,
%               Roll, Pitch, Yaw) is interpreted as:
%               - The (x, y, z) coordinate vector defines the UTM position
%                 of the vehicle in meters.
%               - The (Roll, Pitch, Yaw) vector defines the vehicle angles:
%                   Roll : Angle acording to the x axis of the robot {V}.
%                   Pitch: Angle acording to the y axis of the robot {V}.
%                   Yaw  : Angle acording to the z World axis {W} that 
%                          corresponds to the North. It's a clockwise angle
%                          (it is sign inverted).
%               - By default (when V_Pose_C parameter is not given) the
%                 vehicle and the camera center have the same 3D position and 
%                 a fixed 90� angle between the camera an the vehicle is applied.
%              x
%    {i} u      ^ 
%       +---->--|-------+    - (u, v) The hor. and vert. axis of the 2D Image Plane {i}.
%     v |       |       |    - (x, y) The hor. and vert. axis of the 3D Vehicle Frame {V}.
%       |       | {V}   | y  - The z axis of the vehicle is pointing down.
%       v       +-------->   - The 3D Camera Frame {C} is rotated 90� w.r.t.
%       |       | {C}   | x'   3D Vehicle Frame {V} arround the z.
%       |       |       |
%       +-------|-------+
%               v
%              y'
%
%    - Nomenclature for the transformations used in the code:
%       * 3D Rotation: D_R_S (S = 3D Source Frame; D = 3D Destination Frame)
%        Read as: D_R_S is the rotation that transforms coordinates
%                       in the S Frame to the D Frame.
%       * 3D Translation: D_T_S (S = 3D Source Frame; D = 3D Destination Frame)
%        Read as: D_T_S is the translation of the S frame from the (0, 0, 0)
%                       defined in coordinates of the D frame.
%
%        Example: Transformation of a 3D Point in World Frame {W}
%                 into a 3D point in the Camera Frame {C}:
%                   C_X = C_R_W * W_X + C_T_W
%
%       * In 2D is done in the same way but in non capital letters:
%           d_H_s is a planar transformation from the 2D Source Frame s
%                 to the 2D Destination Frame d.
%
%  Usage:
%
%      M = mosaicCalcPoseFromMosaic ( M, RTs [, V_Pose_C] );
%
%     Input Parameters:
%      M: GMML structure representing the mosaic. Only the mosaic origin and
%         the number of nodes will be used.
%      RTs: Vector that, for each node in the mosaic, contains an structure with
%           the C_R_W (cRw) rotation matrix and the translation vector W_T_C
%           (wTc). Although a "w" used in the input RTs, it refers to the
%           3D Mosaic frame.
%      V_Pose_C: The rigid motion of the 3D Camera Frame {C} with respect
%                to the 3D Vehicle Frame {V} in format (X, Y, Z, Roll,
%                Pitch, Yaw).
%                The (X, Y, Z) is the 3D Camera Frame (the camera center)
%                translation with respect to the 3D Vehicle Frame {V}.
%                The rotation can be defined in one of the following ways:
%                  1. According to the fixed axis of the 3D Vehicle Frame {V}
%                     as a Yaw rotation in Z, a Pitch rotation in Y and
%                     a Roll rotation in X.
%                  2. According to the mobile axis of the 3D Camera Frame {C}
%                     as a Roll rotation in X, a Pitch rotation in Y and a Yaw
%                     rotation in Z.
%
%     Output Parameters:
%      M: The following fields of the input GMML structure representing the
%         mosaic will be calculated:
%          - Vehicle Pose Matrix.
%          - The covariance matrices corresponding to the Homography
%            coefficients is set to 0.
%          - The Mosaic Origin in pixels in respect to the UTM coordinates.
%          - The Mosaic Size in pixels.
%

function M = mosaicCalcPoseFromMosaic ( M, RTs, V_Pose_C )
  % Test the Input Parameters
  error ( nargchk ( 2, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % If the Rigid Motion between Camera and Vehicle is not given assume
  % the vehicle position as the camera center and that the camera rotated 90�
  % relative to the z axis of the vehicle (see WARNING notes).
  if nargin < 3;
    V_Pose_C = [ 0 0 0 0 0 pi/2 ];
  else
    % Check Vector Size
    [m n] = size ( V_Pose_C );
    if ( m ~= 6 || n ~= 1 ) && ( m ~= 1 || n ~= 6 );
      error ( 'MATLAB:mosaicCalcPoseFromMosaic:Input', ...
              'V_Pose_C must be a 1x6 vector containing (x, y, z, Roll, Pitch, Yaw)!' );
    end
  end

  % Check Number of Nodes
  NumNodes = size ( M.nodes, 2 );
  if NumNodes ~= size ( RTs, 2 );
    error ( 'MATLAB:mosaicCalcPoseFromMosaic:Input', ...
            'Number of nodes in M and length of RTs must agree!' );
  end

  % Rigid Camera Frame {C} Motion w.r.t. the 3D Vehicle Frame {V}
  V_X_C     = V_Pose_C(1);
  V_Y_C     = V_Pose_C(2);
  V_Z_C     = V_Pose_C(3);
  V_Roll_C  = V_Pose_C(4);
  V_Pitch_C = V_Pose_C(5);
  V_Yaw_C   = V_Pose_C(6);

  % Compute the Transformation 3D Mosaic Frame -> 3D World Frame: W_X = W_R_M * M_X + W_T_M
  % Mosaic Rotation w.r.t. 3D World Frame
  W_R_M = RotX ( pi );
  % Mosaic Translation w.r.t. 3D World Frame
  % (The mosaic is translated according to the X, Y origin but not in moved in Z).
  W_T_M = [ M.init.mosaic_origin.x; M.init.mosaic_origin.y; 0 ];

  % Compute the Transformation 3D Camera Frame -> 3D Vehicle Frame: V_X = V_R_C * C_X + V_T_C
  % Camera Rotation w.r.t. 3D Vehicle Frame
  V_R_C = RotX ( V_Roll_C ) * RotY ( V_Pitch_C ) * RotZ ( V_Yaw_C );
  % Camera Translation w.r.t. 3D Vehicle Frame
  V_T_C = [ V_X_C; V_Y_C; V_Z_C ];

  % Compute translation of the vehicle center in 3D Camera Coordinate Frame
  C_T_V = -V_R_C' * V_T_C;
  
  % Scan Camera Poses
  for i = 1 : NumNodes;
    % Camera Pose with respect to 3D Mosaic Frame.
    %   RTs: cRw  +  wTc  =>  "w" == "3D Mosaic Frame"
    C_R_M = RTs(i).cRw;
    M_R_C = C_R_M';
    M_T_C = RTs(i).wTc;

    % Compute rotation and translation from 3D Vehicle Frame to 3D Mosaic Frame.
    M_R_V = M_R_C * V_R_C';
    M_T_V = M_R_C * C_T_V + M_T_C;

    % Compute rotation and translation from 3D Vehicle Frame to 3D World Frame.
    W_R_V = W_R_M * M_R_V;
    W_T_V = W_R_M * M_T_V + W_T_M;

    % Vehicle Rotation w.r.t. 3D World Frame (Computed allways in mobile axis: post-multiplication)
    %  Rotation in X (pi): Point the Z axis down (vehicle frame is like that.
    %                      As yaw is a clockwise angle, now is rotation in the right direction.
    %  Rotation in Z (Yaw-pi/2): First rotation with respect to the heading angle. As zero of angles is
    %                       "x" axis, but Yaw=0 is North (y axis), angle must be corrected with pi/2 rads.
    %                       Note that after first rotation in x, this correction is negative.
    %  Rotation in Y (Pitch): Rotation of the vehicle in Y direction according to the gravity vector.
    %  Rotation in X (Roll): Rotation of the vehicle in X direction according to the gravity vector.
    %     W_R_V = RotX ( pi ) * RotZ ( Yaw - pi/2 ) * RotY ( Pitch ) * RotX ( Roll );
    % Inverse Approach to go back to Roll Pitch and Heading angles:
    %
%    % Elimiate RotZ:
%    W_R_V = RotZ ( pi / 2 )' * W_R_V;
%    % Get YPR angles:
%    [Yawi Pitchi Rolli] = RotMat2YPR ( W_R_V );
%    % Yaw => Heading (clockwise angle)
%    Yaw = -Yawi;
%    W_R_Vi = RotX ( pi )' * RotZ ( Yawi )' * W_R_V;
%    [Yawi Pitch Roll] = RotMat2YPR ( W_R_Vi );

    % Strightforward (and more efficient) way to convert back the angles knowing
    % the fixed rotations involved
    [Yawi Pitchi Rolli] = RotMat2YPR ( W_R_V );
    Roll = Rolli + pi;
    Pitch = -Pitchi;
    Yaw = pi / 2 - Yawi;
    
%     ResPos = M.nodes(i).pose.matrix(1:3) - W_T_V';
%     ResRot = [ NormAngle(M.nodes(i).pose.matrix(4) - Roll) ...
%                NormAngle(M.nodes(i).pose.matrix(5) - Pitch) ...
%                NormAngle(M.nodes(i).pose.matrix(6) - Yaw) ];
%     ResPos = sqrt ( sum ( ResPos .* ResPos ) );
%     ResRot = sqrt ( sum ( ResRot .* ResRot ) ); 
%     
%     if ResRot > 0.0000001;
%       disp ( sprintf ( 'Warning converting Rotation in node %d (Rot Err %f)\n', i, ResRot ) );
%     end
% 
%     if ResPos > 0.00000001;
%       disp ( sprintf ( 'Warning converting Position in node %d (Pos Err %f)\n', i, ResPos ) );
%     end

    M.nodes(i).pose.matrix = [ W_T_V' Roll Pitch Yaw ];
  end
end


% % Normalize Angle to be in the range: -2*pi < Alpha < 2*pi
% %
% function Alpha = NormAngle ( Alpha )
%   if Alpha > pi; Alpha = Alpha - 2 * pi; end;
%   if Alpha < -pi; Alpha = Alpha + 2 * pi; end;
% end
