%
%  Autor(s)      : Jordi Ferrer Plana & Olivier Delaunoy
%  e-mail        : jferrerp@eia.udg.edu, delaunoy@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute absolute Homographies using the navigation pose.
%
%  File          : mosaicCalcAbsFromPose.m
%  Date          : 03/04/2006 - 07/08/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%
%  mosaicCalcAbsFromPose Compute the Mosaic Absolute Homography according 
%                        to the pose of the camera given by the sensors
%                        of the mobile robot (please see WARNING Notes).
%
%    - WARNING: The pose information that is read in the file (x, y, z,
%               Roll, Pitch, Yaw) is interpreted as:
%               - The (x, y, z) coordinate vector defines the UTM position
%                 of the vehicle in meters.
%               - The (Roll, Pitch, Yaw) vector defines the vehicle angles:
%                   Roll : Angle acording to the x axis of the robot {V}.
%                   Pitch: Angle acording to the y axis of the robot {V}.
%                   Yaw  : Angle acording to the z World axis {W} that 
%                          corresponds to the North. It's a clockwise angle
%                          (it is sign inverted).
%               - By default (when V_Pose_C parameter is not given) the
%                 vehicle and the camera center have the same 3D position and 
%                 a fixed 90� angle between the camera an the vehicle is applied.
%              x
%    {i} u      ^ 
%       +---->--|-------+    - (u, v) The hor. and vert. axis of the 2D Image Plane {i}.
%     v |       |       |    - (x, y) The hor. and vert. axis of the 3D Vehicle Frame {V}.
%       |       | {V}   | y  - The z axis of the vehicle is pointing down.
%       v       +-------->   - The 3D Camera Frame {C} is rotated 90� w.r.t.
%       |       | {C}   | x'   3D Vehicle Frame {V} arround the z.
%       |       |       |
%       +-------|-------+
%               v
%              y'
%
%    - Nomenclature for the transformations used in the code:
%       * 3D Rotation: D_R_S (S = 3D Source Frame; D = 3D Destination Frame)
%        Read as: D_R_S is the rotation that transforms coordinates
%                       in the S Frame to the D Frame.
%       * 3D Translation: D_T_S (S = 3D Source Frame; D = 3D Destination Frame)
%        Read as: D_T_S is the translation of the S frame from the (0, 0, 0)
%                       defined in coordinates of the D frame.
%
%        Example: Transformation of a 3D Point in World Frame {W}
%                 into a 3D point in the Camera Frame {C}:
%                   C_X = C_R_W * W_X + C_T_W
%
%       * In 2D is done in the same way but in non capital letters:
%           d_H_s is a planar transformation from the 2D Source Frame s
%                 to the 2D Destination Frame d.
%
%  Usage:
%
%      M = mosaicCalcAbsFromPose ( M, pixelSizeFor1MeterOrK ...
%                                  [, pixelSizeForMosaic, Projective, V_Pose_C] );
%
%     Input Parameters:
%      M: GMML structure representing the mosaic. Only the pose, and the image 
%         associated to the first node (to get the image size) are used.
%      pixelSizeFor1MeterOrK: If it is scalar is the pixel size in meters
%                             of a pixel at one meter. If it is a 3x3
%                             matrix is the K matrix.
%      pixelSizeForMosaic: Size in meters of on pixel in the mosaic. If it
%                          is not given, the mean of the altitude of the
%                          robot will be used to calculate it.
%      Projective: Set to 1 to use the Pitch and Roll angles of the robot.
%                  If it's not given or is 0, Roll and Pitch are not used
%                  (then only 2D Similarity Homographies will be estimated).
%      V_Pose_C: The rigid motion of the 3D Camera Frame {C} with respect
%                to the 3D Vehicle Frame {V} in format (X, Y, Z, Roll,
%                Pitch, Yaw).
%                The (X, Y, Z) is the 3D Camera Frame (the camera center)
%                translation with respect to the 3D Vehicle Frame {V}.
%                The rotation can be defined in one of the following ways:
%                  1. According to the fixed axis of the 3D Vehicle Frame {V}
%                     as a Yaw rotation in Z, a Pitch rotation in Y and
%                     a Roll rotation in X.
%                  2. According to the mobile axis of the 3D Camera Frame {C}
%                     as a Roll rotation in X, a Pitch rotation in Y and a Yaw
%                     rotation in Z.
%
%     Output Parameters:
%      M: The following fields of the input GMML structure representing the
%         mosaic will be calculated:
%          - Absolute Homographies (Matrix, Type and Model).
%          - The covariance matrices corresponding to the Homography
%            coefficients is set to 0.
%          - The Mosaic Origin in pixels in respect to the UTM coordinates.
%          - The Mosaic Size in pixels.
%

function M = mosaicCalcAbsFromPose ( M, pixelSizeFor1MeterOrK, ...
                                     pixelSizeForMosaic, Projective, V_Pose_C )
  % Test the Input Parameters
  error ( nargchk ( 2, 5, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Width and Height assumed to be the same for all the images
  [Height Width] = size ( imread ( M.nodes(1).image ) );

  % Use the given Pixel Size at 1m or the K
  if isscalar ( pixelSizeFor1MeterOrK );
    % If the K is not given compute one using the Pixel Size at 1m
    K = [ 1/pixelSizeFor1MeterOrK             0              Width/2;
                    0               1/pixelSizeFor1MeterOrK  Height/2;
                    0                         0                1  ];
    pixelSizeFor1Meter = pixelSizeFor1MeterOrK;
  else
    % Check the size of K
    if ~all ( size ( pixelSizeFor1MeterOrK ) == [ 3 3 ] );
      error ( 'MATLAB:mosaicCalcAbsFromPose:Input', ...
              'pixelSizeFor1MeterOrK must be a scalar or a 3x3 matrix!' );
    end

    K = pixelSizeFor1MeterOrK;

    % alfa_u K(1,1) and alfa_v K(2,2) can be different: Use the mean to
    % calculate the Mosaic Size
    pixelSizeFor1Meter = 2 / (K(1,1) + K(2,2));
  end

  % Calculate Minimum and Maximum in X and Y in UTM.
  minimumX = M.nodes(1).pose.matrix(1);
  maximumX = minimumX; 
  minimumY = M.nodes(1).pose.matrix(2);
  maximumY = minimumY;
  % To calculate the Mean in Z in UTM.
  meanZ = M.nodes(1).pose.matrix(3);
  for i = 2 : size ( M.nodes, 2 );
    x = M.nodes(i).pose.matrix(1);
    y = M.nodes(i).pose.matrix(2);
    if x < minimumX; minimumX = x; end;
    if x > maximumX; maximumX = x; end;
    if y < minimumY; minimumY = y; end;
    if y > maximumY; maximumY = y; end;
    meanZ = meanZ + M.nodes(i).pose.matrix(3);
  end

  % If the Rigid Motion between Camera and Vehicle is not given assume
  % the vehicle position as the camera center and that the camera rotated 90�
  % relative to the z axis of the vehicle (see WARNING notes).
  if nargin < 5;
    V_Pose_C = [ 0 0 0 0 0 pi/2 ];
  else
    % Check Vector Size
    [m n] = size ( V_Pose_C );
    if ( m ~= 6 || n ~= 1 ) && ( m ~= 1 || n ~= 6 );
      error ( 'MATLAB:mosaicCalcAbsFromPose:Input', ...
              'V_Pose_C must be a 1x6 vector containing (x, y, z, Roll, Pitch, Yaw)!' );
    end
  end

  % If the Projective Parameter is not given assume 0 (Similarity Homographies)
  if nargin < 4;
    Projective = 0;
  end

  % If the pixelSizeForMosaic is not given use use the mean of the camera altitude
  if nargin < 3;
    meanZ = meanZ / size ( M.nodes, 2 );
    pixelSizeForMosaic = pixelSizeFor1Meter * meanZ;
  else
    meanZ = pixelSizeForMosaic / pixelSizeFor1Meter;
  end

  % Calculate the origin of the mosaic
  M.init.mosaic_origin.x = minimumX;
  M.init.mosaic_origin.y = maximumY;
  M.init.mosaic_origin.z = meanZ;

  % Write the pixel size of the mosaic
  M.init.pixel_size.x = pixelSizeForMosaic;
  M.init.pixel_size.y = pixelSizeForMosaic;

  % Pre-Compute the Homography Model
  if Projective; Model = 'pro'; else Model = 'sim'; end;

  % Rigid Camera Frame {C} Motion w.r.t. the 3D Vehicle Frame {V}
  V_X_C     = V_Pose_C(1);
  V_Y_C     = V_Pose_C(2);
  V_Z_C     = V_Pose_C(3);
  V_Roll_C  = V_Pose_C(4);
  V_Pitch_C = V_Pose_C(5);
  V_Yaw_C   = V_Pose_C(6);

  % Compute the Transformation 3D Mosaic Frame -> 3D World Frame: W_X = W_R_M * M_X + W_T_M
  % Mosaic Rotation w.r.t. 3D World Frame
  W_R_M = RotX ( pi );
  % Mosaic Translation w.r.t. 3D World Frame
  % (The mosaic is translated according to the X, Y origin but not in moved in Z).
  W_T_M = [ M.init.mosaic_origin.x; M.init.mosaic_origin.y; 0 ];

  % Compute Inverse Transformation 3D Wold Frame -> 3D Mosaic Frame: M_X = M_R_W * W_X + M_T_W
  M_R_W = W_R_M';
  M_T_W = -M_R_W * W_T_M;

  % Compute the Transformation 3D Camera Frame -> 3D Vehicle Frame: V_X = V_R_C * C_X + V_T_C
  % Camera Rotation w.r.t. 3D Vehicle Frame
  V_R_C = RotX ( V_Roll_C ) * RotY ( V_Pitch_C ) * RotZ ( V_Yaw_C );
  % Camera Translation w.r.t. 3D Vehicle Frame
  V_T_C = [ V_X_C; V_Y_C; V_Z_C ];

  % 2D Planar Homography with the scaling factor to save the final Absolute
  % Homographies in pixels
  Hs = [ 1/pixelSizeForMosaic          0            0; 
                  0           1/pixelSizeForMosaic  0;
                  0                    0            1 ];

  % Scan all the Mosaic Nodes
  for i = 1 : size ( M.nodes, 2 );
    % The Yaw angle
    Yaw = M.nodes(i).pose.matrix(6);

    % Compute projective motion or not
    if ~Projective;
      Roll = 0;
      Pitch = 0;
    else
      Roll = M.nodes(i).pose.matrix(4);
      Pitch = M.nodes(i).pose.matrix(5);
    end

%    % Vehicle Rotation w.r.t. 3D World Frame (Computed allways in mobile axis: post-multiplication)
%    %  Rotation in Z (pi/2): Point X axis of the vehicle to the north to use the Yaw.
%    %  Rotation in Z (Yaw): Yaw of the vehicle in the world frame.
%    %  Rotation in X (Pi): Point Z axis of the vehicle looking down.
%    %  Rotation in Y (Pitch): Pitch in the vehicle frame.
%    %  Rotation in X (Roll): Roll in the vehicle frame.
%    W_R_V_old = RotZ ( pi / 2 ) * RotZ ( -Yaw ) * RotX ( pi ) * RotY ( Pitch ) * RotX ( Roll );

    % Vehicle Rotation w.r.t. 3D World Frame (Computed allways in mobile axis: post-multiplication)
    %  Rotation in X (pi): Point the Z axis down (vehicle frame is like that.
    %                      As yaw is a clockwise angle, now is rotation in the right direction.
    %  Rotation in Z (Yaw-pi/2): First rotation with respect to the heading angle. As zero of angles is
    %                       "x" axis, but Yaw=0 is North (y axis), angle must be corrected with pi/2 rads.
    %                       Note that after first rotation in x, this correction is negative.
    %  Rotation in Y (Pitch): Rotation of the vehicle in Y direction according to the gravity vector.
    %  Rotation in X (Roll): Rotation of the vehicle in X direction according to the gravity vector.
    W_R_V = RotX ( pi ) * RotZ ( Yaw - pi/2 ) * RotY ( Pitch ) * RotX ( Roll );

    % Vehicle Translation w.r.t. 3D World Frame
    W_T_V = [ M.nodes(i).pose.matrix(1); M.nodes(i).pose.matrix(2); M.nodes(i).pose.matrix(3) ];

    % Convert a Pose (V_T_C, V_R_C) w.r.t. 3D Vehicle Frame to the 3D World Frame
    % This new pose will also be the Transformation 3D Camera Frame -> 3D World Frame: W_X = W_R_C * C_X + W_T_C
    W_R_C = W_R_V * V_R_C;
    W_T_C = W_R_V * V_T_C + W_T_V;

    % Convert a Pose (W_T_C, W_R_C) w.r.t. 3D Camera Frame to the 3D Mosaic Frame
    % This new pose will also be the Trasformation 3D Camera Frame -> 3D Mosaic Frame: M_X = M_R_C * C_X + M_T_C
    M_R_C = M_R_W * W_R_C;
    M_T_C = M_R_W * W_T_C + M_T_W;

    % Compute Inverse Transformation 3D Mosaic Frame -> 3D Camera Frame: C_X = C_R_M * M_X + C_T_M
    C_R_M = M_R_C';
    C_T_M = -C_R_M * M_T_C;

    % Compute Extrinsics using the 3D Mosaic Frame
    C_M_M = [C_R_M C_T_M];
    % Compute Projection Matrix (Intrinsics * Extrinsics): 3D Mosaic Frame -> 2D Image Plane
    i_P_M = K * C_M_M;

    % Set Z = 0 (Delete 3th Column) in the Projection Matrix: 3D world to 2D
    % Up-To-Scale Planar Projective Homography: 2D Mosaic Frame to 2D Image Frame
    i_H_m = i_P_M(:, [1 2 4]);
    % Absolute Homography to be stored into the mosaic: 2D Image Plane to 2D Mosaic Frame
    % Add the scaling factor to have it in pixels.
    m_H_i = Hs * inv ( i_H_m );

    % Store Normalized Homography into the mosaic structure ([3,3] element is one).
    M.nodes(i).homo.matrix = m_H_i ./ m_H_i(3,3);
    M.nodes(i).homo.covar = [ 0,0,0; 0,0,0; 0,0,0 ];
    M.nodes(i).homo.model = Model;
    M.nodes(i).homo.type = 'abs';
  end
end
