function M = mosaic_calc_abs_from_pose(M, pixelSizefor1MeterOrK, pixelSizeForMosaic)

% M = mosaic_calc_abs_from_pose(M, pixelSizefor1MeterOrK, pixelSizeForMosaic)
%
%       M                  - GMML structure
%       ccd_size           - size in mm of the CCD
%       pixelSizefor1MeterOrK - size in meter of one pixel at one meter
%       pixelSizeForMosaic - size in meter of one pixel in the mosaic
%
% compute the absolute homography according to the pose of the camera 
%
% Developed by Arman
% Dep. of Electronics, Informatics and Automation, University of Girona



%build the camera matrix for given resolution at 1m or find the pixelSize
[sizeY sizeX] = size(imread(M.nodes(1).image));
if(isscalar(pixelSizefor1MeterOrK)),
    K=[1/pixelSizefor1MeterOrK 0 sizeX/2;0 1/pixelSizefor1MeterOrK sizeY/2;0 0 1];
    pixelSizefor1Meter = pixelSizefor1MeterOrK;
else
    K = pixelSizefor1MeterOrK;
    pixelSizefor1Meter = 2 / (K(1,1) + K(2,2));
%    pixelSizefor1Meter = (K(1,1) + K(2,2)) / 2;
end



minimumX = M.nodes(1).pose.matrix(1); %used for the origine
maximumX = M.nodes(1).pose.matrix(1); 
minimumY = M.nodes(1).pose.matrix(2);
maximumY = M.nodes(1).pose.matrix(2); %used for the origine
meanZ = M.nodes(1).pose.matrix(3);
for i=2:length(M.nodes)
    if (M.nodes(i).pose.matrix(1)<minimumX), minimumX = M.nodes(i).pose.matrix(1); end; %used for the origine
    if (M.nodes(i).pose.matrix(1)>maximumX), maximumX = M.nodes(i).pose.matrix(1); end;
    if (M.nodes(i).pose.matrix(2)<minimumY), minimumY = M.nodes(i).pose.matrix(2); end;
    if (M.nodes(i).pose.matrix(2)>maximumY), maximumY = M.nodes(i).pose.matrix(2); end; %used for the origine
    meanZ = meanZ + M.nodes(i).pose.matrix(3);
end;
meanZ = meanZ / length(M.nodes);

if(exist('pixelSizeForMosaic','var')) 
    meanZ = pixelSizeForMosaic/pixelSizefor1Meter;
else
    pixelSizeForMosaic = (pixelSizefor1Meter*meanZ);
end

M.init.mosaic_origin.x= minimumX;     	% origine de la mosaique dans le monde
M.init.mosaic_origin.y= maximumY;     	% origine de la mosaique dans le monde
M.init.mosaic_origin.z= meanZ;        	% origine de la mosaique dans le monde
M.init.pixel_size.x=pixelSizeForMosaic;
M.init.pixel_size.y=pixelSizeForMosaic;

projective = 0;

C=[-M.init.mosaic_origin.x;M.init.mosaic_origin.y;M.init.mosaic_origin.z];
HM1=K*[eye(3) C];
HM1=HM1(1:3,[1 2 4]); %Z=0 3D-to-2D planar projective mapping

for i=1:length(M.nodes)
    %rotation matrices of camera centers w.r.t world frame
    yaw=-M.nodes(i).pose.matrix(6);
    roll=M.nodes(i).pose.matrix(4);
    pitch=M.nodes(i).pose.matrix(5);
    if ~projective,
        roll = 0; 
        pitch = 0;
    end
    R_x=[1 0 0;0 cos(roll) -sin(roll);0 sin(roll) cos(roll)];
    R_y=[cos(pitch) 0 sin(pitch);0 1 0;-sin(pitch) 0 cos(pitch)];
    R_z=[cos(yaw) -sin(yaw) 0;sin(yaw) cos(yaw) 0;0 0 1];
    cRw=(R_x*R_y*R_z);
    
    %Translation parameters of camera centers w.r.t world frame (cTw)
    %cX=cRw.wX+cTw
    %camera center w.r.t. camera frame is cX=(0,0,0)
    cTw=cRw*[-M.nodes(i).pose.matrix(1) M.nodes(i).pose.matrix(2) M.nodes(i).pose.matrix(3)]';%camera centers
    %%Homography from 3D world to 2D 
    H=K*[cRw cTw];
    %Z=0 delete the third coloumn, 3D-to-2D planar projective homography
    H=H(:,[1 2 4]);
    %from 3D-to-2D to 2D-to-2D homography 
    temp=HM1*inv(H);
    M.nodes(i).homo.matrix=temp./temp(3,3);
    M.nodes(i).homo.covar=[0,0,0;0,0,0;0,0,0];
    if projective;
      M.nodes(i).homo.model='pro';
    else
      M.nodes(i).homo.model='sim';
    end
    M.nodes(i).homo.type='abs';
end

%correction of the shift due to the reference homography
M.init.mosaic_origin.x= M.init.mosaic_origin.x - sizeX/2*pixelSizeForMosaic;     	% origine de la mosaique dans le monde
M.init.mosaic_origin.y= M.init.mosaic_origin.y + sizeY/2*pixelSizeForMosaic;     	% origine de la mosaique dans le monde

