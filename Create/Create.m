%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Create the Initial Mosaic from pose.
%
%  File          : Create.m
%  Date          : 23/03/2006 - 19/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
%close all; clc;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Paths
BasePath = '/mnt/data/MosaicBundle';
addpath ( [ BasePath '/Optimization' ] );
addpath ( [ BasePaht '/Create' ] );
addpath ( BasePath, '-END' );

% Input Data
ImageFilePath = '/mnt/uvl/DataSets/Lustre96/';
NavigationFileName = 'navFile.txt';

% Compute K or use Olivier's estimation
% Use Apendix E "pixel size" or the other parameters
UsePixelSize = true;

% Output Data
MosaicFileName = [ BasePath '/Data/gmml_Similarity2.gmml' ];

% Save Current Folder Location
CurrentFolder = pwd;

% Goto Image Folder
cd ( ImageFilePath );

% Parameters in Apendix E: Image Sizes, K, etc...
InitParamLucky;

% Apendix E "pixel size"
pixelSizeForMosaic = 0.015;
pixelSizeFor1MeterOrK = 1.44e-3;
V_CameraPose = [ 0 0 0 0 0 pi/2 ];

% Estimate Projective Motion
Projective = 1;

if ~exist ( 'M', 'var' );
  disp ( 'Loading Initial Mosaic...' );
  M = mosaic_generate_from_navigation_file ( NavigationFileName );
end

disp ( 'Calculating absolute Homographies from pose...' );
if UsePixelSize;
%  M2 = mosaic_calc_abs_from_pose ( M, pixelSizeFor1MeterOrK, pixelSizeForMosaic );
  M2 = mosaicCalcAbsFromPose ( M, pixelSizeFor1MeterOrK, pixelSizeForMosaic, Projective, V_CameraPose );
else
%  M2 = mosaic_calc_abs_from_pose ( M, K, pixelSizeForMosaic );
  M2 = mosaicCalcAbsFromPose ( M, K, pixelSizeForMosaic, Projective, V_CameraPose );
end


disp ( 'Calculating mosaic size...' );
M3 = mosaic_calc_size ( M2 );

% Add the ImageFilePath in front of the FileName
disp ( 'Add Image File Path...' );
for i = 1 : size ( M3.nodes, 2 );
  % If there is a FileName
  if ~isempty ( M3.nodes(i).image );
    % If it is not absolute, add the ImageFilePath
    if strcmpi ( M3.nodes(i).image(1:2), './' );
      M3.nodes(i).image = strrep ( M3.nodes(i).image, './', [ ImageFilePath '/' ] );
    end
  end
end

disp ( 'Display preview...' );
mosaic_preview_abs ( M3 );

disp ( 'Saving Final Mosaic...' );
% gmml_save ( MosaicFileName, M3 );

% Goto Save Folder Location
cd ( CurrentFolder );
