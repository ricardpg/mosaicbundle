function M = mosaic_generate_from_navigation_file(navFile)

% M = mosaic_generate_from_navigation_file(navFile)
%
%       M                   - GMML structure
%       navFile             - navigation file
%
% Generate a GMML structure with the information of the navigation file
%
% The navigation file should contain :
%       the 1st column should be the name of the file
%       the 2nd column should be the utm X
%       the 3rd column should be the utm Y
%       the 4th column should be the altitude
%       the 5th column should be the yaw in rad 
%                           with respect to the north, clockwise
%       the 6th column should be the pitch in rad (optional)
%       the 7th column should be the roll in rad (optional)
%
% Developed by Olivier Delaunoy (delaunoy@eia.udg.edu)
% Dep. of Electronics, Informatics and Automation, University of Girona



M.init.ver             = 3;         % Version of GMML
M.init.subver          = 0;         % Subversion of GMML
M.init.os              = '';    
M.init.cpu             = '';
M.init.comment         = '';
M.init.conf            = '';        % Submarine configuration file
M.init.nav_data        = navFile;   % Navigation file
M.init.mosaic_origin.x = 0;         % World origine of the mosaic (UTM)
M.init.mosaic_origin.y = 0;         % World origine of the mosaic (UTM)
M.init.mosaic_origin.z = 0;         % World origine of the mosaic (UTM)
M.init.pixel_size.x    = 1;         % Size in meter of one pixel of the mosaic
M.init.pixel_size.y    = 1;         % Size in meter of one pixel of the mosaic
M.init.mosaic_size.x   = 0;       	% Final size of the mosaic
M.init.mosaic_size.y   = 0;       	% Final size of the mosaic

fid = fopen(navFile,'r');
Mindex=1;
while ~feof(fid),
    s=fgetl(fid);
    [name s]=strtok(s, ' ');
    [utmx s]=strtok(s, ' ');
    [utmy s]=strtok(s, ' ');
    [alt s]=strtok(s, ' ');
    [yaw s]=strtok(s, ' ');
    [pitch s]=strtok(s, ' ');
    [roll s]=strtok(s, ' ');
    if isempty(pitch), pitch = '0'; end
    if isempty(roll), roll = '0'; end
    
    if exist(name,'file'),
        M.nodes(Mindex).index       = Mindex;
        M.nodes(Mindex).image       = name;
        M.nodes(Mindex).time        = '';
        M.nodes(Mindex).homo.model  = '';
        M.nodes(Mindex).homo.type   = 'abs';
        M.nodes(Mindex).homo.matrix = [0,0,0;0,0,0;0,0,0];
        M.nodes(Mindex).homo.covar  = [0,0,0;0,0,0;0,0,0];
        M.nodes(Mindex).pointmatches= '';
        M.nodes(Mindex).pose.type   = 'abs';
        M.nodes(Mindex).pose.matrix = [str2double(utmx),str2double(utmy),str2double(alt),str2double(roll),str2double(pitch),str2double(yaw)];
        M.nodes(Mindex).pose.covar  = [0,0,0,0,0,0];
        M.nodes(Mindex).edges       = [];
        Mindex=Mindex+1;
    end
end

