%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Given a GMML structure plot the trajectory.
%
%  File          : CalculateSubMosaic.m
%  Date          : 07/03/2006 - 20/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Toolboxes
% addpath /opt/Matlab/toolbox/...;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Data
DataPath = '/mnt/data/MosaicBundle/Data';

% Use the crossings in the mosaic (or =0 in a table file)
UseMosaicCrossings = 1;
UseMatFile = 1;

% Check that the Crossings GPMMLs exit
CheckGPMML = false;

if UseMatFile;
  MosaicFileName = '/Filtered_Sequential_Crossings.mat';
else
  MosaicFileName = '/Filtered_Sequential_Crossings.gmml';
end

MosaicFilePath = [ DataPath MosaicFileName ];

FiguresFilePath = [ DataPath '/Figures' ];

% FinalCrossingsFileName = '/FinalCrossings_5.mat';
FinalCrossingsFileName = '/Filtered_FinalCrossings_5.mat';
FinalCrossingsFilePath = [ DataPath FinalCrossingsFileName ];
GPMMLFilePath = [ DataPath '/GPMMLFiles/Crossings/' ];
PathToResults = [ DataPath '/FIM/' ];
PathToResultsFIM = [ PathToResults 'Crossings/ResultsFIM/' ];

% Final Mosaic
PathToFinalMosaic = [ DataPath '/Sub_Sequential_Crossings.gmml' ];

% Image Sizes, K, etc...
InitParamLucky;

% Load Mosaic if it is not already loaded
if ~exist ( 'M', 'var' );
  disp ( sprintf ( 'Loading Mosaic: "%s"...', MosaicFilePath ) );
  tic;
  if UseMatFile;
    load ( MosaicFilePath );
  else
    M = gmml_load ( MosaicFilePath );
  end
  toc
end

% Calculate SubMosaic
disp ( 'Calculating Sub Mosaic...' );
Mn = subMosaicGui ( M );

if ~isempty ( Mn )
  disp ( 'Calculating mosaic size...' );
  s = warning ( 'off' );
  MRes = mosaic_calc_size ( Mn );
  warning ( s );
 
  % Save the final Sub Mosaic with the crossings
  disp ( sprintf ( 'Saving Final Mosaic: "%s"...', PathToFinalMosaic ) );
  gmml_save ( PathToFinalMosaic, MRes );

  % Save the figure
  FigureFileName = [ FiguresFilePath '/SubMosaic.fig' ];
  disp ( sprintf ( 'Saving Figure as: "%s"...', FigureFileName ) );
  saveas ( gcf, FigureFileName, 'fig' );
else
  disp ( 'Selection Step Skiped. Don''t save anything!' );
end
