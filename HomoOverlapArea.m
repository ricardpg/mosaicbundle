%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute the overlapping area between two images.
%
%  File          : HomoOverlapArea.m
%  Date          : 08/03/2006 - 17/04/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  HomoOverlapArea Compute the overlapping area between the source image and
%                  the warped image according to the planar transformation H.
%
%      A = HomoOverlapArea ( H, Width, Height [, NumTests ] )
%
%     Input Parameters:
%      H: 3x3 planar homography.
%      Width: Image Width.
%      Height: Image Height.
%      NumTests: Number of samples that will be used to estimate the area.
%                If it is not given Width*Height that means the whole image
%                will be used.
%
%     Output Parameters:
%      A: Overlapping area in percentage [0..1].
%

function A = HomoOverlapArea ( H, Width, Height, NumTests )
  % Test the input parameters
  error ( nargchk ( 3, 4, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  if ~isscalar ( Width ) || ~isscalar ( Height ) || Width < 1 || Height < 1;
    warning ( 'MATLAB:HomoOverlapArea:Input', 'Width and Height must be two positive scalars!' );
  end

  [r c] = size ( H );
  if r ~= 3 || c ~= 3;
    warning ( 'MATLAB:HomoOverlapArea:Input', 'H must be a 3x3 planar transformation!' );
  end

  % Size Of the Image
  Size = Width * Height;

  % Default number of tests
  if nargin < 4; NumTests = Size; end

  % Compute subsample distance
  Step = floor ( Size / NumTests );
  % If it is wrong, use the whole image
  if Step < 1 || Step > Size; Step = 1; end;

  % Compute the linear indices
  Seq = 1:Step:Size;
  % Calculate matrix indices
  [Rh Rw] = ind2sub ( [Height, Width], Seq );

  % Warp coordinates according to the homography
  T = H * [Rw; Rh; ones(1,NumTests)];
  T(1,:) = T(1,:) ./ T(3,:);
  T(2,:) = T(2,:) ./ T(3,:);

  % Check whether they are inside the source image after warping
  Ok = T(1,:) >= 1 & T(1,:) <= Width & T(2,:) >= 1 & T(2,:) <= Height; 
  % Calculate the percentage
  TestOk = sum ( Ok );
  A = TestOk / size ( Seq, 2 );
end
