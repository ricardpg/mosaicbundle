%
%  Autor(s)      : Arman Elibol & Jordi Ferrer Plana
%  e-mail        : aelibol@eia.udg.edu, jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Preview given Mosaic using Absolute Homographies.
%
%  File          : ComputeFloatingNodes.m
%  Date          : 16/07/2007 - 16/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ComputeFloatingNodes Compute floating nodes for the input mosaic.
%                       Floating nodes are the ones that don't have any
%                       consecutive parent or any consecutive child.
%
%      [StartFloatNodes StartFloatNodesIdx EndFloatNodes EndFloatNodesIdx] =
%             ComputeFloatingNodes ( M )
%
%     Input Parameters:
%      M: GMML Mosaic Structure.
%
%     Output Parameters:
%      StartFloatNodes: 1xn vector containing the id (index) of the nodes
%                       that don't have any consecutive parent.
%      StartFloatNodesIdx: 1xn vector contining the position in M of the
%                          nodes that don't have any consecutive parent.
%      EndFloatNodes: 1xm vector containing the id (index) of the nodes
%                     that don't have any consecutive child.
%      EndFloatNodesIdx: 1xm vector containing the position in M of the
%                        nodes that don't have any consecutive child.
%

function [StartFloatNodes StartFloatNodesIdx EndFloatNodes EndFloatNodesIdx] = ...
  ComputeFloatingNodes ( M )
  % Test the input parameters
  error ( nargchk ( 1, 1, nargin ) );
  error ( nargoutchk ( 4, 4, nargout ) );

  % Number of Nodes
  NumNodes = size ( M.nodes, 2 );

  % Build a List with the nodes without any child
  EndFloatNodes = zeros ( 1, NumNodes );
  EndFloatNodesIdx = zeros ( 1, NumNodes );
  l = 0;
  for i = 1 : NumNodes;
    % Check whether all the nodes are consecutive or not
    s = size ( M.nodes(i).edges, 2 );
    Found = false;
    j = 1;
    while j <= s && ~Found
      Found = M.nodes(i).edges(j).index == i - 1;
      if ~Found; j = j + 1; end;
    end
    if ~Found;
      l = l + 1;
      EndFloatNodes(l) = M.nodes(i).index;
      EndFloatNodesIdx(l) = i;
    end
  end

  % Cut the list
  if l > 0; EndFloatNodes = EndFloatNodes(1:l); EndFloatNodesIdx = EndFloatNodesIdx(1:l); end;

  % Look for the Biggest node index to know the maximum size of the list
  BiggestNode = M.nodes(1).index;
  for i = 2 : NumNodes;
    if M.nodes(i).index > BiggestNode; BiggestNode = M.nodes(i).index; end;
  end

  % All the nodes are not used initially
  PointedNodes = zeros ( 1, BiggestNode );
  % Mark the real nodes in the lookup table.
  for i = 1 : NumNodes;
    PointedNodes(M.nodes(i).index) = 1;
  end

  % Unmark the nodes that are pointed by an edge.
  for i = 1 : NumNodes;
    for j = 1 : size ( M.nodes(i).edges, 2 );
      % Check that is an edge to a consecutive node
      if M.nodes(i).edges(j).index == i - 1;
        PointedNodes ( M.nodes( M.nodes(i).edges(j).index ).index ) = 0;
      end
    end
  end

  StartFloatNodes = find ( PointedNodes );

  % Compute temporary all the nodes in the submosaic
  AllNodes = zeros ( 1, NumNodes );
  for i = 1 : NumNodes
    AllNodes(i) = M.nodes(i).index;
  end
  [Dummy, StartFloatNodesIdx] = intersect ( AllNodes, StartFloatNodes );

  % Check errors
  % The segments must have an starting and ending point
  if length ( StartFloatNodes ) ~= length ( EndFloatNodes );
    error ( 'MATLAB:ComputeFloatingNodes:Input', 'Number of segments don''t match in the mosaic!' );
  end
end
