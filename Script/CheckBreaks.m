%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Find the non-consecutive elements in the input list.
%
%  File          : CheckBreaks.m
%  Date          : 10/03/2006 - 12/03/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CheckBreaks Find the non-consecutive elements in the input list.
%
%      Idx = CheckBreaks ( List )
%
%     Input Parameters:
%      List: 1xn list indicating a sequence of elements.
%
%     Output Parameters:
%      Idx: Indices to the non consecutive elements in the list.
%

function Idx = CheckBreaks ( List )
  % Test the input parameters
  error ( nargchk ( 1, 1, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Sizes
  [r, n] = size ( List );
  if r ~= 1 || n < 1;
    error ( 'MATLAB:CheckBreaks:Input', 'The input list must be a 1xn vector!' );
  end

  Idx = zeros ( 1, n );
  l = 0;
  for i = 1:n-1;
    if List(i) ~= List(i+1) - 1;
      l = l + 1;
      Idx(l) = i;
    end
  end

  % Cut the List of indices
  if l > 0; Idx = Idx(1:l); else Idx = []; end
end
