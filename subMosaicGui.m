function MM = subMosaicGui(M)
    % To handle objects on the picture
    LineHandle = [];
    
    warning off all

    %  Create and hide the GUI as it is being constructed.
    f = figure('Visible', 'off', ...
              'Name', 'Exctract Mosaic', ...
              'MenuBar', 'none', ...
              'ToolBar', 'figure', ...% 'Renderer', 'OpenGL', ...
              'Position', [360,300,600,330], ...
              'CloseRequestFcn', {@closebutton_Callback});

    hextract = uicontrol('Style', 'pushbutton', ...
                         'String', 'Extract Mosaic', ...
                         'Position', [10,35,90,20], ...
                         'Callback', {@extractbutton_Callback}); 
    hsave = uicontrol('Style', 'pushbutton', ...
                      'String', 'Save', ...
                      'Position', [110,35,90,20], ...
                      'Callback', {@savebutton_Callback});
    hedit = uicontrol('Style', 'edit', ...
                      'String', 'newGmml.gmml', ...
                      'BackgroundColor', [1 1 1], ...
                      'Position', [210,35,180,20], ...
                      'Callback', {@savebutton_Callback});
    hremove = uicontrol('Style', 'pushbutton', ...
                      'String', 'Remove Nodes', ...
                      'Position', [400,35,90,20], ...
                      'Callback', {@removebutton_Callback});    
    hindex = uicontrol('Style', 'togglebutton', ...
                      'String', 'Index', ...
                      'Position', [500,35,90,20], ...
                      'Callback', {@indexbutton_Callback});  
    hnode = uicontrol('Style', 'pushbutton', ...
                      'String', 'Node n� ...', ...
                      'Position', [400,10,90,20], ...
                      'Callback', {@nodebutton_Callback});    
    hterminate = uicontrol('Style', 'togglebutton', ...
                      'String', 'Terminate', ...
                      'Position', [500,10,90,20], ...
                      'Callback', {@terminatebutton_Callback}); 
    hb = axes('Units', 'Pixels', 'DrawMode', 'fast', 'Position', [320,80,240,240]); 
    axis(hb, 'equal');
    axis(hb, 'ij');
    hold(hb, 'on');
    ha = axes('Units', 'Pixels', 'DrawMode', 'fast', 'Position', [40,80,240,240]); 
    axis(ha, 'equal');
    axis(ha, 'ij');
    hold(ha, 'on');
    
    % Change units to normalized so components resize automatically.
    set([f, ha, hb, hextract, hsave, hedit, hremove, hindex, hnode, hterminate],'Units','normalized');
    % Move the GUI to the center of the screen.
    movegui(f,'center')
    
    
    
    % Generate the data to plot.
    plotIndex = false;
    MM.init = M.init;
    MM.nodes = [];
    MMList = [];
    selectedNode = [];
    
    [h,w] = size(imread(M.nodes(1).image));
    c = [w/2; h/2; 1];
    MList = zeros(3,size(M.nodes,2));
    for no=1:length(M.nodes)
        MList(:,no) = M.nodes(no).homo.matrix * c;
    end
    MList(1,:) = MList(1,:) ./ MList(3,:);
    MList(2,:) = MList(2,:) ./ MList(3,:);

    % Configuration
    PlotLines = 0;
    PlotNonConsecutive = 0;
    PlotDots = 1;
    
    if PlotDots;
       plot(MList(1,:), MList(2,:), 'b.');
    end

    if PlotLines;
     for no = 1 : size(M.nodes,2)
         for ed = 1 : size(M.nodes(no).edges, 2)
             if M.nodes(no).edges(ed).index == no-1 %consecutive
                 plot(MList(1,[M.nodes(no).edges(ed).index no]), MList(2,[M.nodes(no).edges(ed).index no]), 'b');
             else
                 if PlotNonConsecutive;
                   plot(MList(1,[M.nodes(no).edges(ed).index no]), MList(2,[M.nodes(no).edges(ed).index no]), 'r');
                 end
             end
         end
     end
    end

    set(f, 'Visible', 'on');



    function extractbutton_Callback(source,eventdata) 
        % Select the nodes that are in the square and build the segments
        MM.init = M.init;
        MM.nodes = [];

        sr = getrect(ha);
        Lx = [ sr(1) sr(1)+sr(3) sr(1)+sr(3) sr(1) sr(1) ];
        Ly = [ sr(2) sr(2) sr(2)+sr(4) sr(2)+sr(4) sr(2) ];
        axes(ha);

        % Delete previous
        if ~isempty ( LineHandle )
          delete ( LineHandle );
        end

        LineHandle = line ( Lx, Ly, 'Color', [0 1 0], 'LineStyle', '--', 'LineWidth', 3 );
        tic
        pos = find(MList(1,:) > sr(1) & MList(1,:) < sr(1)+sr(3) & MList(2,:) > sr(2) & MList(2,:) < sr(2)+sr(4));
        MM.nodes = M.nodes(pos);
        segments = false(size(MM.nodes,2)/25,size(MM.nodes,2)); %preallocation (+/-25 nodes / segments)
        segpos = 0;
        for no=1 : size(MM.nodes,2)
            found = false;
            ed = size(MM.nodes(no).edges,2);
            while ed > 0
                newind = find(pos == MM.nodes(no).edges(ed).index);
                if ~isempty(newind)
                    MM.nodes(no).edges(ed).index = newind;
                    found = newind == no - 1;
                else
                    MM.nodes(no).edges(ed) = [];
                end
                ed = ed -1;
            end
            if ~found,
                segpos = segpos + 1;
            end
            segments(segpos,no) = true;
        end

        
        
        % Find the biggest connected mosaic
        for no=2 : size(MM.nodes,2)
            for ed = 1 : size(MM.nodes(no).edges,2)
                if MM.nodes(no).edges(ed).index ~= no - 1,
                    segnode = find(segments(:,no));
                    segedge = find(segments(:,MM.nodes(no).edges(ed).index));
                    if segnode ~= segedge,
                        segments(segnode,:) = segments(segedge,:) | segments(segnode,:);
                        segments(segedge,:) = false;
                    end
                end
            end
        end
        [val ind] = max(sum(segments,2));

        
        
        % Select the nodes that belong to the biggest connected mosaic
        pos = find(segments(ind,:));
        MM.nodes = MM.nodes(pos);
        for no=1 : size(MM.nodes,2)
            ed = size(MM.nodes(no).edges,2);
            while ed > 0
                newind = find(pos == MM.nodes(no).edges(ed).index);
                if ~isempty(newind)
                    MM.nodes(no).edges(ed).index = newind;
                else
                    MM.nodes(no).edges(ed) = [];
                end
                ed = ed -1;
            end
        end
        MM = mosaic_calc_size(MM);
        
        
        selectedNode = [];
        set(hnode, 'String', 'Node n� ...');
        
        % Plot the sub mosaic on the second axis
        plotMosaic(MM, hb);
        fprintf('biggest connected: %d nodes; in %.2f sec\n', val, toc);

    end



    function savebutton_Callback(source,eventdata) 
        filename = get(hedit, 'String');
        gmml_save(filename, M);
    end



    function removebutton_Callback(source,eventdata) 
        % Select the nodes that are in the square and build the segments
        sr = getrect(hb);
        tic
        prevM = MM;
        pos = find((MMList(1,:) < sr(1) | MMList(1,:) > sr(1)+sr(3)) | (MMList(2,:) < sr(2) | MMList(2,:) > sr(2)+sr(4)));
        MM.nodes = prevM.nodes(pos);
        segments = false(size(MM.nodes,2)/25,size(MM.nodes,2)); %preallocation (+/-25 nodes / segments)
        segpos = 0;
        for no=1 : size(MM.nodes,2)
            found = false;
            ed = size(MM.nodes(no).edges,2);
            while ed > 0
                newind = find(pos == MM.nodes(no).edges(ed).index);
                if ~isempty(newind)
                    MM.nodes(no).edges(ed).index = newind;
                    found = MM.nodes(newind).index == MM.nodes(no).index - 1;
                else
                    MM.nodes(no).edges(ed) = [];
                end
                ed = ed -1;
            end
            if ~found,
                segpos = segpos + 1;
            end
            segments(segpos,no) = true;
        end

        
        
        % Find the biggest connected mosaic
        for no=2 : size(MM.nodes,2)
            for ed = 1 : size(MM.nodes(no).edges,2)
                if MM.nodes(no).edges(ed).index ~= no - 1,
                    segnode = find(segments(:,no));
                    segedge = find(segments(:,MM.nodes(no).edges(ed).index));
                    if segnode ~= segedge,
                        segments(segnode,:) = segments(segedge,:) | segments(segnode,:);
                        segments(segedge,:) = false;
                    end
                end
            end
        end
        [val ind] = max(sum(segments,2));

        
        
        % Select the nodes that belong to the biggest connected mosaic
        pos = find(segments(ind,:));
        MM.nodes = MM.nodes(pos);
        for no=1 : size(MM.nodes,2)
            ed = size(MM.nodes(no).edges,2);
            while ed > 0
                newind = find(pos == MM.nodes(no).edges(ed).index);
                if ~isempty(newind)
                    MM.nodes(no).edges(ed).index = newind;
                else
                    MM.nodes(no).edges(ed) = [];
                end
                ed = ed -1;
            end
        end
        MM = mosaic_calc_size(MM);
        
        
        
        % Plot the sub mosaic on the second axis
        plotMosaic(MM, hb);
        fprintf('biggest connected: %d nodes; in %.2f sec\n', val, toc);
    end



    function indexbutton_Callback(source,eventdata) 
        val = get(hindex, 'Value');
        if val == 1,
            plotIndex = true;
        else
            plotIndex = false;
        end
        plotMosaic(MM, hb)
    end



    function nodebutton_Callback(source,eventdata) 
        if ~isempty(MMList)
            p = ginput(1);
            [val pos] = min((MMList(1,:)-p(1)).^2 + (MMList(2,:)-p(2)).^2);
            selectedNode = [MM.nodes(pos).index MMList(1:2,pos)'];
            set(hnode, 'String', ['Node n� ' num2str(selectedNode(1))]);
            plotMosaic(MM, hb)
        end
    end



    function plotMosaic(M, h)
        axes(h);
        cla(h);  % to clear the axis
        axis auto;
        plot([0 0 M.init.mosaic_size.x M.init.mosaic_size.x 0], [0 M.init.mosaic_size.y M.init.mosaic_size.y 0 0], 'k');
        MMList = zeros(3,size(M.nodes,2));
        for no=1:length(M.nodes)
            MMList(:,no) = M.nodes(no).homo.matrix * c;
        end
        MMList(1,:) = MMList(1,:) ./ MMList(3,:);
        MMList(2,:) = MMList(2,:) ./ MMList(3,:);
        plot(MMList(1,:), MMList(2,:), 'b.');
        for no = 1 : size(M.nodes,2)
            for ed = 1 : size(M.nodes(no).edges, 2)
                if M.nodes(no).edges(ed).index == no-1 %consecutive
                    plot(MMList(1,[M.nodes(no).edges(ed).index no]), MMList(2,[M.nodes(no).edges(ed).index no]), 'b');
                else
                    plot(MMList(1,[M.nodes(no).edges(ed).index no]), MMList(2,[M.nodes(no).edges(ed).index no]), 'r');
                end
            end
            if ~isempty(selectedNode)
                plot(selectedNode(2), selectedNode(3), 'g+');
            end
            if plotIndex,
                text(MMList(1,no)+5, MMList(2,no)+5, num2str(M.nodes(no).index));
            end
        end
    end



    function closebutton_Callback(source,eventdata) 
        quiting = true;
        delete(f)
        MM = [];
    end



    function terminatebutton_Callback(source,eventdata) 
        quiting = true;
        set([hextract, hremove, hindex, hnode, hterminate],'Visible','off');

        % Configuration
        SavePicture = 0;
        
        if ~SavePicture;
          set([hsave, hedit],'Visible','off');

          % Only show the image
%          set ( ha, 'OuterPosition', [0 0 1 1] );
%          set ( ha, 'Position', [0 0 1 1] );
%          set ( hb, 'OuterPosition', [0 0 1 1] );
%          set ( hb, 'Position', [0 0 1 1] );
        end
    end



    quiting = false;
    while ~quiting
        pause(1);
    end
end
