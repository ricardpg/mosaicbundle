function M = mosaic_calc_size(M)

% M = mosaic_calc_size(M)
%
%     M  - mosaic structure
% 
% Calculate the size of the mosaic with the absolute homographies and shift
% them to positive value.
%
% Developed by Olivier Delaunoy (delaunoy@eia.udg.edu)
% Dep. of Electronics, Informatics and Automation, University of Girona


    if ~isempty(M.nodes)
        [h w d] = size(imread(M.nodes(1).image));
        c = [1 1 w w; 1 h h 1; 1 1 1 1];
        minx = inf;
        maxx = -inf;
        miny = inf;
        maxy = -inf;

        for no = 1 : length(M.nodes)
            cc = M.nodes(no).homo.matrix * c;
            cc(1,:) = cc(1,:) ./ cc(3,:);
            cc(2,:) = cc(2,:) ./ cc(3,:);
            minx = min([minx, cc(1,:)]);
            maxx = max([maxx, cc(1,:)]);
            miny = min([miny, cc(2,:)]);
            maxy = max([maxy, cc(2,:)]);
        end

        H=[1 0 -minx+1;0 1 -miny+1;0 0 1];
        for i = 1 : length(M.nodes)
            M.nodes(i).homo.matrix = H * M.nodes(i).homo.matrix;
        end

        M.init.mosaic_origin.x = M.init.mosaic_origin.x - (-minx+1)*M.init.pixel_size.x;
        M.init.mosaic_origin.y = M.init.mosaic_origin.y + (-miny+1)*M.init.pixel_size.y;
        M.init.mosaic_size.x = ceil(maxx-minx)+2; %+2 due to the 0 and the round
        M.init.mosaic_size.y = ceil(maxy-miny)+2;
    else
        M.init.mosaic_origin.x = 0;
        M.init.mosaic_origin.y = 0;
        M.init.mosaic_size.x = 0;
        M.init.mosaic_size.y = 0;
    end
end
