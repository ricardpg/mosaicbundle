function MM = mosaic_reduce_edges(M, maxEdges, suppresionEdges)

    if ~exist('suppresionEdges', 'var'), suppresionEdges = 10; end
    if ~exist('numEdges', 'var'), maxEdges = 5; end
    
    
    MM = M;
    [h w d] = size(imread(M.nodes(1).image));
    for no = 1 : size(M.nodes,2)
        if isempty(M.nodes(no).edges),
            MM.nodes(no).edges = [];
%         elseif size(M.nodes(no).edges,2) <= 3,
%             MM.nodes(no).edges = M.nodes(no).edges;
        else
            edges = zeros(size(M.nodes(no).edges,2), 4);
            for ed = 1 : size(M.nodes(no).edges,2)
                edges(ed,1) = ed;
                edges(ed,2) = M.nodes(no).edges(ed).index;
%                 corr = gpmml_load(M.nodes(no).edges(ed).pointmatches);
%                 edges(ed,3) = size(corr,1);
                edges(ed,4) = min ( [ mosaic_nodes_overlap(M.nodes(no).edges(ed).homo.matrix, h, w) ...
                                      mosaic_nodes_overlap(inv(M.nodes(no).edges(ed).homo.matrix), h, w)] );
            end
            edges = sortrows(edges,2);
            MM.nodes(no).edges = M.nodes(no).edges(edges(end,1));
            pos = find(edges(:,2) >= edges(end,2)-suppresionEdges & edges(:,2) <= edges(end,2)+suppresionEdges);
            edges(pos,:) = [];
            
            index = 1;
            while ~isempty(edges) && index < maxEdges
                index = index+1;
                [m pos] = max(edges(:,4));
                MM.nodes(no).edges(index) = M.nodes(no).edges(edges(pos,1));
                pos = find(edges(:,2) >= edges(pos,2)-suppresionEdges & edges(:,2) <= edges(pos,2)+suppresionEdges);
                edges(pos,:) = [];
            end
        end       
    end
end