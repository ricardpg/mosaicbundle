%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Filter crossings candidates using distance and line criteria.
%
%  File          : FindInitialCrossings.m
%  Date          : 27/02/2006 - 23/03/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  FilterCrossings Given a GMML structure, and a set of crossings filter
%                  the ones that don't define a line or they are too close.
%
%      ListR = FilterCrossings ( M, ImgWidth, ImgHeight, List, NumImages,
%                                RThresh, DThresh )
%
%     Input Parameters:
%      M: GMML structure representing the mosaic.
%      ImgWidth: Images Width.
%      ImgHeight: Images Height.
%      List: Input list of pairs.
%      NumImages: Number of image in any direction taken into account.
%      RThresh: R^2 Linear Regression Coefficient Threshold.
%      DThresh: Distance between First an Last frame Threshold.
%
%     Output Parameters:
%      List: Set of 2xn pairs on indices which correspond to the
%            non-consecutive crossing candidates.
%

function ListR = FilterCrossings ( M, ImgWidth, ImgHeight, List, NumImages, RThresh, DThresh )
  % Test the input parameters
  error ( nargchk ( 7, 7, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the Thresholds
  if RThresh < 0 || RThresh > 1; ...
    error ( 'MATLAB:FilterCrossings:Input', ...
            'Threshold must be in the range [0..1] from least to most restritive!' );
  end

  % Edges and Center coordinates
  c = [ImgWidth/2; ImgHeight/2; 1];

  Lines = zeros ( 2, NumImages * 2 + 1 );
  % Store the Regression coefficients and the R-square
  S = zeros ( 1, 2 );
  d = zeros ( 1, 2 );
  % Resulting list
  ListR = zeros ( size ( List ) );
  % Index to current right line
  t = 1;
  % Scan all the possible pairs
  for i = 1 : size ( List, 2 );
    for k = 1 : 2
      % Compute the nodes arrown the crossing
      First = List(k,i) - NumImages;
      Last = List(k,i) + NumImages;

      l = 1;
      for j = First : Last
        H = M.nodes(j).homo.matrix;
        % Compute the warping
        p = H * c;  p = p ./ p(3,1);
        if j == First;
          pi = p;
        elseif j == Last;
          ds = p - pi;
          ds = ds(1:2) .* ds(1:2);
          d(k) = sqrt ( sum ( ds ) );
        end
        % Store the resulting centers
        Lines(:,l) = p(1:2);
        l = l + 1;
      end

      % Use distance criterion to avoid regressinn
      if all ( d(k) > DThresh );
        X = [ones(size(Lines(1,:),2),1) Lines(1,:)'];
        y = Lines(2,:)';
        [b bint r rint stats] = regress ( y, X );
        % r^2 Coefficient
        S(k) = stats(1);
      else
        % Never will be selected
        S(1) = -1;
      end
    end
    % Test if they are two lines more or less
    if all ( S >= RThresh )
      % 
      ListR(:,t) = List(:,i);
      t = t + 1;
    end
  end

  % Erase empty elements in the list
  if t > 1;
    ListR = ListR(:,1:t-1);
  else
    ListR = [];
  end
end
