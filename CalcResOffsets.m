%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate number of residuals and offset in residual vector.
%
%  File          : CalcResOffsets.m
%  Date          : 15/07/2007 - 19/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CalcResOffsets Calculate number of residuals and it's offset in the
%                 residual vector.
%
%     Input Parameters:
%      MatchPointSize: Size of Match-Point residual matrix.
%      LBLPointSize: Size of LBLPoint residual matrix.
%      LBLCamSize: Size of LBLCam residual matrix.
%      AngCamSize: Size of AngCam residual matrix.
%
%     Output Parameters:
%      NumMatchPoint: Number of Match-Point residuals.
%      NumLBLPoint: Number of LBLPoint residuals.
%      NumLBLCam: Number of LBLCam residuals.
%      NumAngCam: Number of AngCam residuals.
%      MatchPointOffs: Offset of Match-Point in the residual vector.
%      LBLPointOffs: Offset of LBLPoint in the residual vector.
%      LBLCamOffs: Offset of LBLCam the residual vector.
%      AngCamOffs: Offset of AngCam the residual vector.
%

function [ NumMatchPoint NumLBLPoint NumLBLCam NumAngCam ...
           MatchPointOffs LBLPointOffs LBLCamOffs AngCamOffs] = ...
             CalcResOffsets ( MatchPointSize, MatchIdxTableSize, LBLPointSize, LBLCamSize, AngCamSize )
  % Test the input parameters
  error ( nargchk ( 5, 5, nargin ) );
  error ( nargoutchk ( 4, 8, nargout ) );

  if ( ~isempty ( MatchPointSize ) && length ( MatchPointSize ) ~= 3 ) || ...
     ( ~isempty ( MatchIdxTableSize ) && length ( MatchIdxTableSize ) ~= 2 ) || ...
     ( ~isempty ( LBLPointSize ) && length ( LBLPointSize ) ~= 2 ) || ...
     ( ~isempty ( LBLCamSize ) && length ( LBLCamSize ) ~= 2 ) || ...
     ( ~isempty ( AngCamSize ) && length ( AngCamSize ) ~= 2 );
    error ( 'MATLAB:CalcResOffsets:Input', ...
            'Wrong length in input tables (must be 3 for MatchPointSize and 2 for the rest)!\n' );

  end

  % Compute How many residuals there are in each set
  NumMatchPoint = 4 * MatchPointSize(2) * MatchIdxTableSize(1);
  NumLBLPoint = 2 * LBLPointSize(1);
  NumLBLCam = 3 * LBLCamSize(1);
  NumAngCam = 3 * AngCamSize(1);

  if nargout > 4;
    MatchPointOffs = 1;
    LBLPointOffs = NumMatchPoint + 1;
    LBLCamOffs = NumMatchPoint + NumLBLPoint + 1;
    AngCamOffs = NumMatchPoint + NumLBLPoint + NumLBLCam + 1;
  end
end
