%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Given a GMML structure plot the trajectory.
%
%  File          : CalculateSequentialMatches.m
%  Date          : 08/03/2006 - 19/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Toolboxes
% addpath /opt/Matlab/toolbox/...;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Data
DataPath = '/mnt/data/MosaicBundle/Data';

% Input Data
MosaicFileName = '/gmml_Similarity.gmml';
MosaicFilePath = [ DataPath MosaicFileName ];

PathToFIM = './FIM';
PathToImages =  '/mnt/uvl/DataSets/Lustre96';
PathToResults = [ DataPath '/FIM/' ];
PathToAccepted = [ PathToResults 'Sequential/Accepted/' ];
PathToMotion = [ PathToResults 'Sequential/Motion/' ];
PathToMosaic = [ PathToResults 'Sequential/Mosaic/' ];
PathToResultsFIM = [ PathToResults 'Sequential/ResultsFIM/' ];

addpath ( PathToFIM ); 
SetPathsFIM ( PathToFIM );

% Image Sizes, K, etc...
InitParamLucky;

% Load Mosaic if it is not already loaded
if ~exist ( 'M', 'var' );
  disp ( 'Loading Mosaic...' );
  M = gmml_load ( MosaicFilePath );
end

% Parameters for the Homography
OverlapThreshold = 0.2;

% Configure FIM

% - Image Processing Parameters --------------------------------------------

ProcessingParameters.SingleChannel = '';              % 0-no, ''-default, % 1-red, 2-green, 3-blue, 4-grayscale, 5-PCA, 6-Y channel of YIQ
ProcessingParameters.Equalization = 1;                % 0-no, ''-default, 1-full, 2-Clahe, 3-CLAHS
% ProcessingParameters = 1;                           % <- to use default Processing parameters
% ProcessingParameters = 0;                           % <- to cancel Preprocessing of images: Images must have single color channel!

% - Detection Parameters ---------------------------------------------------

Detectors   = { 'SIFT', 'SURF', 'Harris', ...         % 1 - 3 
                'Laplacian', 'Hessian', 'MSER' };     % 4 - 6
          
Descriptors = { 'SIFT', 'SURF', 'SURF-128', ...       % 1 - 3
                'SURF-36', 'U-SURF', ...              % 4 - 5
                'U-SURF-128', 'U-SURF-36' };          % 6 - 7

DetectionParameters.DetType = Detectors(2);
DetectionParameters.DescType = Descriptors(2);        % Chose the descriptor.
DetectionParameters.MaxNumber = 7000;                 % max number of features to detect
DetectionParameters.RadiusNonMaximal = 2;             % radius for non-maximal suppression for detector
DetectionParameters.Sigma = 1;                        % sigma of the Gaussian used in Harris and Laplacian detectors
DetectionParameters.Threshold = 0;                    % [0] - default for all detectors except MSER ( threshold to reject poorer features); 
                                                      % [30] (pixels) - default for MSER -> minimum region size. 
% DetectionParameters = 1;                            % <- to use default Detection parameters
% DetectionParameters = 0;                            % <- to cancel Detection; Description, Matching and RANSAC are
                                                      % cancelled then automatically
Mask = [];                                            % Mask

% - Descriptor Parameters --------------------------------------------------

% Select parameters to calculate the descriptor of the keypoins:
if isstruct ( DetectionParameters )
  DescriptionParameters.DescType = DetectionParameters.DescType; 
else 
  DescriptionParameters.DescType = Descriptors(2); 
end
% DescriptionParameters = 1;                          % <- to use default Description parameters
% DescriptionParameters = 0;                          % <- to cancel calculation of descriptors; Matching and 
                                                      % RANSAC are cancelled then automatically
% - Matching Parameters ----------------------------------------------------

MatchingParameters.Matcher = 'Sift';
MatchingParameters.SiftRatio = 1.5;                   % ratio between the next closest and closest nearest neighbour for SIFT matching     
MatchingParameters.Bidirectional = 1;                 % If ~= 0 Intersection between "image1 to image2" and "image2 to image1" matches
% MatchingParameters = 1;                             % <- to use default Matching parameters
% MatchingParameters = 0;                             % <- to cancel Matching, RANSAC is cancelled then by default

% - RANSAC Parameters ------------------------------------------------------

HomographyModels = { 'Euclidean', 'Similarity', ...   % 1 - 2
                     'Affine', 'Projective' };        % 3 - 4
                 
RansacParameters.HomographyModel = HomographyModels(4);
RansacParameters.SpatialStdev = 5;                   % stdev of spatial coord of features, in pixels
% RansacParameters = 1;                               % <- to use default parameters for RANSAC
% RansacParameters = 0;                               % <- to cancel execution of RANSAC

% - Display Parameters -----------------------------------------------------

DisplayParameters.DisplayConfig = 1;                  % 1 -> display FIM configuretion; 0 -> not display
DisplayParameters.DisplayResults = 1;                 % 1 -> display statistics of FIM execution results; 0 -> not display
DisplayParameters.SepLines = 1;                       % 1 -> display separation lines; 0 -> not display
% DisplayParameters = 1;                              % <- to use default display parameters
% DisplayParameters = 0;                              % <- to cancel display output of FIM

% - Plotting Parameters ----------------------------------------------------

PlottingParameters.PlotInitialImages = 1;             % 1 -> initial images are displayed; 0 -> the processed grayscale images are displayed;
PlottingParameters.SaveFigures = 1;             
PlottingParameters.SaveInFormat = 'jpg';              % supported: 'jpg', 'tif', 'eps', 'png', 'ppm', 'wmf' ('wmf' only for Windows)
PlottingParameters.PlotImages = 0;                    % 1 -> display images with keypoints; 0 -> not display
PlottingParameters.PlotInitialMatches = 0;            % 1 -> display initial matches; 0 -> not display
PlottingParameters.PlotRejectedMatches = 0;           % 1 -> display rejected by RANSAC matches; 0 -> not display
PlottingParameters.PlotAcceptedMatches = 1;           % 1 -> display accepted by RANSAC matches; 0 -> not display
PlottingParameters.PlotMotion = 1;                    % 1 -> display matched pairs in the first image; 0 -> not display;
PlottingParameters.PlotMosaic = 1;                    % 1 -> display mosaic of two images; 0 -> not display;
PlottingParameters.WhereToSave = PathToResults; 
% PlottingParameters = 0;                             % suppress plotting

% ========================== FIM Execution ===============================

if ~exist ( 'i', 'var' );
  % Create the Directory with Results
  if PlottingParameters.SaveFigures; 
%     mkdir ( PathToResults );

%     mkdir ( PathToAccepted );   delete ( [ PathToAccepted '/*.*' ] );
%     mkdir ( PathToMotion );     delete ( [ PathToMotion '/*.*' ] );
%     mkdir ( PathToMosaic );     delete ( [ PathToMosaic '/*.*' ] );
%     mkdir ( PathToResultsFIM ); delete ( [ PathToResultsFIM '/*.*' ] );
  end

  FirstNode = 1;
else
  FirstNode = i;
end

MaxNodes = size ( M.nodes, 2 );

if ~exist ( 'LastNode', 'var' );
  LastNode = MaxNodes - 1;
end

if LastNode < FirstNode || LastNode >= MaxNodes || FirstNode <= 0;
  error ( 'MATLAB:CalculateSequentialMatches:Input', ...
          sprintf ( 'FirstNode (%d) must be bigger than the LastNode (%d) and both in the range [%d, %d]', ...
          FirstNode, LastNode, 1, MaxNodes ) );
end

InitTime = cputime;
EstimatedHomographies = 0;
N = LastNode - FirstNode + 1;

for i = FirstNode:LastNode;
  ItTime = cputime;

  % Reference Image
%  IRefName = [PathToImages '/' M.nodes(i).image];
  IRefName = [M.nodes(i).image];
  IRef = imread ( IRefName );

  % Current Image
%  ICurName = [PathToImages '/' M.nodes(i+1).image];
  ICurName = [M.nodes(i+1).image];
  ICur = imread ( ICurName );

  disp ( sprintf ( 'Trying image %d / %d', i, LastNode ) );
  disp ( sprintf ( '  Ref. Image %d: "%s"', i, IRefName ) );
  disp ( sprintf ( '  Cur. Image %d: "%s"', i+1, ICurName ) );

  [ Img1, Img2, ResultsFIM, ConfigurationFIM ] = FIM ( IRef, ICur, Mask, ...
                                                       ProcessingParameters, ...
                                                       DetectionParameters, ...
                                                       DescriptionParameters, ...
                                                       MatchingParameters, ...
                                                       RansacParameters, ...
                                                       DisplayParameters );

  % Prefix in the sequence
  Prefix = sprintf ( '%5.5d_%5.5d', i + 1, i );

  % Rename variables
  ResultsFIMName = [ 'ResultsFIM_' Prefix ];
  ResultsFIMRenameCmd = [ ResultsFIMName '=ResultsFIM;' ];
  eval ( ResultsFIMRenameCmd );

  ConfigurationFIMName = [ 'ConfigurationFIM_' Prefix ];
  ConfigurationFIMRenameCmd = [ ConfigurationFIMName '=ConfigurationFIM;' ];
  eval ( ConfigurationFIMRenameCmd );

  % Save results
  save ( [ PathToResultsFIM Prefix '.mat' ], ResultsFIMName, ConfigurationFIMName );

  % Clear variables
  eval ( [ 'clear ' ResultsFIMName ] );
  eval ( [ 'clear ' ConfigurationFIMName ] );

  % Compute how many matches where found
  if isfield ( ResultsFIM, 'Homography' ) && ~isempty ( ResultsFIM.Homography );

    EstimatedHomographies = EstimatedHomographies + 1;

    if PlottingParameters.SaveFigures;
      PlottingParameters.PlotAcceptedMatches = 1;
      PlottingParameters.PlotMotion = 0;
      PlottingParameters.PlotMosaic = 0;
      PlottingParameters.WhereToSave = [ PathToAccepted Prefix '_' ];
      PlotFIM ( IRef, ICur, Img1, Img2, ResultsFIM, PlottingParameters );

      PlottingParameters.PlotAcceptedMatches = 0;
      PlottingParameters.PlotMotion = 1;
      PlottingParameters.PlotMosaic = 0;
      PlottingParameters.WhereToSave = [ PathToMotion Prefix '_' ];
      PlotFIM ( IRef, ICur, Img1, Img2, ResultsFIM, PlottingParameters );

      PlottingParameters.PlotAcceptedMatches = 0;
      PlottingParameters.PlotMotion = 0;
      PlottingParameters.PlotMosaic = 1;
      PlottingParameters.WhereToSave = [ PathToMosaic Prefix '_' ];
      PlotFIM ( IRef, ICur, Img1, Img2, ResultsFIM, PlottingParameters );

      close all;
    end
  else
    disp ( sprintf ( 'Homography couldn''t be estimated!' ) );
  end

  FinalTime = cputime;
  Time = FinalTime - ItTime;
  AllTime = (( FinalTime - InitTime ) * (N - i + 1)) / (i - FirstCrossing + 1);
  disp ( sprintf ( 'Last Iteration time %f s. Expected Time %f s. (%f hours)',  Time, AllTime, AllTime / 3600 ) );
  disp ( sprintf ( 'Well Estimated Homographies %d\n\n', EstimatedHomographies ) ); 
end

