%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Given a GMML structure plot the trajectory.
%
%  File          : CalculateSubMosaic.m
%  Date          : 07/03/2006 - 19/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Toolboxes
% addpath /opt/Matlab/toolbox/...;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Data
DataPath = '/mnt/data/MosaicBundle/Data';

% Use the crossings in the mosaic (or =0 in a table file)
UseMosaicCrossings = 1;

% Check that the Crossings GPMMLs exit
CheckGPMML = false;

% Input Data
if UseMosaicCrossings;
  MosaicFileName = '/Filtered_Sequential_Crossings.gmml';
else
  MosaicFileName = '/Filtered_Sequential.gmml';
end

MosaicFilePath = [ DataPath MosaicFileName ];
% FinalCrossingsFileName = '/FinalCrossings_5.mat';
FinalCrossingsFileName = '/Filtered_FinalCrossings_5.mat';
FinalCrossingsFilePath = [ DataPath FinalCrossingsFileName ];
GPMMLFilePath = [ DataPath '/GPMMLFiles/Crossings/' ];
PathToResults = [ DataPath '/FIM/' ];
PathToResultsFIM = [ PathToResults 'Crossings/ResultsFIM/' ];

% Final Mosaic
PathToFinalMosaic = [ DataPath '/Sub_Sequential_Crossings.gmml' ];

% How many connected regions to extract.
NumConnectedRegions = 1;

% Image Sizes, K, etc...
InitParamLucky;

% Load Final Crossings
if ~exist ( 'FinalCrossings', 'var' );
  disp ( sprintf ( 'Loading Final Crossings: "%s"...', FinalCrossingsFilePath ) );
  load ( FinalCrossingsFilePath );
  % In the case it comes from filtered.
  if ~exist ( 'FinalCrossings', 'var' );
    FinalCrossings = FilteredFinalCrossings;
  end
end

% Load Mosaic if it is not already loaded
if ~exist ( 'Morg', 'var' );
  disp ( sprintf ( 'Loading Mosaic: "%s"...', MosaicFilePath ) );
  tic;
  M = gmml_load ( MosaicFilePath );
  toc

  if UseMosaicCrossings;
    % Mosaic With crossings to extract Homographies from crossings
    Morg = M;

    % Eliminate the Crossings in M
    for i = 1 : size ( M.nodes, 2 );
      M.nodes(i).edges = [];
      s = size ( Morg.nodes(i).edges, 2 );
      if s > 0;
        j = 1;
        Found = false;
        while j <= s && ~Found;
          Found = Morg.nodes(i).edges(j).index == i - 1;
          if ~Found; j = j + 1; end;
        end
        if Found;
          M.nodes(i).edges = Morg.nodes(i).edges(j);
        end
      end
    end
  end
end


% Plot the source Mosaic
H = figure;
figure ( H );
axis equal;
axis ij;
hold on;
% MosaicTrajectoryAbs ( M, ImgWidth, ImgHeight, -1, -1, FinalCrossings, [], false, false, false );
MosaicTrajectoryAbs ( M, ImgWidth, ImgHeight, -1, -1, FinalCrossings, [], false, false, false );

disp ( 'Zoom and pan to the area to compute the submosaic and press RETURN to continue...' );
pause;
disp ( 'Use the mouse to select the rect that includes the mosaic...' );

Rect = getrect ( H );

if Rect(3) == 0 || Rect(4) == 0;
  error ( 'MATLAB:CalculateSubMosaic:Input', 'Invalid rect selected!' );
end

% Plot the square region selected in red
Lx = [ Rect(1) Rect(1)+Rect(3) Rect(1)+Rect(3) Rect(1) Rect(1) ];
Ly = [ Rect(2) Rect(2) Rect(2)+Rect(4) Rect(2)+Rect(4) Rect(2) ];
line ( Lx, Ly, 'Color', [0 1 0], 'LineStyle', '--' );

[Mw, NodeMap, Segments] = SubMosaicWindow ( M, ImgWidth, ImgHeight, -1, -1, ...
                                           [Rect(1), Rect(2)], [Rect(1)+Rect(3), Rect(2)+Rect(4)] );

% Remap indices
NewFinalCrossings = [];
if ~isempty ( FinalCrossings );
  NewFinalCrossings(1,:) = NodeMap(FinalCrossings(1,:));
  NewFinalCrossings(2,:) = NodeMap(FinalCrossings(2,:));
  % Filter 0s
  Idx = find ( NewFinalCrossings(1,:) .* NewFinalCrossings(2,:) );
  NewFinalCrossings = NewFinalCrossings(:,Idx);
end

% Remap indices
NewSegments = [];
if ~isempty ( Segments );
  NewSegments(1,:) = NodeMap(Segments(1,:));
  NewSegments(2,:) = NodeMap(Segments(2,:));
  % Filter 0s
  NewSegments = NewSegments(:, NewSegments(1,:) ~= 0 & NewSegments(2,:) ~= 0 );
end

figure;
axis equal;
axis ij;
hold on;
MosaicTrajectoryAbs ( Mw, ImgWidth, ImgHeight, -1, -1, NewFinalCrossings, NewSegments, false, false, true );
line ( Lx, Ly, 'Color', [0 1 0], 'LineStyle', '--' );

% Calculate the connected segments
NumSegments = size ( NewSegments, 2 );

% Compute Indices from any frame to the segment
NumNewNodes = size ( Mw.nodes, 2 );
% Create a lookup table to access into the "Segment-Connected" Matrix directly from the node index
LookUpNode = zeros ( 1, NumNewNodes );
for i = 1:NumSegments;
  for j = NewSegments(1,i):NewSegments(2,i);
    LookUpNode(j) = i;
  end
end

% No segment is connected to any other
Connected = zeros ( NumSegments, NumSegments );
% Detect problems
RepeatedCrossings = 0;

% Each segment is connected to itself
for i = 1:NumSegments; Connected(i, i) = 1; end

% Mark the connections using the crossings
for i = 1:size ( NewFinalCrossings, 2 )
  % Connect in both directions
  if Connected(LookUpNode(NewFinalCrossings(1,i)),LookUpNode(NewFinalCrossings(2,i))) == 0;
    Connected(LookUpNode(NewFinalCrossings(1,i)),LookUpNode(NewFinalCrossings(2,i))) = 1;
  else
    RepeatedCrossings = RepeatedCrossings + 1;
  end
  if Connected(LookUpNode(NewFinalCrossings(2,i)),LookUpNode(NewFinalCrossings(1,i))) == 0;
    Connected(LookUpNode(NewFinalCrossings(2,i)),LookUpNode(NewFinalCrossings(1,i))) = 1;
  else
    RepeatedCrossings = RepeatedCrossings + 1;
  end
end

if RepeatedCrossings;
  warning ( 'MATLAB:CalculateSubMosaic:Input', ...
            sprintf ( 'There are %d crossing transects with repeated crossing points!', RepeatedCrossings ) );
end

% Look for the connected Segments in Groups
SegmentToGroup = SegmentCCL ( Connected );
NumGroups = max ( SegmentToGroup );
disp ( sprintf ( 'There are %d different groups!', NumGroups ) );

% Size in images for each segment
ImagesPerSegment = NewSegments(2,:) - NewSegments(1,:) + 1;

% Compute the size for each connected group of segments
ConnectedGroupSize = zeros ( 1, NumGroups );
for i = 1 : NumSegments;
  ConnectedGroupSize(SegmentToGroup(i)) = ConnectedGroupSize(SegmentToGroup(i)) + ImagesPerSegment(i);
end

% Work using a copy
TmpConnectedGroupSize = ConnectedGroupSize;

% Iterate over the biggest connected regions
for j = 1 : NumConnectedRegions;
  % The bigges group
  [MaxImages GroupIdx] = max ( TmpConnectedGroupSize );
  % Mark it to avoid newer selection
  TmpConnectedGroupSize(GroupIdx) = -1;

  % Detect errors
  CrossingError = 0;

  CleanSegments = zeros ( 2, NumSegments ); 
  s = 0;
  for i = 1 : NumSegments;
    if SegmentToGroup(i) == GroupIdx;
      LowLimit = NewSegments(1, i);
      HighLimit = NewSegments(2, i);
      n = HighLimit - LowLimit;
      if n >= 0;
        % Write the final segments
        s = s + 1;
        CleanSegments(1, s) = HighLimit;
        CleanSegments(2, s) = LowLimit;
      else
        CrossingError = CrossingError + 1;
      end
    end
  end

  % This should never happen if the segments are right
  if CrossingError > 0;
    warning ( 'MATLAB:CalculateSubMosaic:Input', ...
              sprintf ( 'There are %d crossing errors!', CrossingError ) );
  end

  % Cut the list of segments
  if s > 0; CleanSegments = CleanSegments(:,1:s); else CleanSegments = []; end;

  % Calculate the submosaic using a set of nodes
  [Mn, SubNodeMap] = SubMosaicSegments ( Mw, CleanSegments );

  % Remap indices
  SubFinalCrossingsClean = [];
  if ~isempty ( NewFinalCrossings );
    SubFinalCrossings = [];
    SubFinalCrossings(1,:) = SubNodeMap(NewFinalCrossings(1,:));
    SubFinalCrossings(2,:) = SubNodeMap(NewFinalCrossings(2,:));
    % Filter 0s
    SubFinalCrossingsClean = SubFinalCrossings(:,SubFinalCrossings(1,:) ~= 0 & SubFinalCrossings(2,:) ~= 0);
  end

  % Remap indices
  SubSegments = [];
  if ~isempty ( NewSegments );
    SubSegments(1,:) = SubNodeMap(NewSegments(1,:));
    SubSegments(2,:) = SubNodeMap(NewSegments(2,:));
    % Filter 0s
    Idx = find ( SubSegments(1,:) .* SubSegments(2,:) );
    SubSegments = SubSegments(:,Idx);
  end

  % Plot the Final Mosaic
  h = figure;
  s = sprintf ( 'Biggest Connected Mosaic %d', j );
  set ( h, 'Name', s );
  title ( gca ( h ), s );
  axis equal;
  axis ij;
  hold on;
  MosaicTrajectoryAbs ( Mn, ImgWidth, ImgHeight, -1, -1, SubFinalCrossingsClean, SubSegments, false, false, false );
end


% Final Consecutive Mosaic : Mn
% List of Crossings        : SubFinalCrossingsClean
%

% Compute back the original index for the crossings
NumCrossings = size ( SubFinalCrossingsClean, 2 );
OriginalCrossingsMap = zeros ( 2, NumCrossings );
for i = 1 : NumCrossings;
  OriginalCrossingsMap(1,i) = Mn.nodes(SubFinalCrossingsClean(1,i)).index;
  OriginalCrossingsMap(2,i) = Mn.nodes(SubFinalCrossingsClean(2,i)).index;
end

% Look for the Homographies in the saved data
for i = 1 : NumCrossings;
  Prefix = sprintf ( '%5.5d_%5.5d', OriginalCrossingsMap(1,i), OriginalCrossingsMap(2,i) );

  if ~UseMosaicCrossings;
    ResultsFIMFileName = [ Prefix '.mat' ];
    ResultsFIMFilePath = [ PathToResultsFIM ResultsFIMFileName ];
    ResultsFIMName = [ 'ResultsFIM_' Prefix ];
  
    if exist ( ResultsFIMFilePath, 'file' ) ~= 0;
      % Load Data
      ResultsFIMStruct = load ( ResultsFIMFilePath, ResultsFIMName );
      ResultsFIM = ResultsFIMStruct.(ResultsFIMName);

      H = ResultsFIM.Homography;
      if isempty ( H );
        H = eye ( 3 );
      end
    else
      error ( 'MATLAB:SequentialHomographies:Input', ...
              sprintf ( 'GPMML file "%s" not found!', ResultsFIMFilePath  ));
    end
  else
    % The crossings are written in the mosaic
    j = 1;
    s = size ( Morg.nodes( OriginalCrossingsMap(1,i) ).edges, 2 );
    Found = false;
    while j <= s && ~Found;
      Found = Morg.nodes( OriginalCrossingsMap(1,i) ).edges(j).index == ...
                OriginalCrossingsMap(2,i);
      if ~Found; j = j + 1; end;
    end

    if Found;
      H = Morg.nodes( OriginalCrossingsMap(1,i) ).edges(j).homo.matrix;
    else
      error ( 'MATLAB:SequentialHomographies:Input', ...
              sprintf ( 'Node %d must have edges!', OriginalCrossingsMap(1,i) ) );
    end
  end
  
  Idx = SubFinalCrossingsClean(1,i);

  % Compute the tape Subfolder
  ImageFileName = Mn.nodes(Idx).image;
  TapeList = regexp ( ImageFileName, 'tape[\d]+', 'match' );
  if isempty ( TapeList ) || length ( TapeList ) ~= 1;
    error ( 'MATLAB:SequentialHomographies:Input', ...
            sprintf ( 'Tape not found at "%s"', ImageFileName ) );
  end
  % Get the first
  Tape = TapeList{1};

  % Setup the edge
  Edge.index = SubFinalCrossingsClean(2,i);
  Edge.homo.model = 'pro';
  Edge.homo.matrix = H;
  Edge.homo.type = 'rel';
  Edge.homo.covar = [0 0 0; 0 0 0; 0 0 0];
  Edge.pointmatches = [ GPMMLFilePath Tape '/PM-' Prefix '.gpmml' ];

  % Test that the file exists
  if CheckGPMML;
    if exist ( Edge.pointmatches, 'file' ) == 0;
      error ( 'MATLAB:SequentialHomographies:Input', ...
              sprintf ( 'GPMML file "%s" not found!', Edge.pointmatches ) );
    end
  end

  % Add the new edge
  NumEdges = length ( Mn.nodes(Idx).edges );
  if NumEdges == 0;
    Mn.nodes(Idx).edges = Edge;
  elseif NumEdges == 1;
    Mn.nodes(Idx).edges(NumEdges+1) = Edge;
  end
end


disp ( 'Calculating mosaic size...' );
s = warning ( 'off' );
MRes = mosaic_calc_size ( Mn );
warning ( s );

% Save the final Sub Mosaic with the crossings
disp ( sprintf ( 'Saving Final Mosaic: "%s"...', PathToFinalMosaic ) );
gmml_save ( PathToFinalMosaic, MRes );
