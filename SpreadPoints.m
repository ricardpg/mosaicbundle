%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Select a subset of points close to the edges.
%
%  File          : SpreadPoints.m
%  Date          : 31/05/2007 - 31/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  SpreadPoints Select a subset of k points close to the edges of the
%               image (0, 0) - (Width, Height).
%
%      [M I] = SpreadPoints ( P, k )
%
%     Input Parameters:
%      P: nx4 matrix containing pairs of (x,y); (x',y') points.
%      k: Number of wanted points (<=n).
%      Width: Image Width.
%      Height: Image Height.
%
%     Output Parameters:
%      M: kx4 matrix containing the resulting selected points.
%      I: kx1 vector containing the indices to the input data.
%

function [M I] = SpreadPoints ( P, k, Width, Height )
  % Test the input parameters
  error ( nargchk ( 4, 4, nargin ) );
  error ( nargoutchk ( 1, 2, nargout ) );

  % Number of Nodes
  [n, c] = size ( P );

  if c ~= 4 || k > n;
    error ( 'MATLAB:SpreadPoints:Input', 'Input P matrix must be a nx4 matrix (n<=k)!' );
  end

  % Trivial case
  if k == n;
    M = P;
    I = 1:k;
  else
    % Matrix containing distances to the corners:
    % 1..4: Points => 1: Top Left; 2: Top Right; 3: Bottom Right; 4: Bottom Left
    % 5..8: Matches
    DistCorn = zeros ( n, 8 );
    Rest = [ 0 0; Width 0; Width Height; 0 Height ];

    for i = 1 : 8;
      % Select Points or Matches
      if i < 5; Corn = P(:,1:2); else Corn = P(:,3:4); end;
      j = mod ( i-1, 4) + 1;
      Corn(:,1) = Rest(j,1) - Corn(:,1);
      Corn(:,2) = Rest(j,2) - Corn(:,2);
      Corn = Corn .* Corn;
      Corn = sqrt ( sum ( Corn, 2 ) );
      DistCorn(:,i) = Corn;
    end

    % Resulting set
    M = zeros ( k, 4 );
    if nargout > 1; I = zeros ( k, 1 ); end;

    % Select the points
    for i = 1 : k; 
      j = mod ( i-1, 8 ) + 1;
      
      [z, Idx] = min ( DistCorn(:, j ) );
      % Copy
      M(i,:) = P(Idx,:);
      % Mark
      DistCorn(Idx,:) = Inf;

      % Index
      if nargout > 1; I(i) = Idx; end;
    end
  end
end


