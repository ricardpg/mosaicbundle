%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Lucky Strike Data Set Bundle Adjustment.
%
%  File          : OptimLucky.m
%  Date          : 14/03/2006 - 03/09/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Base Path
BasePath = '/mnt/data/MosaicBundle';

% Stereo Tracker Path
StereoTrackerPath = [ BasePath '/STracker/Stereo' ];
addpath ( [StereoTrackerPath '/5_MotionEstimation/3D/Quaternions/' ], ...
          [StereoTrackerPath '/7_Plotting/' ], [StereoTrackerPath '/A_OpenInventor/' ], ...
          [StereoTrackerPath '/5_MotionEstimation/3D/' ], ...
          [StereoTrackerPath '/5_MotionEstimation/3D/msba/' ], ...
           '-END' );

% Optimization Path
addpath ( [ BasePath '/Optimization' ], '-END' );
addpath ( [ BasePath '/HomoTest' ], '-END' );
addpath ( [ BasePath '/Create' ], '-END' );

% Data
DataPath = '/mnt/mosaic/BlendTest';
ImgPath = '';

% Input Data
MosaicFileName = '/mosaic.gmml';
MosaicFilePath = [ DataPath MosaicFileName ];

OptimizedMosaicFileName = '/Opt_mosaic.gmml';
ResultingDataFileName = '/Opt_Data.mat';
OptimizedMosaicFilePath = [ DataPath OptimizedMosaicFileName ];
ResultingDataFilePath = [ DataPath ResultingDataFileName ];

Trajectory3DFileName = '/mosaic.iv';
Trajectory3DFilePath = [ DataPath Trajectory3DFileName ];
OptTrajectory3DFileName = '/Opt_mosaic.iv';
OptTrajectory3DFilePath = [ DataPath OptTrajectory3DFileName ];

FiguresFilePath = [ DataPath '/Figures' ];

% Configurations
DoPlot3DTrajectory = true;
  DoPlot3DTrajectorySource = true;
  DoPlot3DTrajectorySourceFigure = false;
  DoPlot3DTrajectoryOptimized = true;
  DoPlot3DTrajectoryOptimizedFigure = false;
DoPlotResidual = true;
  DoPlotResidualHistory = true;
  DoPlotSortedResiduals = true;
DoDraw2DMosaics = false;
DoPlotInitial2DTrajectory = false;
DoPlotFinal2DTrajectory = false;
DoPlotInitial2DMosaic = false;
DoPlotFinal2DMosaic = false;
DoSaveFigures = true;

DoPlotOptimizationEvolution = true;  % Plot evolution of the residuals while optimizing
DoNormalization = true;              % Perform variable normalization in optimization
NumIterations = 0;                   % Number of initial Iterations (0 = Ask user)

NumMaxPoints = 5;      % Number of Correspondences for the Point-Match (0 => Corners)
DoReduceEdges = true;  % Reduce initial set of edges
  NumMaxOverlaps = 25; % Maximum number of overlapping nodes for node. (0 => All)
  EdgeWindow = 3;      % When reducing edges, take the ones close to the sequential -+ EdgeWindow

% Load Mosaic if it is not already loaded
if ~exist ( 'M0', 'var' );
  disp ( sprintf ( 'Loading Mosaic "%s"...', MosaicFilePath ) );
  M0 = gmml_load ( MosaicFilePath );
end

if DoReduceEdges;
  disp ( 'Reducing edges in the source mosaic...' );
  disp ( sprintf ( '  Maximum Overlaps: %d', NumMaxOverlaps ) );
  disp ( sprintf ( '  Neighborhood window: %d', EdgeWindow ) );

  M0 = mosaic_reduce_edges ( M0, NumMaxOverlaps, EdgeWindow );
  % Do not try to reduce further when Converting Data to Bundle
  NumMaxOverlaps = 0;
end

% Image Sizes, K, etc...
InitParamBlend;

% 2D Planar Homography with the scaling factor to save the final Absolute
% Homographies in pixels
Hs = [ 1/M0.init.pixel_size.x            0               0; 
                0             1/M0.init.pixel_size.y     0;
                0                        0               1 ];


% --[ Convert Data in order to be used in the Optimization ]---------------

disp ( 'Computing Trajectory Segment End-Nodes...' );

FloatingNodes = [];

% Points at UTM coordinates
FixedPointNodes = [];

% Fuse the edges with CameraAtUTM
FixedCameraNodes = [];

% Number of nodes in the Source Mosaic
NumNodes = size ( M0.nodes, 2 );

% Use all the angular readings.
AngCamNodes = [];

disp ( 'Converting source data for the Bundle...' );
tic;
[RTs, MatchIdxTable, MatchPointData, LBLPointData, LBLCamData, AngCamData] = ...
      ConvertDataToBundle ( M0, ImgPath, FixedPointNodes, FixedCameraNodes, ...
                            AngCamNodes, NumMaxOverlaps, NumMaxPoints );
toc

% Compute Nodes.
CameraNodesMatched = zeros ( 1, size ( LBLCamData, 1 ) );
for i = 1 : size ( LBLCamData, 1 );
  CameraNodesMatched(i) = M0.nodes(LBLCamData(i, 1)).index;
end
% Compute final indices for floating nodes (To compute weights them in LBL Camera Data)
FloatingNodesIdx = find ( ismember ( CameraNodesMatched, FloatingNodes ) );

disp ( sprintf ( 'Found %d LBL Camera nodes and %d Floating (Total = %d)...', ...
       size ( LBLCamData, 1 ) - size ( FloatingNodesIdx, 2 ), size ( FloatingNodesIdx, 2 ), size ( LBLCamData, 1 ) ) );
if ~isempty ( FloatingNodesIdx) && ~isempty ( FloatingNodes ) && ~all ( size ( FloatingNodesIdx ) == size ( FloatingNodes ) )
  error ( 'MATLAB:Optimization:Input', 'Error calculating floating nodes!' );
end
  
% --[ Plot Source Trajectory and Mosaic ]----------------------------------

if DoPlotInitial2DTrajectory;
  disp ( 'Displaying Initial 2D Trajectory...' );
  Initial2DTrajectoryFig = figure;
  s = 'Initial 2D Trajectory';
  set ( Initial2DTrajectoryFig, 'Name', s );
  title ( gca ( Initial2DTrajectoryFig ), s );
  axis equal;
  axis ij;
  hold on;
  MosaicTrajectoryAbs ( M, ImgWidth, ImgHeight, -1, -1, [], [], false, false, false );
end

if DoPlotInitial2DMosaic;
  disp ( 'Displaying Initial Mosaic...' );
  Initial2DMosaicFig = figure;
  s = 'Source Mosaic Preview';
  set ( Initial2DMosaicFig, 'Name', s );
  title ( gca ( Initial2DMosaicFig ), s );
  axis equal;
  axis ij;
  hold on;
  MosaicPreviewAbs ( M0, ImgWidth, ImgHeight, -1, -1, 1, [], true );
end


% --[ Perform the Global Optimization ]------------------------------------

disp ( 'Running Optimization...' );
% Residual Weights
WMatchPoint = 1;  % Meters
WLBLPoint = 0;
WLBLCam = 0;
WAngCam = 0;

% First iteration parameters
x0 = [];
xt0 = [];
JPattern0 = [];
Residualt0 = [];

disp ( 'Weight Configuration...' );
fprintf ( '  Point-Match: ' );
if ~isempty ( WMatchPoint ); fprintf ( '%f\n', WMatchPoint ); else fprintf ( 'Not Used\n' ); end;
fprintf ( '  LBL-Point  : ' ); 
if ~isempty ( WLBLPoint ); fprintf ( '%f\n', WLBLPoint ); else fprintf ( 'Not Used\n' ); end;
fprintf ( '  LBL-Camera : ' );
if ~isempty ( WLBLCam );
  fprintf ( '\n' );
  fprintf ( '    Vector Length: %d\n', size ( WLBLCam, 1 ) );
else
  fprintf ( 'Not Used\n' );
end
fprintf ( '  Cam Angle : ' );
if ~isempty ( WAngCam );
  fprintf ( '\n' );
  fprintf ( '    Vector Length: %d\n', size ( WAngCam, 1 ) );
else
  fprintf ( 'Not Used\n' );
end

[iHw0 wHi0 iHw wHi x xt Jacobians JPattern Residual Residualt Normalization] = ...
     RunOptimization ( K, RTs, NumIterations, WMatchPoint, MatchIdxTable, MatchPointData, ...
                       WLBLPoint, LBLPointData, WLBLCam, ...
                       LBLCamData, WAngCam, AngCamData, x0, xt0, JPattern0, Residualt0, ...
                       DoNormalization, DoPlotOptimizationEvolution );


% Number of Homographies (The same than entries in RTs)
NumCameras = size ( iHw, 3 );

% Copy the source mosaic
M = M0;

if NumCameras ~= size ( M.nodes, 2 );
  error ( 'MATLAB:Optimization:Input', 'Incorrect number of output homographies' );
end

% Optimized poses
RTs_Opt = RTs;
j = 1;
% Compute the final homography
for i = 1 : NumCameras;
  % New Homography
  H = Hs * wHi(:,:,i);
  M.nodes(i).homo.matrix = H / H(3,3);

  % Optimized pose
  RTs_Opt(i).cRw = quat2rotmatMx ( x(j:j+3) );
  RTs_Opt(i).wTc = x(j+4:j+6)';
  j = j + 7;
end


% --[ Look for potential outliers ]----------------------------------------

% Compute How many residuals there are in each set
[ NumMatchPoint NumLBLPoint NumLBLCam NumAngCam ...
  OffsMatchPoint OffsLBLPoint OffsLBLCam OffsAngCam ] = ...
    CalcResOffsets ( size ( MatchPointData ), size ( MatchIdxTable ), ...
                     size ( LBLPointData ), size ( LBLCamData ), size ( AngCamData ) );

% Compute offsets and delete minimization figures
if NumMatchPoint > 0;
  ResIdx{1} = OffsMatchPoint+0:4:OffsMatchPoint+NumMatchPoint-1;
  ResIdx{2} = OffsMatchPoint+1:4:OffsMatchPoint+NumMatchPoint-1;
  ResIdx{3} = OffsMatchPoint+2:4:OffsMatchPoint+NumMatchPoint-1;
  ResIdx{4} = OffsMatchPoint+3:4:OffsMatchPoint+NumMatchPoint-1;
end
if NumLBLPoint > 0;
  ResIdx{5} = OffsLBLPoint+0:2:OffsLBLPoint+NumLBLPoint-1;
  ResIdx{6} = OffsLBLPoint+1:2:OffsLBLPoint+NumLBLPoint-1;
end
if NumLBLCam > 0;
  ResIdx{7} = OffsLBLCam+0:3:OffsLBLCam+NumLBLCam-1;
  ResIdx{8} = OffsLBLCam+1:3:OffsLBLCam+NumLBLCam-1;
  ResIdx{9} = OffsLBLCam+2:3:OffsLBLCam+NumLBLCam-1;
end
if NumAngCam > 0;
  ResIdx{10} = OffsAngCam+0:3:OffsAngCam+NumAngCam-1;
  ResIdx{11} = OffsAngCam+1:3:OffsAngCam+NumAngCam-1;
  ResIdx{12} = OffsAngCam+2:3:OffsAngCam+NumAngCam-1;
end

% To consider outlier ns times stdev from mean
disp ( 'Look for potential outliers...' );
% 2 => 95%,  3 => 99.7%
NumStDev{1} = 5;
NumStDev{2} = 5;
NumStDev{3} = 5;
NumStDev{4} = 5;
NumStDev{5} = 3;
NumStDev{6} = 3;
NumStDev{7} = 3;
NumStDev{8} = 3;
NumStDev{9} = 3;
NumStDev{10} = 3;
NumStDev{11} = 3;
NumStDev{12} = 3;

VarName{1} = 'x';     VarName{2} = 'y';      VarName{3} = 'xp';   VarName{4} = 'yp';
VarName{5} = 'X';     VarName{6} = 'Y';
VarName{7} = 'X';     VarName{8} = 'Y';      VarName{9} = 'Z';
VarName{10} = 'Roll'; VarName{11} = 'Pitch'; VarName{12} = 'Yaw';

ResType{1} = 'Match Point';     ResType{2}  = ResType{1};  ResType{3}  = ResType{1}; ResType{4} = ResType{1};
ResType{5} = 'LBL Point';       ResType{6}  = ResType{5};
ResType{7} = 'LBL Camera';      ResType{8}  = ResType{7};  ResType{9}  = ResType{7};
ResType{10} = 'Angular Camera'; ResType{11} = ResType{10}; ResType{12} = ResType{10};

for i = 1 : 12;
  % Calculate Unweighted Residuals
  if ( i >= 1 && i <= 4 && NumMatchPoint > 0 );
    SubRes = Residual(ResIdx{i}) ./ WMatchPoint;
  elseif ( i >= 5 && i <= 6 && NumLBLPoint > 0 );
    SubRes = Residual(ResIdx{i}) ./ WLBLPoint;

    % Remove the scale if it is necessary
    if ~isempty ( Normalization );
      SubRes = SubRes * Normalization(4);
    end
  elseif ( i >= 7 && i <= 9 && NumLBLCam > 0 );
    if isscalar ( WLBLCam )
      SubRes = Residual(ResIdx{i}) ./ WLBLCam;
    else
      SubRes = Residual(ResIdx{i}) ./ WLBLCam(1+i-7:3:NumLBLCam);
    end

    % Remove the scale if it is necessary
    if ~isempty ( Normalization );
      SubRes = SubRes * Normalization(4);
    end
  elseif ( i >= 10 && i <= 12 && NumAngCam > 0 );
    if isscalar ( WAngCam )
      SubRes = Residual(ResIdx{i}) ./ WAngCam;
    else
      SubRes = Residual(ResIdx{i}) ./ WAngCam(1+i-10:3:NumAngCam);
    end
  end
  
  if ( i >= 1 && i <= 4 && NumMatchPoint > 0 ) || ...
     ( i >= 5 && i <= 6 && NumLBLPoint > 0 ) || ...
     ( i >= 7 && i <= 9 && NumLBLCam > 0 ) || ...
     ( i >= 10 && i <= 12 && NumAngCam > 0 );
    m = mean ( SubRes );
    s = std ( SubRes );
    Ix = find ( SubRes < ( m - NumStDev{i} * s ) | SubRes > ( m + NumStDev{i} * s ) );
  end

  if i >= 1 && i <= 4 && NumMatchPoint > 0;
    % Calculate the number of points
    if NumMaxPoints == 0; NumUsedPoints = 4; else NumUsedPoints = NumMaxPoints; end;

    % Pair Indices in SubMosaic
    Ixv = fix ( ( Ix - 1 ) / NumUsedPoints );

    % Only unique elements (each overlapping frame has at least 4 correspondences)
    [I Idxv] = unique ( Ixv, 'rows' );

    % Allocate Space
    Nodes = zeros ( size ( I, 1 ), 7 );
    for j = 1 : size ( I, 1 );
      k1 = MatchIdxTable(I(j)+1, 1);
      k2 = MatchIdxTable(I(j)+1, 2);

      % Compute Maximum Absolute Residual and Point Number in the subset of NumMaxPoint per frame
      Ii = I(j) * NumUsedPoints;
      [mx k] = max ( abs ( SubRes(Ii+1 : Ii+NumUsedPoints) ) );

      Nodes(j,:) = [ j k1 k2 M.nodes(k1).index M.nodes(k2).index k+1 SubRes(Ii+k) ];
    end
    
  elseif ( i >= 5 && i <= 6 && NumLBLPoint > 0 );
    % Compute the values
    Irv = intersect ( SubRes(Ix), SubRes );

    % Allocate Space
    Nodes = zeros ( size ( Ix, 1 ), 3 );
    for j = 1 : size ( Ix, 1 );
      k = LBLPointData(Ix(j),1);
      Nodes(j,:) = [j k M.nodes(k).index, Irv(j) ];
    end

  elseif ( i >= 7 && i <= 9 && NumLBLCam > 0 );
    % Compute the values
    Irv = intersect ( SubRes(Ix), SubRes );

    % Alocate Space
    Nodes = zeros ( size ( Ix, 1 ), 4 );
    for j = 1 : size ( Ix, 1 );
      k = LBLCamData(Ix(j),1);
      Nodes(j,:) = [ j k M.nodes(k).index Irv(j) ];
    end

  elseif ( i >= 10 && i <= 12 && NumAngCam > 0 );
    % Compute the values
    Irv = intersect ( SubRes(Ix), SubRes );

    % Alocate Space
    Nodes = zeros ( size ( Ix, 1 ), 4 );
    for j = 1 : size ( Ix, 1 );
      k = AngCamData(Ix(j),1);
      Nodes(j,:) = [ j k M.nodes(k).index Irv(j) ];
    end    
  else
    Nodes = [];
  end

  if ~isempty ( Nodes );
    disp ( sprintf ( 'Frames with %s residual (%s) too high (outlier?)', ResType{i}, VarName{i} ) );
    disp ( sprintf ( '  Mean  = %f', m ) );
    disp ( sprintf ( '  Stdev = %f', s ) );
    disp ( sprintf ( '  Interval = m - %d * s < m < m + %d * s', NumStDev{i}, NumStDev{i} ) );
  end

  % Display statistics
  if i >= 1 && i <= 4 && NumMatchPoint > 0 && ~isempty ( Nodes );
      disp ( sprintf ( '  Index  | SubMosaic I1 | SubMosaic I2 | Mosaic I1 | Mosaic I2 | PM Index |  Residual (pix.)' ) );
      disp ( sprintf ( '---------+--------------+--------------+-----------+-----------+----------+-----------------' ) );

    % Sort Descending by Residual Magnitude
    NodesAbs = Nodes;
    NodesAbs(:,7) = abs ( NodesAbs(:,7) );
    [NodesS NodesI] = sortrows ( NodesAbs, -7 );
    
    for j = 1 : size ( Nodes, 1 );
      k = NodesI(j);
      disp ( sprintf ( ' %6d       %6d         %6d       %6d      %6d    %6d      %12.4f', ...
        Nodes(k,1), Nodes(k,2), Nodes(k,3), Nodes(k,4), Nodes(k,5), Nodes(k,6), Nodes(k,7) ) );
    end
    sprintf ( '\n' );
    disp ( sprintf ( '\n' ) );

  elseif ( i >= 5 && i <= 6 && NumLBLPoint > 0 && ~isempty ( Nodes ) ) || ...
         ( i >= 7 && i <= 9 && NumLBLCam > 0  && ~isempty ( Nodes ) ) || ...
         ( i >= 10 && i <= 12 && NumAngCam > 0 && ~isempty ( Nodes ) );
      st = sprintf ( '  Index  | SubMosaic | Mosaic |  Residual ' );
      if ( i >= 5 && i <= 6 ); s = [ st '(m)' ];
      elseif ( i >= 7 && i <= 9 ); s = [ st '(pix.)' ];
      else s = [ st '(rad)'];
      end
      disp ( s );
      disp ( sprintf ( '---------+-----------+--------+-----------------' ) );

    % Sort Descending by Residual Magnitude
    NodesAbs = Nodes;
    NodesAbs(:,4) = abs ( NodesAbs(:,4) );
    [NodesS NodesI] = sortrows ( NodesAbs, -4 );

    for j = 1 : size ( Nodes, 1 );
      k = NodesI(j);
      disp ( sprintf ( ' %6d     %6d     %6d   %12.4f', ...
        Nodes(k,1), Nodes(k,2), Nodes(k,3), Nodes(k,4) ) );
    end
    disp ( ' ' );
  end
end


% --[ Plot Residual ]------------------------------------------------------

if DoPlotResidual;
  % Create figures to plot residuals
  [ ResMatchPointFig ResLBLPointFig ResLBLCamFig ResAngCamFig ] = ...
      CreateResidualFigures ( NumMatchPoint, NumLBLPoint, NumLBLCam, NumAngCam );

  % Remove the scale if necessary to the LBL => Divide weight
  if ~isempty ( Normalization );
    WLBLCamS = WLBLCam / Normalization(4);
    WLBLPointS = WLBLPoint / Normalization(4);
  else
    WLBLCamS = WLBLCam;
    WLBLPointS = WLBLPoint;
  end

  % Plot residual
  PlotResidual ( Residual, WMatchPoint, WLBLPointS, WLBLCamS, WAngCam, ...
                 ResMatchPointFig, ResLBLPointFig, ResLBLCamFig, ResAngCamFig, ...
                 NumMatchPoint, NumLBLPoint, NumLBLCam, NumAngCam );

  % Plot Residuals Sorted
  if DoPlotSortedResiduals;
    disp ( 'Plotting sorted evolution...' );
    
    % Plot residual
    if NumMatchPoint > 0;
      SubResMP = [ Residual(ResIdx{1})./WMatchPoint Residual(ResIdx{2})./WMatchPoint Residual(ResIdx{3})./WMatchPoint Residual(ResIdx{1})./WMatchPoint];
      SubResSrtMP = sort ( abs(SubResMP) );
      ResSrtMatchPointFig = figure;
      s = 'Sorted Match-Point Absolute Residual (pixels)'; set ( ResSrtMatchPointFig, 'Name', s ); title ( gca ( ResSrtMatchPointFig ), s );
      hold on;
      plot ( SubResSrtMP );
      legend ( 'x', 'y', 'x''', 'y''', -1 );
    end
    if NumLBLPoint > 0;
      SubResLBLPoint = [ Residual(ResIdx{5})./WLBLPoingM Residual(ResIdx{6})./WLBLPoingM ];

      % Remove the scale if it is necessary
      if ~isempty ( Normalization );
        SubResLBLPoint = SubResLBLPoint * Normalization(4);
      end

      SubResSrtLBLPoint = sort ( abs ( SubResLBLPoint ) );
      ResSrtLBLPointFig = figure;
      s = 'Sorted LBL-Point Absolute Residual (pixels)'; set ( ResSrtLBLPointFig, 'Name', s ); title ( gca ( ResSrtLBLPointFig ), s );
      hold on;
      plot ( SubResSrtLBLPoint );
      legend ( 'X', 'Y', -1 );
    end
    if NumLBLCam > 0;
      if isscalar ( WLBLCam )
        SubResLBLCam = [ Residual(ResIdx{7}) Residual(ResIdx{8}) Residual(ResIdx{9}) ] ./ WLBLCam;
      else
        SubResLBLCam = [ Residual(ResIdx{7})./WLBLCam(1:3:NumLBLCam) Residual(ResIdx{8})./WLBLCam(2:3:NumLBLCam) Residual(ResIdx{9})./WLBLCam(3:3:NumLBLCam) ];
      end

      % Remove the scale if it is necessary
      if ~isempty ( Normalization );
        SubResLBLCam = SubResLBLCam * Normalization(4);
      end
    
      SubResSrtLBLCam = sort ( abs ( SubResLBLCam ) );
      ResSrtLBLCamFig = figure;
      s = 'Sorted LBL-Camera Absolute Residual (m)'; set ( ResSrtLBLCamFig, 'Name', s ); title ( gca ( ResSrtLBLCamFig ), s );
      hold on;
      plot ( SubResSrtLBLCam );
      legend ( 'X', 'Y', 'Z', -1 );      
    end
    if NumAngCam > 0;
      if isscalar ( WAngCam )
        SubResAngCam = [ Residual(ResIdx{10}) Residual(ResIdx{11}) Residual(ResIdx{12}) ] ./ WAngCam;
      else
        SubResAngCam = [ Residual(ResIdx{10})./WAngCam(1:3:NumAngCam) Residual(ResIdx{11})./WAngCam(2:3:NumAngCam) Residual(ResIdx{12})./WAngCam(3:3:NumAngCam)];
      end

      SubResSrtAngCam = sort ( abs ( SubResAngCam ) );
      ResSrtAngCamFig = figure;
      s = 'Sorted Angular-Camera Absolute Residual (m)'; set ( ResSrtAngCamFig, 'Name', s ); title ( gca ( ResSrtAngCamFig ), s );
      hold on;
      plot ( SubResSrtAngCam );
      legend ( 'Roll', 'Pitch', 'Yaw', -1 );
    end
  end

  % Compute Display Residual History
  if DoPlotResidualHistory;
    disp ( 'Plotting residual evolution...' );

    if NumMatchPoint > 0;
      ResMatchPointx = Residualt(ResIdx{1},:);
      ResMatchPointy = Residualt(ResIdx{2},:);
      ResMatchPointxp = Residualt(ResIdx{3},:);
      ResMatchPointyp = Residualt(ResIdx{4},:);
      ResMatchPoint = [ ResMatchPointx; ResMatchPointy; ResMatchPointxp; ResMatchPointyp ];

      ResEvMatchPointFig = figure; mesh ( ResMatchPoint );
      s = 'Match-Point Residual Evolution (pixels)'; set ( ResEvMatchPointFig, 'Name', s ); title ( gca ( ResEvMatchPointFig ), s );
    end

    if NumLBLPoint > 0;
      ResLBLPointX = Residualt(ResIdx{5},:);
      ResLBLPointY = Residualt(ResIdx{6},:);
      ResLBLPoint = [ ResLBLPointX; ResLBLPointY ];

      ResEvLBLPointFig = figure; mesh ( ResLBLPoint );
      s = 'LBL-Point Residual Evolution (pixels)'; set ( ResEvLBLPointFig, 'Name', s ); title ( gca ( ResEvLBLPointFig ), s );
    end

    if NumLBLCam > 0;
      ResLBLCamX = Residualt(ResIdx{7},:);
      ResLBLCamY = Residualt(ResIdx{8},:);
      ResLBLCamZ = Residualt(ResIdx{9},:);
      ResLBLCam = [ ResLBLCamX; ResLBLCamY; ResLBLCamZ ];

      ResEvLBLCamFig = figure; mesh ( ResLBLCam );
      s = 'LBL-Camera Residual Evolution (m)'; set ( ResEvLBLCamFig, 'Name', s ); title ( gca ( ResEvLBLCamFig ), s );
    end

    if NumAngCam > 0;
      ResAngCamX = Residualt(ResIdx{10},:);
      ResAngCamY = Residualt(ResIdx{11},:);
      ResAngCamZ = Residualt(ResIdx{12},:);
      ResAngCam = [ ResAngCamX; ResAngCamY; ResAngCamZ ];

      ResEvAngCamFig = figure; mesh ( ResAngCam );
      s = 'Angular Readings Residual Evolution (rads)'; set ( ResEvAngCamFig, 'Name', s ); title ( gca ( ResEvAngCamFig ), s );
    end
  end
end


% --[ Plot Destination Trajectory and Mosaic ]-----------------------------

if DoPlotFinal2DTrajectory;
  disp ( 'Displaying Final Trajectory...' );
  Final2DTrajectoryFig = figure;
  s = 'Final Trajectory';
  set ( Final2DTrajectoryFig, 'Name', s );
  title ( gca ( Final2DTrajectoryFig ), s );
  axis equal;
  axis ij;
  hold on;
  MosaicTrajectoryAbs ( M, ImgWidth, ImgHeight, -1, -1, [], [], false, false, false );
end

if DoPlotFinal2DMosaic;
  disp ( 'Displaying Final Mosaic...' );
  Final2DMosaicFig = figure;
  s = 'Final Mosaic Preview';
  set ( Final2DMosaicFig, 'Name', s );
  title ( gca ( Final2DMosaicFig ), s );
  axis equal;
  axis ij;
  hold on;
  MosaicPreviewAbs ( M, ImgWidth, ImgHeight, -1, -1, 1, [], true );
end


% --[ Save the Optimized final Mosaic ]------------------------------------

% Convert poses to original frames (Heading, X, Y, Z from World, and R, P, Y from vehicle.
disp ( 'Converting back to source frames...' );
M = mosaicCalcPoseFromMosaic ( M, RTs_Opt );

disp ( 'Calculating mosaic size...' );
M_Res = mosaic_calc_size ( M );

disp ( sprintf ( 'Saving Final Optimized Mosaic "%s"...', OptimizedMosaicFilePath ) );
gmml_save ( OptimizedMosaicFilePath, M_Res );


% --[ Draw Mosaics ]-------------------------------------------------------

if DoDraw2DMosaics;
  disp ( 'Drawing mosaics...' );
  mosaic_draw ( M0 );
  mosaic_draw ( M_Res );
end


% --[ Save 3D Trajectories ]-----------------------------------------------

% Compute And Display 3D Camera Trajectory
if DoPlot3DTrajectory;

  % Scale Factor to convert to pixels (allow
  ScaleXYZ = [1/M0.init.pixel_size.x  1/M0.init.pixel_size.y 1/M0.init.pixel_size.x];

  if DoPlot3DTrajectorySource;
    if DoPlot3DTrajectorySourceFigure;
      TrajectoryFig = figure;
      s = sprintf ( 'Source 3D Trajectory' ); title ( s );
      title ( s ); set ( gcf, 'Name', s );
      TrajectoryAxesHandle = get ( TrajectoryFig, 'CurrentAxes' );
      set ( TrajectoryAxesHandle, 'XGrid','on', 'YGrid', 'on', 'ZGrid', 'on' );
      xlabel 'X Direction'; ylabel 'Y Direction'; zlabel 'Z Direction';
      whitebg ( TrajectoryFig, 'white' );
      hold on;
      view ( 3 );
    end

    disp ( sprintf ( 'Saving Initial 3D Trajectory to "%s"...', Trajectory3DFilePath ) );
    
    Ws = warning ( 'off', 'MATLAB:msbaPlotTrajectory:Input' );
    
    % Convert Cameras to 7xNumCameras Matrix
    Cameras = reshape ( xt(1,:), 7, NumCameras );

    % Normalize the resulting quaternions
    for i = 1 : size ( Cameras, 2 );
      q = Cameras(1:4, i);
      Cameras(1:4, i) = q / norm ( q );
      % Conjugate => Inverse rotation from cRw to wRc
      Cameras(2:4, i) = -Cameras(2:4, i);
    end
    SclCameras(1:4,:) = Cameras(1:4,:);
    % Scale X, Y and Z
    SclCameras(5,:) = Cameras(5,:) * ScaleXYZ(1);
    SclCameras(6,:) = Cameras(6,:) * ScaleXYZ(2);
    SclCameras(7,:) = Cameras(7,:) * ScaleXYZ(3);

    % Save 3D Optimized Trajectory in an Open Inventor File
    FIDTrajectory3D = oiCreate ( Trajectory3DFilePath );
    % Plot to the figure or not
    if DoPlot3DTrajectorySourceFigure; FFID = FIDTrajectory3D; else FFID = [FIDTrajectory3D 0]; end;

%    % Save All Transects idependently
%    for i = 1:size ( EndFloatNodesIdx, 2 );
%      TransectIdx = EndFloatNodesIdx(i):StartFloatNodesIdx(i);
%      TransectCodes = EndFloatNodes(i):StartFloatNodes(i);
%      PtCameras = SclCameras(:,TransectIdx);
%      msbaPlotTrajectory ( FFID, K, PtCameras, [], ...
%                           [], [], ...
%                           CCDWidth, [ImgHeight ImgWidth], ...
%                           TransectCodes, 'g', [] );
%    end
    msbaPlotTrajectory ( FFID, K, SclCameras, [], ...
                         [], [], ...
                         CCDWidth, [ImgHeight ImgWidth], ...
                         [1:size(SclCameras,2)], 'g', [] );
                         
    % Plot the mosaic
    oifMosaicPreviewAbs ( FFID, M0, ImgWidth, ImgHeight, -1, -1, 1 );
    
    % Close File
    fclose ( FIDTrajectory3D );

    warning ( Ws );
  end

  if DoPlot3DTrajectoryOptimized;
    if DoPlot3DTrajectoryOptimizedFigure;
      TrajectoryOptimizedFig = figure;
      s = sprintf ( 'Optimized 3D Trajectory' ); title ( s );
      title ( s ); set ( gcf, 'Name', s );
      TrajectoryOptimizedAxesHandle = get ( TrajectoryOptimizedFig, 'CurrentAxes' );
      set ( TrajectoryOptimizedAxesHandle, 'XGrid','on', 'YGrid', 'on', 'ZGrid', 'on' );
      xlabel 'X Direction'; ylabel 'Y Direction'; zlabel 'Z Direction';
      whitebg ( TrajectoryOptimizedFig, 'white' );
      hold on;
      view ( 3 );
    end

    disp ( sprintf ( 'Saving Optimized 3D Trajectory to "%s"...', OptTrajectory3DFilePath ) );

    Ws = warning ( 'off', 'MATLAB:msbaPlotTrajectory:Input' );

    % Convert Cameras to 7xNumCameras Matrix
    OptCameras = reshape ( x, 7, NumCameras );

    % Normalize the resulting quaternions
    for i = 1 : size ( OptCameras, 2 );
      q = OptCameras(1:4, i);
      OptCameras(1:4, i) = q / norm ( q );
      % Conjugate => Inverse rotation from cRw to wRc
      OptCameras(2:4, i) = -OptCameras(2:4, i);
    end
    % Scale X, Y and Z
    SclOptCameras(1:4,:) = OptCameras(1:4,:);
    SclOptCameras(5,:) = OptCameras(5,:) * ScaleXYZ(1);
    SclOptCameras(6,:) = OptCameras(6,:) * ScaleXYZ(2);
    SclOptCameras(7,:) = OptCameras(7,:) * ScaleXYZ(3);

    % Save 3D Optimized Trajectory in an Open Inventor File
    FIDTrajectory3D = oiCreate ( OptTrajectory3DFilePath );
    % Plot to the figure or not
    if DoPlot3DTrajectoryOptimizedFigure; FFID = FIDTrajectory3D; else FFID = [FIDTrajectory3D 0]; end;

%    % Save All Transects idependently
%    for i = 1:size ( EndFloatNodesIdx, 2 );
%      TransectIdx = EndFloatNodesIdx(i):StartFloatNodesIdx(i);
%      TransectCodes = EndFloatNodes(i):StartFloatNodes(i);
%      PtCameras = SclOptCameras(:,TransectIdx);
%      msbaPlotTrajectory ( FFID, K, PtCameras, [], ...
%                           [], [], ...
%                           CCDWidth, [ImgHeight ImgWidth], ...
%                           TransectCodes, 'g', [] );
%    end
      
    msbaPlotTrajectory ( FFID, K, SclOptCameras, [], ...
                         [], [], ...
                         CCDWidth, [ImgHeight ImgWidth], ...
                         [1:size(SclOptCameras,2)], 'g', [] );

    % Plot the mosaic
    oifMosaicPreviewAbs ( FFID, M, ImgWidth, ImgHeight, -1, -1, 1 );

    % Close File
    fclose ( FIDTrajectory3D );
    
    warning ( Ws );
  end
end


% --[ Save Figures ]-------------------------------------------------------

% Save Figures
if DoSaveFigures;
  % Initial 2D Trajectories and Mosaics
  FigureHandles{1} = 'Initial2DMosaicFig';
  FigureHandles{2} = 'Initial2DTrajectoryFig';
  % Residuals
  FigureHandles{3} = 'ResMatchPointFig';
  FigureHandles{4} = 'ResLBLPointFig';
  FigureHandles{5} = 'ResLBLCamFig';
  FigureHandles{6} = 'ResAngCamFig';
  % Sorted Residuals
  FigureHandles{7} = 'ResSrtMatchPointFig';
  FigureHandles{8} = 'ResSrtLBLPointFig';
  FigureHandles{9} = 'ResSrtLBLCamFig';
  FigureHandles{10} = 'ResSrtAngCamFig';
  % Residual Evolution
  FigureHandles{11} = 'ResEvMatchPointFig';
  FigureHandles{12} = 'ResEvLBLPointFig';
  FigureHandles{13} = 'ResEvLBLCamFig';
  FigureHandles{14} = 'ResEvAngCamFig';
  % Final 2D Trajectories and Mosaics
  FigureHandles{15} = 'Final2DMosaicFig';
  FigureHandles{16} = 'Final2DTrajectoryFig';
  % 3D Trajectories
  FigureHandles{17} = 'TrajectoryOptimizedFig';
  FigureHandles{18} = 'TrajectoryFig';

  disp ( sprintf ( 'Saving Figures to path "%s/"...', FiguresFilePath ) );
  for i = 1 : size ( FigureHandles, 2 );
    j = char ( FigureHandles{i} );
    if exist ( j, 'var' );
      % Test that is not an Empty Figure Handle
      FigHandle = eval ( j );
      if ~isempty ( FigHandle );
        FigName = [ get(FigHandle,'Name') '.fig' ];
        disp ( sprintf ( '  %d. %s => "%s"', i, j, FigName ) );
        saveas ( FigHandle, [ FiguresFilePath '/' FigName ], 'fig' );
      else
        disp ( sprintf ( '  %d. %s => Skiped! Empty Handle!', i, j ) );        
      end
    else
      disp ( sprintf ( '  %d. %s => Doesn''t exist!', i, j ) );        
    end
  end
end

% --[ Save Workspace ]-----------------------------------------------------

disp ( sprintf ( 'Saving Data (Residual, poses, etc...) "%s"...', ResultingDataFilePath ) );
save ( ResultingDataFilePath );

