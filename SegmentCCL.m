%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Connected Component Labeling.
%
%  File          : SegmentCCL.m
%  Date          : 15/03/2006 - 15/03/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  SegmentCCL Connected Component Labeling using a Connection table between
%             each i, j objects.
%
%      Equivalences = SegmentCCL ( Connected )
%
%     Input Parameters:
%      Connected: NxN table indicating the connection between i, j using a
%                 non-zero value. Only the upper-triangular matrix will be
%                 used.
%
%     Output Parameters:
%      Equivalences: 1xN vector indicating at the position i wich is the
%                    connected group. This table must be relabeled using
%                    sequential values for the groups.
%

function Equivalences = SegmentCCL ( Connected )
  % Test the input parameters
  error ( nargchk ( 1, 1, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Number of Segments14
  [NumSegments, N] = size ( Connected );
  if NumSegments < 1 || NumSegments ~= N;
    error ( 'MATLAB:SegmentCCL:Input', 'The Connected table must be a NxN table!' );
  end

  % Equivalence Table
  InitialEquivalences = 1:NumSegments;

  % Repeat while changes 
  Changes = true;
  while Changes;
    Changes = false;
    % Scan Upper-Triangular Matrix.
    for i = 1 : NumSegments;
      for j = i : NumSegments;
        % If there is connection between i, j objects
        if Connected(i,j) ~= 0;
          % Compute the NewLabel for the group as the minimum
          % number i, j, and their equivalences in the Equivalence Table
          NewLabel = min ( [i j InitialEquivalences(i) InitialEquivalences(j)] );

          % If the new label is not written in the Equivalence Table, do it.
          if NewLabel ~= InitialEquivalences(i);
            InitialEquivalences(i) = NewLabel;
            Changes = true;
          end
          if NewLabel ~= InitialEquivalences(j);
            InitialEquivalences(j) = NewLabel;
            Changes = true;
          end
        end
      end
    end
  end

  % The Equivalence table can contain non-consecutive Labels -> Re-Label
  Equivalences = zeros ( 1, NumSegments );
  % Map between Old values and the New one.
  OldToNew = zeros ( 1, max ( InitialEquivalences ) );
  GroupCounter = 0;
  LastLabel = 0;
  for i = 1 : NumSegments;
    % When the label increases in respect to the biggest found, indicates
    % a new group
    if InitialEquivalences(i) > LastLabel
      % Next new Label
      GroupCounter = GroupCounter + 1;
      % New biggest 
      LastLabel = InitialEquivalences(i);
      % Map the Old to the new for further Re-Labeling
      OldToNew(InitialEquivalences(i)) = GroupCounter;
      % Write the Re-Labeled
      Equivalences(i) = GroupCounter;
    else
      % Re-Label using the new assignement
      Equivalences(i) = OldToNew(InitialEquivalences(i));
    end
  end

  % Check whether the Re-Labeling is ok or not!
  % If the algorithm is right, this test cannot fail!
  MaxGroups = max ( Equivalences );
  if GroupCounter ~= MaxGroups;
    error ( 'MATLAB:SegmentCCL:Input', ...
            sprintf ( 'There are %d different groups and the maximum in the map is %d!', GroupCounter, MaxGroups ) );
  end
end
