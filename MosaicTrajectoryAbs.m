%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Given a GMML structure plot the trajectory.
%
%  File          : MosaicTrajectoryAbs.m
%  Date          : 05/03/2006 - 15/03/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  MosaicTrajectoryAbs Given a GMML structure plot the trajectory marking
%                      in red the nodes in the list.
%
%      MosaicTrajectoryAbs ( M, ImgWidth, ImgHeight, First, Last, List )
%
%     Input Parameters:
%      M: GMML Mosaic Structure.
%      ImgWidth: Image Width.
%      ImgHeight: Image Height.
%      First: Initial Image.
%      Last: Final Image
%      CrossingList: Crossing Nodes to be marked in red.
%      SegmentList: List of nodes to mark in blue.
%      PlotCrossingsNumbers: True to plot the numbers at each crossing.
%      PlotSegmentNumbers: True to plot the numbers in the SegmentList.
%      PlotSegmentLines: True to plot the lines marking the starting and
%                        end point for any segment in the SegmentList.
%

function MosaicTrajectoryAbs ( M, ImgWidth, ImgHeight, First, Last, ...
                               CrossingsList, SegmentList, ...
                               PlotCrossingsNumbers, PlotSegmentNumbers, ...
                               PlotSegmentLines )
  % Test the input parameters
  error ( nargchk ( 10, 10, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  N = size ( M.nodes, 2 );
  % If they are out of range, take the limits
  if First < 1 || First > N; First = 1; end
  if Last > N || Last < First; Last = N; end;

  % Center coordinates
  pc = [ImgWidth/2; ImgHeight/2; 1];

  % To detect "Errors" in the mosaic
  MultipleChild = 0;  % One node has more than one child
  ParentError = 0;    % The parent of a node n is not the node n-1
  EmptyNodes = 0;
  MarkerColor = [0 0 0.75];

  % Scan the Nodes
  for i = First : Last;
    % Homography
    H = M.nodes(i).homo.matrix;
    % Compute the warping
    c = H * pc;  c = c ./ c(3,1);

    NEdges = length ( M.nodes(i).edges );
    if NEdges == 1;
      if M.nodes(i).edges(1).index == (i - 1);
        % Plot the position avoiding the first iteration
        if exist ( 'c_ant', 'var' );
          plot ( [c_ant(1) c(1)], [c_ant(2) c(2)], ...
                 'Color', [0 0 0], 'Marker', 'o', 'MarkerSize', 4, ...
                 'MarkerFaceColor', MarkerColor, 'MarkerEdgeColor', 'none' );
        end
      else
        % Edge is not pointing to the previous node
        ParentError = ParentError + 1;
      end
    elseif NEdges == 0;
      EmptyNodes = EmptyNodes + 1;
      
      plot ( [c(1) c(1)], [c(2) c(2)], ...
             'Color', [0 0 0], 'Marker', 'o', 'MarkerSize', 4, ...
             'MarkerFaceColor', MarkerColor, 'MarkerEdgeColor', 'none' );

    else
      % More than one edge
      MultipleChild = MultipleChild + 1;
    end

    % Store last position
    c_ant = c;
  end
  
  for j = 1:2;
    % Use the Right List and parameters
    if j == 1;
      List = CrossingsList;
      Color = [1 0 0];
      TextColor = [0 1 0];
      Marker = 'x';
      PlotNum = PlotCrossingsNumbers;
      PlotLines = true;
    elseif j == 2;
      List = SegmentList;
      Color = [0 0 1];
      TextColor = [0 0 0];
      Marker = '+';
      PlotNum = PlotSegmentNumbers;
      PlotLines = PlotSegmentLines;
    else
       warning ( 'MATLAB:MosaicTrajectoryAbs:Input', 'Internal Error!' );
    end

    % Plot the crossings
    for i = 1 : size ( List, 2 );
      Idx1 = List(1, i);
      Idx2 = List(2, i);
      if Idx1 >= First && Idx1 <= Last && Idx2 >= First && Idx2 <= Last;
        % First Homography
        H1 = M.nodes(Idx1).homo.matrix;
        H2 = M.nodes(Idx2).homo.matrix;
        % Compute the warping
        c1 = H1 * pc;  c1 = c1 ./ c1(3,1);
        c2 = H2 * pc;  c2 = c2 ./ c2(3,1);

        % If there is a crossing: plot in red
        if PlotLines;
          line ( [c1(1) c2(1)], [c1(2) c2(2)], 'Color', Color, 'Marker', Marker );
        end

        if PlotNum;
          SIdx1 = sprintf ( '%d', M.nodes(Idx1).index );
          SIdx2 = sprintf ( '%d', M.nodes(Idx2).index );
          text ( c1(1), c1(2), SIdx1, 'Color', TextColor );
          text ( c2(1), c2(2), SIdx2, 'Color', TextColor );
        end
      end
    end
  end

  % Show warnings
  if EmptyNodes > 0;
    warning ( 'MATLAB:MosaicTrajectoryAbs:Input', sprintf ( 'There are %d nodes without any child!', EmptyNodes ) );
  end
  if MultipleChild > 0;
    warning ( 'MATLAB:MosaicTrajectoryAbs:Input', sprintf ( 'There are %d nodes with multiple children!', MultipleChild ) );
  end
  if ParentError > 0;
    warning ( 'MATLAB:MosaicTrajectoryAbs:Input', sprintf ( 'There are %d nodes that the node parent n is not the node n-1!', ParentError ) );
  end
end
