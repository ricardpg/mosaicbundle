%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the input data for the Bundle Adjustement
%
%  File          : ConvertDataToBundle.m
%  Date          : 03/04/2006 - 07/08/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ConvertDataToBundle Using the GMML mosaic structure and the list of LBL
%                      readings, the function computes the data structures
%                      needed for the bundle.
%
%     Input Parameters:
%      M: GMML structure representing the mosaic.
%      ListPoints: List of nodes in M that there are LBL readings for the
%                  central point of the image.
%      ListCamera: List of nodes in M that there are LBL readings for the
%                  camera position.
%      ListAngular: List of nodes in M that there are Angular readings for
%                   the camera position.
%      NumOverlaps: Number of edges taken from any node. Use zero to take
%                   all.
%      NumPoints: Zero to compute the 4 corners using the estimated
%                 homography or the number of point-matches that should be
%                 included. If it is not given the corners are used
%
%     Output Parameters:
%      RTs: Vector that, for each node in the mosaic, contains an structure with
%           the C_R_W (cRw) rotation matrix and the translation vector W_T_C
%           (wTc).
%           The C_R_W matrix is the matrix than can transform coordinates in
%           the 3D World Frame to the 3D Camera Frame and the vector W_T_C is
%           the translation of the 3D Camera Frame from the 3D World Frame.
%      MatchIdxTable: kx2 matrix containing k overlapped image frames. The
%                     matrix will contain for each reading (row): Frame
%                     Index 1 and Frame Index 2.
%      MatchPointData: kxNumPointsx4 matrix containing k sets of image
%                      correspondences (according to the frames in
%                      MatchIdxTable). For each set, there are NumPoints
%                      points that are matched between the pair of
%                      overlapping frames. For each set and each match,
%                      there are 4 stored values: feature x, y (in image 1)
%                      and correspondence x', y' (in image 2).
%      LBLPointData: lx5 matrix containing l imaged points and it's 2D LBL
%                    coordinate. The matrix will contain for each reading
%                    (row): Frame Index, x, y, LBL X and LBL Y.
%      LBLCamData: tx4 matrix containing t Camera LBL positions. The matrix
%                  will contain for each reading (row): Frame Index, LBL X,
%                  LBL Y and LBL Z.
%      AngCamData: ax4 matrix containing a angular readings. The matrix
%                  will contain for each reading (row): Frame Index, Roll,
%                  Pitch and Yaw in respect to the 3D Mosaic Frame.
%

function [RTs, MatchIdxTable, MatchPointData, LBLPointData, LBLCamData, AngCamData] = ...
      ConvertDataToBundle ( M, ImgPath, ListPoint, ListCamera, ListAngular, NumOverlaps, NumPoints )
  % Test the input parameters
  error ( nargchk ( 6, 7, nargin ) );
  error ( nargoutchk ( 6, 6, nargout ) );

  % Read the first Image and assume that all are the same size.
  I = imread ( [ImgPath M.nodes(1).image] );
  [h w] = size ( I );

  % Image Corners and the Center
  XTL = [0 0 1]';
  XTR = [w 0 1]';
  XBL = [0 h 1]';
  XBR = [w h 1]';
  XCE = [w/2 h/2 1]';

  % Point used in the Point-Match
%  p = [XTL XTR XBL XBR XCE];
  p = [XTL XTR XBL XBR];

  if ( nargin < 7 ) || ( nargin >= 7 && NumPoints == 0 );
    NumPoints = size ( p, 2 );
    UseCorners = true;
  else
    UseCorners = false;
  end
  
  % Count the number of edges in the mosaic to avoid growing arrays
  NumNodes = length ( M.nodes );
  MaxEdges = 0;
  for node = 1 : NumNodes;
    MaxEdges = MaxEdges + length ( M.nodes(node).edges );
  end

  % Allocate the MatchIdxTable to don't grow it
  MatchIdxTable = zeros ( MaxEdges, 2 );
  % Allocate the RTs table to don't grow it
  RTs_Struc.cRw = zeros ( 3, 3 );
  RTs_Struc.wTc = zeros ( 3, 1 );
  RTs = repmat ( RTs_Struc, 1, NumNodes );

  % Initial Fixed Motions
  % Argo II 3D Camera Frame pose w.r.t. 3D Vehicle Frame
  V_Pose_C = [ 0 0 0 0 0 pi/2 ];
  % Rigid Camera Frame {C} Motion w.r.t. the 3D Vehicle Frame {V}
  V_X_C     = V_Pose_C(1);
  V_Y_C     = V_Pose_C(2);
  V_Z_C     = V_Pose_C(3);
  V_Roll_C  = V_Pose_C(4);
  V_Pitch_C = V_Pose_C(5);
  V_Yaw_C   = V_Pose_C(6);

  % Compute the Transformation 3D Mosaic Frame -> 3D World Frame: W_X = W_R_M * M_X + W_T_M
  % Mosaic Rotation w.r.t. 3D World Frame
  W_R_M = RotX ( pi );
  % Mosaic Translation w.r.t. 3D World Frame
  % (The mosaic is translated according to the X, Y origin but not in moved in Z).
  W_T_M = [ M.init.mosaic_origin.x; M.init.mosaic_origin.y; 0 ];

  % Compute Inverse Transformation 3D Wold Frame -> 3D Mosaic Frame: M_X = M_R_W * W_X + M_T_W
  M_R_W = W_R_M';
  M_T_W = -M_R_W * W_T_M;

  % Compute the Transformation 3D Camera Frame -> 3D Vehicle Frame: V_X = V_R_C * C_X + V_T_C
  % Camera Rotation w.r.t. 3D Vehicle Frame
  V_R_C = RotX ( V_Roll_C ) * RotY ( V_Pitch_C ) * RotZ ( V_Yaw_C );
  % Camera Translation w.r.t. 3D Vehicle Frame
  V_T_C = [ V_X_C; V_Y_C; V_Z_C ];

  % Don't use the Roll and Pitch
  Projective = 1;

  % Look for the maximum node in the LBL Readings List for Central Image Point
  MaxPointNodes = max ( ListPoint );
  % Allocate the Map from Node to
  PointNodeToIndex = zeros ( 1, MaxPointNodes );
  % Compute the map LBL Reading Node to the Mosaic Node
  for i = 1 : NumNodes;
    PointNodeToIndex(M.nodes(i).index) = i;
  end

  NumLBLPoints = length ( find ( PointNodeToIndex(ListPoint) ) );
  LBLPointData = zeros ( NumLBLPoints, 5 );
  l = 0;

  % Look for the maximum node in the LBL Readings List for the Camera Position
  MaxCameraNodes = max ( ListCamera );
  % Allocate the Map from Node to
  CameraNodeToIndex = zeros ( 1, MaxCameraNodes );
  % Compute the map LBL Reading Node to the Mosaic Node
  for i = 1 : NumNodes;
    CameraNodeToIndex(M.nodes(i).index) = i;
  end
  
  NumLBLCameras = length ( find ( CameraNodeToIndex(ListCamera) ) );
  LBLCamData = zeros ( NumLBLCameras, 4 );
  t = 0;

  % Look for the maximum node in the Angular Readings List for the Camera Position
  MaxAngularNodes = max ( ListAngular );
  % Allocate the Map from Node to
  AngularNodeToIndex = zeros ( 1, MaxAngularNodes );
  % Compute the map LBL Reading Node to the Mosaic Node
  for i = 1 : NumNodes;
    AngularNodeToIndex(M.nodes(i).index) = i;
  end

  NumAngCamNodes = length ( find ( AngularNodeToIndex(ListAngular) ) );
  AngCamData = zeros ( NumAngCamNodes, 4 );
  a = 0;
 
  % If not using the corners, compute the minimum number of Point-Match Pairs
  if ~UseCorners
    disp ( 'Looking for the minimum number of point-matches...' );

    % Look for the minimum number of correspondences
    UseNPoints = Inf;
    for node = 1 : NumNodes;
      for edge = 1 : length ( M.nodes(node).edges );
%        disp ( sprintf ( '  Loading GPMML "%s"', M.nodes(node).edges(edge).pointmatches ) );
        PM = gpmml_load ( M.nodes(node).edges(edge).pointmatches );
        NumP = size ( PM, 1 );
        if NumP < UseNPoints; UseNPoints = NumP; end;
      end
      % Debug information
      if mod ( node, 500 ) == 0; 
        disp ( sprintf ( '  Current node checking %d', node ) );
      end
    end
    % If the minimum is smaller that the asked number, use that minimum
    if UseNPoints < NumPoints; NumPoints = UseNPoints; end;

    disp ( sprintf ( '  %d correspondences per each pair will be finally used!', NumPoints ) );
  end

  disp ( 'Writting output tables...' );

  % Allocate the MatchPointData  table to don't grow it
  MatchPointData = zeros ( MaxEdges, NumPoints, 4 );

  % Build MatchPointData and Initial Homographies
  k = 0;
  for node = 1 : NumNodes;
     NumEdges = length ( M.nodes(node).edges );
     if NumEdges > NumOverlaps && NumOverlaps ~= 0;
       NodeIndices = zeros ( NumEdges, 2 );
       for j = 1 : NumEdges;
         NodeIndices(j,:) = [ j M.nodes(node).edges(j).index; ];
       end
       NodeIndicesSort = sortrows ( NodeIndices, 2 );
       EdgeIndicesIdx = round ( 1:(NumEdges-1)/(NumOverlaps-1):NumEdges );
       EdgeIndices = NodeIndicesSort(EdgeIndicesIdx,1);
     else
       % Take all of them
       EdgeIndices = 1:NumEdges;
     end

    for edge = 1 : length ( EdgeIndices );
      k = k + 1;
      % Table containing the indices for the Current and Reference Frames
      MatchIdxTable(k, 1) = node;                                          % Current
      MatchIdxTable(k, 2) = M.nodes(node).edges(EdgeIndices(edge)).index;  % Reference

      if UseCorners;
        % Compute the Matches in the Edge Pointed Node
        m = M.nodes(node).edges(EdgeIndices(edge)).homo.matrix * p;
        m(1,:) = m(1,:) ./ m(3,:);
        m(2,:) = m(2,:) ./ m(3,:);
        Perm = 1:NumPoints;
      else
        PM = gpmml_load ( M.nodes(node).edges(EdgeIndices(edge)).pointmatches );
        % Get the Points and Matches
        p = PM(:,1:2)';
        m = PM(:,4:5)';

        % Compute the random sequence and take the first NumPoint
%        Perm = randperm ( NumPoints );
%        Perm = Perm(1:NumPoints);
        % Elements closer to the edges of the image
        [Dummy Perm] = SpreadPoints ( [p', m'], NumPoints, w, h );
      end

      % Point-Match structure
      MatchPointData(k,1:NumPoints,1) = p(1,Perm);  % Current Image
      MatchPointData(k,1:NumPoints,2) = p(2,Perm);
      MatchPointData(k,1:NumPoints,3) = m(1,Perm);  % Reference Image
      MatchPointData(k,1:NumPoints,4) = m(2,Perm);
    end    

    % The Yaw angle
    Yaw = M.nodes(node).pose.matrix(6);

    % Compute projective motion or not
    if ~Projective;
      Roll = 0;
      Pitch = 0;
    else
      Roll = M.nodes(node).pose.matrix(4);
      Pitch = M.nodes(node).pose.matrix(5);
    end

%    % Vehicle Rotation w.r.t. 3D World Frame (Computed allways in mobile axis: post-multiplication)
%    %  Rotation in Z (pi/2): Point X axis of the vehicle to the north to use the Yaw.
%    %  Rotation in Z (Yaw): Yaw of the vehicle in the world frame.
%    %  Rotation in X (Pi): Point Z axis of the vehicle looking down.
%    %  Rotation in Y (Pitch): Pitch in the vehicle frame.
%    %  Rotation in X (Roll): Roll in the vehicle frame.
%    W_R_V_old = RotZ ( pi / 2 ) * RotZ ( -Yaw ) * RotX ( pi ) * RotY ( Pitch ) * RotX ( Roll );
    
    % Vehicle Rotation w.r.t. 3D World Frame (Computed allways in mobile axis: post-multiplication)
    %  Rotation in X (pi): Point the Z axis down (vehicle frame is like that).
    %                      As yaw is a clockwise angle, now is rotation in the right direction.
    %  Rotation in Z (Yaw-pi/2): First rotation with respect to the heading angle. As zero of angles is
    %                       "x" axis, but Yaw=0 is North (y axis), angle must be corrected with pi/2 rads.
    %                       Note that after first rotation in x, this correction is negative.
    %  Rotation in Y (Pitch): Rotation of the vehicle in Y direction according to the gravity vector.
    %  Rotation in X (Roll): Rotation of the vehicle in X direction according to the gravity vector.
    W_R_V = RotX ( pi ) * RotZ ( Yaw - pi/2 ) * RotY ( Pitch ) * RotX ( Roll );
%    Err = W_R_V - W_R_V_old;
%    NErr = norm ( Err, 'fro' );
%    if NErr > 1e-15;
%      disp ( sprintf ( 'Ups! Error converting frame %d: %e', node, NErr ) );
%    end
    
    % Vehicle Translation w.r.t. 3D World Frame
    W_T_V = [ M.nodes(node).pose.matrix(1); M.nodes(node).pose.matrix(2); M.nodes(node).pose.matrix(3) ];

    % Convert a Pose (V_T_C, V_R_C) w.r.t. 3D Vehicle Frame to the 3D World Frame
    % This new pose will also be the Transformation 3D Camera Frame -> 3D World Frame: W_X = W_R_C * C_X + W_T_C
    W_R_C = W_R_V * V_R_C;
    W_T_C = W_R_V * V_T_C + W_T_V;

    % Convert a Pose (W_T_C, W_R_C) w.r.t. 3D Camera Frame to the 3D Mosaic Frame
    % This new pose will also be the Trasformation 3D Camera Frame -> 3D Mosaic Frame: M_X = M_R_C * C_X + M_T_C
    M_R_C = M_R_W * W_R_C;
    M_T_C = M_R_W * W_T_C + M_T_W;

    % Store the information
    RTs(node).cRw = M_R_C';
    RTs(node).wTc = M_T_C;

    
    % LBL Point Readings
    if ~isempty ( find ( ListPoint == M.nodes(node).index, 1 ) );
      l = l + 1;
      LBLPointData(l,1) = node;
      LBLPointData(l,2) = XCE(1,1);  % x
      LBLPointData(l,3) = XCE(2,1);  % y
      LBLPointData(l,4) = M_T_C(1);  % X
      LBLPointData(l,5) = M_T_C(2);  % Y
    end

    % LBL Camera Readings
    if ~isempty ( find ( ListCamera == M.nodes(node).index, 1 ) );
      t = t + 1;
      LBLCamData(t,1) = node;
      LBLCamData(t,2) = M_T_C(1);  % X
      LBLCamData(t,3) = M_T_C(2);  % Y
      LBLCamData(t,4) = M_T_C(3);  % Z
    end

    % Angular Camera Readings
    if ~isempty ( find ( ListAngular == M.nodes(node).index, 1 ) );
      a = a + 1;
      AngCamData(a,1) = node;
%      AngCamData(a,2) = M.nodes(node).pose.matrix(4);  % Roll
%      AngCamData(a,3) = M.nodes(node).pose.matrix(5);  % Pitch
%      AngCamData(a,4) = M.nodes(node).pose.matrix(6);  % Yaw

       [R, P, Y] = RotMat2RPY ( M_R_C' );
       AngCamData(a,2) = R;    % Roll
       AngCamData(a,3) = P;    % Pitch
       AngCamData(a,4) = Y;    % Yaw
    end
    
    % Debug information
    if mod ( node, 1000 ) == 0; 
      disp ( sprintf ( '  Current node conversion %d', node ) );
    end
  end

  % Cut the lists
  MatchIdxTable = MatchIdxTable(1:k,:);
  MatchPointData = MatchPointData(1:k,:,:);
end
