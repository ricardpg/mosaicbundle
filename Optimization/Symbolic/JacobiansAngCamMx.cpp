//
// C++ Automatically generated file using 'CostSymbolicAngCam.m'
// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>
// Date: 19-Jun-2007 13:03:15
//

#include <mex.h>          // Matlab Mex Functions
#include <cmath>

#define ERROR_HEADER       "MATLAB:JacobiansAngCamMx.cpp:Input\n"
#define VARIABLES          "Vars = (q1i, q2i, q3i, q4i)"

// Identify the input parameters by it's index
enum { VARS=0, S1, S2, S3, S4, S5, S6 };
// Identify the output parameters by it's index
enum { J1=0, J2, J3 };

// Identify elements in Vars vector by it's index
enum { Q1I=0, Q2I, Q3I, Q4I };

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( nlhs != 3 || nrhs != 1 )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: [j1, j2, j3] = JacobiansAngCamMx.cpp ( Vars )\n\n"
                   VARIABLES );

  int M = mxGetM ( prhs[VARS] );
  int N = mxGetN ( prhs[VARS] );
  if ( M != 1 || N != 4 )
    mexErrMsgTxt ( ERROR_HEADER
                   "Vars must be a 1x4 vector\n\n"
                   VARIABLES );

  double *Vars = (double *)mxGetPr ( prhs[VARS] );

  double q1i = Vars[Q1I];
  double q2i = Vars[Q2I];
  double q3i = Vars[Q3I];
  double q4i = Vars[Q4I];

  double s1[2];
  double s2[3];
  double s3[2];
  double s4[2];
  double s5[3];
  double s6[2];

  // "x" value in atan ( y / x ) for Roll, Pitch and Yaw
  // to check whether the positive definition or the negative must be used
  double R_x = (q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*sqrt(1.0+pow(2.0*q1i*q3i+2.0*q2i*q4i,2.0)/(2.0*q1i*q1i*q2i*q2i-8.0*q1i*q2i*q3i*q4i+2.0*q3i*q3i*q4i*q4i+q1i*q1i*q1i*q1i-2.0*q1i*q1i*q3i*q3i+2.0*q1i*q1i*q4i*q4i+q2i*q2i*q2i*q2i+2.0*q2i*q2i*q3i*q3i-2.0*q2i*q2i*q4i*q4i+q3i*q3i*q3i*q3i+q4i*q4i*q4i*q4i));
  double P_x = sqrt(2.0*q1i*q1i*q2i*q2i-8.0*q1i*q2i*q3i*q4i+2.0*q3i*q3i*q4i*q4i+q1i*q1i*q1i*q1i-2.0*q1i*q1i*q3i*q3i+2.0*q1i*q1i*q4i*q4i+q2i*q2i*q2i*q2i+2.0*q2i*q2i*q3i*q3i-2.0*q2i*q2i*q4i*q4i+q3i*q3i*q3i*q3i+q4i*q4i*q4i*q4i);
  double Y_x = (q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)*sqrt(1.0+pow(2.0*q1i*q3i+2.0*q2i*q4i,2.0)/(2.0*q1i*q1i*q2i*q2i-8.0*q1i*q2i*q3i*q4i+2.0*q3i*q3i*q4i*q4i+q1i*q1i*q1i*q1i-2.0*q1i*q1i*q3i*q3i+2.0*q1i*q1i*q4i*q4i+q2i*q2i*q2i*q2i+2.0*q2i*q2i*q3i*q3i-2.0*q2i*q2i*q4i*q4i+q3i*q3i*q3i*q3i+q4i*q4i*q4i*q4i));

  // Select which are the right Jacobians (Positive or Negative)
  bool SelectJac[6] = { R_x > 0.0, P_x > 0.0, Y_x > 0.0, R_x < 0.0, P_x < 0.0, Y_x < 0.0 };

  // Normal Case
  if ( P_x > 1e-8 )
  {
  if ( SelectJac[0] )
  {
    s1[0] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s1[1] = q1i*q2i-q3i*q4i;
  }

  if ( SelectJac[1] )
  {
    s2[0] = q2i*q2i+q1i*q1i+2.0*q1i*q3i+q3i*q3i+2.0*q2i*q4i+q4i*q4i;
    s2[1] = q2i*q2i+q1i*q1i-2.0*q1i*q3i+q3i*q3i-2.0*q2i*q4i+q4i*q4i;
    s2[2] = q1i*q3i+q2i*q4i;
  }

  if ( SelectJac[2] )
  {
    s3[0] = q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i;
    s3[1] = q1i*q4i-q2i*q3i;
  }

  if ( SelectJac[3] )
  {
    s4[0] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s4[1] = q1i*q2i-q3i*q4i;
  }

  if ( SelectJac[4] )
  {
    s5[0] = q2i*q2i+q1i*q1i+2.0*q1i*q3i+q3i*q3i+2.0*q2i*q4i+q4i*q4i;
    s5[1] = q2i*q2i+q1i*q1i-2.0*q1i*q3i+q3i*q3i-2.0*q2i*q4i+q4i*q4i;
    s5[2] = q1i*q3i+q2i*q4i;
  }

  if ( SelectJac[5] )
  {
    s6[0] = q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i;
    s6[1] = q1i*q4i-q2i*q3i;
  }


  if ( SelectJac[0] )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j1 = (double *)mxGetPr ( plhs[J1] );
    j1[0] = -(2.0*q2i/s1[0  ]-4.0*s1[1  ]/pow(s1[0  ],2.0)*q1i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
    j1[1] = -(2.0*q1i/s1[0  ]+4.0*s1[1  ]/pow(s1[0  ],2.0)*q2i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
    j1[2] = -(-2.0*q4i/s1[0  ]+4.0*s1[1  ]/pow(s1[0  ],2.0)*q3i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
    j1[3] = -(-2.0*q3i/s1[0  ]-4.0*s1[1  ]/pow(s1[0  ],2.0)*q4i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
  }

  if ( SelectJac[1] )
  {
    plhs[J2] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j2 = (double *)mxGetPr ( plhs[J2] );
    j2[0] = -(2.0*q3i/sqrt(s2[1  ]*s2[0  ])-s2[2  ]/sqrt(pow(s2[1  ],3.0)*pow(s2[0  ],3.0))*((2.0*q1i-2.0*q3i)*s2[0  ]+s2[1  ]*(2.0*q1i+2.0*q3i)))/(1.0+4.0*pow(s2[2  ],2.0)/s2[1  ]/s2[0  ]);
    j2[1] = -(2.0*q4i/sqrt(s2[1  ]*s2[0  ])-s2[2  ]/sqrt(pow(s2[1  ],3.0)*pow(s2[0  ],3.0))*((2.0*q2i-2.0*q4i)*s2[0  ]+s2[1  ]*(2.0*q2i+2.0*q4i)))/(1.0+4.0*pow(s2[2  ],2.0)/s2[1  ]/s2[0  ]);
    j2[2] = -(2.0*q1i/sqrt(s2[1  ]*s2[0  ])-s2[2  ]/sqrt(pow(s2[1  ],3.0)*pow(s2[0  ],3.0))*((-2.0*q1i+2.0*q3i)*s2[0  ]+s2[1  ]*(2.0*q1i+2.0*q3i)))/(1.0+4.0*pow(s2[2  ],2.0)/s2[1  ]/s2[0  ]);
    j2[3] = -(2.0*q2i/sqrt(s2[1  ]*s2[0  ])-s2[2  ]/sqrt(pow(s2[1  ],3.0)*pow(s2[0  ],3.0))*((-2.0*q2i+2.0*q4i)*s2[0  ]+s2[1  ]*(2.0*q2i+2.0*q4i)))/(1.0+4.0*pow(s2[2  ],2.0)/s2[1  ]/s2[0  ]);
  }

  if ( SelectJac[2] )
  {
    plhs[J3] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j3 = (double *)mxGetPr ( plhs[J3] );
    j3[0] = -(2.0*q4i/s3[0  ]-4.0*s3[1  ]/pow(s3[0  ],2.0)*q1i)/(1.0+4.0*pow(s3[1  ],2.0)/pow(s3[0  ],2.0));
    j3[1] = -(-2.0*q3i/s3[0  ]-4.0*s3[1  ]/pow(s3[0  ],2.0)*q2i)/(1.0+4.0*pow(s3[1  ],2.0)/pow(s3[0  ],2.0));
    j3[2] = -(-2.0*q2i/s3[0  ]+4.0*s3[1  ]/pow(s3[0  ],2.0)*q3i)/(1.0+4.0*pow(s3[1  ],2.0)/pow(s3[0  ],2.0));
    j3[3] = -(2.0*q1i/s3[0  ]+4.0*s3[1  ]/pow(s3[0  ],2.0)*q4i)/(1.0+4.0*pow(s3[1  ],2.0)/pow(s3[0  ],2.0));
  }

  if ( SelectJac[3] )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j1 = (double *)mxGetPr ( plhs[J1] );
    j1[0] = -(2.0*q2i/s4[0  ]-4.0*s4[1  ]/pow(s4[0  ],2.0)*q1i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
    j1[1] = -(2.0*q1i/s4[0  ]+4.0*s4[1  ]/pow(s4[0  ],2.0)*q2i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
    j1[2] = -(-2.0*q4i/s4[0  ]+4.0*s4[1  ]/pow(s4[0  ],2.0)*q3i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
    j1[3] = -(-2.0*q3i/s4[0  ]-4.0*s4[1  ]/pow(s4[0  ],2.0)*q4i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
  }

  if ( SelectJac[4] )
  {
    plhs[J2] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j2 = (double *)mxGetPr ( plhs[J2] );
    j2[0] = -(2.0*q3i/sqrt(s5[1  ]*s5[0  ])-s5[2  ]/sqrt(pow(s5[1  ],3.0)*pow(s5[0  ],3.0))*((2.0*q1i-2.0*q3i)*s5[0  ]+s5[1  ]*(2.0*q1i+2.0*q3i)))/(1.0+4.0*pow(s5[2  ],2.0)/s5[1  ]/s5[0  ]);
    j2[1] = -(2.0*q4i/sqrt(s5[1  ]*s5[0  ])-s5[2  ]/sqrt(pow(s5[1  ],3.0)*pow(s5[0  ],3.0))*((2.0*q2i-2.0*q4i)*s5[0  ]+s5[1  ]*(2.0*q2i+2.0*q4i)))/(1.0+4.0*pow(s5[2  ],2.0)/s5[1  ]/s5[0  ]);
    j2[2] = -(2.0*q1i/sqrt(s5[1  ]*s5[0  ])-s5[2  ]/sqrt(pow(s5[1  ],3.0)*pow(s5[0  ],3.0))*((-2.0*q1i+2.0*q3i)*s5[0  ]+s5[1  ]*(2.0*q1i+2.0*q3i)))/(1.0+4.0*pow(s5[2  ],2.0)/s5[1  ]/s5[0  ]);
    j2[3] = -(2.0*q2i/sqrt(s5[1  ]*s5[0  ])-s5[2  ]/sqrt(pow(s5[1  ],3.0)*pow(s5[0  ],3.0))*((-2.0*q2i+2.0*q4i)*s5[0  ]+s5[1  ]*(2.0*q2i+2.0*q4i)))/(1.0+4.0*pow(s5[2  ],2.0)/s5[1  ]/s5[0  ]);
  }

  if ( SelectJac[5] )
  {
    plhs[J3] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j3 = (double *)mxGetPr ( plhs[J3] );
    j3[0] = -(2.0*q4i/s6[0  ]-4.0*s6[1  ]/pow(s6[0  ],2.0)*q1i)/(1.0+4.0*pow(s6[1  ],2.0)/pow(s6[0  ],2.0));
    j3[1] = -(-2.0*q3i/s6[0  ]-4.0*s6[1  ]/pow(s6[0  ],2.0)*q2i)/(1.0+4.0*pow(s6[1  ],2.0)/pow(s6[0  ],2.0));
    j3[2] = -(-2.0*q2i/s6[0  ]+4.0*s6[1  ]/pow(s6[0  ],2.0)*q3i)/(1.0+4.0*pow(s6[1  ],2.0)/pow(s6[0  ],2.0));
    j3[3] = -(2.0*q1i/s6[0  ]+4.0*s6[1  ]/pow(s6[0  ],2.0)*q4i)/(1.0+4.0*pow(s6[1  ],2.0)/pow(s6[0  ],2.0));
  }

  // If there is no valid Jacobian => 0
  if ( !SelectJac[0] && !SelectJac[3] )
  {
    mexPrintf ( "Jacobian 1/4 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J1] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J1] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;
    j[3] = 0.0;
  }

  if ( !SelectJac[1] && !SelectJac[4] )
  {
    mexPrintf ( "Jacobian 2/5 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J2] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J2] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;
    j[3] = 0.0;
  }

  if ( !SelectJac[2] && !SelectJac[5] )
  {
    mexPrintf ( "Jacobian 3/6 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J3] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J3] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;
    j[3] = 0.0;
  }

  }
  else
  {
    // Special Case: When P_x is close to 0:
    //               Pitch = 90, Yaw = 0 and Roll = atan2 (...)
  if ( SelectJac[0] )
  {
    s1[0] = q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i;
    s1[1] = q1i*q2i+q3i*q4i;
  }

  if ( SelectJac[1] )
  {
  }

  if ( SelectJac[2] )
  {
  }

  if ( SelectJac[3] )
  {
    s4[0] = q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i;
    s4[1] = q1i*q2i+q3i*q4i;
  }

  if ( SelectJac[4] )
  {
  }

  if ( SelectJac[5] )
  {
  }


  if ( SelectJac[0] )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j1 = (double *)mxGetPr ( plhs[J1] );
    j1[0] = -(2.0*q2i/s1[0  ]-4.0*s1[1  ]/pow(s1[0  ],2.0)*q1i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
    j1[1] = -(2.0*q1i/s1[0  ]+4.0*s1[1  ]/pow(s1[0  ],2.0)*q2i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
    j1[2] = -(2.0*q4i/s1[0  ]-4.0*s1[1  ]/pow(s1[0  ],2.0)*q3i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
    j1[3] = -(2.0*q3i/s1[0  ]+4.0*s1[1  ]/pow(s1[0  ],2.0)*q4i)/(1.0+4.0*pow(s1[1  ],2.0)/pow(s1[0  ],2.0));
  }

  if ( SelectJac[1] )
  {
    plhs[J2] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j2 = (double *)mxGetPr ( plhs[J2] );
    j2[0] = 0.0;
    j2[1] = 0.0;
    j2[2] = 0.0;
    j2[3] = 0.0;
  }

  if ( SelectJac[2] )
  {
    plhs[J3] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j3 = (double *)mxGetPr ( plhs[J3] );
    j3[0] = 0.0;
    j3[1] = 0.0;
    j3[2] = 0.0;
    j3[3] = 0.0;
  }

  if ( SelectJac[3] )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j1 = (double *)mxGetPr ( plhs[J1] );
    j1[0] = -(2.0*q2i/s4[0  ]-4.0*s4[1  ]/pow(s4[0  ],2.0)*q1i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
    j1[1] = -(2.0*q1i/s4[0  ]+4.0*s4[1  ]/pow(s4[0  ],2.0)*q2i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
    j1[2] = -(2.0*q4i/s4[0  ]-4.0*s4[1  ]/pow(s4[0  ],2.0)*q3i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
    j1[3] = -(2.0*q3i/s4[0  ]+4.0*s4[1  ]/pow(s4[0  ],2.0)*q4i)/(1.0+4.0*pow(s4[1  ],2.0)/pow(s4[0  ],2.0));
  }

  if ( SelectJac[4] )
  {
    plhs[J2] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j2 = (double *)mxGetPr ( plhs[J2] );
    j2[0] = 0.0;
    j2[1] = 0.0;
    j2[2] = 0.0;
    j2[3] = 0.0;
  }

  if ( SelectJac[5] )
  {
    plhs[J3] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j3 = (double *)mxGetPr ( plhs[J3] );
    j3[0] = 0.0;
    j3[1] = 0.0;
    j3[2] = 0.0;
    j3[3] = 0.0;
  }

  // If there is no valid Jacobian => 0
  if ( !SelectJac[0] && !SelectJac[3] )
  {
    mexPrintf ( "Jacobian 1/4 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J1] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J1] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;
    j[3] = 0.0;
  }

  if ( !SelectJac[1] && !SelectJac[4] )
  {
    mexPrintf ( "Jacobian 2/5 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J2] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J2] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;
    j[3] = 0.0;
  }

  if ( !SelectJac[2] && !SelectJac[5] )
  {
    mexPrintf ( "Jacobian 3/6 cannot be calculated: x = 0 in atan (y / x)!\n" );

    plhs[J3] = mxCreateDoubleMatrix ( 4, 1, mxREAL );
    double *j = (double *)mxGetPr ( plhs[J3] );
    j[0] = 0.0;
    j[1] = 0.0;
    j[2] = 0.0;
    j[3] = 0.0;
  }

  }
}
