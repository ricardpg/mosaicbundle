%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobians for LBL Point Readings Algebraically.
%
%  File          : CostSymbolicLBLPoint.m
%  Date          : 26/04/2006 - 20/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CostSymbolicPM Calculate the Jacobians for the LBL Point Reading data
%                 algebraically. The function creates a C++ Mex file and
%                 compiles it into a Mex function.
%
%     Input Parameters:
%      JacobianLBLPointFileName: String containing the C++ generated file name. 
%                                By default is "JacobiansLBLPointMx.cpp".
%
%     Output Parameters:
%

function CostSymbolicLBLPoint ( JacobiansLBLPointFileName )
  % Test the input parameters
  error ( nargchk ( 0, 1, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  % Default value for the output filename
  if nargin < 1 
    JacobiansLBLPointFileName = 'JacobiansLBLPointMx.cpp';
  end

  % Symbolic Quaternion Fields for Image i
  q1i = sym ( 'q1i', 'real' );
  q2i = sym ( 'q2i', 'real' );
  q3i = sym ( 'q3i', 'real' );
  q4i = sym ( 'q4i', 'real' );

  % Symbolic Translation Parameters for Image i
  txi = sym ( 'txi', 'real' );
  tyi = sym ( 'tyi', 'real' );
  tzi = sym ( 'tzi', 'real' );

  % Symbolic Points and Matches
  px = sym ( 'px', 'real' );
  py = sym ( 'py', 'real' );
  mx = sym ( 'mx', 'real' );
  my = sym ( 'my', 'real' );

  % Symbolic Camera Parameters
  au = sym ( 'au', 'real' );
  av = sym ( 'av', 'real' );
  cx = sym ( 'cx', 'real' );
  cy = sym ( 'cy', 'real' );

  % Symblic Camera Intrinsic Parameters
  K = [ au    0  cx;
          0  av  cy;
          0   0   1 ];

  % Symblic Elements of Rotation Matrices Represented by Quaternions for Image i
  ri_1 = q1i^2+q2i^2-q3i^2-q4i^2;
  ri_5 = q1i^2-q2i^2+q3i^2-q4i^2;
  ri_9 = q1i^2-q2i^2-q3i^2+q4i^2;
  ri_2 = -2*q1i*q4i+2*q2i*q3i;
  ri_3 = 2*q1i*q3i+2*q2i*q4i;
  ri_4 = 2*q1i*q4i+2*q2i*q3i;
  ri_6 = -2*q1i*q2i+2*q3i*q4i;
  ri_7 = -2*q1i*q3i+2*q2i*q4i;
  ri_8 = 2*q1i*q2i+2*q3i*q4i;

  Ri = [ri_1 ri_2 ri_3;ri_4 ri_5 ri_6;ri_7 ri_8 ri_9];

  % Translation for Factorized Projection Matrix 
  Ti = [1 0 -txi; 0 1 -tyi; 0 0 -tzi];

  % Compute iHw = K * cRw * wTc
  iHw = K * Ri * Ti;

  % Residuals from LBL Readings
  %   px, py: Image coordinates
  %   mx, my: LBL coordinates
  disp ( 'Calculating Residuals for LBL Point Readings (x and y)...' );
  CostLBLPoint_x = px - (iHw(1,:)*[mx;my;1]) / (iHw(3,:)*[mx;my;1]);
  CostLBLPoint_y = py - (iHw(2,:)*[mx;my;1]) / (iHw(3,:)*[mx;my;1]);

  disp ( 'Simplifying expressions...' );
  sCostLBLPoint_x = simple ( CostLBLPoint_x );
  sCostLBLPoint_y = simple ( CostLBLPoint_y );

  % Sizes
  NumJac = 2;
  NumVars = 7;

  % Allocate Cell-Array
  s = cell ( NumJac, 1 );
  dsCostLBLPoint = cell ( NumJac, 1 );

  % Jacobians for LBL Point Readings
  disp ( 'Calculating Jacobian for x...' );
  dCostLBLPoint_x = jacobian ( sCostLBLPoint_x, [q1i, q2i, q3i, q4i, txi, tyi, tzi] );
  [dsCostLBLPoint{1} s{1}] = subexpr ( dCostLBLPoint_x, 's1' );

  disp ( 'Calculating Jacobian for y...' );
  dCostLBLPoint_y = jacobian ( sCostLBLPoint_y, [q1i, q2i, q3i, q4i, txi, tyi, tzi] );
  [dsCostLBLPoint{2} s{2}] = subexpr ( dCostLBLPoint_y, 's2' );

  FId = fopen ( JacobiansLBLPointFileName, 'w' );
  if FId ~= -1;
    disp ( sprintf ( 'Generating "%s" Mex File...', JacobiansLBLPointFileName ) );

    fprintf ( FId, '//\n' );
    fprintf ( FId, '// C++ Automatically generated file using ''%s.m''\n', mfilename );
    fprintf ( FId, '// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>\n' );
    fprintf ( FId, '// Date: %s\n', datestr ( now ) );
    fprintf ( FId, '//\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#include <mex.h>          // Matlab Mex Functions\n' );
    fprintf ( FId, '#include <cmath>\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#define ERROR_HEADER       "MATLAB:%s:Input\\n"\n', JacobiansLBLPointFileName );
    fprintf ( FId, '#define VARIABLES          "Vars = (au, av, cx, cy, q1i, q2i, q3i, q4i, txi, tyi, tzi[, mx, my])"\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify the input parameters by it''s index\n' );
    fprintf ( FId, 'enum { VARS=0, S1, S2 };\n' );

    fprintf ( FId, '// Identify the output parameters by it''s index\n' );
    fprintf ( FId, 'enum { J1=0, J2 };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify elements in Vars vector by it''s index\n' );
    fprintf ( FId, 'enum { AU=0, AV, CX, CY, Q1I, Q2I, Q3I, Q4I, TXI, TYI, TZI, MX, MY };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, 'void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )\n' );
    fprintf ( FId, '{\n' );
    fprintf ( FId, '  // Parameter checks\n');
    fprintf ( FId, '  if ( nlhs != 2 || ( nrhs != 1 && nrhs != 3 ) )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Usage: [s1, s2] = %s ( Vars )\\n"\n', JacobiansLBLPointFileName );
    fprintf ( FId, '                   "   or  [j1, j2] = %s ( Vars, s1, s2 )\\n\\n"\n', JacobiansLBLPointFileName );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  int M = mxGetM ( prhs[VARS] );\n' );
    fprintf ( FId, '  int N = mxGetN ( prhs[VARS] );\n' );
    fprintf ( FId, '  if ( ( M != 1 ) || ( N != 11 && N != 13 ) )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Vars must be a 1x11 or 1x13 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *Vars = (double *)mxGetPr ( prhs[VARS] );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  // Check for mx, my when si are provided\n' );
    fprintf ( FId, '  if ( nrhs != 1 && N != 13 )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "When s1, s2 are provided, Vars must be a 1x13 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double au = Vars[AU];\n' );
    fprintf ( FId, '  double av = Vars[AV];\n' );
    fprintf ( FId, '  double cx = Vars[CX];\n' );
    fprintf ( FId, '  double cy = Vars[CY];\n' );
    fprintf ( FId, '  double q1i = Vars[Q1I];\n' );
    fprintf ( FId, '  double q2i = Vars[Q2I];\n' );
    fprintf ( FId, '  double q3i = Vars[Q3I];\n' );
    fprintf ( FId, '  double q4i = Vars[Q4I];\n' );
    fprintf ( FId, '  double txi = Vars[TXI];\n' );
    fprintf ( FId, '  double tyi = Vars[TYI];\n' );
    fprintf ( FId, '  double tzi = Vars[TZI];\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '  double mx, my;\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '  // If it is the case of computing the jaccobians, get the Point-Match positions\n' );
    fprintf ( FId, '  if ( nrhs != 1 )\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    mx = Vars[MX];\n' );
    fprintf ( FId, '    my = Vars[MY];\n' );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '  else\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    mx = my = 0.0;\n' );
    fprintf ( FId, '  }\n' );

    fprintf ( FId, '\n' );

    % Workarround to avoid difficult computations to know the number of
    % Maple-Generated Dummy Variables.
    fprintf ( FId, '  double MapleGenVar1' );
    for k = 2:9;
      fprintf ( FId, ', MapleGenVar%d', k );
    end
    fprintf ( FId, ';\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *s1' );
    for i = 2:NumJac;
      fprintf ( FId, ', *s%d', i );
    end
    fprintf ( FId, ';\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '  // Test whether s_i must be computed or they are provided\n' );
    fprintf ( FId, '  if ( nrhs == 1 )\n' );
    fprintf ( FId, '  {\n' );
    for i = 1:NumJac;
      fprintf ( FId, '    plhs[J%d] = mxCreateDoubleMatrix ( %d, 1, mxREAL );\n', i, size ( s{i}, 1 ) );
      fprintf ( FId, '    s%d = (double *)mxGetPr ( plhs[J%d] );\n', i, i );
    end
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '  else\n' );
    fprintf ( FId, '  {\n' );
    for i = 1:NumJac;
      fprintf ( FId, '    s%d = mxGetPr ( prhs[S%d] );\n', i, i );
    end
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '\n' );

    % Calculate Sigmas
    fprintf ( FId, CalculateSigmasStr ( s, 'mx|my', true, false ) );
    fprintf ( FId, '\n' );

    % Compute the jacobians
    fprintf ( FId, CalculateJacobiansStr ( dsCostLBLPoint, NumVars, true, false ) );
    fprintf ( FId, '}\n' );

    fclose ( FId );
  else
    disp ( sprintf ( 'Error writting file "%s"\n', JacobiansLBLPointFileName ) );
  end

  disp ( sprintf ( 'Mexifying "%s" file...', JacobiansLBLPointFileName ) );
  eval ( [ 'mex ' JacobiansLBLPointFileName ] )
end
