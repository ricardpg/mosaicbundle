
% Function to replace si(nn.0) by si[nn-1] that is converting matlab/maple
% vector convention to C array convention.
%
function StrRes = AdjustMatrixIndices ( StrRes )
  TokSta = regexp ( StrRes, 's\d\([\d]*.\d\)', 'start' );
  TokEnd = regexp ( StrRes, 's\d\([\d]*.\d\)', 'end' );
  for l = 1: size ( TokSta, 2 );
    StrRes(TokSta(l)+2) = '[';
    StrRes(TokEnd(l)) = ']';
    StrRes(TokEnd(l)-1) = ' ';
    StrRes(TokEnd(l)-2) = ' ';
    Val = str2double ( StrRes (TokSta(l)+3:TokEnd(l)-3 ) ) - 1;
    Str = num2str ( Val );
    % Erase the content: [xxx] => [   ] to prevent problems n digits becomes n-1 digits
    StrRes(TokSta(l)+3:TokEnd(l)-1) = ' ';
    StrRes (TokSta(l)+3:TokSta(l)+3+length(Str)-1) = Str;
  end
end
