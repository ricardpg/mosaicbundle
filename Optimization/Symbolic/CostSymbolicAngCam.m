%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobians for Angular Camera Readings Algebraically.
%
%  File          : CostSymbolicAngCam.m
%  Date          : 27/04/2006 - 19/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CostSymbolicAngCam Calculate the Jacobians for the Angular Camera Reading
%                     data algebraically. The function creates a C++ Mex file
%                     and compiles it into a Mex function.
%
%     Input Parameters:
%      JacobiansAngCamFileName: String containing the C++ generated file name. 
%                               By default is "JacobiansAngCamMx.cpp".
%
%     Output Parameters:
%

function CostSymbolicAngCam ( JacobiansAngCamFileName )
  % Test the input parameters
  error ( nargchk ( 0, 1, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  % Default value for the output filename
  if nargin < 1 
    JacobiansAngCamFileName = 'JacobiansAngCamMx.cpp';
  end

  % Symbolic Quaternion Fields for Image i
  q1i = sym ( 'q1i', 'real' );
  q2i = sym ( 'q2i', 'real' );
  q3i = sym ( 'q3i', 'real' );
  q4i = sym ( 'q4i', 'real' );

  % Symblic Elements of Rotation Matrices Represented by Quaternions for Image i
  ri_1 = q1i^2+q2i^2-q3i^2-q4i^2;
  ri_5 = q1i^2-q2i^2+q3i^2-q4i^2;
  ri_9 = q1i^2-q2i^2-q3i^2+q4i^2;
  ri_2 = -2*q1i*q4i+2*q2i*q3i;
  ri_3 = 2*q1i*q3i+2*q2i*q4i;
  ri_4 = 2*q1i*q4i+2*q2i*q3i;
  ri_6 = -2*q1i*q2i+2*q3i*q4i;
  ri_7 = -2*q1i*q3i+2*q2i*q4i;
  ri_8 = 2*q1i*q2i+2*q3i*q4i;

  Ri = [ri_1 ri_2 ri_3;ri_4 ri_5 ri_6;ri_7 ri_8 ri_9];

  % Symbolic Roll, Pitch and Yaw.
  P = sym ( 'P', 'real' );
  Y = sym ( 'Y', 'real' );
  R = sym ( 'R', 'real' );

  % Definition of atan2 according to FDLIBM www.netlib.org ( IEEE 754 )
  %   http://www.netlib.org/fdlibm/e_atan2.c
  %                    /  atan ( y / x )         if x > 0
  %   atan2 ( y, x ) = |
  %                    \  pi - atan ( y / -x )   if x < 0

  % Definitions according to: Rxyz = Rx ( Roll ) * Ry ( Pitch ) * Rz ( Yaw )
  %

  % Px = sqrt ( R(2,3)*R(2,3) + R(3,3)*R(3,3) );
  %
  % if Px > 1e-8;
  %   Pitch = atan2 ( R(1,3), Px );
  %   cP = cos ( Pitch );
  %   Yaw = atan2 ( -R(1,2)/cP, R(1,1)/cP );
  %   Roll = atan2 ( -R(2,3)/cP, R(3,3)/cP );
  % else
  %   Pitch = pi / 2;
  %   Yaw = 0;
  %   Roll = atan2 ( R(3,2), R(2,2) );
  % end

  % Positive Definitions 
  P_x = sqrt ( Ri(2,3)*Ri(2,3) + Ri(3,3)*Ri(3,3) );
  P_p = atan ( Ri(1,3) / P_x );
  Y_x = Ri(1,1) / cos ( P_p );
  Y_p = atan ( -Ri(1,2) / Ri(1,1) );
  R_x = Ri(3,3) / cos ( P_p );
  R_p = atan ( -Ri(2,3) / Ri(3,3) );

  % Negative Definitions
  P_n = pi - atan ( -Ri(1,3) / P_x );
  Y_n = pi - atan ( Ri(1,2) / Ri(1,1) );
  R_n = pi - atan ( Ri(2,3) / Ri(3,3) );
  
  % Special Case: When P_x is close to 0 => Pitch = 90, Yaw = 0 and Roll = atan2
  P2_p = pi / 2;
  Y2_p = 0;
  R2_p = atan ( Ri(3,2) / Ri(2,2) );

  P2_n = pi / 2;
  Y2_n = 0;
  R2_n = pi - atan ( -Ri(3,2) / Ri(2,2) );
  
  disp ( 'Calculating Residuals for Angular Camera Readings (Roll, Pitch, Yaw)...' );
  % Residuals definitions according to positive definition
  CostR_p = R - R_p;
  CostP_p = P - P_p;
  CostY_p = Y - Y_p;
  
  % Residuals definitions according to negative definition
  CostR_n = R - R_n;
  CostP_n = P - P_n;
  CostY_n = Y - Y_n;

  CostR2_p = R - R2_p;
  CostP2_p = P - P2_p;
  CostY2_p = Y - Y2_p;
  
  CostR2_n = R - R2_n;
  CostP2_n = P - P2_n;
  CostY2_n = Y - Y2_n;

  disp ( 'Simplifying expressions...' );
  % Positive Definitions
  sCostR_p = simple ( CostR_p );
  sCostP_p = simple ( CostP_p );
  sCostY_p = simple ( CostY_p );

  % Negative Definitions
  sCostR_n = simple ( CostR_n );
  sCostP_n = simple ( CostP_n );
  sCostY_n = simple ( CostY_n );

  sCostR2_p = simple ( CostR2_p );
  sCostP2_p = simple ( CostP2_p );
  sCostY2_p = simple ( CostY2_p );

  sCostR2_n = simple ( CostR2_n );
  sCostP2_n = simple ( CostP2_n );
  sCostY2_n = simple ( CostY2_n );

  % Sizes
  NumJac = 3;
  NumVars = 4;

  % Allocate Cell-Array
  s = cell ( NumJac*2, 1 );
  dsCostAngCam = cell ( NumJac*2, 1 );

  s2 = cell ( NumJac*2, 1 );
  dsCostAngCam2 = cell ( NumJac*2, 1 );

  % Jacobians of the cost definitions
  % Positive Definitions
  dCostR_p = jacobian ( sCostR_p, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam{1}, s{1}] = subexpr ( dCostR_p, 's1' );

  dCostP_p = jacobian ( sCostP_p, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam{2}, s{2}] = subexpr ( dCostP_p, 's2' );

  dCostY_p = jacobian ( sCostY_p, [q1i, q2i, q3i, q4i]);
  [dsCostAngCam{3}, s{3}] = subexpr ( dCostY_p, 's3' );

  % Negative Definitions
  dCostR_n = jacobian ( sCostR_n, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam{4}, s{4}] = subexpr ( dCostR_n, 's4' );

  dCostP_n = jacobian ( sCostP_n, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam{5}, s{5}] = subexpr ( dCostP_n, 's5' );

  dCostY_n = jacobian ( sCostY_n, [ q1i, q2i, q3i, q4i ] );
  [dsCostAngCam{6}, s{6}] = subexpr ( dCostY_n, 's6' );

  % Special Case
  dCostR2_p = jacobian ( sCostR2_p, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam2{1}, s2{1}] = subexpr ( dCostR2_p, 's1' );

  dCostP2_p = jacobian ( sCostP2_p, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam2{2}, s2{2}] = subexpr ( dCostP2_p, 's2' );

  dCostY2_p = jacobian ( sCostY2_p, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam2{3}, s2{3}] = subexpr ( dCostY2_p, 's3' );

  dCostR2_n = jacobian ( sCostR2_n, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam2{4}, s2{4}] = subexpr ( dCostR2_n, 's4' );

  dCostP2_n = jacobian ( sCostP2_n, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam2{5}, s2{5}] = subexpr ( dCostP2_n, 's5' );

  dCostY2_n = jacobian ( sCostY2_n, [q1i, q2i, q3i, q4i] );
  [dsCostAngCam2{6}, s2{6}] = subexpr ( dCostY2_n, 's6' );

  FId = fopen ( JacobiansAngCamFileName, 'w' );
  if FId ~= -1;
    disp ( sprintf ( 'Generating "%s" Mex File...', JacobiansAngCamFileName ) );

    fprintf ( FId, '//\n' );
    fprintf ( FId, '// C++ Automatically generated file using ''%s.m''\n', mfilename );
    fprintf ( FId, '// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>\n' );
    fprintf ( FId, '// Date: %s\n', datestr ( now ) );
    fprintf ( FId, '//\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#include <mex.h>          // Matlab Mex Functions\n' );
    fprintf ( FId, '#include <cmath>\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#define ERROR_HEADER       "MATLAB:%s:Input\\n"\n', JacobiansAngCamFileName );
    fprintf ( FId, '#define VARIABLES          "Vars = (q1i, q2i, q3i, q4i)"\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify the input parameters by it''s index\n' );
    fprintf ( FId, 'enum { VARS=0, S1, S2, S3, S4, S5, S6 };\n' );

    fprintf ( FId, '// Identify the output parameters by it''s index\n' );
    fprintf ( FId, 'enum { J1=0, J2, J3 };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify elements in Vars vector by it''s index\n' );
    fprintf ( FId, 'enum { Q1I=0, Q2I, Q3I, Q4I };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, 'void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )\n' );
    fprintf ( FId, '{\n' );
    fprintf ( FId, '  // Parameter checks\n');
    fprintf ( FId, '  if ( nlhs != 3 || nrhs != 1 )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Usage: [j1, j2, j3] = %s ( Vars )\\n\\n"\n', JacobiansAngCamFileName );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  int M = mxGetM ( prhs[VARS] );\n' );
    fprintf ( FId, '  int N = mxGetN ( prhs[VARS] );\n' );
    fprintf ( FId, '  if ( M != 1 || N != 4 )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Vars must be a 1x4 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *Vars = (double *)mxGetPr ( prhs[VARS] );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double q1i = Vars[Q1I];\n' );
    fprintf ( FId, '  double q2i = Vars[Q2I];\n' );
    fprintf ( FId, '  double q3i = Vars[Q3I];\n' );
    fprintf ( FId, '  double q4i = Vars[Q4I];\n' );
    fprintf ( FId, '\n' );

    for i = 1:NumJac*2;
      fprintf ( FId, '  double s%d[%d];\n', i, size ( s{i}, 1 ) );
    end
    fprintf ( FId, '\n' );

    fprintf ( FId, '  // "x" value in atan ( y / x ) for Roll, Pitch and Yaw\n' );
    fprintf ( FId, '  // to check whether the positive definition or the negative must be used\n' );    
    Str = strtrim ( ccode ( R_x ) );
    R_s = strrep ( Str, '~', '' );
    R_s = strrep ( R_s, 't0 =', 'double R_x =' );
    fprintf ( FId, '  %s\n', R_s );

    Str = strtrim ( ccode ( P_x ) );
    P_s = strrep ( Str, '~', '' );
    P_s = strrep ( P_s, 't0 =', 'double P_x =' );
    fprintf ( FId, '  %s\n', P_s );

    Str = strtrim ( ccode ( Y_x ) );
    Y_s = strrep ( Str, '~', '' );
    Y_s = strrep ( Y_s, 't0 =', 'double Y_x =' );
    fprintf ( FId, '  %s\n', Y_s );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  // Select which are the right Jacobians (Positive or Negative)\n' );
    fprintf ( FId, '  bool SelectJac[%d] = { R_x > 0.0, P_x > 0.0, Y_x > 0.0, R_x < 0.0, P_x < 0.0, Y_x < 0.0 };\n', NumJac*2 );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  // Normal Case\n' );
    fprintf ( FId, '  if ( P_x > 1e-8 )\n' );
    fprintf ( FId, '  {\n' );
    % Calculate Sigmas
    fprintf ( FId, CalculateSigmasStr ( s, '', false, true ) );
    fprintf ( FId, '\n' );
    % Compute the jacobians
    fprintf ( FId, CalculateJacobiansStr ( dsCostAngCam, NumVars, false, true ) );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '  else\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    // Special Case: When P_x is close to 0:\n' );
    fprintf ( FId, '    //               Pitch = 90, Yaw = 0 and Roll = atan2 (...)\n' );
    % Calculate Sigmas
    fprintf ( FId, CalculateSigmasStr ( s2, '', false, true ) );
    fprintf ( FId, '\n' );
    % Compute the jacobians
    fprintf ( FId, CalculateJacobiansStr ( dsCostAngCam2, NumVars, false, true ) );
    fprintf ( FId, '  }\n' );

    fprintf ( FId, '}\n' );

    fclose ( FId );
  else
    disp ( sprintf ( 'Error writting file "%s"\n', JacobiansAngCamFileName ) );
  end

  disp ( sprintf ( 'Mexifying "%s" file...', JacobiansAngCamFileName ) );
  eval ( [ 'mex ' JacobiansAngCamFileName ] )
end
