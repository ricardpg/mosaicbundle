%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Subexpressions Computation in C++ code.
%
%  File          : CalculateJacobiansStr.m
%  Date          : 26/04/2006 - 09/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CalculateJacobiansStr Calculate the C code to compute N subexpressions
%                        in input cell-array.
%
%     Input Parameters:
%      CostFunc: Cell-Array containing the subexpressions to be converted
%                in C++ code.
%      RegExpVars: Regular expression to detect variables in a string.
%      IfCheck: Perform If-Then-Else check on the number of input
%               parameters.
%      SelectJac: Select whether a si substring must be calculated or not.
%
%     Output Parameters:
%      OutStr: String containing the output C++ code.
%

function OutStr = CalculateSigmasStr ( s, RegExpVars, IfCheck, SelectJac )
  % Test the input parameters
  error ( nargchk ( 3, 4, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % By default if check done
  if nargin < 4; SelectJac = false; end;

  % Number of subexpressions
  NumS = size ( s );

  % Output Str
  OutStr = '';
  Indent = '';

  if IfCheck;
    Indent = '  ';
    OutStr = strcat ( OutStr, sprintf ( '  if ( nrhs == 1 )\\n' ) );
    OutStr = strcat ( OutStr, sprintf ( '  {\\n' ) );
  end

  if SelectJac;
    Indent = [Indent '  '];
  end

  for i = 1:NumS;
    if SelectJac;
      OutStr = strcat ( OutStr, sprintf ( '%sif ( SelectJac[%d] )\\n', Indent, i-1 ) );
      OutStr = strcat ( OutStr, sprintf ( '%s{\\n', Indent ) );
    end
    
    for j = 1:size ( s{i}, 1 );
      Str = strtrim ( ccode ( s{i}(j) ) );

      % Erase Dummy ~ (I don't know why it appears using ccode)
      Str = strrep ( Str, '~', '' );

      Val = strfind ( Str, '      ' );

      % Check if it is any mx, my 
      ComputeAllways = ~isempty ( regexp ( Str, RegExpVars, 'once' ) );
      if ComputeAllways;
        OutStr = strcat ( OutStr, sprintf ( '%s  s%d[%d] = 0;\\n', Indent, i, j - 1 ) );
        OutStr = strcat ( OutStr, sprintf ( '%s}\\n', Indent ) );
        OutStr = strcat ( OutStr, sprintf ( '%selse\\n', Indent ) );
        OutStr = strcat ( OutStr, sprintf ( '%s{\\n', Indent ) );
      end

      if ~isempty ( Val );
        ValL = [1 Val];
        ValR = [Val size(Str,2)];
        for k = 1:size ( ValL, 2 );
          StrRes = strtrim ( Str(ValL(k):ValR(k)) );
          if strfind ( StrRes, 't0 =' );
            StrRes = strrep ( StrRes, 't0 =', sprintf ( 's%d[%d] =', i, j - 1 ) );
          end
          OutStr = strcat ( OutStr, sprintf ( '%s  %s\\n', Indent, StrRes ) );
        end
      else
        StrRes = strrep ( Str, 't0 =', sprintf ( 's%d[%d] =', i, j - 1 ) );
        OutStr = strcat ( OutStr, sprintf ( '%s  %s\\n', Indent, StrRes ) );
      end

      if ComputeAllways;
        OutStr = strcat ( OutStr, sprintf ( '%s}\\n', Indent ) );
        OutStr = strcat ( OutStr, sprintf ( '\\n' ) );
        OutStr = strcat ( OutStr, sprintf ( '%sif ( nrhs == 1 )\\n', Indent ) );
        OutStr = strcat ( OutStr, sprintf ( '%s{\\n', Indent ) );
      end
    end

    if SelectJac;
      OutStr = strcat ( OutStr, sprintf ( '%s}\\n', Indent ) );
    end

    OutStr = strcat ( OutStr, sprintf ( '\\n' ) );
  end

  if IfCheck;
    OutStr = strcat ( OutStr, sprintf ( '  }\\n' ) );
  end
end
