%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobians for Point and Match Algebraically.
%
%  File          : CostSymbolicPM.m
%  Date          : 26/04/2006 - 08/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CostSymbolicPM Calculate the Jacobians for the Point-Match data
%                 algebraically. The function creates a C++ Mex file and
%                 compiles it into a Mex function.
%
%     Input Parameters:
%      JacobiansPMFileName: String containing the C++ generated file name.
%                           By default is "JacobiansPMMx.cpp".
%
%     Output Parameters:
%

function CostSymbolicPM ( JacobiansPMFileName )
  % Test the input parameters
  error ( nargchk ( 0, 1, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  % Default value for the output filename
  if nargin < 1 
    JacobiansPMFileName = 'JacobiansPMMx.cpp';
  end

  % Symbolic Quaternion Fields for Image i and j
  q1i = sym ( 'q1i', 'real' );
  q2i = sym ( 'q2i', 'real' );
  q3i = sym ( 'q3i', 'real' );
  q4i = sym ( 'q4i', 'real' );
  q1j = sym ( 'q1j', 'real' );
  q2j = sym ( 'q2j', 'real' );
  q3j = sym ( 'q3j', 'real' );
  q4j = sym ( 'q4j', 'real' );

  % Symbolic Translation Parameters for Image i and j
  txi = sym ( 'txi', 'real' );
  tyi = sym ( 'tyi', 'real' );
  tzi = sym ( 'tzi', 'real' );
  txj = sym ( 'txj', 'real' );
  tyj = sym ( 'tyj', 'real' );
  tzj = sym ( 'tzj', 'real' );

  % Symbolic Points and Matches
  px = sym ( 'px', 'real' );
  py = sym ( 'py', 'real' );
  mx = sym ( 'mx', 'real' );
  my = sym ( 'my', 'real' );

  % Symbolic Camera Parameters
  au = sym ( 'au', 'real' );
  av = sym ( 'av', 'real' );
  cx = sym ( 'cx', 'real' );
  cy = sym ( 'cy', 'real' );

  % Symblic Camera Intrinsic Parameters
  K = [ au    0  cx;
        0  av  cy;
        0   0   1 ];
  K_1 = inv ( K );

  % Symblic Elements of Rotation Matrices Represented by Quaternions for image i and j
  ri_1 = q1i^2+q2i^2-q3i^2-q4i^2;
  ri_5 = q1i^2-q2i^2+q3i^2-q4i^2;
  ri_9 = q1i^2-q2i^2-q3i^2+q4i^2;
  ri_2 = -2*q1i*q4i+2*q2i*q3i;
  ri_3 = 2*q1i*q3i+2*q2i*q4i;
  ri_4 = 2*q1i*q4i+2*q2i*q3i;
  ri_6 = -2*q1i*q2i+2*q3i*q4i;
  ri_7 = -2*q1i*q3i+2*q2i*q4i;
  ri_8 = 2*q1i*q2i+2*q3i*q4i;

  Ri = [ri_1 ri_2 ri_3;ri_4 ri_5 ri_6;ri_7 ri_8 ri_9];

  rj_1 = q1j^2+q2j^2-q3j^2-q4j^2;
  rj_5 = q1j^2-q2j^2+q3j^2-q4j^2;
  rj_9 = q1j^2-q2j^2-q3j^2+q4j^2;
  rj_2 = -2*q1j*q4j+2*q2j*q3j;
  rj_3 = 2*q1j*q3j+2*q2j*q4j;
  rj_4 = 2*q1j*q4j+2*q2j*q3j;
  rj_6 = -2*q1j*q2j+2*q3j*q4j;
  rj_7 = -2*q1j*q3j+2*q2j*q4j;
  rj_8 = 2*q1j*q2j+2*q3j*q4j;

  Rj = [rj_1 rj_2 rj_3;rj_4 rj_5 rj_6;rj_7 rj_8 rj_9];

  % Translation for Factorized Projection Matrix 
  Ti = [1 0 -txi; 0 1 -tyi; 0 0 -tzi];
  Tj = [1 0 -txj; 0 1 -tyj; 0 0 -tzj];

  % Compute wHi = K * cRw * wTc
  Hi = K * Ri * Ti;
  Hj = K * Rj * Tj;

  % Compute iHj and jHi from wHi and wHj
  Hij = Hi * inv(Tj) * Rj' * K_1;
  Hji = Hj * inv(Ti) * Ri' * K_1;

  disp ( 'Calculating Residuals for Point and Match (Px, Py, Mx and My)...' );  
  CostPM_Px = px - ((Hij(1,:)*[mx;my;1]) / (Hij(3,:)*[mx;my;1]));
  CostPM_Py = py - ((Hij(2,:)*[mx;my;1]) / (Hij(3,:)*[mx;my;1]));
  CostPM_Mx = mx - ((Hji(1,:)*[px;py;1]) / (Hji(3,:)*[px;py;1]));
  CostPM_My = my - ((Hji(2,:)*[px;py;1]) / (Hji(3,:)*[px;py;1]));

  disp ( 'Simplifying expressions...' );
  sCostPM_Px = simple ( CostPM_Px );
  sCostPM_Py = simple ( CostPM_Py );
  sCostPM_Mx = simple ( CostPM_Mx );
  sCostPM_My = simple ( CostPM_My );

  % Sizes
  NumJac = 4;
  NumVars = 14;
  
  % Allocate Cell-Array
  s = cell ( NumJac, 1 );
  dsCostPM = cell ( NumJac, 1 );

  % Jacobians for the Point and Match
  disp ( 'Calculating Jacobian for Px...' );
  dCostPM_Px = jacobian ( sCostPM_Px, [q1i, q2i, q3i, q4i, txi, tyi, tzi, ...
                                       q1j, q2j, q3j, q4j, txj, tyj, tzj] );
  [dsCostPM{1}, s{1}] = subexpr ( dCostPM_Px, 's1' );

  disp ( 'Calculating Jacobian for Py...' );
  dCostPM_Py = jacobian ( sCostPM_Py, [q1i, q2i, q3i, q4i, txi, tyi, tzi, ...
                                       q1j, q2j, q3j, q4j, txj, tyj, tzj] );
  [dsCostPM{2}, s{2}] = subexpr ( dCostPM_Py, 's2' );

  disp ( 'Calculating Jacobian for Mx...' );
  dCostPM_Mx = jacobian ( sCostPM_Mx, [q1i, q2i, q3i, q4i, txi, tyi, tzi, ...
                                       q1j, q2j, q3j, q4j, txj, tyj, tzj] );
  [dsCostPM{3}, s{3}] = subexpr ( dCostPM_Mx, 's3' );

  disp ( 'Calculating Jacobian for My...' );
  dCostPM_My = jacobian ( sCostPM_My, [q1i, q2i, q3i, q4i, txi, tyi, tzi, ...
                                       q1j, q2j, q3j, q4j, txj, tyj, tzj] );
  [dsCostPM{4}, s{4}] = subexpr ( dCostPM_My, 's4' );

  % Create the output Mex file
  FId = fopen ( JacobiansPMFileName, 'w' );
  if FId ~= -1;
    disp ( sprintf ( 'Generating "%s" Mex File...', JacobiansPMFileName ) );

    fprintf ( FId, '//\n' );
    fprintf ( FId, '// C++ Automatically generated file using ''%s.m''\n', mfilename );
    fprintf ( FId, '// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>\n' );
    fprintf ( FId, '// Date: %s\n', datestr ( now ) );
    fprintf ( FId, '//\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#include <mex.h>          // Matlab Mex Functions\n' );
    fprintf ( FId, '#include <cmath>\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '#define ERROR_HEADER       "MATLAB:%s:Input\\n"\n', JacobiansPMFileName );
    fprintf ( FId, '#define VARIABLES          "Vars = (au, av, cx, cy, q1i, q2i, q3i, q4i, txi, tyi, tzi, q1j, q2j, q3j, q4j, txj, tyj, tzj[, px, py, mx, my])"\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify the input parameters by it''s index\n' );
    fprintf ( FId, 'enum { VARS=0, S1, S2, S3, S4 };\n' );

    fprintf ( FId, '// Identify the output parameters by it''s index\n' );
    fprintf ( FId, 'enum { J1=0, J2, J3, J4 };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '// Identify elements in Vars vector by it''s index\n' );
    fprintf ( FId, 'enum { AU=0, AV, CX, CY, Q1I, Q2I, Q3I, Q4I, TXI, TYI, TZI, Q1J, Q2J, Q3J, Q4J, TXJ, TYJ, TZJ, PX, PY, MX, MY };\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, 'void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )\n' );
    fprintf ( FId, '{\n' );
    fprintf ( FId, '  // Parameter checks\n');
    fprintf ( FId, '  if ( nlhs != 4 || ( nrhs != 1 && nrhs != 5 ) )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Usage: [s1, s2, s3, s4] = %s ( Vars )\\n"\n', JacobiansPMFileName );
    fprintf ( FId, '                   "   or  [j1, j2, j3, j4] = %s ( Vars, s1, s2, s3, s4 )\\n\\n"', JacobiansPMFileName );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  int M = mxGetM ( prhs[VARS] );\n' );
    fprintf ( FId, '  int N = mxGetN ( prhs[VARS] );\n' );
    fprintf ( FId, '  if ( ( M != 1 ) || ( N != 18 && N != 22 ) )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "Vars must be a 1x18 or 1x22 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *Vars = (double *)mxGetPr ( prhs[VARS] );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  // Check for px, py, mx, my when si are provided\n' );
    fprintf ( FId, '  if ( nrhs != 1 && N != 22 )\n' );
    fprintf ( FId, '    mexErrMsgTxt ( ERROR_HEADER\n' );
    fprintf ( FId, '                   "When s1, s2, s3, s4 are provided, Vars must be a 1x22 vector\\n\\n"\n' );
    fprintf ( FId, '                   VARIABLES );\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double au = Vars[AU];\n' );
    fprintf ( FId, '  double av = Vars[AV];\n' );
    fprintf ( FId, '  double cx = Vars[CX];\n' );
    fprintf ( FId, '  double cy = Vars[CY];\n' );
    fprintf ( FId, '  double q1i = Vars[Q1I];\n' );
    fprintf ( FId, '  double q2i = Vars[Q2I];\n' );
    fprintf ( FId, '  double q3i = Vars[Q3I];\n' );
    fprintf ( FId, '  double q4i = Vars[Q4I];\n' );
    fprintf ( FId, '  double txi = Vars[TXI];\n' );
    fprintf ( FId, '  double tyi = Vars[TYI];\n' );
    fprintf ( FId, '  double tzi = Vars[TZI];\n' );
    fprintf ( FId, '  double q1j = Vars[Q1J];\n' );
    fprintf ( FId, '  double q2j = Vars[Q2J];\n' );
    fprintf ( FId, '  double q3j = Vars[Q3J];\n' );
    fprintf ( FId, '  double q4j = Vars[Q4J];\n' );
    fprintf ( FId, '  double txj = Vars[TXJ];\n' );
    fprintf ( FId, '  double tyj = Vars[TYJ];\n' );
    fprintf ( FId, '  double tzj = Vars[TZJ];\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '  double px, py, mx, my;\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '  // If it is the case of computing the jaccobians, get the Point-Match positions\n' );
    fprintf ( FId, '  if ( nrhs != 1 )\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    px = Vars[PX];\n' );
    fprintf ( FId, '    py = Vars[PY];\n' );
    fprintf ( FId, '    mx = Vars[MX];\n' );
    fprintf ( FId, '    my = Vars[MY];\n' );
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '  else\n' );
    fprintf ( FId, '  {\n' );
    fprintf ( FId, '    px = py = mx = my = 0.0;\n' );
    fprintf ( FId, '  }\n' );

    fprintf ( FId, '\n' );

    % Workarround to avoid difficult computations to know the number of
    % Maple-Generated Dummy Variables.
    fprintf ( FId, '  double MapleGenVar1' );
    for k = 2:9;
      fprintf ( FId, ', MapleGenVar%d', k );
    end
    fprintf ( FId, ';\n' );
    fprintf ( FId, '\n' );

    fprintf ( FId, '  double *s1' );
    for i = 2:NumJac;
      fprintf ( FId, ', *s%d', i );
    end
    fprintf ( FId, ';\n' );
    fprintf ( FId, '\n' );
    fprintf ( FId, '  // Test whether s_i must be computed or they are provided\n' );
    fprintf ( FId, '  if ( nrhs == 1 )\n' );
    fprintf ( FId, '  {\n' );
    for i = 1:NumJac;
      fprintf ( FId, '    plhs[J%d] = mxCreateDoubleMatrix ( %d, 1, mxREAL );\n', i, size ( s{i}, 1 ) );
      fprintf ( FId, '    s%d = (double *)mxGetPr ( plhs[J%d] );\n', i, i );
    end
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '  else\n' );
    fprintf ( FId, '  {\n' );
    for i = 1:NumJac;
      fprintf ( FId, '    s%d = mxGetPr ( prhs[S%d] );\n', i, i );
    end
    fprintf ( FId, '  }\n' );
    fprintf ( FId, '\n' );

    % Calculate Sigmas
    fprintf ( FId, CalculateSigmasStr ( s, 'px|py|mx|my', true, false ) );
    fprintf ( FId, '\n' );

    % Compute the jacobians
    fprintf ( FId, CalculateJacobiansStr ( dsCostPM, NumVars, true, false ) );
    fprintf ( FId, '}\n');

    fclose ( FId );

    disp ( sprintf ( 'Mexifying "%s" file...', JacobiansPMFileName ) );
    eval ( [ 'mex ' JacobiansPMFileName ] )

  else
    disp ( sprintf ( 'Error writting file "%s"\n', JacobiansPMFileName ) );
  end
end
