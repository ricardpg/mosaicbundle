%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate Jacobians Computation in C++ code.
%
%  File          : CalculateJacobiansstr.m
%  Date          : 26/04/2006 - 08/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CalculateJacobiansStr Calculate the C code to compute N jacobians in the
%                        input cell-array.
%
%     Input Parameters:
%      CostFunc: Cell-Array containing the Jacobians to be converted in C++
%                code.
%      NumVars: Number of variables of the function.
%      IfCheck: Perform If-Then-Else check on the number of input
%               parameters.
%      SelectJac: Select whether a Jacobian must be calculated or not.
%
%     Output Parameters:
%      OutStr: String containing the output C++ code.
%

function OutStr = CalculateJacobiansStr ( CostFunc, NumVars, IfCheck, SelectJac )
  % Test the input parameters
  error ( nargchk ( 3, 4, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % If SelectJac is not specified => false
  if nargin < 4; SelectJac = false; end;
  
  % Number of Jacobians to be computed
  NumJac = size ( CostFunc, 1 );

  % Output Str
  OutStr = '';
  Indent = '';

  if IfCheck;
    Indent = '  ';
    OutStr = strcat ( OutStr, sprintf ( '  // Compute the jacobians\\n' ) );
    OutStr = strcat ( OutStr, sprintf ( '  if ( nrhs != 1 )\\n' ) );
    OutStr = strcat ( OutStr, sprintf ( '  {\\n' ) );
  end

  if SelectJac;
    Indent = [Indent '  '];
    RealJac = NumJac / 2;
  end

  for i = 1:NumJac;
    if SelectJac;
      NJac = mod ( i-1, RealJac )+1;
      
      OutStr = strcat ( OutStr, sprintf ( '%sif ( SelectJac[%d] )\\n', Indent, i-1 ) );
      OutStr = strcat ( OutStr, sprintf ( '%s{\\n', Indent ) );
    else
      NJac = i;
    end

    OutStr = strcat ( OutStr, sprintf ( '%s  plhs[J%d] = mxCreateDoubleMatrix ( %d, 1, mxREAL );\\n', Indent, NJac, NumVars ) );
    OutStr = strcat ( OutStr, sprintf ( '%s  double *j%d = (double *)mxGetPr ( plhs[J%d] );\\n', Indent, NJac, NJac ) );

    for j = 1:size ( CostFunc{i}, 2 );
      % Work arround to the stupid undisconnectable maple message:
      %    `codegen/C/expression:`, `Unknown function:`, s1, `will be left as is.`
      DummyS = sym ( sprintf ( 's%d(0)', i ) );
      DummyStr = ccode ( DummyS );
      Str = strtrim ( ccode ( CostFunc{i}(j) ) );
      Val = strfind ( Str, '      ' );
      if ~isempty ( Val );
        ValL = [1 Val];
        ValR = [Val size(Str,2)];
        for k = 1:size ( ValL, 2 );
          StrRes = strtrim ( Str(ValL(k):ValR(k)) );

          % Erase Dummy ~ (I don't know why it appears using ccode)
          StrRes = strrep ( StrRes, '~', '' );

          % Replace si(nn.n) by si[nn-1]
          StrRes = AdjustMatrixIndices ( StrRes );

          if strfind ( StrRes, 't0 =' );
            StrRes = strrep ( StrRes, 't0 =', sprintf ( 'j%d[%d] =', NJac, j-1 ) );
          end
          OutStr = strcat ( OutStr, sprintf ( '%s  %s\\n', Indent, StrRes ) );
        end
      else
        % Erase Dummy ~ (I don't know why it appears using ccode)
        StrRes = strrep ( Str, '~', '' );

        % Replace si(nn.n) by si[nn-1]
        StrRes = AdjustMatrixIndices ( StrRes );

        StrRes = strrep ( StrRes, 't0 =', sprintf ( 'j%d[%d] =', NJac, j-1 ) );
        OutStr = strcat ( OutStr, sprintf ( '%s  %s\\n', Indent, StrRes ) );
      end
    end

    if SelectJac;
      OutStr = strcat ( OutStr, sprintf ( '%s}\\n', Indent ) );
    end
    
    OutStr = strcat ( OutStr, sprintf ( '\\n' ) );
  end

  % If the x is 0 => the atan cannot be computed => Jacobian = 0
  if SelectJac;    
    OutStr = strcat ( OutStr, sprintf ( '%s// If there is no valid Jacobian => 0\\n', Indent ) );
  
    for i = 1:RealJac;
      OutStr = strcat ( OutStr, sprintf ( '%sif ( !SelectJac[%d] && !SelectJac[%d] )\\n', Indent, i-1, RealJac + i - 1 ) );
      OutStr = strcat ( OutStr, sprintf ( '%s{\\n', Indent ) );
      OutStr = strcat ( OutStr, sprintf ( '%s  mexPrintf ( "Jacobian %d/%d cannot be calculated: x = 0 in atan (y / x)!%s" );\\n', Indent, i, i + RealJac, '\\n' ) );
      OutStr = strcat ( OutStr, '\n' );
      OutStr = strcat ( OutStr, sprintf ( '%s  plhs[J%d] = mxCreateDoubleMatrix ( %d, 1, mxREAL );\\n', Indent, i, NumVars ) );
      OutStr = strcat ( OutStr, sprintf ( '%s  double *j = (double *)mxGetPr ( plhs[J%d] );\\n', Indent, i ) );
      for j = 1:NumVars;
        OutStr = strcat ( OutStr, sprintf ( '%s  j[%d] = 0.0;\\n', Indent, j-1 ) );
      end
      OutStr = strcat ( OutStr, sprintf ( '%s}\\n', Indent ) );
      OutStr = strcat ( OutStr, '\n' );
    end
  end

  if IfCheck;
    OutStr = strcat ( OutStr, sprintf ( '  }\\n' ) );
  end
end
