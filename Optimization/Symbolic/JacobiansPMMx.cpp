//
// C++ Automatically generated file using 'CostSymbolicPM.m'
// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>
// Date: 09-May-2007 09:36:28
//

#include <mex.h>          // Matlab Mex Functions
#include <cmath>

#define ERROR_HEADER       "MATLAB:JacobiansPMMx.cpp:Input\n"
#define VARIABLES          "Vars = (au, av, cx, cy, q1i, q2i, q3i, q4i, txi, tyi, tzi, q1j, q2j, q3j, q4j, txj, tyj, tzj[, px, py, mx, my])"

// Identify the input parameters by it's index
enum { VARS=0, S1, S2, S3, S4 };
// Identify the output parameters by it's index
enum { J1=0, J2, J3, J4 };

// Identify elements in Vars vector by it's index
enum { AU=0, AV, CX, CY, Q1I, Q2I, Q3I, Q4I, TXI, TYI, TZI, Q1J, Q2J, Q3J, Q4J, TXJ, TYJ, TZJ, PX, PY, MX, MY };

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( nlhs != 4 || ( nrhs != 1 && nrhs != 5 ) )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: [s1, s2, s3, s4] = JacobiansPMMx.cpp ( Vars )\n"
                   "   or  [j1, j2, j3, j4] = JacobiansPMMx.cpp ( Vars, s1, s2, s3, s4 )\n\n"                   VARIABLES );

  int M = mxGetM ( prhs[VARS] );
  int N = mxGetN ( prhs[VARS] );
  if ( ( M != 1 ) || ( N != 18 && N != 22 ) )
    mexErrMsgTxt ( ERROR_HEADER
                   "Vars must be a 1x18 or 1x22 vector\n\n"
                   VARIABLES );

  double *Vars = (double *)mxGetPr ( prhs[VARS] );

  // Check for px, py, mx, my when si are provided
  if ( nrhs != 1 && N != 22 )
    mexErrMsgTxt ( ERROR_HEADER
                   "When s1, s2, s3, s4 are provided, Vars must be a 1x22 vector\n\n"
                   VARIABLES );

  double au = Vars[AU];
  double av = Vars[AV];
  double cx = Vars[CX];
  double cy = Vars[CY];
  double q1i = Vars[Q1I];
  double q2i = Vars[Q2I];
  double q3i = Vars[Q3I];
  double q4i = Vars[Q4I];
  double txi = Vars[TXI];
  double tyi = Vars[TYI];
  double tzi = Vars[TZI];
  double q1j = Vars[Q1J];
  double q2j = Vars[Q2J];
  double q3j = Vars[Q3J];
  double q4j = Vars[Q4J];
  double txj = Vars[TXJ];
  double tyj = Vars[TYJ];
  double tzj = Vars[TZJ];

  double px, py, mx, my;

  // If it is the case of computing the jaccobians, get the Point-Match positions
  if ( nrhs != 1 )
  {
    px = Vars[PX];
    py = Vars[PY];
    mx = Vars[MX];
    my = Vars[MY];
  }
  else
  {
    px = py = mx = my = 0.0;
  }

  double MapleGenVar1, MapleGenVar2, MapleGenVar3, MapleGenVar4, MapleGenVar5, MapleGenVar6, MapleGenVar7, MapleGenVar8, MapleGenVar9;

  double *s1, *s2, *s3, *s4;

  // Test whether s_i must be computed or they are provided
  if ( nrhs == 1 )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 48, 1, mxREAL );
    s1 = (double *)mxGetPr ( plhs[J1] );
    plhs[J2] = mxCreateDoubleMatrix ( 45, 1, mxREAL );
    s2 = (double *)mxGetPr ( plhs[J2] );
    plhs[J3] = mxCreateDoubleMatrix ( 48, 1, mxREAL );
    s3 = (double *)mxGetPr ( plhs[J3] );
    plhs[J4] = mxCreateDoubleMatrix ( 48, 1, mxREAL );
    s4 = (double *)mxGetPr ( plhs[J4] );
  }
  else
  {
    s1 = mxGetPr ( prhs[S1] );
    s2 = mxGetPr ( prhs[S2] );
    s3 = mxGetPr ( prhs[S3] );
    s4 = mxGetPr ( prhs[S4] );
  }

  if ( nrhs == 1 )
  {
    s1[0] = q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j;
    s1[1] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s1[2] = 2.0*q1i*q2i+2.0*q3i*q4i;
    s1[3] = -2.0*q1i*q3i+2.0*q2i*q4i;
    s1[4] = -(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi;
    s1[5] = (-2.0*q1i*q3i+2.0*q2i*q4i)*txj/(tzj*tzj)+(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/(tzj*tzj)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/(tzj*tzj);
    s1[6] = -2.0*q1j*q2j+2.0*q3j*q4j;
    s1[7] = 2.0*q1j*q3j+2.0*q2j*q4j;
    s1[8] = -(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj;
    s1[9] = 2.0*q1j*q2j+2.0*q3j*q4j;
    s1[10] = -2.0*q1j*q3j+2.0*q2j*q4j;
    s1[11] = q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j;
    s1[12] = 2.0*q1j*q4j+2.0*q2j*q3j;
    s1[13] = (-2.0*q1i*q3i+2.0*q2i*q4i)*(2.0*q1j*q4j+2.0*q2j*q3j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j);
    s1[14] = -2.0*q1j*q4j+2.0*q2j*q3j;
    s1[15] = q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j;
    s1[16] = (-2.0*q1i*q3i+2.0*q2i*q4i)*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j);
    s1[17] = 0;
  }
  else
  {
    MapleGenVar2 = ((-2.0*q1i*q3i+2.0*q2i*q4i)*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j))/au*mx;
    MapleGenVar4 = ((-2.0*q1i*q3i+2.0*q2i*q4i)*(2.0*q1j*q4j+2.0*q2j*q3j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j))/av*my;
    MapleGenVar5 = -((-2.0*q1i*q3i+2.0*q2i*q4i)*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j))*cx/au;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar2 = MapleGenVar1-((-2.0*q1i*q3i+2.0*q2i*q4i)*(2.0*q1j*q4j+2.0*q2j*q3j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j))*cy/av;
    s1[17] = MapleGenVar2+(-2.0*q1i*q3i+2.0*q2i*q4i)*(-2.0*q1j*q3j+2.0*q2j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(2.0*q1j*q2j+2.0*q3j*q4j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j);
  }

  if ( nrhs == 1 )
  {
    s1[18] = au*(2.0*q1i*q3i+2.0*q2i*q4i);
    s1[19] = au*(-2.0*q1i*q4i+2.0*q2i*q3i);
    s1[20] = au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i);
    s1[21] = -(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi;
    s1[22] = -(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj;
    s1[23] = (au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*(2.0*q1j*q4j+2.0*q2j*q3j)+(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j);
    s1[24] = (au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j);
    s1[25] = 0;
  }
  else
  {
    MapleGenVar4 = (au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j);
    MapleGenVar5 = (au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j);
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar4 = 1/au*mx;
    MapleGenVar2 = MapleGenVar3*MapleGenVar4;
    MapleGenVar6 = (au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*(2.0*q1j*q4j+2.0*q2j*q3j);
    MapleGenVar7 = (au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = 1/av*my;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar7 = -(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j);
    MapleGenVar8 = -(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*(-2.0*q1j*q4j+2.0*q2j*q3j)-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j);
    MapleGenVar6 = MapleGenVar7+MapleGenVar8;
    MapleGenVar7 = cx/au;
    MapleGenVar5 = MapleGenVar6*MapleGenVar7;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar3 = MapleGenVar1;
    MapleGenVar6 = -(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*(2.0*q1j*q4j+2.0*q2j*q3j);
    MapleGenVar7 = -(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = cy/av;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar2 = MapleGenVar3+MapleGenVar4;
    MapleGenVar3 = MapleGenVar2+(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*(-2.0*q1j*q3j+2.0*q2j*q4j);
    s1[25] = MapleGenVar3+(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*(2.0*q1j*q2j+2.0*q3j*q4j)+(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j);
  }

  if ( nrhs == 1 )
  {
    s1[26] = (au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/(tzj*tzj)+(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/(tzj*tzj)+(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/(tzj*tzj);
    s1[27] = 2.0*(-2.0*q1i*q3i+2.0*q2i*q4i)*q1j-2.0*(2.0*q1i*q2i+2.0*q3i*q4i)*q4j+2.0*(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*q3j;
    s1[28] = 2.0*(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*q1j-2.0*(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*q4j+2.0*(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*q3j;
    s1[29] = 2.0*(-2.0*q1i*q3i+2.0*q2i*q4i)*q2j+2.0*(2.0*q1i*q2i+2.0*q3i*q4i)*q3j+2.0*(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(2.0*q1i*q2i+2.0*q3i*q4i)*tyj/tzj-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*q4j;
    s1[30] = 2.0*(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*q2j+2.0*(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*q3j+2.0*(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj-(-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*q4j;
    s1[31] = -q1i*q1i+q2i*q2i+q3i*q3i-q4i*q4i;
    s1[32] = -2.0*q1i*q2i-2.0*q3i*q4i;
    s1[33] = 2.0*q1i*q3i-2.0*q2i*q4i;
    s1[34] = -2.0*q2i*txj/tzj-2.0*q3i*tyj/tzj-(-2.0*q2i*txi-2.0*q3i*tyi-2.0*q4i*tzi)/tzj;
    s1[35] = 2.0*au*q2i+2.0*cx*q4i;
    s1[36] = -2.0*au*q1i+2.0*cx*q3i;
    s1[37] = -2.0*au*q4i+2.0*cx*q2i;
    s1[38] = -(-2.0*au*q4i+2.0*cx*q2i)*txj/tzj-(-2.0*au*q1i+2.0*cx*q3i)*tyj/tzj-(-(-2.0*au*q4i+2.0*cx*q2i)*txi-(-2.0*au*q1i+2.0*cx*q3i)*tyi-(2.0*au*q2i+2.0*cx*q4i)*tzi)/tzj;
    s1[39] = 2.0*q1i*txj/tzj-2.0*q4i*tyj/tzj-(2.0*q1i*txi-2.0*q4i*tyi+2.0*q3i*tzi)/tzj;
    s1[40] = 2.0*au*q1i-2.0*cx*q3i;
    s1[41] = -2.0*au*q3i-2.0*cx*q1i;
    s1[42] = -(-2.0*au*q3i-2.0*cx*q1i)*txj/tzj-(2.0*au*q2i+2.0*cx*q4i)*tyj/tzj-(-(-2.0*au*q3i-2.0*cx*q1i)*txi-(2.0*au*q2i+2.0*cx*q4i)*tyi-(2.0*au*q1i-2.0*cx*q3i)*tzi)/tzj;
    s1[43] = -2.0*q4i*txj/tzj-2.0*q1i*tyj/tzj-(-2.0*q4i*txi-2.0*q1i*tyi+2.0*q2i*tzi)/tzj;
    s1[44] = 2.0*au*q3i+2.0*cx*q1i;
    s1[45] = -(2.0*au*q2i+2.0*cx*q4i)*txj/tzj-(2.0*au*q3i+2.0*cx*q1i)*tyj/tzj-(-(2.0*au*q2i+2.0*cx*q4i)*txi-(2.0*au*q3i+2.0*cx*q1i)*tyi-(2.0*au*q4i-2.0*cx*q2i)*tzi)/tzj;
    s1[46] = 2.0*q3i*txj/tzj-2.0*q2i*tyj/tzj-(2.0*q3i*txi-2.0*q2i*tyi-2.0*q1i*tzi)/tzj;
    s1[47] = -(2.0*au*q1i-2.0*cx*q3i)*txj/tzj-(-2.0*au*q4i+2.0*cx*q2i)*tyj/tzj-(-(2.0*au*q1i-2.0*cx*q3i)*txi-(-2.0*au*q4i+2.0*cx*q2i)*tyi-(2.0*au*q3i+2.0*cx*q1i)*tzi)/tzj;

    s2[0] = q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j;
    s2[1] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s2[2] = 2.0*q1i*q2i+2.0*q3i*q4i;
    s2[3] = -2.0*q1i*q3i+2.0*q2i*q4i;
    s2[4] = (-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi;
    s2[5] = -2.0*q1i*q2i-2.0*q3i*q4i;
    s2[6] = (-2.0*q1i*q3i+2.0*q2i*q4i)*txj/(tzj*tzj)-(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/(tzj*tzj)-((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/(tzj*tzj);
    s2[7] = -2.0*q1j*q2j+2.0*q3j*q4j;
    s2[8] = 2.0*q1j*q3j+2.0*q2j*q4j;
    s2[9] = -(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj+(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/tzj+((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj;
    s2[10] = 2.0*q1j*q2j+2.0*q3j*q4j;
    s2[11] = -2.0*q1j*q3j+2.0*q2j*q4j;
    s2[12] = (-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/tzj-((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj;
    s2[13] = -q1j*q1j+q2j*q2j-q3j*q3j+q4j*q4j;
    s2[14] = -2.0*q1j*q4j-2.0*q2j*q3j;
    s2[15] = -2.0*q1j*q4j+2.0*q2j*q3j;
    s2[16] = q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j;
    s2[17] = q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j;
    s2[18] = 2.0*q1j*q4j+2.0*q2j*q3j;
    s2[19] = 0;
  }
  else
  {
    MapleGenVar2 = ((-2.0*q1i*q3i+2.0*q2i*q4i)*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj+(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/tzj+((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j))/au*mx;
    MapleGenVar4 = ((-2.0*q1i*q3i+2.0*q2i*q4i)*(2.0*q1j*q4j+2.0*q2j*q3j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj+(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/tzj+((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j))/av*my;
    MapleGenVar5 = (-(-2.0*q1i*q3i+2.0*q2i*q4i)*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)-(2.0*q1i*q2i+2.0*q3i*q4i)*(-2.0*q1j*q4j+2.0*q2j*q3j)-(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj+(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/tzj+((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j))*cx/au;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar2 = MapleGenVar1+((-2.0*q1j*q4j-2.0*q2j*q3j)*(-2.0*q1i*q3i+2.0*q2i*q4i)+(-q1j*q1j+q2j*q2j-q3j*q3j+q4j*q4j)*(2.0*q1i*q2i+2.0*q3i*q4i)+((-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj-(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/tzj-((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j))*cy/av;
    s2[19] = MapleGenVar2+(-2.0*q1i*q3i+2.0*q2i*q4i)*(-2.0*q1j*q3j+2.0*q2j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(2.0*q1j*q2j+2.0*q3j*q4j)+(-(-2.0*q1i*q3i+2.0*q2i*q4i)*txj/tzj+(-2.0*q1i*q2i-2.0*q3i*q4i)*tyj/tzj+((-2.0*q1i*q3i+2.0*q2i*q4i)*txi+(2.0*q1i*q2i+2.0*q3i*q4i)*tyi+(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi)/tzj)*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j);
  }

  if ( nrhs == 1 )
  {
    s2[20] = av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i);
    s2[21] = av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i);
    s2[22] = av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i);
    s2[23] = (av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi;
    s2[24] = -(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj;
    s2[25] = (av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*(2.0*q1j*q4j+2.0*q2j*q3j)+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j);
    s2[26] = (av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j);
    s2[27] = 0;
  }
  else
  {
    MapleGenVar4 = (av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j);
    MapleGenVar5 = (av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*(-2.0*q1j*q4j+2.0*q2j*q3j)+(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j);
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar4 = 1/au*mx;
    MapleGenVar2 = MapleGenVar3*MapleGenVar4;
    MapleGenVar6 = (av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*(2.0*q1j*q4j+2.0*q2j*q3j);
    MapleGenVar7 = (av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = 1/av*my;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar7 = -(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j);
    MapleGenVar8 = -(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*(-2.0*q1j*q4j+2.0*q2j*q3j)-(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(2.0*q1j*q3j+2.0*q2j*q4j);
    MapleGenVar6 = MapleGenVar7+MapleGenVar8;
    MapleGenVar7 = cx/au;
    MapleGenVar5 = MapleGenVar6*MapleGenVar7;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar3 = MapleGenVar1;
    MapleGenVar6 = -(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*(2.0*q1j*q4j+2.0*q2j*q3j);
    MapleGenVar7 = -(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)-(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(-2.0*q1j*q2j+2.0*q3j*q4j);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = cy/av;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar2 = MapleGenVar3+MapleGenVar4;
    MapleGenVar3 = MapleGenVar2+(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*(-2.0*q1j*q3j+2.0*q2j*q4j);
    s2[27] = MapleGenVar3+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*(2.0*q1j*q2j+2.0*q3j*q4j)+(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j);
  }

  if ( nrhs == 1 )
  {
    s2[28] = (av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/(tzj*tzj)+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/(tzj*tzj)-((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/(tzj*tzj);
    s2[29] = 2.0*(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*q1j-2.0*(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*q4j+2.0*(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*q3j;
    s2[30] = 2.0*(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*q2j+2.0*(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*q3j+2.0*(-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txj/tzj-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyj/tzj+((av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+(av*(-2.0*q1i*q2i+2.0*q3i*q4i)+cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi)/tzj)*q4j;
    s2[31] = -2.0*q2i*txj/tzj-2.0*q3i*tyj/tzj+(2.0*q2i*txi+2.0*q3i*tyi+2.0*q4i*tzi)/tzj;
    s2[32] = 2.0*av*q3i+2.0*cy*q4i;
    s2[33] = -2.0*av*q4i+2.0*cy*q3i;
    s2[34] = 2.0*av*q1i+2.0*cy*q2i;
    s2[35] = -(2.0*av*q1i+2.0*cy*q2i)*txj/tzj-(-2.0*av*q4i+2.0*cy*q3i)*tyj/tzj+((2.0*av*q1i+2.0*cy*q2i)*txi+(-2.0*av*q4i+2.0*cy*q3i)*tyi+(2.0*av*q3i+2.0*cy*q4i)*tzi)/tzj;
    s2[36] = 2.0*q1i*txj/tzj-2.0*q4i*tyj/tzj+(-2.0*q1i*txi+2.0*q4i*tyi-2.0*q3i*tzi)/tzj;
    s2[37] = 2.0*av*q4i-2.0*cy*q3i;
    s2[38] = 2.0*av*q2i-2.0*cy*q1i;
    s2[39] = -(2.0*av*q2i-2.0*cy*q1i)*txj/tzj-(2.0*av*q3i+2.0*cy*q4i)*tyj/tzj+((2.0*av*q2i-2.0*cy*q1i)*txi+(2.0*av*q3i+2.0*cy*q4i)*tyi+(2.0*av*q4i-2.0*cy*q3i)*tzi)/tzj;
    s2[40] = -2.0*q4i*txj/tzj-2.0*q1i*tyj/tzj+(2.0*q4i*txi+2.0*q1i*tyi-2.0*q2i*tzi)/tzj;
    s2[41] = -2.0*av*q2i+2.0*cy*q1i;
    s2[42] = -(2.0*av*q3i+2.0*cy*q4i)*txj/tzj-(-2.0*av*q2i+2.0*cy*q1i)*tyj/tzj+((2.0*av*q3i+2.0*cy*q4i)*txi+(-2.0*av*q2i+2.0*cy*q1i)*tyi+(-2.0*av*q1i-2.0*cy*q2i)*tzi)/tzj;
    s2[43] = 2.0*q3i*txj/tzj-2.0*q2i*tyj/tzj+(-2.0*q3i*txi+2.0*q2i*tyi+2.0*q1i*tzi)/tzj;
    s2[44] = -(2.0*av*q4i-2.0*cy*q3i)*txj/tzj-(2.0*av*q1i+2.0*cy*q2i)*tyj/tzj+((2.0*av*q4i-2.0*cy*q3i)*txi+(2.0*av*q1i+2.0*cy*q2i)*tyi+(-2.0*av*q2i+2.0*cy*q1i)*tzi)/tzj;

    s3[0] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s3[1] = -q1j*q1j+q2j*q2j+q3j*q3j-q4j*q4j;
    s3[2] = -2.0*q1i*q2i+2.0*q3i*q4i;
    s3[3] = 2.0*q1i*q3i+2.0*q2i*q4i;
    s3[4] = q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j;
    s3[5] = 2.0*q1j*q2j+2.0*q3j*q4j;
    s3[6] = -2.0*q1j*q3j+2.0*q2j*q4j;
    s3[7] = -(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj;
    s3[8] = -(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi;
    s3[9] = 2.0*q1i*q2i+2.0*q3i*q4i;
    s3[10] = -2.0*q1i*q3i+2.0*q2i*q4i;
    s3[11] = q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i;
    s3[12] = 2.0*q1i*q4i+2.0*q2i*q3i;
    s3[13] = (-2.0*q1j*q3j+2.0*q2j*q4j)*(2.0*q1i*q4i+2.0*q2i*q3i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    s3[14] = -2.0*q1i*q4i+2.0*q2i*q3i;
    s3[15] = q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i;
    s3[16] = (-2.0*q1j*q3j+2.0*q2j*q4j)*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    s3[17] = 0;
  }
  else
  {
    MapleGenVar2 = ((-2.0*q1j*q3j+2.0*q2j*q4j)*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i))/au*px;
    MapleGenVar4 = ((-2.0*q1j*q3j+2.0*q2j*q4j)*(2.0*q1i*q4i+2.0*q2i*q3i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i))/av*py;
    MapleGenVar5 = -((-2.0*q1j*q3j+2.0*q2j*q4j)*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i))*cx/au;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar2 = MapleGenVar1-((-2.0*q1j*q3j+2.0*q2j*q4j)*(2.0*q1i*q4i+2.0*q2i*q3i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i))*cy/av;
    s3[17] = MapleGenVar2+(-2.0*q1i*q3i+2.0*q2i*q4i)*(-2.0*q1j*q3j+2.0*q2j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(2.0*q1j*q2j+2.0*q3j*q4j)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i);
  }

  if ( nrhs == 1 )
  {
    s3[18] = au*(2.0*q1j*q3j+2.0*q2j*q4j);
    s3[19] = au*(-2.0*q1j*q4j+2.0*q2j*q3j);
    s3[20] = au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j);
    s3[21] = -(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj;
    s3[22] = -(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi;
    s3[23] = (au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*(2.0*q1i*q4i+2.0*q2i*q3i)+(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    s3[24] = (au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    s3[25] = 0;
  }
  else
  {
    MapleGenVar4 = (au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i);
    MapleGenVar5 = (au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar4 = 1/au*px;
    MapleGenVar2 = MapleGenVar3*MapleGenVar4;
    MapleGenVar6 = (au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*(2.0*q1i*q4i+2.0*q2i*q3i);
    MapleGenVar7 = (au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = 1/av*py;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar7 = -(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i);
    MapleGenVar8 = -(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*(-2.0*q1i*q4i+2.0*q2i*q3i)-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    MapleGenVar6 = MapleGenVar7+MapleGenVar8;
    MapleGenVar7 = cx/au;
    MapleGenVar5 = MapleGenVar6*MapleGenVar7;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar3 = MapleGenVar1;
    MapleGenVar6 = -(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*(2.0*q1i*q4i+2.0*q2i*q3i);
    MapleGenVar7 = -(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = cy/av;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar2 = MapleGenVar3+MapleGenVar4;
    MapleGenVar3 = MapleGenVar2+(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*(-2.0*q1i*q3i+2.0*q2i*q4i);
    s3[25] = MapleGenVar3+(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*(2.0*q1i*q2i+2.0*q3i*q4i)+(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i);
  }

  if ( nrhs == 1 )
  {
    s3[26] = -2.0*q1j*q2j-2.0*q3j*q4j;
    s3[27] = 2.0*q1j*q3j-2.0*q2j*q4j;
    s3[28] = -2.0*q2j*txi/tzi-2.0*q3j*tyi/tzi-(-2.0*q2j*txj-2.0*q3j*tyj-2.0*q4j*tzj)/tzi;
    s3[29] = 2.0*au*q2j+2.0*cx*q4j;
    s3[30] = -2.0*au*q1j+2.0*cx*q3j;
    s3[31] = -2.0*au*q4j+2.0*cx*q2j;
    s3[32] = -(-2.0*au*q4j+2.0*cx*q2j)*txi/tzi-(-2.0*au*q1j+2.0*cx*q3j)*tyi/tzi-(-(-2.0*au*q4j+2.0*cx*q2j)*txj-(-2.0*au*q1j+2.0*cx*q3j)*tyj-(2.0*au*q2j+2.0*cx*q4j)*tzj)/tzi;
    s3[33] = 2.0*q1j*txi/tzi-2.0*q4j*tyi/tzi-(2.0*q1j*txj-2.0*q4j*tyj+2.0*q3j*tzj)/tzi;
    s3[34] = 2.0*au*q1j-2.0*cx*q3j;
    s3[35] = -2.0*au*q3j-2.0*cx*q1j;
    s3[36] = -(-2.0*au*q3j-2.0*cx*q1j)*txi/tzi-(2.0*au*q2j+2.0*cx*q4j)*tyi/tzi-(-(-2.0*au*q3j-2.0*cx*q1j)*txj-(2.0*au*q2j+2.0*cx*q4j)*tyj-(2.0*au*q1j-2.0*cx*q3j)*tzj)/tzi;
    s3[37] = -2.0*q4j*txi/tzi-2.0*q1j*tyi/tzi-(-2.0*q4j*txj-2.0*q1j*tyj+2.0*q2j*tzj)/tzi;
    s3[38] = 2.0*au*q3j+2.0*cx*q1j;
    s3[39] = -(2.0*au*q2j+2.0*cx*q4j)*txi/tzi-(2.0*au*q3j+2.0*cx*q1j)*tyi/tzi-(-(2.0*au*q2j+2.0*cx*q4j)*txj-(2.0*au*q3j+2.0*cx*q1j)*tyj-(2.0*au*q4j-2.0*cx*q2j)*tzj)/tzi;
    s3[40] = 2.0*q3j*txi/tzi-2.0*q2j*tyi/tzi-(2.0*q3j*txj-2.0*q2j*tyj-2.0*q1j*tzj)/tzi;
    s3[41] = -(2.0*au*q1j-2.0*cx*q3j)*txi/tzi-(-2.0*au*q4j+2.0*cx*q2j)*tyi/tzi-(-(2.0*au*q1j-2.0*cx*q3j)*txj-(-2.0*au*q4j+2.0*cx*q2j)*tyj-(2.0*au*q3j+2.0*cx*q1j)*tzj)/tzi;
    s3[42] = (-2.0*q1j*q3j+2.0*q2j*q4j)*txi/(tzi*tzi)+(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/(tzi*tzi)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/(tzi*tzi);
    s3[43] = (au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/(tzi*tzi)+(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/(tzi*tzi)+(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/(tzi*tzi);
    s3[44] = 2.0*q1i*(-2.0*q1j*q3j+2.0*q2j*q4j)-2.0*q4i*(2.0*q1j*q2j+2.0*q3j*q4j)+2.0*(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*q3i;
    s3[45] = 2.0*(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*q1i-2.0*(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*q4i+2.0*(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*q3i;
    s3[46] = 2.0*q2i*(-2.0*q1j*q3j+2.0*q2j*q4j)+2.0*q3i*(2.0*q1j*q2j+2.0*q3j*q4j)+2.0*(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*q4i;
    s3[47] = 2.0*(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*q2i+2.0*(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*q3i+2.0*(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(au*(q1j*q1j+q2j*q2j-q3j*q3j-q4j*q4j)+cx*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(au*(-2.0*q1j*q4j+2.0*q2j*q3j)+cx*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(au*(2.0*q1j*q3j+2.0*q2j*q4j)+cx*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*q4i;

    s4[0] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s4[1] = -q1j*q1j+q2j*q2j+q3j*q3j-q4j*q4j;
    s4[2] = -2.0*q1i*q2i+2.0*q3i*q4i;
    s4[3] = 2.0*q1i*q3i+2.0*q2i*q4i;
    s4[4] = q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j;
    s4[5] = 2.0*q1j*q2j+2.0*q3j*q4j;
    s4[6] = -2.0*q1j*q3j+2.0*q2j*q4j;
    s4[7] = -(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj;
    s4[8] = -(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi;
    s4[9] = 2.0*q1i*q2i+2.0*q3i*q4i;
    s4[10] = -2.0*q1i*q3i+2.0*q2i*q4i;
    s4[11] = q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i;
    s4[12] = 2.0*q1i*q4i+2.0*q2i*q3i;
    s4[13] = (-2.0*q1j*q3j+2.0*q2j*q4j)*(2.0*q1i*q4i+2.0*q2i*q3i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    s4[14] = -2.0*q1i*q4i+2.0*q2i*q3i;
    s4[15] = q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i;
    s4[16] = (-2.0*q1j*q3j+2.0*q2j*q4j)*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    s4[17] = 0;
  }
  else
  {
    MapleGenVar2 = ((-2.0*q1j*q3j+2.0*q2j*q4j)*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i))/au*px;
    MapleGenVar4 = ((-2.0*q1j*q3j+2.0*q2j*q4j)*(2.0*q1i*q4i+2.0*q2i*q3i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i))/av*py;
    MapleGenVar5 = -((-2.0*q1j*q3j+2.0*q2j*q4j)*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i))*cx/au;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar2 = MapleGenVar1-((-2.0*q1j*q3j+2.0*q2j*q4j)*(2.0*q1i*q4i+2.0*q2i*q3i)+(2.0*q1j*q2j+2.0*q3j*q4j)*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i))*cy/av;
    s4[17] = MapleGenVar2+(-2.0*q1i*q3i+2.0*q2i*q4i)*(-2.0*q1j*q3j+2.0*q2j*q4j)+(2.0*q1i*q2i+2.0*q3i*q4i)*(2.0*q1j*q2j+2.0*q3j*q4j)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i);
  }

  if ( nrhs == 1 )
  {
    s4[18] = av*(-2.0*q1j*q2j+2.0*q3j*q4j);
    s4[19] = av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j);
    s4[20] = av*(2.0*q1j*q4j+2.0*q2j*q3j);
    s4[21] = -(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj;
    s4[22] = -(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi;
    s4[23] = (av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*(2.0*q1i*q4i+2.0*q2i*q3i)+(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    s4[24] = (av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    s4[25] = 0;
  }
  else
  {
    MapleGenVar4 = (av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i);
    MapleGenVar5 = (av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*(-2.0*q1i*q4i+2.0*q2i*q3i)+(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar4 = 1/au*px;
    MapleGenVar2 = MapleGenVar3*MapleGenVar4;
    MapleGenVar6 = (av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*(2.0*q1i*q4i+2.0*q2i*q3i);
    MapleGenVar7 = (av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = 1/av*py;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar7 = -(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i);
    MapleGenVar8 = -(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*(-2.0*q1i*q4i+2.0*q2i*q3i)-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(2.0*q1i*q3i+2.0*q2i*q4i);
    MapleGenVar6 = MapleGenVar7+MapleGenVar8;
    MapleGenVar7 = cx/au;
    MapleGenVar5 = MapleGenVar6*MapleGenVar7;
    MapleGenVar3 = MapleGenVar4+MapleGenVar5;
    MapleGenVar1 = MapleGenVar2+MapleGenVar3;
    MapleGenVar3 = MapleGenVar1;
    MapleGenVar6 = -(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*(2.0*q1i*q4i+2.0*q2i*q3i);
    MapleGenVar7 = -(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(-2.0*q1i*q2i+2.0*q3i*q4i);
    MapleGenVar5 = MapleGenVar6+MapleGenVar7;
    MapleGenVar6 = cy/av;
    MapleGenVar4 = MapleGenVar5*MapleGenVar6;
    MapleGenVar2 = MapleGenVar3+MapleGenVar4;
    MapleGenVar3 = MapleGenVar2+(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*(-2.0*q1i*q3i+2.0*q2i*q4i);
    s4[25] = MapleGenVar3+(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*(2.0*q1i*q2i+2.0*q3i*q4i)+(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i);
  }

  if ( nrhs == 1 )
  {
    s4[26] = -2.0*q1j*q2j-2.0*q3j*q4j;
    s4[27] = 2.0*q1j*q3j-2.0*q2j*q4j;
    s4[28] = -2.0*q2j*txi/tzi-2.0*q3j*tyi/tzi-(-2.0*q2j*txj-2.0*q3j*tyj-2.0*q4j*tzj)/tzi;
    s4[29] = 2.0*av*q3j+2.0*cy*q4j;
    s4[30] = -2.0*av*q4j+2.0*cy*q3j;
    s4[31] = 2.0*av*q1j+2.0*cy*q2j;
    s4[32] = -(2.0*av*q1j+2.0*cy*q2j)*txi/tzi-(-2.0*av*q4j+2.0*cy*q3j)*tyi/tzi-(-(2.0*av*q1j+2.0*cy*q2j)*txj-(-2.0*av*q4j+2.0*cy*q3j)*tyj-(2.0*av*q3j+2.0*cy*q4j)*tzj)/tzi;
    s4[33] = 2.0*q1j*txi/tzi-2.0*q4j*tyi/tzi-(2.0*q1j*txj-2.0*q4j*tyj+2.0*q3j*tzj)/tzi;
    s4[34] = 2.0*av*q4j-2.0*cy*q3j;
    s4[35] = 2.0*av*q2j-2.0*cy*q1j;
    s4[36] = -(2.0*av*q2j-2.0*cy*q1j)*txi/tzi-(2.0*av*q3j+2.0*cy*q4j)*tyi/tzi-(-(2.0*av*q2j-2.0*cy*q1j)*txj-(2.0*av*q3j+2.0*cy*q4j)*tyj-(2.0*av*q4j-2.0*cy*q3j)*tzj)/tzi;
    s4[37] = -2.0*q4j*txi/tzi-2.0*q1j*tyi/tzi-(-2.0*q4j*txj-2.0*q1j*tyj+2.0*q2j*tzj)/tzi;
    s4[38] = -2.0*av*q2j+2.0*cy*q1j;
    s4[39] = -(2.0*av*q3j+2.0*cy*q4j)*txi/tzi-(-2.0*av*q2j+2.0*cy*q1j)*tyi/tzi-(-(2.0*av*q3j+2.0*cy*q4j)*txj-(-2.0*av*q2j+2.0*cy*q1j)*tyj-(-2.0*av*q1j-2.0*cy*q2j)*tzj)/tzi;
    s4[40] = 2.0*q3j*txi/tzi-2.0*q2j*tyi/tzi-(2.0*q3j*txj-2.0*q2j*tyj-2.0*q1j*tzj)/tzi;
    s4[41] = -(2.0*av*q4j-2.0*cy*q3j)*txi/tzi-(2.0*av*q1j+2.0*cy*q2j)*tyi/tzi-(-(2.0*av*q4j-2.0*cy*q3j)*txj-(2.0*av*q1j+2.0*cy*q2j)*tyj-(-2.0*av*q2j+2.0*cy*q1j)*tzj)/tzi;
    s4[42] = (-2.0*q1j*q3j+2.0*q2j*q4j)*txi/(tzi*tzi)+(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/(tzi*tzi)+(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/(tzi*tzi);
    s4[43] = (av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/(tzi*tzi)+(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/(tzi*tzi)+(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/(tzi*tzi);
    s4[44] = 2.0*q1i*(-2.0*q1j*q3j+2.0*q2j*q4j)-2.0*q4i*(2.0*q1j*q2j+2.0*q3j*q4j)+2.0*(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*q3i;
    s4[45] = 2.0*(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*q1i-2.0*(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*q4i+2.0*(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*q3i;
    s4[46] = 2.0*q2i*(-2.0*q1j*q3j+2.0*q2j*q4j)+2.0*q3i*(2.0*q1j*q2j+2.0*q3j*q4j)+2.0*(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txi/tzi-(2.0*q1j*q2j+2.0*q3j*q4j)*tyi/tzi-(-(-2.0*q1j*q3j+2.0*q2j*q4j)*txj-(2.0*q1j*q2j+2.0*q3j*q4j)*tyj-(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j)*tzj)/tzi)*q4i;
    s4[47] = 2.0*(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*q2i+2.0*(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*q3i+2.0*(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txi/tzi-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyi/tzi-(-(av*(2.0*q1j*q4j+2.0*q2j*q3j)+cy*(-2.0*q1j*q3j+2.0*q2j*q4j))*txj-(av*(q1j*q1j-q2j*q2j+q3j*q3j-q4j*q4j)+cy*(2.0*q1j*q2j+2.0*q3j*q4j))*tyj-(av*(-2.0*q1j*q2j+2.0*q3j*q4j)+cy*(q1j*q1j-q2j*q2j-q3j*q3j+q4j*q4j))*tzj)/tzi)*q4i;

  }

  // Compute the jacobians
  if ( nrhs != 1 )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 14, 1, mxREAL );
    double *j1 = (double *)mxGetPr ( plhs[J1] );
    j1[0] = -((s1[40  ]*s1[15  ]+s1[37  ]*s1[14  ]+s1[47  ]*s1[7  ])/au*mx+(s1[40  ]*s1[12  ]+s1[37  ]*s1[11  ]+s1[47  ]*s1[6  ])/av*my-(s1[40  ]*s1[15  ]+s1[37  ]*s1[14  ]+s1[47  ]*s1[7  ])*cx/au-(s1[40  ]*s1[12  ]+s1[37  ]*s1[11  ]+s1[47  ]*s1[6  ])*cy/av+s1[40  ]*s1[10  ]+s1[37  ]*s1[9   ]+s1[47  ]*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*((-2.0*q3i*s1[15  ]+2.0*q2i*s1[14  ]+s1[46  ]*s1[7  ])/au*mx+(-2.0*q3i*s1[12  ]+2.0*q2i*s1[11  ]+s1[46  ]*s1[6  ])/av*my-(-2.0*q3i*s1[15  ]+2.0*q2i*s1[14  ]+s1[46  ]*s1[7  ])*cx/au-(-2.0*q3i*s1[12  ]+2.0*q2i*s1[11  ]+s1[46  ]*s1[6  ])*cy/av-2.0*q3i*s1[10  ]+2.0*q2i*s1[9   ]+s1[46  ]*s1[0  ]);
    j1[1] = -((s1[35  ]*s1[15  ]+s1[44  ]*s1[14  ]+s1[45  ]*s1[7  ])/au*mx+(s1[35  ]*s1[12  ]+s1[44  ]*s1[11  ]+s1[45  ]*s1[6  ])/av*my-(s1[35  ]*s1[15  ]+s1[44  ]*s1[14  ]+s1[45  ]*s1[7  ])*cx/au-(s1[35  ]*s1[12  ]+s1[44  ]*s1[11  ]+s1[45  ]*s1[6  ])*cy/av+s1[35  ]*s1[10  ]+s1[44  ]*s1[9   ]+s1[45  ]*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*((2.0*q4i*s1[15  ]+2.0*q1i*s1[14  ]+s1[43  ]*s1[7  ])/au*mx+(2.0*q4i*s1[12  ]+2.0*q1i*s1[11  ]+s1[43  ]*s1[6  ])/av*my-(2.0*q4i*s1[15  ]+2.0*q1i*s1[14  ]+s1[43  ]*s1[7  ])*cx/au-(2.0*q4i*s1[12  ]+2.0*q1i*s1[11  ]+s1[43  ]*s1[6  ])*cy/av+2.0*q4i*s1[10  ]+2.0*q1i*s1[9   ]+s1[43  ]*s1[0  ]);
    j1[2] = -((s1[41  ]*s1[15  ]+s1[35  ]*s1[14  ]+s1[42  ]*s1[7  ])/au*mx+(s1[41  ]*s1[12  ]+s1[35  ]*s1[11  ]+s1[42  ]*s1[6  ])/av*my-(s1[41  ]*s1[15  ]+s1[35  ]*s1[14  ]+s1[42  ]*s1[7  ])*cx/au-(s1[41  ]*s1[12  ]+s1[35  ]*s1[11  ]+s1[42  ]*s1[6  ])*cy/av+s1[41  ]*s1[10  ]+s1[35  ]*s1[9   ]+s1[42  ]*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*((-2.0*q1i*s1[15  ]+2.0*q4i*s1[14  ]+s1[39  ]*s1[7  ])/au*mx+(-2.0*q1i*s1[12  ]+2.0*q4i*s1[11  ]+s1[39  ]*s1[6  ])/av*my-(-2.0*q1i*s1[15  ]+2.0*q4i*s1[14  ]+s1[39  ]*s1[7  ])*cx/au-(-2.0*q1i*s1[12  ]+2.0*q4i*s1[11  ]+s1[39  ]*s1[6  ])*cy/av-2.0*q1i*s1[10  ]+2.0*q4i*s1[9   ]+s1[39  ]*s1[0  ]);
    j1[3] = -((s1[37  ]*s1[15  ]+s1[36  ]*s1[14  ]+s1[38  ]*s1[7  ])/au*mx+(s1[37  ]*s1[12  ]+s1[36  ]*s1[11  ]+s1[38  ]*s1[6  ])/av*my-(s1[37  ]*s1[15  ]+s1[36  ]*s1[14  ]+s1[38  ]*s1[7  ])*cx/au-(s1[37  ]*s1[12  ]+s1[36  ]*s1[11  ]+s1[38  ]*s1[6  ])*cy/av+s1[37  ]*s1[10  ]+s1[36  ]*s1[9   ]+s1[38  ]*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*((2.0*q2i*s1[15  ]+2.0*q3i*s1[14  ]+s1[34  ]*s1[7  ])/au*mx+(2.0*q2i*s1[12  ]+2.0*q3i*s1[11  ]+s1[34  ]*s1[6  ])/av*my-(2.0*q2i*s1[15  ]+2.0*q3i*s1[14  ]+s1[34  ]*s1[7  ])*cx/au-(2.0*q2i*s1[12  ]+2.0*q3i*s1[11  ]+s1[34  ]*s1[6  ])*cy/av+2.0*q2i*s1[10  ]+2.0*q3i*s1[9   ]+s1[34  ]*s1[0  ]);
    j1[4] = -(-(-s1[20  ]-cx*s1[3  ])/tzj*s1[7  ]/au*mx-(-s1[20  ]-cx*s1[3  ])/tzj*s1[6  ]/av*my+(-s1[20  ]-cx*s1[3  ])/tzj*s1[7  ]*cx/au+(-s1[20  ]-cx*s1[3  ])/tzj*s1[6  ]*cy/av-(-s1[20  ]-cx*s1[3  ])/tzj*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(-s1[33  ]/tzj*s1[7  ]/au*mx-s1[33  ]/tzj*s1[6  ]/av*my+s1[33  ]/tzj*s1[7  ]*cx/au+s1[33  ]/tzj*s1[6  ]*cy/av-s1[33  ]/tzj*s1[0  ]);
    j1[5] = -(-(-s1[19  ]-cx*s1[2  ])/tzj*s1[7  ]/au*mx-(-s1[19  ]-cx*s1[2  ])/tzj*s1[6  ]/av*my+(-s1[19  ]-cx*s1[2  ])/tzj*s1[7  ]*cx/au+(-s1[19  ]-cx*s1[2  ])/tzj*s1[6  ]*cy/av-(-s1[19  ]-cx*s1[2  ])/tzj*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(-s1[32  ]/tzj*s1[7  ]/au*mx-s1[32  ]/tzj*s1[6  ]/av*my+s1[32  ]/tzj*s1[7  ]*cx/au+s1[32  ]/tzj*s1[6  ]*cy/av-s1[32  ]/tzj*s1[0  ]);
    j1[6] = -(-(-s1[18  ]-cx*s1[1  ])/tzj*s1[7  ]/au*mx-(-s1[18  ]-cx*s1[1  ])/tzj*s1[6  ]/av*my+(-s1[18  ]-cx*s1[1  ])/tzj*s1[7  ]*cx/au+(-s1[18  ]-cx*s1[1  ])/tzj*s1[6  ]*cy/av-(-s1[18  ]-cx*s1[1  ])/tzj*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(-s1[31  ]/tzj*s1[7  ]/au*mx-s1[31  ]/tzj*s1[6  ]/av*my+s1[31  ]/tzj*s1[7  ]*cx/au+s1[31  ]/tzj*s1[6  ]*cy/av-s1[31  ]/tzj*s1[0  ]);
    j1[7] = -(s1[28  ]/au*mx+(2.0*(s1[20  ]+cx*s1[3  ])*q4j+2.0*(s1[19  ]+cx*s1[2  ])*q1j-2.0*s1[22  ]*q2j)/av*my-s1[28  ]*cx/au-(2.0*(s1[20  ]+cx*s1[3  ])*q4j+2.0*(s1[19  ]+cx*s1[2  ])*q1j-2.0*s1[22  ]*q2j)*cy/av-2.0*(s1[20  ]+cx*s1[3  ])*q3j+2.0*(s1[19  ]+cx*s1[2  ])*q2j+2.0*s1[22  ]*q1j)/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(s1[27  ]/au*mx+(2.0*s1[3  ]*q4j+2.0*s1[2  ]*q1j-2.0*s1[8  ]*q2j)/av*my-s1[27  ]*cx/au-(2.0*s1[3  ]*q4j+2.0*s1[2  ]*q1j-2.0*s1[8  ]*q2j)*cy/av-2.0*s1[3  ]*q3j+2.0*s1[2  ]*q2j+2.0*s1[8  ]*q1j);
    j1[8] = -(s1[30  ]/au*mx+(2.0*(s1[20  ]+cx*s1[3  ])*q3j-2.0*(s1[19  ]+cx*s1[2  ])*q2j-2.0*s1[22  ]*q1j)/av*my-s1[30  ]*cx/au-(2.0*(s1[20  ]+cx*s1[3  ])*q3j-2.0*(s1[19  ]+cx*s1[2  ])*q2j-2.0*s1[22  ]*q1j)*cy/av+2.0*(s1[20  ]+cx*s1[3  ])*q4j+2.0*(s1[19  ]+cx*s1[2  ])*q1j-2.0*s1[22  ]*q2j)/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(s1[29  ]/au*mx+(2.0*s1[3  ]*q3j-2.0*s1[2  ]*q2j-2.0*s1[8  ]*q1j)/av*my-s1[29  ]*cx/au-(2.0*s1[3  ]*q3j-2.0*s1[2  ]*q2j-2.0*s1[8  ]*q1j)*cy/av+2.0*s1[3  ]*q4j+2.0*s1[2  ]*q1j-2.0*s1[8  ]*q2j);
    j1[9] = -((-2.0*(s1[20  ]+cx*s1[3  ])*q3j+2.0*(s1[19  ]+cx*s1[2  ])*q2j+2.0*s1[22  ]*q1j)/au*mx+s1[30  ]/av*my-(-2.0*(s1[20  ]+cx*s1[3  ])*q3j+2.0*(s1[19  ]+cx*s1[2  ])*q2j+2.0*s1[22  ]*q1j)*cx/au-s1[30  ]*cy/av-2.0*(s1[20  ]+cx*s1[3  ])*q1j+2.0*(s1[19  ]+cx*s1[2  ])*q4j-2.0*s1[22  ]*q3j)/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*((-2.0*s1[3  ]*q3j+2.0*s1[2  ]*q2j+2.0*s1[8  ]*q1j)/au*mx+s1[29  ]/av*my-(-2.0*s1[3  ]*q3j+2.0*s1[2  ]*q2j+2.0*s1[8  ]*q1j)*cx/au-s1[29  ]*cy/av-2.0*s1[3  ]*q1j+2.0*s1[2  ]*q4j-2.0*s1[8  ]*q3j);
    j1[10] = -((-2.0*(s1[20  ]+cx*s1[3  ])*q4j-2.0*(s1[19  ]+cx*s1[2  ])*q1j+2.0*s1[22  ]*q2j)/au*mx+s1[28  ]/av*my-(-2.0*(s1[20  ]+cx*s1[3  ])*q4j-2.0*(s1[19  ]+cx*s1[2  ])*q1j+2.0*s1[22  ]*q2j)*cx/au-s1[28  ]*cy/av+2.0*(s1[20  ]+cx*s1[3  ])*q2j+2.0*(s1[19  ]+cx*s1[2  ])*q3j+2.0*s1[22  ]*q4j)/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*((-2.0*s1[3  ]*q4j-2.0*s1[2  ]*q1j+2.0*s1[8  ]*q2j)/au*mx+s1[27  ]/av*my-(-2.0*s1[3  ]*q4j-2.0*s1[2  ]*q1j+2.0*s1[8  ]*q2j)*cx/au-s1[27  ]*cy/av+2.0*s1[3  ]*q2j+2.0*s1[2  ]*q3j+2.0*s1[8  ]*q4j);
    j1[11] = -(-(s1[20  ]+cx*s1[3  ])/tzj*s1[7  ]/au*mx-(s1[20  ]+cx*s1[3  ])/tzj*s1[6  ]/av*my+(s1[20  ]+cx*s1[3  ])/tzj*s1[7  ]*cx/au+(s1[20  ]+cx*s1[3  ])/tzj*s1[6  ]*cy/av-(s1[20  ]+cx*s1[3  ])/tzj*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(-s1[3  ]/tzj*s1[7  ]/au*mx-s1[3  ]/tzj*s1[6  ]/av*my+s1[3  ]/tzj*s1[7  ]*cx/au+s1[3  ]/tzj*s1[6  ]*cy/av-s1[3  ]/tzj*s1[0  ]);
    j1[12] = -(-(s1[19  ]+cx*s1[2  ])/tzj*s1[7  ]/au*mx-(s1[19  ]+cx*s1[2  ])/tzj*s1[6  ]/av*my+(s1[19  ]+cx*s1[2  ])/tzj*s1[7  ]*cx/au+(s1[19  ]+cx*s1[2  ])/tzj*s1[6  ]*cy/av-(s1[19  ]+cx*s1[2  ])/tzj*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(-s1[2  ]/tzj*s1[7  ]/au*mx-s1[2  ]/tzj*s1[6  ]/av*my+s1[2  ]/tzj*s1[7  ]*cx/au+s1[2  ]/tzj*s1[6  ]*cy/av-s1[2  ]/tzj*s1[0  ]);
    j1[13] = -(s1[26  ]*s1[7  ]/au*mx+s1[26  ]*s1[6  ]/av*my-s1[26  ]*s1[7  ]*cx/au-s1[26  ]*s1[6  ]*cy/av+s1[26  ]*s1[0  ])/s1[17  ]+s1[25  ]/pow(s1[17  ],2.0)*(s1[5  ]*s1[7  ]/au*mx+s1[5  ]*s1[6  ]/av*my-s1[5  ]*s1[7  ]*cx/au-s1[5  ]*s1[6  ]*cy/av+s1[5  ]*s1[0  ]);

    plhs[J2] = mxCreateDoubleMatrix ( 14, 1, mxREAL );
    double *j2 = (double *)mxGetPr ( plhs[J2] );
    MapleGenVar1 = -((s2[37  ]*s2[16  ]+s2[34  ]*s2[15  ]+s2[44  ]*s2[8  ])/au*mx+(s2[37  ]*s2[18  ]+s2[34  ]*s2[17  ]+s2[44  ]*s2[7  ])/av*my-(s2[37  ]*s2[16  ]+s2[34  ]*s2[15  ]+s2[44  ]*s2[8  ])*cx/au-(s2[37  ]*s2[18  ]+s2[34  ]*s2[17  ]+s2[44  ]*s2[7  ])*cy/av+s2[37  ]*s2[11  ]+s2[34  ]*s2[10  ]+s2[44  ]*s2[0  ])/s2[19  ];
    MapleGenVar2 = s2[27  ]/pow(s2[19  ],2.0)*((-2.0*q3i*s2[16  ]+2.0*q2i*s2[15  ]+s2[43  ]*s2[8  ])/au*mx+(-2.0*q3i*s2[18  ]+2.0*q2i*s2[17  ]+s2[43  ]*s2[7  ])/av*my+(2.0*q3i*s2[16  ]-2.0*q2i*s2[15  ]-s2[43  ]*s2[8  ])*cx/au+(-2.0*s2[14  ]*q3i+2.0*s2[13  ]*q2i+(-2.0*q3i*txj/tzj+2.0*q2i*tyj/tzj-(-2.0*q3i*txi+2.0*q2i*tyi+2.0*q1i*tzi)/tzj)*s2[7  ])*cy/av-2.0*q3i*s2[11  ]+2.0*q2i*s2[10  ]+s2[43  ]*s2[0  ]);
    j2[0] = MapleGenVar1+MapleGenVar2;
    MapleGenVar1 = -((s2[32  ]*s2[16  ]+s2[41  ]*s2[15  ]+s2[42  ]*s2[8  ])/au*mx+(s2[32  ]*s2[18  ]+s2[41  ]*s2[17  ]+s2[42  ]*s2[7  ])/av*my-(s2[32  ]*s2[16  ]+s2[41  ]*s2[15  ]+s2[42  ]*s2[8  ])*cx/au-(s2[32  ]*s2[18  ]+s2[41  ]*s2[17  ]+s2[42  ]*s2[7  ])*cy/av+s2[32  ]*s2[11  ]+s2[41  ]*s2[10  ]+s2[42  ]*s2[0  ])/s2[19  ];
    MapleGenVar2 = s2[27  ]/pow(s2[19  ],2.0)*((2.0*q4i*s2[16  ]+2.0*q1i*s2[15  ]+s2[40  ]*s2[8  ])/au*mx+(2.0*q4i*s2[18  ]+2.0*q1i*s2[17  ]+s2[40  ]*s2[7  ])/av*my+(-2.0*q4i*s2[16  ]-2.0*q1i*s2[15  ]-s2[40  ]*s2[8  ])*cx/au+(2.0*s2[14  ]*q4i+2.0*s2[13  ]*q1i+(2.0*q4i*txj/tzj+2.0*q1i*tyj/tzj-(2.0*q4i*txi+2.0*q1i*tyi-2.0*q2i*tzi)/tzj)*s2[7  ])*cy/av+2.0*q4i*s2[11  ]+2.0*q1i*s2[10  ]+s2[40  ]*s2[0  ]);
    j2[1] = MapleGenVar1+MapleGenVar2;
    MapleGenVar1 = -((s2[38  ]*s2[16  ]+s2[32  ]*s2[15  ]+s2[39  ]*s2[8  ])/au*mx+(s2[38  ]*s2[18  ]+s2[32  ]*s2[17  ]+s2[39  ]*s2[7  ])/av*my-(s2[38  ]*s2[16  ]+s2[32  ]*s2[15  ]+s2[39  ]*s2[8  ])*cx/au-(s2[38  ]*s2[18  ]+s2[32  ]*s2[17  ]+s2[39  ]*s2[7  ])*cy/av+s2[38  ]*s2[11  ]+s2[32  ]*s2[10  ]+s2[39  ]*s2[0  ])/s2[19  ];
    MapleGenVar2 = s2[27  ]/pow(s2[19  ],2.0)*((-2.0*q1i*s2[16  ]+2.0*q4i*s2[15  ]+s2[36  ]*s2[8  ])/au*mx+(-2.0*q1i*s2[18  ]+2.0*q4i*s2[17  ]+s2[36  ]*s2[7  ])/av*my+(2.0*q1i*s2[16  ]-2.0*q4i*s2[15  ]-s2[36  ]*s2[8  ])*cx/au+(-2.0*s2[14  ]*q1i+2.0*s2[13  ]*q4i+(-2.0*q1i*txj/tzj+2.0*q4i*tyj/tzj-(-2.0*q1i*txi+2.0*q4i*tyi-2.0*q3i*tzi)/tzj)*s2[7  ])*cy/av-2.0*q1i*s2[11  ]+2.0*q4i*s2[10  ]+s2[36  ]*s2[0  ]);
    j2[2] = MapleGenVar1+MapleGenVar2;
    MapleGenVar1 = -((s2[34  ]*s2[16  ]+s2[33  ]*s2[15  ]+s2[35  ]*s2[8  ])/au*mx+(s2[34  ]*s2[18  ]+s2[33  ]*s2[17  ]+s2[35  ]*s2[7  ])/av*my-(s2[34  ]*s2[16  ]+s2[33  ]*s2[15  ]+s2[35  ]*s2[8  ])*cx/au-(s2[34  ]*s2[18  ]+s2[33  ]*s2[17  ]+s2[35  ]*s2[7  ])*cy/av+s2[34  ]*s2[11  ]+s2[33  ]*s2[10  ]+s2[35  ]*s2[0  ])/s2[19  ];
    MapleGenVar2 = s2[27  ]/pow(s2[19  ],2.0)*((2.0*q2i*s2[16  ]+2.0*q3i*s2[15  ]+s2[31  ]*s2[8  ])/au*mx+(2.0*q2i*s2[18  ]+2.0*q3i*s2[17  ]+s2[31  ]*s2[7  ])/av*my+(-2.0*q2i*s2[16  ]-2.0*q3i*s2[15  ]-s2[31  ]*s2[8  ])*cx/au+(2.0*s2[14  ]*q2i+2.0*s2[13  ]*q3i+(2.0*q2i*txj/tzj+2.0*q3i*tyj/tzj-(2.0*q2i*txi+2.0*q3i*tyi+2.0*q4i*tzi)/tzj)*s2[7  ])*cy/av+2.0*q2i*s2[11  ]+2.0*q3i*s2[10  ]+s2[31  ]*s2[0  ]);
    j2[3] = MapleGenVar1+MapleGenVar2;
    j2[4] = -(s2[22  ]/tzj*s2[8  ]/au*mx+s2[22  ]/tzj*s2[7  ]/av*my-s2[22  ]/tzj*s2[8  ]*cx/au-s2[22  ]/tzj*s2[7  ]*cy/av+s2[22  ]/tzj*s2[0  ])/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*(s2[3  ]/tzj*s2[8  ]/au*mx+s2[3  ]/tzj*s2[7  ]/av*my-s2[3  ]/tzj*s2[8  ]*cx/au-s2[3  ]/tzj*s2[7  ]*cy/av+s2[3  ]/tzj*s2[0  ]);
    j2[5] = -(s2[21  ]/tzj*s2[8  ]/au*mx+s2[21  ]/tzj*s2[7  ]/av*my-s2[21  ]/tzj*s2[8  ]*cx/au-s2[21  ]/tzj*s2[7  ]*cy/av+s2[21  ]/tzj*s2[0  ])/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*(s2[2  ]/tzj*s2[8  ]/au*mx+s2[2  ]/tzj*s2[7  ]/av*my-s2[2  ]/tzj*s2[8  ]*cx/au-s2[2  ]/tzj*s2[7  ]*cy/av+s2[2  ]/tzj*s2[0  ]);
    j2[6] = -(s2[20  ]/tzj*s2[8  ]/au*mx+s2[20  ]/tzj*s2[7  ]/av*my-s2[20  ]/tzj*s2[8  ]*cx/au-s2[20  ]/tzj*s2[7  ]*cy/av+s2[20  ]/tzj*s2[0  ])/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*(s2[1  ]/tzj*s2[8  ]/au*mx+s2[1  ]/tzj*s2[7  ]/av*my-s2[1  ]/tzj*s2[8  ]*cx/au-s2[1  ]/tzj*s2[7  ]*cy/av+s2[1  ]/tzj*s2[0  ]);
    j2[7] = -(s2[29  ]/au*mx+(2.0*s2[22  ]*q4j+2.0*s2[21  ]*q1j-2.0*s2[24  ]*q2j)/av*my-s2[29  ]*cx/au-(2.0*s2[22  ]*q4j+2.0*s2[21  ]*q1j-2.0*s2[24  ]*q2j)*cy/av-2.0*s2[22  ]*q3j+2.0*s2[21  ]*q2j+2.0*s2[24  ]*q1j)/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*((2.0*s2[3  ]*q1j-2.0*s2[2  ]*q4j+2.0*s2[9   ]*q3j)/au*mx+(2.0*s2[3  ]*q4j+2.0*s2[2  ]*q1j-2.0*s2[9   ]*q2j)/av*my+(-2.0*s2[3  ]*q1j+2.0*s2[2  ]*q4j-2.0*s2[9   ]*q3j)*cx/au+(-2.0*s2[3  ]*q4j-2.0*s2[2  ]*q1j-2.0*s2[12  ]*q2j)*cy/av-2.0*s2[3  ]*q3j+2.0*s2[2  ]*q2j+2.0*s2[9   ]*q1j);
    j2[8] = -(s2[30  ]/au*mx+(2.0*s2[22  ]*q3j-2.0*s2[21  ]*q2j-2.0*s2[24  ]*q1j)/av*my-s2[30  ]*cx/au-(2.0*s2[22  ]*q3j-2.0*s2[21  ]*q2j-2.0*s2[24  ]*q1j)*cy/av+2.0*s2[22  ]*q4j+2.0*s2[21  ]*q1j-2.0*s2[24  ]*q2j)/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*((2.0*s2[3  ]*q2j+2.0*s2[2  ]*q3j+2.0*s2[9   ]*q4j)/au*mx+(2.0*s2[3  ]*q3j-2.0*s2[2  ]*q2j-2.0*s2[9   ]*q1j)/av*my+(-2.0*s2[3  ]*q2j-2.0*s2[2  ]*q3j-2.0*s2[9   ]*q4j)*cx/au+(-2.0*s2[3  ]*q3j+2.0*s2[2  ]*q2j-2.0*s2[12  ]*q1j)*cy/av+2.0*s2[3  ]*q4j+2.0*s2[2  ]*q1j-2.0*s2[9   ]*q2j);
    j2[9] = -((-2.0*s2[22  ]*q3j+2.0*s2[21  ]*q2j+2.0*s2[24  ]*q1j)/au*mx+s2[30  ]/av*my-(-2.0*s2[22  ]*q3j+2.0*s2[21  ]*q2j+2.0*s2[24  ]*q1j)*cx/au-s2[30  ]*cy/av-2.0*s2[22  ]*q1j+2.0*s2[21  ]*q4j-2.0*s2[24  ]*q3j)/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*((-2.0*s2[3  ]*q3j+2.0*s2[2  ]*q2j+2.0*s2[9   ]*q1j)/au*mx+(2.0*s2[3  ]*q2j+2.0*s2[2  ]*q3j+2.0*s2[9   ]*q4j)/av*my+(2.0*s2[3  ]*q3j-2.0*s2[2  ]*q2j-2.0*s2[9   ]*q1j)*cx/au+(-2.0*s2[3  ]*q2j-2.0*s2[2  ]*q3j+2.0*s2[12  ]*q4j)*cy/av-2.0*s2[3  ]*q1j+2.0*s2[2  ]*q4j-2.0*s2[9   ]*q3j);
    j2[10] = -((-2.0*s2[22  ]*q4j-2.0*s2[21  ]*q1j+2.0*s2[24  ]*q2j)/au*mx+s2[29  ]/av*my-(-2.0*s2[22  ]*q4j-2.0*s2[21  ]*q1j+2.0*s2[24  ]*q2j)*cx/au-s2[29  ]*cy/av+2.0*s2[22  ]*q2j+2.0*s2[21  ]*q3j+2.0*s2[24  ]*q4j)/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*((-2.0*s2[3  ]*q4j-2.0*s2[2  ]*q1j+2.0*s2[9   ]*q2j)/au*mx+(2.0*s2[3  ]*q1j-2.0*s2[2  ]*q4j+2.0*s2[9   ]*q3j)/av*my+(2.0*s2[3  ]*q4j+2.0*s2[2  ]*q1j-2.0*s2[9   ]*q2j)*cx/au+(-2.0*s2[3  ]*q1j+2.0*s2[2  ]*q4j+2.0*s2[12  ]*q3j)*cy/av+2.0*s2[3  ]*q2j+2.0*s2[2  ]*q3j+2.0*s2[9   ]*q4j);
    j2[11] = -(-s2[22  ]/tzj*s2[8  ]/au*mx-s2[22  ]/tzj*s2[7  ]/av*my+s2[22  ]/tzj*s2[8  ]*cx/au+s2[22  ]/tzj*s2[7  ]*cy/av-s2[22  ]/tzj*s2[0  ])/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*(-s2[3  ]/tzj*s2[8  ]/au*mx-s2[3  ]/tzj*s2[7  ]/av*my+s2[3  ]/tzj*s2[8  ]*cx/au+s2[3  ]/tzj*s2[7  ]*cy/av-s2[3  ]/tzj*s2[0  ]);
    j2[12] = -(-s2[21  ]/tzj*s2[8  ]/au*mx-s2[21  ]/tzj*s2[7  ]/av*my+s2[21  ]/tzj*s2[8  ]*cx/au+s2[21  ]/tzj*s2[7  ]*cy/av-s2[21  ]/tzj*s2[0  ])/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*(s2[5  ]/tzj*s2[8  ]/au*mx+s2[5  ]/tzj*s2[7  ]/av*my-s2[5  ]/tzj*s2[8  ]*cx/au-s2[5  ]/tzj*s2[7  ]*cy/av+s2[5  ]/tzj*s2[0  ]);
    j2[13] = -(s2[28  ]*s2[8  ]/au*mx+s2[28  ]*s2[7  ]/av*my-s2[28  ]*s2[8  ]*cx/au-s2[28  ]*s2[7  ]*cy/av+s2[28  ]*s2[0  ])/s2[19  ]+s2[27  ]/pow(s2[19  ],2.0)*(s2[6  ]*s2[8  ]/au*mx+s2[6  ]*s2[7  ]/av*my-s2[6  ]*s2[8  ]*cx/au+(-s2[3  ]*txj/(tzj*tzj)+s2[5  ]*tyj/(tzj*tzj)+s2[4  ]/(tzj*tzj))*s2[7  ]*cy/av+s2[6  ]*s2[0  ]);

    plhs[J3] = mxCreateDoubleMatrix ( 14, 1, mxREAL );
    double *j3 = (double *)mxGetPr ( plhs[J3] );
    j3[0] = -(s3[45  ]/au*px+(2.0*(s3[20  ]+cx*s3[6  ])*q4i+2.0*(s3[19  ]+cx*s3[5  ])*q1i-2.0*s3[22  ]*q2i)/av*py-s3[45  ]*cx/au-(2.0*(s3[20  ]+cx*s3[6  ])*q4i+2.0*(s3[19  ]+cx*s3[5  ])*q1i-2.0*s3[22  ]*q2i)*cy/av-2.0*(s3[20  ]+cx*s3[6  ])*q3i+2.0*(s3[19  ]+cx*s3[5  ])*q2i+2.0*s3[22  ]*q1i)/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(s3[44  ]/au*px+(2.0*q4i*s3[6  ]+2.0*q1i*s3[5  ]-2.0*s3[8  ]*q2i)/av*py-s3[44  ]*cx/au-(2.0*q4i*s3[6  ]+2.0*q1i*s3[5  ]-2.0*s3[8  ]*q2i)*cy/av-2.0*q3i*s3[6  ]+2.0*q2i*s3[5  ]+2.0*s3[8  ]*q1i);
    j3[1] = -(s3[47  ]/au*px+(2.0*(s3[20  ]+cx*s3[6  ])*q3i-2.0*(s3[19  ]+cx*s3[5  ])*q2i-2.0*s3[22  ]*q1i)/av*py-s3[47  ]*cx/au-(2.0*(s3[20  ]+cx*s3[6  ])*q3i-2.0*(s3[19  ]+cx*s3[5  ])*q2i-2.0*s3[22  ]*q1i)*cy/av+2.0*(s3[20  ]+cx*s3[6  ])*q4i+2.0*(s3[19  ]+cx*s3[5  ])*q1i-2.0*s3[22  ]*q2i)/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(s3[46  ]/au*px+(2.0*q3i*s3[6  ]-2.0*q2i*s3[5  ]-2.0*s3[8  ]*q1i)/av*py-s3[46  ]*cx/au-(2.0*q3i*s3[6  ]-2.0*q2i*s3[5  ]-2.0*s3[8  ]*q1i)*cy/av+2.0*q4i*s3[6  ]+2.0*q1i*s3[5  ]-2.0*s3[8  ]*q2i);
    j3[2] = -((-2.0*(s3[20  ]+cx*s3[6  ])*q3i+2.0*(s3[19  ]+cx*s3[5  ])*q2i+2.0*s3[22  ]*q1i)/au*px+s3[47  ]/av*py-(-2.0*(s3[20  ]+cx*s3[6  ])*q3i+2.0*(s3[19  ]+cx*s3[5  ])*q2i+2.0*s3[22  ]*q1i)*cx/au-s3[47  ]*cy/av-2.0*(s3[20  ]+cx*s3[6  ])*q1i+2.0*(s3[19  ]+cx*s3[5  ])*q4i-2.0*s3[22  ]*q3i)/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*((-2.0*q3i*s3[6  ]+2.0*q2i*s3[5  ]+2.0*s3[8  ]*q1i)/au*px+s3[46  ]/av*py-(-2.0*q3i*s3[6  ]+2.0*q2i*s3[5  ]+2.0*s3[8  ]*q1i)*cx/au-s3[46  ]*cy/av-2.0*q1i*s3[6  ]+2.0*q4i*s3[5  ]-2.0*s3[8  ]*q3i);
    j3[3] = -((-2.0*(s3[20  ]+cx*s3[6  ])*q4i-2.0*(s3[19  ]+cx*s3[5  ])*q1i+2.0*s3[22  ]*q2i)/au*px+s3[45  ]/av*py-(-2.0*(s3[20  ]+cx*s3[6  ])*q4i-2.0*(s3[19  ]+cx*s3[5  ])*q1i+2.0*s3[22  ]*q2i)*cx/au-s3[45  ]*cy/av+2.0*(s3[20  ]+cx*s3[6  ])*q2i+2.0*(s3[19  ]+cx*s3[5  ])*q3i+2.0*s3[22  ]*q4i)/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*((-2.0*q4i*s3[6  ]-2.0*q1i*s3[5  ]+2.0*s3[8  ]*q2i)/au*px+s3[44  ]/av*py-(-2.0*q4i*s3[6  ]-2.0*q1i*s3[5  ]+2.0*s3[8  ]*q2i)*cx/au-s3[44  ]*cy/av+2.0*q2i*s3[6  ]+2.0*q3i*s3[5  ]+2.0*s3[8  ]*q4i);
    j3[4] = -(-(s3[20  ]+cx*s3[6  ])/tzi*s3[3  ]/au*px-(s3[20  ]+cx*s3[6  ])/tzi*s3[2  ]/av*py+(s3[20  ]+cx*s3[6  ])/tzi*s3[3  ]*cx/au+(s3[20  ]+cx*s3[6  ])/tzi*s3[2  ]*cy/av-(s3[20  ]+cx*s3[6  ])/tzi*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(-s3[6  ]/tzi*s3[3  ]/au*px-s3[6  ]/tzi*s3[2  ]/av*py+s3[6  ]/tzi*s3[3  ]*cx/au+s3[6  ]/tzi*s3[2  ]*cy/av-s3[6  ]/tzi*s3[0  ]);
    j3[5] = -(-(s3[19  ]+cx*s3[5  ])/tzi*s3[3  ]/au*px-(s3[19  ]+cx*s3[5  ])/tzi*s3[2  ]/av*py+(s3[19  ]+cx*s3[5  ])/tzi*s3[3  ]*cx/au+(s3[19  ]+cx*s3[5  ])/tzi*s3[2  ]*cy/av-(s3[19  ]+cx*s3[5  ])/tzi*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(-s3[5  ]/tzi*s3[3  ]/au*px-s3[5  ]/tzi*s3[2  ]/av*py+s3[5  ]/tzi*s3[3  ]*cx/au+s3[5  ]/tzi*s3[2  ]*cy/av-s3[5  ]/tzi*s3[0  ]);
    j3[6] = -(s3[43  ]*s3[3  ]/au*px+s3[43  ]*s3[2  ]/av*py-s3[43  ]*s3[3  ]*cx/au-s3[43  ]*s3[2  ]*cy/av+s3[43  ]*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(s3[42  ]*s3[3  ]/au*px+s3[42  ]*s3[2  ]/av*py-s3[42  ]*s3[3  ]*cx/au-s3[42  ]*s3[2  ]*cy/av+s3[42  ]*s3[0  ]);
    j3[7] = -((s3[34  ]*s3[15  ]+s3[31  ]*s3[14  ]+s3[41  ]*s3[3  ])/au*px+(s3[34  ]*s3[12  ]+s3[31  ]*s3[11  ]+s3[41  ]*s3[2  ])/av*py-(s3[34  ]*s3[15  ]+s3[31  ]*s3[14  ]+s3[41  ]*s3[3  ])*cx/au-(s3[34  ]*s3[12  ]+s3[31  ]*s3[11  ]+s3[41  ]*s3[2  ])*cy/av+s3[34  ]*s3[10  ]+s3[31  ]*s3[9   ]+s3[41  ]*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*((-2.0*q3j*s3[15  ]+2.0*q2j*s3[14  ]+s3[40  ]*s3[3  ])/au*px+(-2.0*q3j*s3[12  ]+2.0*q2j*s3[11  ]+s3[40  ]*s3[2  ])/av*py-(-2.0*q3j*s3[15  ]+2.0*q2j*s3[14  ]+s3[40  ]*s3[3  ])*cx/au-(-2.0*q3j*s3[12  ]+2.0*q2j*s3[11  ]+s3[40  ]*s3[2  ])*cy/av-2.0*s3[10  ]*q3j+2.0*s3[9   ]*q2j+s3[40  ]*s3[0  ]);
    j3[8] = -((s3[29  ]*s3[15  ]+s3[38  ]*s3[14  ]+s3[39  ]*s3[3  ])/au*px+(s3[29  ]*s3[12  ]+s3[38  ]*s3[11  ]+s3[39  ]*s3[2  ])/av*py-(s3[29  ]*s3[15  ]+s3[38  ]*s3[14  ]+s3[39  ]*s3[3  ])*cx/au-(s3[29  ]*s3[12  ]+s3[38  ]*s3[11  ]+s3[39  ]*s3[2  ])*cy/av+s3[29  ]*s3[10  ]+s3[38  ]*s3[9   ]+s3[39  ]*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*((2.0*q4j*s3[15  ]+2.0*q1j*s3[14  ]+s3[37  ]*s3[3  ])/au*px+(2.0*q4j*s3[12  ]+2.0*q1j*s3[11  ]+s3[37  ]*s3[2  ])/av*py-(2.0*q4j*s3[15  ]+2.0*q1j*s3[14  ]+s3[37  ]*s3[3  ])*cx/au-(2.0*q4j*s3[12  ]+2.0*q1j*s3[11  ]+s3[37  ]*s3[2  ])*cy/av+2.0*s3[10  ]*q4j+2.0*s3[9   ]*q1j+s3[37  ]*s3[0  ]);
    j3[9] = -((s3[35  ]*s3[15  ]+s3[29  ]*s3[14  ]+s3[36  ]*s3[3  ])/au*px+(s3[35  ]*s3[12  ]+s3[29  ]*s3[11  ]+s3[36  ]*s3[2  ])/av*py-(s3[35  ]*s3[15  ]+s3[29  ]*s3[14  ]+s3[36  ]*s3[3  ])*cx/au-(s3[35  ]*s3[12  ]+s3[29  ]*s3[11  ]+s3[36  ]*s3[2  ])*cy/av+s3[35  ]*s3[10  ]+s3[29  ]*s3[9   ]+s3[36  ]*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*((-2.0*q1j*s3[15  ]+2.0*q4j*s3[14  ]+s3[33  ]*s3[3  ])/au*px+(-2.0*q1j*s3[12  ]+2.0*q4j*s3[11  ]+s3[33  ]*s3[2  ])/av*py-(-2.0*q1j*s3[15  ]+2.0*q4j*s3[14  ]+s3[33  ]*s3[3  ])*cx/au-(-2.0*q1j*s3[12  ]+2.0*q4j*s3[11  ]+s3[33  ]*s3[2  ])*cy/av-2.0*s3[10  ]*q1j+2.0*s3[9   ]*q4j+s3[33  ]*s3[0  ]);
    j3[10] = -((s3[31  ]*s3[15  ]+s3[30  ]*s3[14  ]+s3[32  ]*s3[3  ])/au*px+(s3[31  ]*s3[12  ]+s3[30  ]*s3[11  ]+s3[32  ]*s3[2  ])/av*py-(s3[31  ]*s3[15  ]+s3[30  ]*s3[14  ]+s3[32  ]*s3[3  ])*cx/au-(s3[31  ]*s3[12  ]+s3[30  ]*s3[11  ]+s3[32  ]*s3[2  ])*cy/av+s3[31  ]*s3[10  ]+s3[30  ]*s3[9   ]+s3[32  ]*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*((2.0*q2j*s3[15  ]+2.0*q3j*s3[14  ]+s3[28  ]*s3[3  ])/au*px+(2.0*q2j*s3[12  ]+2.0*q3j*s3[11  ]+s3[28  ]*s3[2  ])/av*py-(2.0*q2j*s3[15  ]+2.0*q3j*s3[14  ]+s3[28  ]*s3[3  ])*cx/au-(2.0*q2j*s3[12  ]+2.0*q3j*s3[11  ]+s3[28  ]*s3[2  ])*cy/av+2.0*s3[10  ]*q2j+2.0*s3[9   ]*q3j+s3[28  ]*s3[0  ]);
    j3[11] = -(-(-s3[20  ]-cx*s3[6  ])/tzi*s3[3  ]/au*px-(-s3[20  ]-cx*s3[6  ])/tzi*s3[2  ]/av*py+(-s3[20  ]-cx*s3[6  ])/tzi*s3[3  ]*cx/au+(-s3[20  ]-cx*s3[6  ])/tzi*s3[2  ]*cy/av-(-s3[20  ]-cx*s3[6  ])/tzi*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(-s3[27  ]/tzi*s3[3  ]/au*px-s3[27  ]/tzi*s3[2  ]/av*py+s3[27  ]/tzi*s3[3  ]*cx/au+s3[27  ]/tzi*s3[2  ]*cy/av-s3[27  ]/tzi*s3[0  ]);
    j3[12] = -(-(-s3[19  ]-cx*s3[5  ])/tzi*s3[3  ]/au*px-(-s3[19  ]-cx*s3[5  ])/tzi*s3[2  ]/av*py+(-s3[19  ]-cx*s3[5  ])/tzi*s3[3  ]*cx/au+(-s3[19  ]-cx*s3[5  ])/tzi*s3[2  ]*cy/av-(-s3[19  ]-cx*s3[5  ])/tzi*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(-s3[26  ]/tzi*s3[3  ]/au*px-s3[26  ]/tzi*s3[2  ]/av*py+s3[26  ]/tzi*s3[3  ]*cx/au+s3[26  ]/tzi*s3[2  ]*cy/av-s3[26  ]/tzi*s3[0  ]);
    j3[13] = -(-(-s3[18  ]-cx*s3[4  ])/tzi*s3[3  ]/au*px-(-s3[18  ]-cx*s3[4  ])/tzi*s3[2  ]/av*py+(-s3[18  ]-cx*s3[4  ])/tzi*s3[3  ]*cx/au+(-s3[18  ]-cx*s3[4  ])/tzi*s3[2  ]*cy/av-(-s3[18  ]-cx*s3[4  ])/tzi*s3[0  ])/s3[17  ]+s3[25  ]/pow(s3[17  ],2.0)*(-s3[1  ]/tzi*s3[3  ]/au*px-s3[1  ]/tzi*s3[2  ]/av*py+s3[1  ]/tzi*s3[3  ]*cx/au+s3[1  ]/tzi*s3[2  ]*cy/av-s3[1  ]/tzi*s3[0  ]);

    plhs[J4] = mxCreateDoubleMatrix ( 14, 1, mxREAL );
    double *j4 = (double *)mxGetPr ( plhs[J4] );
    j4[0] = -(s4[45  ]/au*px+(2.0*(s4[20  ]+cy*s4[6  ])*q4i+2.0*(s4[19  ]+cy*s4[5  ])*q1i-2.0*s4[22  ]*q2i)/av*py-s4[45  ]*cx/au-(2.0*(s4[20  ]+cy*s4[6  ])*q4i+2.0*(s4[19  ]+cy*s4[5  ])*q1i-2.0*s4[22  ]*q2i)*cy/av-2.0*(s4[20  ]+cy*s4[6  ])*q3i+2.0*(s4[19  ]+cy*s4[5  ])*q2i+2.0*s4[22  ]*q1i)/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(s4[44  ]/au*px+(2.0*q4i*s4[6  ]+2.0*q1i*s4[5  ]-2.0*s4[8  ]*q2i)/av*py-s4[44  ]*cx/au-(2.0*q4i*s4[6  ]+2.0*q1i*s4[5  ]-2.0*s4[8  ]*q2i)*cy/av-2.0*q3i*s4[6  ]+2.0*q2i*s4[5  ]+2.0*s4[8  ]*q1i);
    j4[1] = -(s4[47  ]/au*px+(2.0*(s4[20  ]+cy*s4[6  ])*q3i-2.0*(s4[19  ]+cy*s4[5  ])*q2i-2.0*s4[22  ]*q1i)/av*py-s4[47  ]*cx/au-(2.0*(s4[20  ]+cy*s4[6  ])*q3i-2.0*(s4[19  ]+cy*s4[5  ])*q2i-2.0*s4[22  ]*q1i)*cy/av+2.0*(s4[20  ]+cy*s4[6  ])*q4i+2.0*(s4[19  ]+cy*s4[5  ])*q1i-2.0*s4[22  ]*q2i)/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(s4[46  ]/au*px+(2.0*q3i*s4[6  ]-2.0*q2i*s4[5  ]-2.0*s4[8  ]*q1i)/av*py-s4[46  ]*cx/au-(2.0*q3i*s4[6  ]-2.0*q2i*s4[5  ]-2.0*s4[8  ]*q1i)*cy/av+2.0*q4i*s4[6  ]+2.0*q1i*s4[5  ]-2.0*s4[8  ]*q2i);
    j4[2] = -((-2.0*(s4[20  ]+cy*s4[6  ])*q3i+2.0*(s4[19  ]+cy*s4[5  ])*q2i+2.0*s4[22  ]*q1i)/au*px+s4[47  ]/av*py-(-2.0*(s4[20  ]+cy*s4[6  ])*q3i+2.0*(s4[19  ]+cy*s4[5  ])*q2i+2.0*s4[22  ]*q1i)*cx/au-s4[47  ]*cy/av-2.0*(s4[20  ]+cy*s4[6  ])*q1i+2.0*(s4[19  ]+cy*s4[5  ])*q4i-2.0*s4[22  ]*q3i)/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*((-2.0*q3i*s4[6  ]+2.0*q2i*s4[5  ]+2.0*s4[8  ]*q1i)/au*px+s4[46  ]/av*py-(-2.0*q3i*s4[6  ]+2.0*q2i*s4[5  ]+2.0*s4[8  ]*q1i)*cx/au-s4[46  ]*cy/av-2.0*q1i*s4[6  ]+2.0*q4i*s4[5  ]-2.0*s4[8  ]*q3i);
    j4[3] = -((-2.0*(s4[20  ]+cy*s4[6  ])*q4i-2.0*(s4[19  ]+cy*s4[5  ])*q1i+2.0*s4[22  ]*q2i)/au*px+s4[45  ]/av*py-(-2.0*(s4[20  ]+cy*s4[6  ])*q4i-2.0*(s4[19  ]+cy*s4[5  ])*q1i+2.0*s4[22  ]*q2i)*cx/au-s4[45  ]*cy/av+2.0*(s4[20  ]+cy*s4[6  ])*q2i+2.0*(s4[19  ]+cy*s4[5  ])*q3i+2.0*s4[22  ]*q4i)/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*((-2.0*q4i*s4[6  ]-2.0*q1i*s4[5  ]+2.0*s4[8  ]*q2i)/au*px+s4[44  ]/av*py-(-2.0*q4i*s4[6  ]-2.0*q1i*s4[5  ]+2.0*s4[8  ]*q2i)*cx/au-s4[44  ]*cy/av+2.0*q2i*s4[6  ]+2.0*q3i*s4[5  ]+2.0*s4[8  ]*q4i);
    j4[4] = -(-(s4[20  ]+cy*s4[6  ])/tzi*s4[3  ]/au*px-(s4[20  ]+cy*s4[6  ])/tzi*s4[2  ]/av*py+(s4[20  ]+cy*s4[6  ])/tzi*s4[3  ]*cx/au+(s4[20  ]+cy*s4[6  ])/tzi*s4[2  ]*cy/av-(s4[20  ]+cy*s4[6  ])/tzi*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(-s4[6  ]/tzi*s4[3  ]/au*px-s4[6  ]/tzi*s4[2  ]/av*py+s4[6  ]/tzi*s4[3  ]*cx/au+s4[6  ]/tzi*s4[2  ]*cy/av-s4[6  ]/tzi*s4[0  ]);
    j4[5] = -(-(s4[19  ]+cy*s4[5  ])/tzi*s4[3  ]/au*px-(s4[19  ]+cy*s4[5  ])/tzi*s4[2  ]/av*py+(s4[19  ]+cy*s4[5  ])/tzi*s4[3  ]*cx/au+(s4[19  ]+cy*s4[5  ])/tzi*s4[2  ]*cy/av-(s4[19  ]+cy*s4[5  ])/tzi*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(-s4[5  ]/tzi*s4[3  ]/au*px-s4[5  ]/tzi*s4[2  ]/av*py+s4[5  ]/tzi*s4[3  ]*cx/au+s4[5  ]/tzi*s4[2  ]*cy/av-s4[5  ]/tzi*s4[0  ]);
    j4[6] = -(s4[43  ]*s4[3  ]/au*px+s4[43  ]*s4[2  ]/av*py-s4[43  ]*s4[3  ]*cx/au-s4[43  ]*s4[2  ]*cy/av+s4[43  ]*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(s4[42  ]*s4[3  ]/au*px+s4[42  ]*s4[2  ]/av*py-s4[42  ]*s4[3  ]*cx/au-s4[42  ]*s4[2  ]*cy/av+s4[42  ]*s4[0  ]);
    j4[7] = -((s4[34  ]*s4[15  ]+s4[31  ]*s4[14  ]+s4[41  ]*s4[3  ])/au*px+(s4[34  ]*s4[12  ]+s4[31  ]*s4[11  ]+s4[41  ]*s4[2  ])/av*py-(s4[34  ]*s4[15  ]+s4[31  ]*s4[14  ]+s4[41  ]*s4[3  ])*cx/au-(s4[34  ]*s4[12  ]+s4[31  ]*s4[11  ]+s4[41  ]*s4[2  ])*cy/av+s4[34  ]*s4[10  ]+s4[31  ]*s4[9   ]+s4[41  ]*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*((-2.0*q3j*s4[15  ]+2.0*q2j*s4[14  ]+s4[40  ]*s4[3  ])/au*px+(-2.0*q3j*s4[12  ]+2.0*q2j*s4[11  ]+s4[40  ]*s4[2  ])/av*py-(-2.0*q3j*s4[15  ]+2.0*q2j*s4[14  ]+s4[40  ]*s4[3  ])*cx/au-(-2.0*q3j*s4[12  ]+2.0*q2j*s4[11  ]+s4[40  ]*s4[2  ])*cy/av-2.0*s4[10  ]*q3j+2.0*s4[9   ]*q2j+s4[40  ]*s4[0  ]);
    j4[8] = -((s4[29  ]*s4[15  ]+s4[38  ]*s4[14  ]+s4[39  ]*s4[3  ])/au*px+(s4[29  ]*s4[12  ]+s4[38  ]*s4[11  ]+s4[39  ]*s4[2  ])/av*py-(s4[29  ]*s4[15  ]+s4[38  ]*s4[14  ]+s4[39  ]*s4[3  ])*cx/au-(s4[29  ]*s4[12  ]+s4[38  ]*s4[11  ]+s4[39  ]*s4[2  ])*cy/av+s4[29  ]*s4[10  ]+s4[38  ]*s4[9   ]+s4[39  ]*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*((2.0*q4j*s4[15  ]+2.0*q1j*s4[14  ]+s4[37  ]*s4[3  ])/au*px+(2.0*q4j*s4[12  ]+2.0*q1j*s4[11  ]+s4[37  ]*s4[2  ])/av*py-(2.0*q4j*s4[15  ]+2.0*q1j*s4[14  ]+s4[37  ]*s4[3  ])*cx/au-(2.0*q4j*s4[12  ]+2.0*q1j*s4[11  ]+s4[37  ]*s4[2  ])*cy/av+2.0*s4[10  ]*q4j+2.0*s4[9   ]*q1j+s4[37  ]*s4[0  ]);
    j4[9] = -((s4[35  ]*s4[15  ]+s4[29  ]*s4[14  ]+s4[36  ]*s4[3  ])/au*px+(s4[35  ]*s4[12  ]+s4[29  ]*s4[11  ]+s4[36  ]*s4[2  ])/av*py-(s4[35  ]*s4[15  ]+s4[29  ]*s4[14  ]+s4[36  ]*s4[3  ])*cx/au-(s4[35  ]*s4[12  ]+s4[29  ]*s4[11  ]+s4[36  ]*s4[2  ])*cy/av+s4[35  ]*s4[10  ]+s4[29  ]*s4[9   ]+s4[36  ]*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*((-2.0*q1j*s4[15  ]+2.0*q4j*s4[14  ]+s4[33  ]*s4[3  ])/au*px+(-2.0*q1j*s4[12  ]+2.0*q4j*s4[11  ]+s4[33  ]*s4[2  ])/av*py-(-2.0*q1j*s4[15  ]+2.0*q4j*s4[14  ]+s4[33  ]*s4[3  ])*cx/au-(-2.0*q1j*s4[12  ]+2.0*q4j*s4[11  ]+s4[33  ]*s4[2  ])*cy/av-2.0*s4[10  ]*q1j+2.0*s4[9   ]*q4j+s4[33  ]*s4[0  ]);
    j4[10] = -((s4[31  ]*s4[15  ]+s4[30  ]*s4[14  ]+s4[32  ]*s4[3  ])/au*px+(s4[31  ]*s4[12  ]+s4[30  ]*s4[11  ]+s4[32  ]*s4[2  ])/av*py-(s4[31  ]*s4[15  ]+s4[30  ]*s4[14  ]+s4[32  ]*s4[3  ])*cx/au-(s4[31  ]*s4[12  ]+s4[30  ]*s4[11  ]+s4[32  ]*s4[2  ])*cy/av+s4[31  ]*s4[10  ]+s4[30  ]*s4[9   ]+s4[32  ]*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*((2.0*q2j*s4[15  ]+2.0*q3j*s4[14  ]+s4[28  ]*s4[3  ])/au*px+(2.0*q2j*s4[12  ]+2.0*q3j*s4[11  ]+s4[28  ]*s4[2  ])/av*py-(2.0*q2j*s4[15  ]+2.0*q3j*s4[14  ]+s4[28  ]*s4[3  ])*cx/au-(2.0*q2j*s4[12  ]+2.0*q3j*s4[11  ]+s4[28  ]*s4[2  ])*cy/av+2.0*s4[10  ]*q2j+2.0*s4[9   ]*q3j+s4[28  ]*s4[0  ]);
    j4[11] = -(-(-s4[20  ]-cy*s4[6  ])/tzi*s4[3  ]/au*px-(-s4[20  ]-cy*s4[6  ])/tzi*s4[2  ]/av*py+(-s4[20  ]-cy*s4[6  ])/tzi*s4[3  ]*cx/au+(-s4[20  ]-cy*s4[6  ])/tzi*s4[2  ]*cy/av-(-s4[20  ]-cy*s4[6  ])/tzi*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(-s4[27  ]/tzi*s4[3  ]/au*px-s4[27  ]/tzi*s4[2  ]/av*py+s4[27  ]/tzi*s4[3  ]*cx/au+s4[27  ]/tzi*s4[2  ]*cy/av-s4[27  ]/tzi*s4[0  ]);
    j4[12] = -(-(-s4[19  ]-cy*s4[5  ])/tzi*s4[3  ]/au*px-(-s4[19  ]-cy*s4[5  ])/tzi*s4[2  ]/av*py+(-s4[19  ]-cy*s4[5  ])/tzi*s4[3  ]*cx/au+(-s4[19  ]-cy*s4[5  ])/tzi*s4[2  ]*cy/av-(-s4[19  ]-cy*s4[5  ])/tzi*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(-s4[26  ]/tzi*s4[3  ]/au*px-s4[26  ]/tzi*s4[2  ]/av*py+s4[26  ]/tzi*s4[3  ]*cx/au+s4[26  ]/tzi*s4[2  ]*cy/av-s4[26  ]/tzi*s4[0  ]);
    j4[13] = -(-(-s4[18  ]-cy*s4[4  ])/tzi*s4[3  ]/au*px-(-s4[18  ]-cy*s4[4  ])/tzi*s4[2  ]/av*py+(-s4[18  ]-cy*s4[4  ])/tzi*s4[3  ]*cx/au+(-s4[18  ]-cy*s4[4  ])/tzi*s4[2  ]*cy/av-(-s4[18  ]-cy*s4[4  ])/tzi*s4[0  ])/s4[17  ]+s4[25  ]/pow(s4[17  ],2.0)*(-s4[1  ]/tzi*s4[3  ]/au*px-s4[1  ]/tzi*s4[2  ]/av*py+s4[1  ]/tzi*s4[3  ]*cx/au+s4[1  ]/tzi*s4[2  ]*cy/av-s4[1  ]/tzi*s4[0  ]);

  }
}
