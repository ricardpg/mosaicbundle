//
// C++ Automatically generated file using 'CostSymbolicLBLPoint.m'
// By Jordi Ferrer Plana <jferrerp@eia.udg.edu>
// Date: 19-Jul-2007 10:17:17
//

#include <mex.h>          // Matlab Mex Functions
#include <cmath>

#define ERROR_HEADER       "MATLAB:JacobiansLBLPointMx.cpp:Input\n"
#define VARIABLES          "Vars = (au, av, cx, cy, q1i, q2i, q3i, q4i, txi, tyi, tzi[, mx, my])"

// Identify the input parameters by it's index
enum { VARS=0, S1, S2 };
// Identify the output parameters by it's index
enum { J1=0, J2 };

// Identify elements in Vars vector by it's index
enum { AU=0, AV, CX, CY, Q1I, Q2I, Q3I, Q4I, TXI, TYI, TZI, MX, MY };

void mexFunction ( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[] )
{
  // Parameter checks
  if ( nlhs != 2 || ( nrhs != 1 && nrhs != 3 ) )
    mexErrMsgTxt ( ERROR_HEADER
                   "Usage: [s1, s2] = JacobiansLBLPointMx.cpp ( Vars )\n"
                   "   or  [j1, j2] = JacobiansLBLPointMx.cpp ( Vars, s1, s2 )\n\n"
                   VARIABLES );

  int M = mxGetM ( prhs[VARS] );
  int N = mxGetN ( prhs[VARS] );
  if ( ( M != 1 ) || ( N != 11 && N != 13 ) )
    mexErrMsgTxt ( ERROR_HEADER
                   "Vars must be a 1x11 or 1x13 vector\n\n"
                   VARIABLES );

  double *Vars = (double *)mxGetPr ( prhs[VARS] );

  // Check for mx, my when si are provided
  if ( nrhs != 1 && N != 13 )
    mexErrMsgTxt ( ERROR_HEADER
                   "When s1, s2 are provided, Vars must be a 1x13 vector\n\n"
                   VARIABLES );

  double au = Vars[AU];
  double av = Vars[AV];
  double cx = Vars[CX];
  double cy = Vars[CY];
  double q1i = Vars[Q1I];
  double q2i = Vars[Q2I];
  double q3i = Vars[Q3I];
  double q4i = Vars[Q4I];
  double txi = Vars[TXI];
  double tyi = Vars[TYI];
  double tzi = Vars[TZI];

  double mx, my;

  // If it is the case of computing the jaccobians, get the Point-Match positions
  if ( nrhs != 1 )
  {
    mx = Vars[MX];
    my = Vars[MY];
  }
  else
  {
    mx = my = 0.0;
  }

  double MapleGenVar1, MapleGenVar2, MapleGenVar3, MapleGenVar4, MapleGenVar5, MapleGenVar6, MapleGenVar7, MapleGenVar8, MapleGenVar9;

  double *s1, *s2;

  // Test whether s_i must be computed or they are provided
  if ( nrhs == 1 )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 10, 1, mxREAL );
    s1 = (double *)mxGetPr ( plhs[J1] );
    plhs[J2] = mxCreateDoubleMatrix ( 10, 1, mxREAL );
    s2 = (double *)mxGetPr ( plhs[J2] );
  }
  else
  {
    s1 = mxGetPr ( prhs[S1] );
    s2 = mxGetPr ( prhs[S2] );
  }

  if ( nrhs == 1 )
  {
    s1[0] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s1[1] = 2.0*q1i*q2i+2.0*q3i*q4i;
    s1[2] = -2.0*q1i*q3i+2.0*q2i*q4i;
    s1[3] = 0;
  }
  else
  {
    s1[3] = (-2.0*q1i*q3i+2.0*q2i*q4i)*mx+(2.0*q1i*q2i+2.0*q3i*q4i)*my-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi;
  }

  if ( nrhs == 1 )
  {
    s1[4] = au*(2.0*q1i*q3i+2.0*q2i*q4i);
    s1[5] = au*(-2.0*q1i*q4i+2.0*q2i*q3i);
    s1[6] = au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i);
    s1[7] = 0;
  }
  else
  {
    s1[7] = (au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*mx+(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*my-(au*(q1i*q1i+q2i*q2i-q3i*q3i-q4i*q4i)+cx*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(au*(-2.0*q1i*q4i+2.0*q2i*q3i)+cx*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi-(au*(2.0*q1i*q3i+2.0*q2i*q4i)+cx*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi;
  }

  if ( nrhs == 1 )
  {
    s1[8] = 2.0*au*q2i+2.0*cx*q4i;
    s1[9] = -2.0*au*q4i+2.0*cx*q2i;

    s2[0] = q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i;
    s2[1] = 2.0*q1i*q2i+2.0*q3i*q4i;
    s2[2] = -2.0*q1i*q3i+2.0*q2i*q4i;
    s2[3] = 0;
  }
  else
  {
    s2[3] = (-2.0*q1i*q3i+2.0*q2i*q4i)*mx+(2.0*q1i*q2i+2.0*q3i*q4i)*my-(-2.0*q1i*q3i+2.0*q2i*q4i)*txi-(2.0*q1i*q2i+2.0*q3i*q4i)*tyi-(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i)*tzi;
  }

  if ( nrhs == 1 )
  {
    s2[4] = (2.0*q1i*q2i-2.0*q3i*q4i)*av-cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i);
    s2[5] = av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i);
    s2[6] = av*(2.0*q1i*q4i+2.0*q2i*q3i);
    s2[7] = 0;
  }
  else
  {
    s2[7] = (av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*mx+(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*my-(av*(2.0*q1i*q4i+2.0*q2i*q3i)+cy*(-2.0*q1i*q3i+2.0*q2i*q4i))*txi-(av*(q1i*q1i-q2i*q2i+q3i*q3i-q4i*q4i)+cy*(2.0*q1i*q2i+2.0*q3i*q4i))*tyi+((2.0*q1i*q2i-2.0*q3i*q4i)*av-cy*(q1i*q1i-q2i*q2i-q3i*q3i+q4i*q4i))*tzi;
  }

  if ( nrhs == 1 )
  {
    s2[8] = 2.0*q1i*av+2.0*cy*q2i;
    s2[9] = 2.0*av*q3i+2.0*cy*q4i;

  }

  // Compute the jacobians
  if ( nrhs != 1 )
  {
    plhs[J1] = mxCreateDoubleMatrix ( 7, 1, mxREAL );
    double *j1 = (double *)mxGetPr ( plhs[J1] );
    j1[0] = -((2.0*au*q1i-2.0*cx*q3i)*mx+s1[9   ]*my-(2.0*au*q1i-2.0*cx*q3i)*txi-s1[9   ]*tyi-(2.0*au*q3i+2.0*cx*q1i)*tzi)/s1[3  ]+s1[7  ]/pow(s1[3  ],2.0)*(-2.0*mx*q3i+2.0*my*q2i+2.0*txi*q3i-2.0*tyi*q2i-2.0*q1i*tzi);
    j1[1] = -(s1[8  ]*mx+(2.0*au*q3i+2.0*cx*q1i)*my-s1[8  ]*txi-(2.0*au*q3i+2.0*cx*q1i)*tyi-(2.0*au*q4i-2.0*cx*q2i)*tzi)/s1[3  ]+s1[7  ]/pow(s1[3  ],2.0)*(2.0*mx*q4i+2.0*q1i*my-2.0*txi*q4i-2.0*q1i*tyi+2.0*q2i*tzi);
    j1[2] = -((-2.0*au*q3i-2.0*cx*q1i)*mx+s1[8  ]*my-(-2.0*au*q3i-2.0*cx*q1i)*txi-s1[8  ]*tyi-(2.0*au*q1i-2.0*cx*q3i)*tzi)/s1[3  ]+s1[7  ]/pow(s1[3  ],2.0)*(-2.0*q1i*mx+2.0*my*q4i+2.0*q1i*txi-2.0*tyi*q4i+2.0*q3i*tzi);
    j1[3] = -(s1[9   ]*mx+(-2.0*au*q1i+2.0*cx*q3i)*my-s1[9   ]*txi-(-2.0*au*q1i+2.0*cx*q3i)*tyi-s1[8  ]*tzi)/s1[3  ]+s1[7  ]/pow(s1[3  ],2.0)*(2.0*q2i*mx+2.0*q3i*my-2.0*q2i*txi-2.0*q3i*tyi-2.0*q4i*tzi);
    j1[4] = -(-s1[6  ]-cx*s1[2  ])/s1[3  ]+s1[7  ]/pow(s1[3  ],2.0)*(2.0*q1i*q3i-2.0*q2i*q4i);
    j1[5] = -(-s1[5  ]-cx*s1[1  ])/s1[3  ]+s1[7  ]/pow(s1[3  ],2.0)*(-2.0*q1i*q2i-2.0*q3i*q4i);
    j1[6] = -(-s1[4  ]-cx*s1[0  ])/s1[3  ]+s1[7  ]/pow(s1[3  ],2.0)*(-q1i*q1i+q2i*q2i+q3i*q3i-q4i*q4i);

    plhs[J2] = mxCreateDoubleMatrix ( 7, 1, mxREAL );
    double *j2 = (double *)mxGetPr ( plhs[J2] );
    j2[0] = -((2.0*av*q4i-2.0*cy*q3i)*mx+s2[8  ]*my-(2.0*av*q4i-2.0*cy*q3i)*txi-s2[8  ]*tyi+(2.0*q2i*av-2.0*q1i*cy)*tzi)/s2[3  ]+s2[7  ]/pow(s2[3  ],2.0)*(-2.0*mx*q3i+2.0*my*q2i+2.0*txi*q3i-2.0*tyi*q2i-2.0*q1i*tzi);
    j2[1] = -(s2[9   ]*mx+(-2.0*q2i*av+2.0*q1i*cy)*my-s2[9   ]*txi-(-2.0*q2i*av+2.0*q1i*cy)*tyi+s2[8  ]*tzi)/s2[3  ]+s2[7  ]/pow(s2[3  ],2.0)*(2.0*mx*q4i+2.0*q1i*my-2.0*txi*q4i-2.0*q1i*tyi+2.0*q2i*tzi);
    j2[2] = -((2.0*q2i*av-2.0*q1i*cy)*mx+s2[9   ]*my-(2.0*q2i*av-2.0*q1i*cy)*txi-s2[9   ]*tyi+(-2.0*av*q4i+2.0*cy*q3i)*tzi)/s2[3  ]+s2[7  ]/pow(s2[3  ],2.0)*(-2.0*q1i*mx+2.0*my*q4i+2.0*q1i*txi-2.0*tyi*q4i+2.0*q3i*tzi);
    j2[3] = -(s2[8  ]*mx+(-2.0*av*q4i+2.0*cy*q3i)*my-s2[8  ]*txi-(-2.0*av*q4i+2.0*cy*q3i)*tyi+(-2.0*av*q3i-2.0*cy*q4i)*tzi)/s2[3  ]+s2[7  ]/pow(s2[3  ],2.0)*(2.0*q2i*mx+2.0*q3i*my-2.0*q2i*txi-2.0*q3i*tyi-2.0*q4i*tzi);
    j2[4] = -(-s2[6  ]-cy*s2[2  ])/s2[3  ]+s2[7  ]/pow(s2[3  ],2.0)*(2.0*q1i*q3i-2.0*q2i*q4i);
    j2[5] = -(-s2[5  ]-cy*s2[1  ])/s2[3  ]+s2[7  ]/pow(s2[3  ],2.0)*(-2.0*q1i*q2i-2.0*q3i*q4i);
    j2[6] = -s2[4  ]/s2[3  ]+s2[7  ]/pow(s2[3  ],2.0)*(-q1i*q1i+q2i*q2i+q3i*q3i-q4i*q4i);

  }
}
