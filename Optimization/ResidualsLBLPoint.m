%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for LBL Point Readings.
%
%  File          : ResidualsLBLPoint.m
%  Date          : 03/04/2006 - 19/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ResidualsLBLPoint Using Planar Homographies from 2D Mosaic Frame to the
%                    2D Image Frame, the function computes the residual by
%                    warping the (X, Y) LBL Reading to the 2D Image Frame
%                    coordiantes and comparing it with the imaged point.
%
%     Input Parameters:
%      iHw: 3x3xn set of 2D Planar Homographies that transform points in the 2D
%           Mosaic Frame to the 2D Image Frame.
%      LBLPointData: kx5 matrix that contain in each row a LBL reading. Each row
%                    contains the Image Index; x, y Image Coordinate and
%                    X, Y LBL coordinates in the 2D Mosaic Frame.
%
%     Output Parameters:
%      r: k*2 vector containing the residuals. It contains the x, y
%         residual for each LBL Reading.
%

function r = ResidualsLBLPoint ( iHw, LBLPointData )

  % Residual Vector: Two Elements for each LBL Reading
  r = zeros ( size ( LBLPointData, 1 ) * 2, 1 );

  % Pointer to the current residual LBL Reading
  k = 1;

  % Scan LBL Point Readings
  for j = 1 : size ( LBLPointData , 1 )
    i  = LBLPointData ( j, 1 ); % Get the Image Index
    px = LBLPointData ( j, 2 ); % Point x coordinate in image
    py = LBLPointData ( j, 3 ); % Point y coordinate in image
    mx = LBLPointData ( j, 4 ); % 3D LBL X
    my = LBLPointData ( j, 5 ); % 3D LBL Y

    % Calculate the Residuals
    P = iHw(:,:,i) * [mx; my; 1];
    r(k  ) = px - P(1) / P(3);
    r(k+1) = py - P(2) / P(3);

    % Next LBL Reading pair in the residual vector
    k = k + 2;
  end
end
