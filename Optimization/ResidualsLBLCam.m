%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for LBL Camera Position Readings.
%
%  File          : ResidualsLBLCam.m
%  Date          : 03/04/2006 - 19/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ResidualsLBLCam Calculate the Residuals for LBL Camera Position Readings.
%
%     Input Parameters:
%      x: 1x(n*NumVariables) variable vector where n is the number of Image Frames.
%      LBLCamData: mx4 matrix containing in each row, the Image Index corresponding to the reading,
%                  and the X, Y and Z LBL coordinates.
%      NumVariables: Number of variables per Frame in x i.e. 7 = (q0, q1, q2, q3, X, Y, Z)
%
%     Output Parameters:
%      r: n*3 vector containing the residuals. It contains the x, y and z
%         residual for each LBL Camera Reading.
%

function r = ResidualsLBLCam ( x, LBLCamData, NumVariables )
  % Residual Vector: Three Elements for each LBL Reading
  r = zeros ( size ( LBLCamData, 1 ) * 3, 1 );

  % Pointer to the current residual LBL Reading
  k = 1;

  % Scan LBL Camera Readings
  for j = 1 : size ( LBLCamData, 1 );
    % Get the image index
    i = LBLCamData (j, 1);

    % Residuals
    r(k  ) = LBLCamData(j, 2) - x((i-1)*NumVariables+5);
    r(k+1) = LBLCamData(j, 3) - x((i-1)*NumVariables+6);
    r(k+2) = LBLCamData(j, 4) - x((i-1)*NumVariables+7);

    % Next LBL Reading triplet in the residual vector
    k = k + 3;
  end
end
