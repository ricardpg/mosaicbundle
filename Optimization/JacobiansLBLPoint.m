%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Jacobians for LBL Point Readings.
%
%  File          : JacobiansLBLPoint.m
%  Date          : 03/04/2006 - 19/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  JacobiansLBLPoint Calculate the Jacobians for LBL Point Readings.
%
%     Input Parameters:
%      xi: 1x7 variable vector (q0, q1, q2, q3, X, Y, Z ) corresponding to
%          ith image frame.
%      K: 3x3 Intrinsic Camera Parameters Matrix.
%      i: Image Index.
%      Idx: Set of LBLPointData rows in which there are a LBL Point reading for image i.
%      NumVariables: Number of variables per Frame in x i.e. 7 = (q0, q1, q2, q3, X, Y, Z)
%      LBLPointData: kx5 Matrix containing the LBL Point Readings. Each row
%                    contain the Frame Index, Image Coordinate x, Image
%                    Coordinate y, LBL X Position and LBL Y Position.
%
%     Output Parameters:
%      Jacobians: 2*NumVariablesxk matrix containing the evaluation of the
%                 derivatives:
%                   2*NumVariables: 2 Residuals: LBL x, LBL y *
%                                   NumVariables Variables Current Image i
%                   1..k: Number of LBL readings in the frame.
%

function Jacobians = JacobiansLBLPoint ( xi, K, Idx, NumVariables, LBLPointData )

  % Camera Intrinsic Parameters
  au = K(1,1);
  av = K(2,2);
  cx = K(1,3);
  cy = K(2,3);

  % Precompute si
  Vars = [ au, av, cx, cy, xi ];
  % Call the Mex function
  [s1, s2] = JacobiansLBLPointMx ( Vars );

  % 2 Residuals x, y per NumVariables Variables
  Jacobians = zeros ( 2*NumVariables, length(Idx) );

  % For each LBL Point in the image i
  for k = 1 : length ( Idx );
    % LBL Reading x, y
    mx = LBLPointData(Idx(k), 4);
    my = LBLPointData(Idx(k), 5);

    [Jacobians(0*NumVariables+1:1*NumVariables,k), Jacobians(1*NumVariables+1:2*NumVariables,k)] = ...
        JacobiansLBLPointMx ( [Vars mx my], s1, s2 );  
  end
end
