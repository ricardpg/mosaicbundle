%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for Angle Camera Readings.
%
%  File          : JacobiansAngCam.m
%  Date          : 03/04/2006 - 18/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  JacobiansAngCam Calculate the Jacobians for Angle Camera Readings.
%
%     Input Parameters:
%      q: 1x4 vector containing the quaternion (subset of the variables).
%
%     Output Parameters:
%      Jacobians: 4x3 matrix containing the derivatives for the q0, q1, q2, q3
%                 q3 variables according to each angle (Roll Pitch Yaw)
%

function Jacobians = JacobiansAngCam ( q )
  Jacobians = zeros ( 4, 3 );
  [Jacobians(1:4,1), Jacobians(1:4,2), Jacobians(1:4,3)] = JacobiansAngCamMx ( q );
end
