%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for Angle Camera Readings.
%
%  File          : ResidualsAngCam.m
%  Date          : 03/04/2006 - 14/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ResidualsAngCam Calculate the Residuals for Angle Camera Readings.
%
%     Input Parameters:
%      x: 1x(n*NumVariables) variable vector where n is the number of Image Frames.
%      AngCamData: mx4 Matrix containing in each row the Image Index corresponding to 
%                  the reading and Roll, Pitch and the Yaw Angle.
%                  NOTE: Angles must be in the interval [-pi, pi].
%      NumVariables: Number of variables per Frame in x i.e. 7 = (q0, q1, q2, q3, X, Y, Z)
%
%     Output Parameters:
%      r: k*3 vector containing the residuals. It contains the Roll, Pitch
%         and Yaw residual for each set of Camera Angle Readings.
%

function r = ResidualsAngCam ( x, AngCamData, NumVariables )
  % Residual Vector: Three Elements for each Angle Reading
  r = zeros ( size ( AngCamData, 1 ) * 3, 1 );

  % Pointer to the current residual Angle Reading
  k = 1;

  % Scan Angle Readings
  for j = 1 : size ( AngCamData, 1 );
    % Get the image index
    i = AngCamData(j, 1);
    % Obtain the Quaternion
    l = (i - 1) * NumVariables;
    q = x(l+1:l+4);

    % Quaternion to Rotation Matrix
    R = quat2rotmatMx ( q );

    [Roll Pitch Yaw] = RotMat2RPY ( R );

    % Residuals
    r(k  ) = AngCamData(j, 2) - Roll;
    r(k+1) = AngCamData(j, 3) - Pitch;
    r(k+2) = AngCamData(j, 4) - Yaw;

    % !!! REVIEW !!!
    % Initially the difference cannot be smaller than -2*pi or bigger than 2*pi
    % sinse the source angles are in the range [-pi, pi]
    % So, only take care that the result is in the range [-pi, pi]
    for l = 0 : 2;
      if r(k+l) > pi;  r(k+l) = r(k+l) - 2.0 * pi; end;
      if r(k+l) < -pi; r(k+l) = r(k+l) + 2.0 * pi; end;
    end 

    % Next Angle Reading triplet in the residual vector
    k = k + 3;
  end
end
