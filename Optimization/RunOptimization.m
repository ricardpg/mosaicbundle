%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : 3D Vehicle Trajectory Parameters Optimization.
%
%  File          : RunOptimization.m
%  Date          : 14/03/2006 - 06/09/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  RunOptimization Perform a Bundle Adjustement of a 3D Vehicle Trajectory
%                  using the Intrinsics (K) extrinsics (HiW), a Point-Match
%                  set of correspondences, LBL Point Readings, LBL Camera
%                  Readings and Vehicle Angular Readings.
%
%     Input Parameters:
%      K: 3x3 Intrinsic parameters matrix.
%      RTs: Array for NbrImages nodes containing for each one the
%           structure with the fields:
%        cRw: Rotation of the 3D World Frame w.r.t. the 3D Camera Frame.
%        wTc: Translation of the 3D Camera Frame w.r.t. the 3D World Frame.
%      NumIter: Maximum number of iterations.
%      WMatchPoint: Scalar containing the weight for the Point-Match residuals.
%      MatchIdxTable: See ResidualsPM.m.
%      MatchPointData: See ResidualsPM.m.
%      WLBLPoint: Scalar containing the weight for the LBL Point residuals.
%      LBLPointData: See ResidualsLBLPoint.m.
%      WLBLCam: Weight for the LBL Camera residuals. It can be scalar or a
%               vector with different weights. If it is a vector the size
%               must fit the size of LBLCamData.
%      LBLCamData: See ResidualsLBLCam.m.
%      WAngCam: 3x1 vector containing the independent weights for the Angular
%               readings residuals or an empty vector.
%      AngCamData: See ResidualsAngCam.m.
%      x: Optional Initial parameter estimation (see Output variable x).
%      x: History of the initial parameter estimation ( see Output variable xt).
%      JPattern: Optional Initial Jacobian Pattern to compute the Jacobians.
%      Residualt: Matrix containing the t steps history evolution of the
%                 residuals.
%      DoNormalization: If it is true, perform variable normalization. By
%                       default is false.
%      PlotDebug: Plot the residual figures at each step.
%
%     Output Parameters:
%      iHw0: 3x3xNbrImages set of initial Planar Homographies that
%            convert coordinates from 2D World Frame to 2D Image Frame.
%      wHi0: 3x3xNbrImages set of initial Planar Homographies that
%            convert coordinates from 2D Image Frame to 2D World Frame.
%      iHw: 3x3xNbrImages set of Optimized Planar Homographies that
%           convert coordinates from 2D World Frame to 2D Image Frame.
%      wHi: 3x3xNbrImages set of Optimized Planar Homographies that
%           convert coordinates from 2D Image Frame to 2D World Frame.
%      x: 7*NbrImages vector containing the poses of the vehicle in format
%         (q0, q1, q2, q3, X, Y, Z).
%      xt: 7*NbrImagesxt matrix containing the t steps history evolution of
%          the poses x.
%      Jacobians: mxn matrix containing the evaluation of the Jacobians in
%                 the last iteration.
%      JPattern: mxn matrix containing the Pattern to use the Jacobian
%                Matrix. To know m and n see the ComputeJacobianPattern
%                function.
%      Residual: vector containing the residuals for the last step.
%      Residualt: Matrix containing the t steps history evolution of the
%                 residuals.
%      Normalization: In case that DoNormalization parameter is true, this
%                     vector contains Cx, Cy, Cz translation and S scale
%                     used to normalize the data. In case that
%                     DoNormalization is false this vector is empty.
%

function [iHw0 wHi0 iHw wHi x xt Jacobians JPattern Residual Residualt Normalization] = ...
     RunOptimization ( K, RTs, NumIter, WMatchPoint, MatchIdxTable, MatchPointData, ...
                       WLBLPoint, LBLPointData, WLBLCam, LBLCamData, ...
                       WAngCam, AngCamData, x, xt, JPattern, Residualt, ...
                       DoNormalization, PlotDebug )
  % Test the input parameters
  error ( nargchk ( 17, 18, nargin ) );
  error ( nargoutchk ( 11, 11, nargout ) );

  % Optional parameters
  if nargin < 18; PlotDebug = false; end;
  
  % Compute offsets (last in previous iterations will be overlapped by the first one).
  if isempty ( xt ); lt = 0; else lt = size ( xt, 1 ); end;
  if isempty ( Residualt ); rt = 0; else rt = size ( Residualt, 2 ); end;

  if PlotDebug;
    % Compute How many residuals there are in each set
    [ NumMatchPoint NumLBLPoint NumLBLCam NumAngCam ] = ...
             CalcResOffsets ( size ( MatchPointData ), size ( MatchIdxTable ), ...
                              size ( LBLPointData ), size ( LBLCamData ), size ( AngCamData ) );
    % Create figures to plot residuals
    [ ResMatchPointFig ResLBLPointFig ResLBLCamFig ResAngCamFig ] = ...
       CreateResidualFigures ( NumMatchPoint, NumLBLPoint, NumLBLCam, NumAngCam );
  end
      
  % Number of Variables: q0, q1, q2, q3, x, y, z
  NumVariables = 7;

  % Use initial values if provided, otherwise compute the initial set
  if isempty ( x );
    disp ( 'Calculating Initial Variables...' );
    x = CalculateVariables ( RTs, NumVariables );
  else
    disp ( 'Using x input as Initial Variable Estimation...' );
  end

  % Calculate the Number Of Images
  TotalVars = length ( x );
  NbrImages = TotalVars / NumVariables;

  % Test the sizes
  if NbrImages ~= size ( RTs, 2 );
    error ( 'MATLAB:RunOptimization:Input', 'Variable vector size does not match!' );
  end

  % Check Weights
  if ~isscalar ( WMatchPoint ) || isempty ( WMatchPoint );
      error ( 'MATLAB:RunOptimization:Input', 'WMatchPoint must be an scalar!' );
  end

  if ~isscalar ( WLBLPoint ) || isempty ( WLBLPoint );
      error ( 'MATLAB:RunOptimization:Input', 'WLBLPoint must be an scalar!' );
  end

  if isempty ( WLBLCam ) || ( ~isscalar ( WLBLCam ) && ( size ( WLBLCam, 1 ) / 3 ) ~= size ( LBLCamData, 1 ) );
      error ( 'MATLAB:RunOptimization:Input', 'WLBLCam must be an scalar or a vector with as many elements as rows * 3 in LBLCamData!' );
  end

  if isempty ( WAngCam ) || ( ~isscalar ( WLBLCam ) && size ( WAngCam, 1 ) ~= 3 && size ( WAngCam, 1 ) / 3 ~= size ( AngCamData, 1 ) );
    error ( 'MATLAB:RunOptimization:Input', 'WAngCam must be a 3x1 vector or a vector with as many elements as rows * 3 in AngCamData!' );
  end

  % Calculate Initial Set of Homographies
  disp ( 'Calculating Initial Homographies...' );
  [iHw0 wHi0] = CalculateHomographies ( x, K, NbrImages, NumVariables );

  if DoNormalization;
    disp ( 'Normalizing Data...' );
    % Normalize X, Y, Z
    Idx = 1:7:TotalVars;
    X = x(Idx+4);
    Y = x(Idx+5);
    Z = x(Idx+6);

    MinX = min ( X ); MaxX = max ( X );
    MinY = min ( Y ); MaxY = max ( Y );
    MinZ = min ( Z ); MaxZ = max ( Z );

    Sx = (MaxX - MinX) / 2;
    Sy = (MaxY - MinY) / 2;
    Sz = (MaxZ - MinZ) / 2;

    S = max ( [Sx Sy Sz] );

    Cx = (MinX + MaxX) / 2;
    Cy = (MinY + MaxY) / 2;
%    Cz = (MinZ + MaxZ) / 2;

    Cz = 0;

    x(Idx+4) = (X - Cx) / S;
    x(Idx+5) = (Y - Cy) / S;
    x(Idx+6) = (Z - Cz) / S;
    
    % LBL Camera Readings are in the same frame than Camera Poses:
    %   Use the same parameters to normalize them.
    LBLCamData(:,2) = ( LBLCamData(:,2) - Cx ) / S;
    LBLCamData(:,3) = ( LBLCamData(:,3) - Cy ) / S;
    LBLCamData(:,4) = ( LBLCamData(:,4) - Cz ) / S;

    % LBL Point Readings are in the same frame than Camera Poses:
    %   Use the same parameters to normalize them.
    LBLPointData(:,4) = ( LBLPointData(:,4) - Cx ) / S;
    LBLPointData(:,5) = ( LBLPointData(:,5) - Cy ) / S;

    Normalization = [Cx Cy Cz S];
  else
    Normalization = [];
  end

  % Calculate whether the residuals for each set must be calculated or not.
  if WMatchPoint == 0; MatchPointData = []; MatchIdxTable = []; end;
  if WLBLPoint == 0; LBLPointData = []; end;
  if WLBLCam == 0; LBLCamData = []; end;
  if WAngCam == 0; AngCamData = []; end;

  % Calculate Final Weigths
  if PlotDebug;
    if DoNormalization;
      % Scale must be used to multiply the residuals, but is the same than
      % dividing the weights
      WLBLCamS = WLBLCam / S;
      WLBLPointS = WLBLPoint / S;
    else
      WLBLCamS = WLBLCam;
      WLBLPointS = WLBLPoint;
    end
  end

  % Function to store history and show intermediate plots and time
  %
  function Stop = OutFunction ( xi, OptimValues, State )
    switch ( State );
      case 'init'
        disp ( sprintf ( '\nIterations started at "%s"\n', datestr(now) ) );
      case 'interrupt'
      case 'done'
      case 'iter'
        if PlotDebug;
          % Plot residual
          PlotResidual ( OptimValues.residual, WMatchPoint, WLBLPointS, WLBLCamS, WAngCam, ...
                         ResMatchPointFig, ResLBLPointFig, ResLBLCamFig, ResAngCamFig, ...
                         NumMatchPoint, NumLBLPoint, NumLBLCam, NumAngCam );
          drawnow;

          Time = toc;
          HourTot = Time / 3600.0;
          Hour = fix ( HourTot );
          MinTot =  ( HourTot - Hour ) * 60.0;
          Min = fix ( MinTot );
          Sec = ( MinTot - Min ) * 60.0;
          disp ( sprintf ( '\b   (%s) Elapsed time: %3.3d:%2.2d:%5.3f', datestr(now), Hour, Min, Sec ) );

          tic;
        end

        % Store x history
        if DoNormalization;
          Idx = 1:7:TotalVars;

          X = xi(Idx+4);
          Y = xi(Idx+5);
          Z = xi(Idx+6);

          xi(Idx+4) = X * S + Cx;
          xi(Idx+5) = Y * S + Cy;
          xi(Idx+6) = Z * S + Cz;
        end

        lt = lt + 1;
        xt(lt, :) = xi;
        
        % Store Residual history
        rt = rt + 1;
        Residualt(:, rt) = OptimValues.residual;
      otherwise
        disp ( 'Unknown State in OutFunction()' );
    end

    Stop = false;
  end


  % Compute the Jacobian checking function if they are not provided
  if isempty ( JPattern );
    disp ( 'Calculating Jacobian Pattern...' );
    JPattern = ComputeJacobianPattern ( NbrImages, NumVariables, ...
                                        MatchPointData, MatchIdxTable, ...
                                        LBLPointData, LBLCamData, AngCamData );
  end

  % Default
  Options = optimset ( 'lsqnonlin' );
  % Medium+Large Scale
  Options = optimset ( Options, 'Display', 'iter' );
  Options = optimset ( Options, 'Diagnostics', 'off' );

  Options = optimset ( Options, 'Jacobian', 'on' );
  Options = optimset ( Options, 'DerivativeCheck', 'off' );

  Options = optimset ( Options, 'FunValCheck', 'off' );
  Options = optimset ( Options, 'GradObj', 'on' );
  Options = optimset ( Options, 'LargeScale', 'on' );
  Options = optimset ( Options, 'MaxFunEvals', [] );
  Options = optimset ( Options, 'OutputFcn', @OutFunction ); % , @OutFunction2, ...
  Options = optimset ( Options, 'PlotFcns', [] );
  Options = optimset ( Options, 'TolCon', [] );
  Options = optimset ( Options, 'TolFun', 10^-15 );
  Options = optimset ( Options, 'TolX', 10^-15 );
  Options = optimset ( Options, 'TypicalX', [] );

  % Large Scale
%  Options = optimset ( Options, 'Hessian', 'off' );
%  Options = optimset ( Options, 'HessMult', [] );
%  Options = optimset ( Options, 'HessPattern', [] );
%  Options = optimset ( Options, 'InitialHessMatrix', );
%  Options = optimset ( Options, 'InitialHessType', );
%  Options = optimset ( Options, 'JacobMult', );
  Options = optimset ( Options, 'JacobPattern', JPattern );
%  Options = optimset ( Options, 'MaxPCGIter', 100 );
  Options = optimset ( Options, 'PrecondBandWidth', Inf );
%  Options = optimset ( Options, 'TolPCG', 0.1 );

  % Medium Scale
%  Options = optimset ( Options, 'DiffMaxChange', [] );
%  Options = optimset ( Options, 'DiffMinChange', [] );
%  Options = optimset ( Options, 'LevenbergMarquardt', 'on' );
%  Options = optimset ( Options, 'LineSearchType', 'cubicpoly' );


  % Continue iterations
  if isempty ( NumIter ) || ~isscalar ( NumIter ) || NumIter == 0;
    NumIter = 0;
    while NumIter <= 0;
      NumIt = input ( 'Enter how many initial iterations: ' );
      NumIter = fix ( NumIt );
    end
  end

  while NumIter > 0;
    Options = optimset ( Options, 'MaxIter', NumIter );

    disp ( 'Starting Iterations...' );
    % Run the Optimization
    if PlotDebug; tic; end;
    [ x, ResNorm, Residual, ExitFlag, Output, Lambda, Jacobians ] = ...
      lsqnonlin ( @(x) myfun ( x, K, WMatchPoint, MatchPointData, MatchIdxTable, ...
                               WLBLPoint, LBLPointData, WLBLCam, LBLCamData, ...
                               WAngCam, AngCamData, NbrImages, NumVariables ), ...
                               x, [], [], Options );
    NumIt = input ( 'Enter how many iterations more: ' );
    NumIter = fix ( NumIt );
  end

  % Unnormalize
  if DoNormalization;
    disp ( 'Unnormalizing Data...' );
    X = x(Idx+4);
    Y = x(Idx+5);
    Z = x(Idx+6);

    x(Idx+4) = X * S + Cx;
    x(Idx+5) = Y * S + Cy;
    x(Idx+6) = Z * S + Cz;
  end

  % Calculate Final Set of Optimized Homographies
  [iHw wHi] = CalculateHomographies ( x, K, NbrImages, NumVariables );

  % Eliminate figures
  if PlotDebug;
    if ~isempty ( ResMatchPointFig ); close ( ResMatchPointFig ); end;
    if ~isempty ( ResLBLPointFig ); close ( ResLBLPointFig ); end;
    if ~isempty ( ResLBLCamFig ); close ( ResLBLCamFig ); end;
    if ~isempty ( ResAngCamFig ); close ( ResAngCamFig ); end;
  end;
end


%
%
function [r J] = myfun ( x, K, WMatchPoint, MatchPointData, MatchIdxTable, ...
                         WLBLPoint, LBLPointData, WLBLCam, LBLCamData, ...
                         WAngCam, AngCamData, NbrImages, NumVariables )
                         
   % Calculate whether the residuals for each set must be calculated or not.
  CompMatchPoint = ~isempty ( MatchPointData ) && ~isempty ( MatchIdxTable );
  CompLBLPoint = ~isempty ( LBLPointData );
  CompLBLCam = ~isempty ( LBLCamData );
  CompAngCam = ~isempty ( AngCamData );

  % Normalize the quaternions
  x = NormQuaternion ( x, NumVariables );

  % Calculate Homographies iHw and wHi from the pose parameters
  % (q0, q1, q2, q3, x, y, z)
  if CompMatchPoint || CompLBLPoint;
    [iHw wHi] = CalculateHomographies ( x, K, NbrImages, NumVariables );
  end

  % Residuals from point and matches
  if CompMatchPoint;
    ResMatchPoint = ResidualsPM ( iHw, wHi, MatchPointData, MatchIdxTable );
  else
    ResMatchPoint = [];
  end

  % Residuals from known LBL coordinates of scene points
  if CompLBLPoint;
    ResLBLPoint = ResidualsLBLPoint ( iHw, LBLPointData );
  else
    ResLBLPoint = [];
  end

  % Residuals from known LBL camera positions
  if CompLBLCam;
    ResLBLCam = ResidualsLBLCam ( x, LBLCamData, NumVariables );
  else
    ResLBLCam = [];
  end

  % Residuals from angle sensors
  if CompAngCam;
    ResAngCam = ResidualsAngCam ( x, AngCamData, NumVariables );
  else
    ResAngCam = [];
  end
                         
  % Merge residuals 
  r = [ WMatchPoint.*ResMatchPoint; WLBLPoint.*ResLBLPoint; WLBLCam.*ResLBLCam; WAngCam.*ResAngCam ];

  % When 2 output arguments: Calculate Jacobians
  if nargout > 1;
    J = CalculateJacobians ( x, K, WMatchPoint, MatchIdxTable, MatchPointData, ...
                             WLBLPoint, LBLPointData, WLBLCam, LBLCamData, ...
                             WAngCam, AngCamData, NbrImages, NumVariables );
  end
end


% Build vector of variables (x, y, z, q0, q1, q2, q3) from the pose in
% format cRw, wTc.
%
function x = CalculateVariables ( RTs, NumVariables )
  N = length ( RTs );
  % Allocate Variable Space
  x = zeros ( 1, N * NumVariables );
  o = 1;
  for i = 1 : N;
    % Convert from Rotation Matrix to Quaternion
    q = rotmat2quatMx ( RTs(i).cRw );
    x(o:o+NumVariables-1) = [ q' RTs(i).wTc' ];
    o = o + NumVariables;
  end
end


% Compute the Sparse Matrix to represent whether a Jacobian must be computed or not
%
% IMPORTANT NOTE: The LBLPointData, LBLCamData and AngCamData matrices MUST
%                 be sorted by the image index (First column).
%
function J = ComputeJacobianPattern ( NbrImages, NumVariables, MatchPointData, MatchIdxTable, ...
                                      LBLPointData, LBLCamData, AngCamData )

  % Sparse Jacobian Matrix
  %
  %   4: For each point match has 4 entries in the residual vector size(MatchPointData,2),
  %       for each overlapping pairs has size(MatchPointData,2) correspondences
  %   2: For each LBL points has 2 entries in the residual vector.
  %   3: For each LBL Camera readings
  %   3: For each Angular Camera readings
  %
  disp ( 'Create Sparse Jacobian Matrix...' );
  J = sparse ( NumVariables * NbrImages, ...
               4 * size ( MatchPointData, 2 ) * size ( MatchIdxTable, 1 ) + ...
               2 * size ( LBLPointData, 1 ) + ...
               3 * size ( LBLCamData, 1 ) + ...
               3 * size ( AngCamData, 1 ) );

  % Row Counter in the Residual Vector
  k = 1;

  % Scan the Point & Match Index Table to locate each pair of overlapping images
  disp ( 'Calculate Point-Match Matches...' );
  tic;
  for l = 1:size ( MatchIdxTable, 1 );
    % Get the image indices: i = Current Image (Points)
    %                        j = Reference Image (Matches)
    i = MatchIdxTable ( l, 1 );
    j = MatchIdxTable ( l, 2 );
    % Point-Match Jacobians must be computed in all the variables
    Iil = (i - 1) * NumVariables + 1;
    Iir = Iil + NumVariables - 1;
    Ijl = (j - 1) * NumVariables + 1;
    Ijr = Ijl + NumVariables - 1;
    for m = 1 : size ( MatchPointData, 2 );
      J(Iil:Iir, k:k+3) = 1;
      J(Ijl:Ijr, k:k+3) = 1;
      k = k + 4;
    end
    if mod ( l-1, 100 ) == 0;
      t = toc;
      disp ( sprintf ( '  Current masks %d/%d (%f sec.)...', l, size ( MatchIdxTable, 1 ), t ) );
      tic;
    end
  end

  % Scan all the LBL Point Readings
  disp ( 'Calculate LBL Point Matches...' );
  for j = 1 : size ( LBLPointData, 1 );
    % Get the image Index
    i = LBLPointData ( j, 1 );
    % LBL Point Readings Jacobians must be computed in all the variables
    Iil = (i - 1) * NumVariables + 1;
    Iir = Iil + NumVariables - 1;
    J(Iil:Iir, k:k+1) = 1;
    k = k + 2;
  end

  % Scan all the LBL Camera Readings
  disp ( 'Calculate LBL Camera Matches...' );
  for j = 1 : size ( LBLCamData, 1 );
    i = LBLCamData ( j, 1 );
    Iil = (i - 1) * NumVariables + 1 + 4;  % Offset X
    Iir = Iil + 2;                         % Offset Z

    % LBL Camera Readings Jacobians only affect to the X, Y and Z variables
    J(Iil:Iir, k:k+2) = 1;
    k = k + 3;
  end

  % Scan all the Angular Readings
  disp ( 'Calculate Angular Readings Matches...' );
  for j = 1 : size ( AngCamData, 1 );
    i = AngCamData ( j, 1 );
    Iil = (i - 1) * NumVariables + 1;  % Offset q0
    Iir = Iil + 3;                     % Offset q3

    % Angular Readings only affect to the q0, q1, q2 and q3 variables
    J(Iil:Iir, k:k+2) = 1;
    k = k + 3;
  end

  % Jacobian Matrix is computed transposed because indexing columns
  % sequentially is faster than indexing rows sequentially.
  disp ( 'Transposing Sparse Jacobian Matrix...' );
  J = J';
end


% Divide the quaternions in the variable vector by its norm in order to 
% satisfy the quaternion constraint: a quaternion that represents a
% rotation must have norm = 1.
% Variable vector x is a vector containing:
%   ( x, y, z, q0, q1, q2, q3, q4 ) x NbrImages
%
function x = NormQuaternion ( x, NumVariables )
  i = 1;
  N = length ( x );
  while i < N;
    q = x(i:i+3);
    n = norm ( q );
    x(i:i+3) = q / n;

    i = i + NumVariables;
  end
end

%
%
function J = CalculateJacobians ( x, K, WMatchPoint, MatchIdxTable, MatchPointData, ...
                                  WLBLPoint, LBLPointData, WLBLCam, LBLCamData, ...
                                  WAngCam, AngCamData, NbrImages, NumVariables )

  J = sparse ( NumVariables * NbrImages, ...
               4 * size ( MatchPointData, 2 ) * size ( MatchIdxTable, 1 ) + ...
               2 * size ( LBLPointData, 1 ) + ...
               3 * size ( LBLCamData, 1 ) + ...
               3 * size ( AngCamData, 1 ) );

  % Index to the Row
  k = 1;

  % Jacobian entries from Point-Match
  if ~isempty ( MatchPointData ) && ~isempty ( MatchIdxTable );
    % For each overlapping pairs
    for l = 1 : size ( MatchPointData, 1 )
      % Get the image indexes:
      %  i = Current Image (Points);  j = Reference Image (Matches)
      i = MatchIdxTable ( l, 1 );
      j = MatchIdxTable ( l, 2 );

      % Compute the Match-Point Coordinates for the current pair of images.
      MPData(:, :) = MatchPointData ( l, :, : );

      % Compute inidices to the variables of the Current and the Reference Image
      Iil = ( i - 1 ) * NumVariables + 1;
      Iir = Iil + NumVariables - 1;
      Ijl = ( j - 1 ) * NumVariables + 1;
      Ijr = Ijl + NumVariables - 1;

      % Camera Extrinsic Parameters: Unknowns of ith Homograhpy (Current)
      xi = x(Iil:Iir);
      % Camera Extrinsic Parameters: Unknowns of jth Homography (Reference)
      xj = x(Ijl:Ijr);
      % Calculate Derivatives for the Unknowns of Image i (Current) and j (Reference)
      JacPM = JacobiansPM ( xi, xj, K, NumVariables, MPData );

      % Assign Jacobians      
      for m = 1 : size ( MatchPointData, 2 );
        J(Iil:Iir, k)   = WMatchPoint * JacPM(0*NumVariables+1:1*NumVariables, m); % px-mx' Image i
        J(Ijl:Ijr, k)   = WMatchPoint * JacPM(1*NumVariables+1:2*NumVariables, m); % px-mx' Image j
        J(Iil:Iir, k+1) = WMatchPoint * JacPM(2*NumVariables+1:3*NumVariables, m); % py-my' Image i
        J(Ijl:Ijr, k+1) = WMatchPoint * JacPM(3*NumVariables+1:4*NumVariables, m); % py-my' Image j
        J(Iil:Iir, k+2) = WMatchPoint * JacPM(4*NumVariables+1:5*NumVariables, m); % mx-px' Image i
        J(Ijl:Ijr, k+2) = WMatchPoint * JacPM(5*NumVariables+1:6*NumVariables, m); % mx-px' Image j
        J(Iil:Iir, k+3) = WMatchPoint * JacPM(6*NumVariables+1:7*NumVariables, m); % my-py' Image i
        J(Ijl:Ijr, k+3) = WMatchPoint * JacPM(7*NumVariables+1:8*NumVariables, m); % my-py' Image j
        k = k + 4;
      end
    end
  end

  % Jacobian entries from LBL Point Readings
  if ~isempty ( LBLPointData )
    for i = 1 : NbrImages;
      % Find the LBL Points appeared in image i
      Idx = find ( LBLPointData(:, 1) == i );
      if ~isempty ( Idx );
        % Compute inidices to the variables of the Current Image
        Iil = ( i - 1 ) * NumVariables + 1;
        Iir = Iil + NumVariables - 1;

        % Obtain the Variables for Current Image
        xi = x(Iil:Iir);

        % Calculate Derivativeons for the Unknowns if Image i.
        JacLBLPoint = JacobiansLBLPoint ( xi, K, Idx, NumVariables, LBLPointData );

        % Assign Jacobians
        for j = 1 : length ( Idx );
          J(Iil:Iir, k)   = WLBLPoint * JacLBLPoint(0*NumVariables+1:1*NumVariables, j); % x
          J(Iil:Iir, k+1) = WLBLPoint * JacLBLPoint(1*NumVariables+1:2*NumVariables, j); % y
          k = k + 2;
        end
      end
    end
  end

  % Jacobian entries from LBL Camera Readings
  if ~isempty ( LBLCamData );
    % When Vector of Weights
    if ~isscalar ( WLBLCam ); l = 1; end;
    for i = 1 : NbrImages;
      % Find the Angular Readings for image i
      if ~isempty ( find ( LBLCamData(:, 1) == i, 1 ) );
        if isscalar ( WLBLCam );
          WLBLCamX = WLBLCam;
          WLBLCamY = WLBLCam;
          WLBLCamZ = WLBLCam;
        else
          WLBLCamX = WLBLCam(l);
          WLBLCamY = WLBLCam(l+1);
          WLBLCamZ = WLBLCam(l+2);
          l = l + 3;
        end
        % Compute inidices to the variables of the Current Image
        Iil = ( i - 1 ) * NumVariables;
        % The JacobiansCam are -1
        JacLBLCam = -1;
        % Assign Jacobians
        J(Iil + 5, k)   = WLBLCamX .* JacLBLCam;  % x
        J(Iil + 6, k+1) = WLBLCamY .* JacLBLCam;  % y
        J(Iil + 7, k+2) = WLBLCamZ .* JacLBLCam;  % z
        k = k + 3;
      end
    end
  end
  
  % Jacobian entries from Angular Camera Readings
  if ~isempty ( AngCamData );
    % When Vector of Weights
    if ~isscalar ( WAngCam ); l = 1; end;
    for i = 1 : NbrImages;
      % Find the Angular Readings for image i
      if ~isempty ( find ( AngCamData(:, 1) == i, 1 ) );
        if isscalar ( WAngCam );
          WAngCamR = WAngCam;
          WAngCamP = WAngCam;
          WAngCamY = WAngCam;
        else
          WAngCamR = WAngCam(l);
          WAngCamP = WAngCam(l+1);
          WAngCamY = WAngCam(l+2);
          l = l + 3;
        end
        % Compute inidices to the variables for the Current Image 
        % (Only the quaternion is needed)
        Iil = ( i - 1 ) * NumVariables + 1;
        Iir = Iil + 3;

        % Obtain the Quaternion
        q = x(Iil:Iir);

        % Calculate Derivatives for the Current Image
        JacAng = JacobiansAngCam ( q );

        % Assign Jacobians
        J(Iil:Iir, k)   = WAngCamR * JacAng(1:4, 1);  % Roll
        J(Iil:Iir, k+1) = WAngCamP * JacAng(1:4, 2);  % Pitch
        J(Iil:Iir, k+2) = WAngCamY * JacAng(1:4, 3);  % Yaw
        k = k + 3;
      end
    end
  end

  % Jacobian Matrix is computed transposed because indexing columns
  % sequentially is faster than indexing rows sequentially.
  J = J';
end

