%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute the planar homography using the 3D vehicle pose.
%
%  File          : CalculateHomographies.m
%  Date          : 03/04/2006 - 19/04/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CalculateHomographies Using the pose (cRw and wTc) as the 3D vehicle
%                        pose (Extrinsic Parameters) and K as Intrinsic
%                        Parameters, compute the planar homography (and it's
%                        inverse) that maps points in the 2D Mosaic Frame
%                        to each 2D Image Frame.
%
%     Input Parameters:
%      x: 1x(NbrImages*NumVariables) variable vector.
%      K: 3x3 Intrinsic Camera Parameters Matrix.
%      NbrImages: Number of Images.
%      NumVariables: Number of variables per Frame in x that is
%                    equal to 7 = (q0, q1, q2, q3, x, y, z)
%
%     Output Parameters:
%      iHm: 3x3xNbrImages Set of absolute 2D Planar Homographies that
%           transform coordinates in the 2D Mosaic Frame to 2D Image Frame.
%      mHi: 3x3xNbrImages Set of absolute 2D Planar Homographies that
%           transform coordinates in the 2D Image Frame to 2D Mosaic Frame.
%
function [iHm mHi] = CalculateHomographies ( x, K, NbrImages, NumVariables )

  % Preallocate Homographies
  iHm = zeros ( 3, 3, NbrImages );
  mHi = iHm;

  % Compute K^(-1)
  K_1 = inv ( K );

  % Index to the current set of variables
  k = 0;

  % Each homography has 7 unknowns
  for i = 1 : NbrImages;
    % Convert Quaternion to Rotation Matrix 
    q = [ x(k+1), x(k+2), x(k+3), x(k+4) ];
    cRw = quat2rotmatMx ( q );

    % Reading X, Y, Z variables from the vector
    X = x(k+5);
    Y = x(k+6);
    Z = x(k+7);

    % Translation in Matrix form
    wITc = [1 0 -X; ...
            0 1 -Y; ...
            0 0 -Z];

    % Inverse of the Translation
    wITc_1 = [1 0 -X/Z; ...
              0 1 -Y/Z; ...
              0 0 -1/Z ];

    % Homography ( 2D Mosaic -> 2D Image )
    iHm(:,:,i) = K * cRw * wITc;
    % Inverse Homography ( 2D Image -> 2D Mosaic )
    mHi(:,:,i) = wITc_1 * cRw' * K_1;

    % Compute using Projection Matrix
    % i_P_M = K * [cRw cRw*IwTc(:,3)];
    % H_(:,:,i) = i_P_M(:, [1 2 4]);
    
    % Next set of Variables corresponding to the following image
    k = k + NumVariables;
  end
end
