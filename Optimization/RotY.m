function R = RotY ( a )
  % Compute a Rotation Matrix around Y Axis of a Rads

  R = [  cos(a),       0,  sin(a);
              0,       1,       0;
        -sin(a),       0,  cos(a) ];
end
