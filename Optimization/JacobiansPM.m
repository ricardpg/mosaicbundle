%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Jacobians for Point and Match.
%
%  File          : JacobiansPM.m
%  Date          : 03/04/2006 - 11/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  JacobiansPM Calculate the Jacobians for the Point-Match data.
%
%     Input Parameters:
%      xi: 1x7 variable vector corresponding to ith image frame.
%      xj: 1x7 variable vector corresponding to jth image frame.
%      K: 3x3 Intrinsic Camera Parameters Matrix.
%      NumVariables: Number of variables per Frame in x i.e. 7 = (q0, q1, q2, q3, X, Y, Z)
%      MatchPointData: kx4 matrix containing (u, v) matches coordinates in
%                      the i image for (u, v) points in the j frame at each
%                      row. For instance, k = 5 with the Top-Left, Top-Right,
%                      Bottom-Right, Bottom-Left edges and the Center as the
%                      points.
%
%     Output Parameters:
%      Jacobians: 4*2*NumVariablesxk matrix containing the evaluation of the
%                 derivatives:
%                   4*2*NumVariables: 4 Residuals: px-mx', py-my', mx-px', my-py' *
%                                     2 Images: Current i,  Reference j *
%                                     NumVariables Variables Current Image i + NumVariables Variables Reference Image j
%                   k: 1..Number of Point-Matches (typically 5: Edges + Image Center).
%

function Jacobians = JacobiansPM ( xi, xj, K, NumVariables, MatchPointData )

  % The Jacobians are computed analytically using the variables:
  %   au, av, cx, cy
  %   q1i, q2i, q3i, q4i, txi, tyi, tzi
  %   q1j, q2j, q3j, q4j, txj, tyj, tzj

  % So, convert input nomenclature to the analytic variables
  %
  % Camera Initrinsic Parameters
  au = K(1,1);
  av = K(2,2);
  cx = K(1,3);
  cy = K(2,3);

  % Precompute si
  Vars = [ au, av, cx, cy, xi, xj ];
  % Call the Mex function
  [s1, s2, s3, s4] = JacobiansPMMx ( Vars );
  
  % Zero rows in the s1, s2, s3 and s4 must contain the information
  % about the Point and Match, so it's substitued in the following for.
 
  % Derivatives in Jacobians must be read as:
  %   for each Point-Match (1..5), for each px, py, mx, my and
  %     for each residual px-mx', py-my', mx-px', my-py' and
  %       for each Image i and j variables
  
  % 4 Residuals for each Point and Match per 2 Images per NumVariables variables
  NumVars = 2 * NumVariables;
  Jacobians = zeros ( 4 * NumVars, size ( MatchPointData, 1 ) );
  for k = 1 : size ( MatchPointData, 1 );
    % k Correspondences Index
    % Current Image (x, y)
    px = MatchPointData ( k, 1 );
    py = MatchPointData ( k, 2 );
    % Reference Image (x, y)
    mx = MatchPointData ( k, 3 );
    my = MatchPointData ( k, 4 );

    % Call the Mex function: Returns Jacobians px-mx', py-my', mx-px', my-py'
    [ Jacobians(0*NumVars+1:1*NumVars, k), Jacobians(1*NumVars+1:2*NumVars, k), ...
      Jacobians(2*NumVars+1:3*NumVars, k), Jacobians(3*NumVars+1:4*NumVars, k) ] = JacobiansPMMx ( [Vars, px, py, mx, my], s1, s2, s3, s4 );
  end
end
