%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Calculate the Residuals for Point and Match.
%
%  File          : ResidualsPM.m
%  Date          : 03/04/2006 - 18/04/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  ResidualsPM Using Planar Homographies and tha Point-Match Information,
%              calculate the residuals.
%
%     Input Parameters:
%      iHw: 3x3xn 2D Planar Homographies that transform points in the 2D
%           Mosaic Frame to the 2D Image Frame.
%      wHi: 3x3xn 2D Planar Homographies that transform points in the 2D
%           Image Frame to the 2D Mosaic Frame.
%      MatchIdxTable: sx2 matrix that contains in each row the indices for s
%                     overlapping frames that can be consecutive or not. The
%                     order doesn't matter but must be according to MatchPointData
%                     Matrix. First Column contains the Current Image and
%                     the second Column contains the Reference Image.
%      MatchPointData: sx5x4 matrix that contain for each overlapping image the
%                      four edges and the center point for the first image and
%                      the matches in the second image.
%
%     Output Parameters:
%      r: s*5*4 vector containing the residuals. Two come from x, y using
%         the relative Homography iHj and two come from x, y using jHi
%         beeing i, j the pairs of overlapping images according to the
%         MatchPointData matrix.
%

function r = ResidualsPM ( iHw, wHi, MatchPointData, MatchIdxTable )

  % Residual Vector: Four Elements for each overlapping Pair of Frames
  r = zeros ( size ( MatchPointData, 1 ) * size ( MatchPointData, 2 ) * 4, 1 );

  % Pointer to the current Point-Match Residual quartet
  k = 1;

  % Scan overlapping pairs of images
  for s = 1 : size ( MatchPointData, 1 )
    % Get the Image Indices
    i = MatchIdxTable ( s, 1 ); % Current Image (Node in the Mosaic)
    j = MatchIdxTable ( s, 2 ); % Reference Image (Edge in the Mosaic)

    % Calculate the Relative Homograhies from Absolute Homographies
    iHj = iHw(:, :, i) * wHi(:, :, j); % cHr (Current H Reference)
    jHi = iHw(:, :, j) * wHi(:, :, i); % rHc (Reference H Current)

    % Scan correspondences in the overlapping pairs
    for t = 1 : size ( MatchPointData, 2 )
      % t correspondences index
      px = MatchPointData ( s, t, 1 ); % Current Image: x
      py = MatchPointData ( s, t, 2 ); %                y
      mx = MatchPointData ( s, t, 3 ); % Reference Image: x
      my = MatchPointData ( s, t, 4 ); %                  y

      % Calculate the Residuals
      % x_i - iHj * x_j
      P = iHj * [mx; my; 1];
      r(k  ) = px - P(1) / P(3);
      r(k+1) = py - P(2) / P(3);

      % x_j - jHi * x_i
      P = jHi * [px; py; 1];
      r(k+2) = mx - P(1) / P(3);
      r(k+3) = my - P(2) / P(3);

      % Next Pair of Frames in the residual vector
      k = k + 4;
    end
  end
end
