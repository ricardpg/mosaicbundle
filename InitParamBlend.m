%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Lucky Strike Data Sate Camera Initial Parameters.
%
%  File          : InitParamLucky.m
%  Date          : 06/09/2007 - 06/09/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Image Sizes (is not stored in the GMML Structure)
ImgWidth = 1024;
ImgHeight = 1024;

% 4 Pixel border eliminated
BorderX = 0;
BorderY = 0;

% Intrinsic Parameters Matrix
CCDWidth = 19.45;
CCDHeight = 19.45;

vFOVd = 51.862675759170592;
hFOVd = vFOVd;

vFOV = vFOVd * pi / 180;
hFOV = hFOVd * pi / 180;

fv = CCDHeight / (tan ( vFOV / 2 ) * 2);
fh = CCDWidth / (tan ( hFOV / 2 ) * 2);

ku = (ImgWidth + BorderX) / CCDWidth;
kv = (ImgHeight + BorderY) / CCDHeight;

u0 = ImgWidth / 2;
v0 = ImgHeight / 2;

% Assume square-pixel, using the mean of the fh and fv
f = ( fh + fv ) / 2;

au = f * ku;
av = f * kv;
K = [ au 0 u0; 0 av v0; 0 0 1];

% Olivier's K Matrix
% 
pixelSizefor1MeterOrK = 0.00095;
pixelSizeForMosaic = 0.010240379433006;

% K = [ 1/pixelSizefor1MeterOrK 0 ImgWidth/2;
%       0 1/pixelSizefor1MeterOrK ImgHeight/2;
%       0 0 1];

         