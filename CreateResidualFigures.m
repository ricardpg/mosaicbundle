%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Create figures to plot residuals.
%
%  File          : CreateResidualFigures.m
%  Date          : 15/07/2007 - 20/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CreateResidualFigures Create figures to plot residuals.
%
%     Input Parameters:
%      NumMatchPoint: Number of Match-Point residuals.
%      NumLBLPoint: Number of LBLPoint residuals.
%      NumLBLCam: Number of LBLCam residuals.
%      NumAngCam: Number of AngCam residuals.
%
%     Output Parameters:
%      ResMatchPointFig: Handle to Match-Point figure.
%      ResLBLPointFig: Handle to LBLPoint figure.
%      ResLBLCamFig: Handle to LBLCam figure.
%      ResAngCamFig: Handle to AngCam figure.
%

function [ ResMatchPointFig ResLBLPointFig ResLBLCamFig ResAngCamFig ] = ...
  CreateResidualFigures ( NumMatchPoint, NumLBLPoint, NumLBLCam, NumAngCam )

  % Test the input parameters
  error ( nargchk ( 4, 4, nargin ) );
  error ( nargoutchk ( 4, 4, nargout ) );

  % Figure Size
  FigW = 0.5;
  FigH = 0.45;    

  % Create individual figures for each set of residuals.
  if NumMatchPoint > 0;
    ResMatchPointFig = figure; s = 'Match-Point Residual (pixels)'; set ( ResMatchPointFig, 'Name', s ); title ( gca ( ResMatchPointFig ), s );
    hold on;
    set ( ResMatchPointFig, 'Units', 'normalized' );
    set ( ResMatchPointFig, 'Position', [0 1-FigH FigW FigH] );
  else
    ResMatchPointFig = [];
  end

  if NumLBLPoint > 0;
    ResLBLPointFig = figure; s = 'LBL-Point Residual (pixels)'; set ( ResLBLPointFig, 'Name', s ); title ( gca ( ResLBLPointFig ), s );
    hold on;
    set ( ResLBLPointFig, 'Units', 'normalized' );
    set ( ResLBLPointFig, 'Position', [0 0 FigW FigH] );
  else
    ResLBLPointFig = [];
  end

  if NumLBLCam > 0;
    ResLBLCamFig = figure; s = 'LBL-Camera Residual (m)'; set ( ResLBLCamFig, 'Name', s ); title ( gca ( ResLBLCamFig ), s );
    hold on;
    set ( ResLBLCamFig, 'Units', 'normalized' );
    set ( ResLBLCamFig, 'Position', [1-FigW 1-FigH FigW FigH] );
  else
    ResLBLCamFig = [];
  end

  if NumAngCam > 0;
    ResAngCamFig = figure; s = 'Angular Readings Residual (rads)'; set ( ResAngCamFig, 'Name', s ); title ( gca ( ResAngCamFig ), s );
    hold on;
    set ( ResAngCamFig, 'Units', 'normalized' );
    set ( ResAngCamFig, 'Position', [1-FigW 0 FigW FigH] );
  else
    ResAngCamFig = [];
  end
end


