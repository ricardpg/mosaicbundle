%
%  Autor(s)      : Arman Elibol & Jordi Ferrer Plana
%  e-mail        : aelibol@eia.udg.edu, jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Refine the input list of crossings.
%
%  File          : RefineCrossings.m
%  Date          : 27/02/2006 - 28/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  RefineCrossings Preview given mosaic with use of absolute homographies.
%                  First frame (node) is in green, last in blue and all
%                  others in red.
%
%      ListR = RefineCrossings ( List, NumImages )
%
%     Input Parameters:
%      List: 2xn Pairs of Images to test if there is a crossing.
%      NumImages: Number of images to test index in each direction arround
%                 each index.
%
%     Output Parameters:
%      ListR: Resulting list of crossing pairs.
%

function ListR = RefineCrossings ( List, NumImages )
  % Test the input parameters
  error ( nargchk ( 2, 2, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check NumImages
  if NumImages < 1 || NumImages > 255; ...
    error ( 'MATLAB:RefineCrossings:Input', 'NumImages must be a number in the range [1..255]!' );
  end
  
  % Number of elements in the input List
  L = size ( List, 2 );
  % Number of occurrences per pair
  K = 2 * NumImages + 1;
  K = K * K;
  % Total number of elements in the resulting list
  N = K * L;
  % Resulting List
  ListR = zeros ( 2, N );
  l = 1;
  for i = 1:L
    % i - NumImages .. i + NumImages    vs.    j - NumImages .. j + NumImages
    e = List(1,i);
    M1 = repmat ( e-NumImages:e+NumImages, NumImages * 2+1, 1 );
    e = List(2,i);
    M2 = repmat ( e-NumImages:e+NumImages, NumImages * 2+1, 1 )';
  
    ListR(:,l:l+K-1) = [ M1(:) M2(:) ]';
    l = l + K;
  end
end
