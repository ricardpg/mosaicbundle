%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute Non Sequential Homographies from data.
%
%  File          : NonSequentialHomographies.m
%  Date          : 13/03/2006 - 17/05/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%


% Clear Workspace
% clear;
close all; clc;

% Adding Toolboxes
% addpath /opt/Matlab/toolbox/...;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Homograpy Tests Path
CrossingsPath = '/mnt/data/MosaicBundle';
addpath ( [CrossingsPath '/HomoTest/' ], ...
          '-END' );

% Data
DataPath = [ CrossingsPath '/Data' ];

% Input Data
MosaicFileName = '/Filtered_Sequential.gmml';
MosaicFilePath = [ DataPath MosaicFileName ];
GPMMLFilePath = [ DataPath '/GPMMLFiles/Crossings/' ];

PathToFIM = './FIM';
PathToImages =  '/mnt/uvl/DataSets/Lustre96';
PathToResults = [ DataPath '/FIM/' ];
% PathToAccepted = [ PathToResults 'Crossings/Accepted/' ];
% PathToMotion = [ PathToResults 'Crossings/Motion/' ];
% PathToMosaic = [ PathToResults 'Crossings/Mosaic/' ];
PathToResultsFIM = [ PathToResults 'Crossings/ResultsFIM/' ];

PathToFilteredMosaic = [ DataPath '/Filtered_Sequential_Crossings.gmml' ];
PathToFilteredMat = [ DataPath '/Filtered_Sequential_Crossings_Criterion.mat' ];
FinalCrossingsFileName = '/FinalCrossings_5.mat';
FinalCrossingsFilePath = [ DataPath FinalCrossingsFileName ];
FilteredFinalCrossingsFileName = '/Filtered_FinalCrossings_5.mat';
FilteredFinalCrossingsFilePath = [ DataPath FilteredFinalCrossingsFileName ];

% Parameters to accept the Homographies
NumMatchesThreshold = 30;
OverlapAreaThreshold = 0.30;

% HRotThresh, YawThresh are more or less the same angle
HRotThresh = 180;

RollThresh = [-25 25];
PitchThresh = [-25 25];
YawThresh = [-180 180];

% Tape folders
FirstTape = 1;
LastTape = 25;

addpath ( PathToFIM );
SetPathsFIM ( PathToFIM );

% Image Sizes, K, etc...
InitParamLucky;

% Load Initial Crossings
disp ( sprintf ( 'Loading Final Crossings: "%s"...', FinalCrossingsFilePath ) );
load ( FinalCrossingsFilePath, 'FinalCrossings' );

% Load Mosaic if it is not already loaded
if ~exist ( 'M', 'var' );
  disp ( sprintf ( 'Loading Mosaic: "%s"...', MosaicFilePath ) );
  M = gmml_load ( MosaicFilePath );
end

if ~exist ( 'Mf', 'var' );
  disp ( 'Initializing Resulting Mosaic...' );
  Mf = M;
end

% Create GPMML Folder
mkdir ( GPMMLFilePath );
% Create Tape folders
for j = FirstTape:LastTape
  Tape = sprintf ( 'tape%3.3d', j );
  mkdir ( GPMMLFilePath, Tape );
end

if ~exist ( 'LastCrossing', 'var' );
  LastCrossing = size ( FinalCrossings, 2 );
end

if ~exist ( 'i', 'var' );
  % The final list

  FilteredFinalCrossings = zeros ( 2, LastCrossing );
  l = 0;

  NotFoundFIMResults = zeros ( 1, LastCrossing );
  b = 0;

  BadHomographies = zeros ( 10, 10000 );
  BadHomographiesData = zeros ( 10, 10000 );
  s = 0;

  FirstCrossing = 1;
else
  FirstCrossing = i;
end

N = LastCrossing - FirstCrossing + 1;

InitTime = cputime;

for i = FirstCrossing:LastCrossing;
  ItTime = cputime;

  Prefix = sprintf ( '%5.5d_%5.5d', FinalCrossings(1,i), FinalCrossings(2,i) );
  ResultsFIMFileName = [ Prefix '.mat' ];
  ResultsFIMFilePath = [ PathToResultsFIM ResultsFIMFileName ];
  ResultsFIMName = [ 'ResultsFIM_' Prefix ];

  if exist ( ResultsFIMFilePath, 'file' ) ~= 0;
    % Load Data
    ResultsFIMStruct = load ( ResultsFIMFilePath, ResultsFIMName );
    ResultsFIM = ResultsFIMStruct.(ResultsFIMName);

    % Check minimum number of matches
    NumMatches = size ( ResultsFIM.AcceptedMatches, 2 );
    NumMatchesOk = NumMatches > NumMatchesThreshold;

    H = ResultsFIM.Homography;

    if ~isempty ( H );
      HInv = inv(H);
      % Check Geometric Integrity
      GOk = CheckHomoGeometric ( ImgWidth, ImgHeight, H );
      GInvOk = CheckHomoGeometric ( ImgWidth, ImgHeight, HInv );
      % Check Overlapping area
%      Area = HomoOverlapArea ( H, ImgWidth, ImgHeight, ImgWidth * ImgHeight );
      Area = mosaic_nodes_overlap ( H, ImgHeight, ImgWidth );
      AreaOk = Area > OverlapAreaThreshold;
%      AreaInv = HomoOverlapArea ( HInv, ImgWidth, ImgHeight, ImgWidth * ImgHeight / 2 );
      AreaInv = mosaic_nodes_overlap ( HInv, ImgHeight, ImgWidth );
      AreaInvOk = AreaInv > OverlapAreaThreshold;
%       % Check Angle
%       [Angle, Vec, R, d1, d2] = checkhomogrot ( H, K );
%       if ~isempty ( Angle );
%         AngleD = Angle * 180 / pi;
%         AngleOk = AngleD > LowAngle & AngleD < HighAngle;
%       else
%         AngleOk = 0;
%       end
      % Homography rotation
      [LooksOk LooksAngleD] = homographyLooksGood ( H, HRotThresh );
      % 3D Rotation
      [Roll, Pitch, Yaw] = HomoTo3DAngles ( H, K );
      RollD = Roll * 180 / pi;
      PitchD = Pitch * 180 / pi;
      YawD = Yaw * 180 / pi;
      RollOk = RollD > RollThresh(1) && RollD < RollThresh(2);
      PitchOk = PitchD > PitchThresh(1) && PitchD < PitchThresh(2);
      YawOk = YawD > YawThresh(1) && YawD < YawThresh(2);
    else
      AreaOk = 0;
      AreaInvOk = 0;
      GOk = 0;
      GInvOk = 0;
      LooksOk = 0;
      RollOk = 0;
      PitchOk = 0;
      YawOk = 0;
    end

    % Check All conditions
    if NumMatchesOk && AreaOk && AreaInvOk && GOk && GInvOk && LooksOk && ...
       RollOk && PitchOk && YawOk

      % Add the filtered crossing
      l = l + 1;
      FilteredFinalCrossings(:,l) = FinalCrossings(:,i);

      Idx = FinalCrossings(1,i);
      
      % Compute the tape Subfolder
      ImageFileName = Mf.nodes(Idx).image;
      TapeList = regexp ( ImageFileName, 'tape[\d]+', 'match' );
      if isempty ( TapeList ) || length ( TapeList ) ~= 1;
        error ( 'MATLAB:SequentialHomographies:Input', ...
                sprintf ( 'Tape not found at "%s"', ImageFileName ) );
      end
      % Get the first
      Tape = TapeList{1};

      % Setup the edge
      Edge.index = FinalCrossings(2,i);
      Edge.homo.model = 'pro';
      Edge.homo.matrix = H;
      Edge.homo.type = 'rel';
      Edge.homo.covar = [0 0 0; 0 0 0; 0 0 0];
      Edge.pointmatches = [ GPMMLFilePath Tape '/PM-' Prefix '.gpmml' ];

      % Add the new edge
      NumEdges = length ( Mf.nodes(Idx).edges );
      if NumEdges == 0;
        Mf.nodes(Idx).edges = Edge;
      else
        Mf.nodes(Idx).edges(NumEdges+1) = Edge;
      end

      % Setup the GPMML
      Points = zeros ( size ( ResultsFIM.AcceptedMatches, 2 ), 3 );
      Points(:,1:2) = ResultsFIM.Img2Features(1:2,ResultsFIM.AcceptedMatches(1,:))';
      Points(:,3) = 1;

      Matches = zeros ( size ( ResultsFIM.AcceptedMatches, 2 ), 3 );
      Matches(:,1:2) = ResultsFIM.Img1Features(1:2,ResultsFIM.AcceptedMatches(2,:))';
      Matches(:,3) = 1;

      % Save the GPMML
      gpmml_save ( Edge.pointmatches, [Points Matches] );
    else

      s = s + 1;
      BadHomographies(1,s) = i;
      BadHomographies(2,s) = NumMatchesOk;
      BadHomographies(3,s) = AreaOk;
      BadHomographies(4,s) = AreaInvOk;
      BadHomographies(5,s) = GOk;
      BadHomographies(6,s) = GInvOk;
      BadHomographies(7,s) = LooksOk;
      BadHomographies(8,s) = RollOk;
      BadHomographies(9,s) = PitchOk;
      BadHomographies(10,s) = YawOk;

      BadHomographiesData(1,s) = i;
      BadHomographiesData(2,s) = NumMatches;
      BadHomographiesData(3,s) = Area;
      BadHomographiesData(4,s) = AreaInv;
      BadHomographiesData(5,s) = GOk;
      BadHomographiesData(6,s) = GInvOk;
      BadHomographiesData(7,s) = LooksOk;
      BadHomographiesData(8,s) = RollD;
      BadHomographiesData(9,s) = PitchD;
      BadHomographiesData(10,s) = YawD;
    end
  else
    b = b + 1;
    NotFoundFIMResults(b) = i;
  end

  if mod ( i, 100 ) == 0;
    FinalTime = cputime;
    Time = FinalTime - ItTime;
    AllTime = (( FinalTime - InitTime ) * (N - i + 1)) / (i - FirstCrossing + 1);
    disp ( sprintf ( 'Last Iteration time %f s. Expected Time %f s. (%f hours)', Time, AllTime, AllTime / 3600 ) );
    disp ( sprintf ( 'Right Homographies: %d, Bad Homographies %d, Not Found FIM Results %d (%d/%d)\n\n', l, s, b, i, N ) );
  end
end

% Cut the Final Lists
if l > 0; FilteredFinalCrossings = FilteredFinalCrossings(:,1:l); else FilteredFinalCrossings = []; end;
if s > 0;
  BadHomographies = BadHomographies(:,1:s);
  BadHomographiesData = BadHomographiesData(:,1:s);
  warning ( 'MATLAB:SequentialHomographies:Input', ...
            'There are bad homographies in the trajectory!' );
else
  BadHomographies = [];
  BadHomographiesData = [];
end
if b > 0;
  NotFoundFIMResults = NotFoundFIMResults(1:b); 
  warning ( 'MATLAB:NonSequentialHomographies:Input', ...
            'There are NOT FOUND FIM Results!' );

else
  NotFoundFIMResults = [];
end

% Load Initial Crossings
disp ( sprintf ( 'Saving Final Filtered Crossings: "%s"...', FilteredFinalCrossingsFilePath ) );
save ( FilteredFinalCrossingsFilePath, 'FilteredFinalCrossings' );

% Save the final mosaic
disp ( sprintf ( 'Saving Final Mosaic: "%s"...', PathToFilteredMosaic ) );
gmml_save ( PathToFilteredMosaic, Mf );

% Save the Filtering Criteria
disp ( sprintf ( 'Saving Filtering Criteria: "%s"...', PathToFilteredMat ) );
save ( PathToFilteredMat, 'BadHomographies', 'BadHomographiesData' );
