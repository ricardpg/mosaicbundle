%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Pianosa Data Set Bundle Adjustment.
%
%  File          : OptimPianosa.m
%  Date          : 14/03/2006 - 19/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Base Path
BasePath = '/mnt/data/MosaicBundle';

% Stereo Tracker Path
StereoTrackerPath = [ BasePath '/STracker/Stereo' ];
addpath ( [StereoTrackerPath '/5_MotionEstimation/3D/Quaternions/' ], ...
           '-END' );

% Optimization Path
addpath ( [ BasePath '/Optimization' ], '-END' );
addpath ( [ BasePath '/HomoTest' ], '-END' );

% Data
DataPath = '/mnt/uvl_data/Pianosa/06-10-04';
%ImgPath = [DataPath '/'];
ImgPath = '';

% Input Data
MosaicFileName = '/Pianosa.gmml';
MosaicFilePath = [ DataPath MosaicFileName ];

OptimizedMosaicFileName = '/Opt_Pianosa.gmml';
ResultingDataFileName = '/Opt_Data.mat';
OptimizedMosaicFilePath = [ DataPath OptimizedMosaicFileName ];
ResultingDataFilePath = [ DataPath ResultingDataFileName ];

% Load Mosaic if it is not already loaded
if ~exist ( 'M', 'var' )
  disp ( sprintf ( 'Loading Mosaic "%s"...', MosaicFilePath ) );
  M = gmml_load ( MosaicFilePath );
end

% Image Sizes, K, etc...
InitParamPianosa;

% 2D Planar Homography with the scaling factor to save the final Absolute
% Homographies in pixels
Hs = [ 1/M.init.pixel_size.x            0             0; 
                0             1/M.init.pixel_size.y   0;
                0                      0              1 ];


% --[ Convert Data in order to be used in the Optimization ]---------------
disp ( 'Computing Trajectory Segment End-Nodes...' );

% Consider the segment extrems that are "floating" as static:
%  Use the LBL as a Real LBL Camera Reading
NumNodes = size ( M.nodes, 2 );

% Points at UTM Coordinates
FixedPointNodes = [];

% Cameras at UTM Coordinates
FixedCameraNodes = [];

% Angular Readings
AngCamNodes = [];
  
% Number of Correspondences for the Point-Match (0 => Corners)
NumPoints = 12;

disp ( 'Converting source data for the Bundle...' );
tic;
[RTs, MatchIdxTable, MatchPointData, LBLPointData, LBLCamData, AngCamData] = ...
      ConvertDataToBundle ( M, ImgPath, FixedPointNodes, FixedCameraNodes, AngCamNodes, NumPoints );
toc

% Setup the LBLPointData Manually (the center of the image is not the LBL Point)
%LBLPointData = [   1 1234 390 10 10;   2 1263 724 10 10;                        % Bottom Right Corner
%                  18 1327  22 10  0;  19 1346 187 10  0;  20 1390 283 10 0;     % Top Right Corner
%                 230 1336 626  0  0; 231 1360 756  0  0; 232 1429 922  0 0 ];   % Top Left Corner

LBLPointData = [ ...
   1 1234 390 10 10;   2 1263 724 10 10;
  18 1327  22 10  0;  19 1346 187 10  0;  20 1390 283 10 0;
 230 1336 626  0  0; 231 1360 756  0  0; 232 1429 922  0 0;

   1  615 865 8   16.25;
   1  460 115 7.7 13.75;
   2  444 334 7.7 13.75;
   3  463 676 7.7 13.75;    
   3  578  34 8   11.25;
   4  480 430 8   11.25;
   5  423 706 8   11.25;
   6  451 222 8    8.75;
   7  482 383 8    8.75;
   8  478 584 8    8.75;
   9  508 768 8    8.75;
   9  428   6 8    6.25;
  10  495 952 8    8.75;
  10  367 179 8    6.25;
  11  360 412 8    6.25;
  12  355 710 8    6.25;
  13  355 914 8    6.25;
  27  860 115 8    6.25;
  28  863 247 8    6.25;
  29  878 427 8    6.25;
  30  842 499 8    6.25;
  31  834 667 8    6.25;
  32  810 832 8    6.25;
  32  822  75 8    8.75;
  33  811 999 8    6.25;
  33  827 224 8    8.75;
  34  822 404 8    8.75;
  35  791 576 8    8.75;
  36  803 728 8    8.75;
  37  784 906 8    8.75;
  37  940  38 8   11.25;
  38  876 244 8   11.25;
  39  826 334 8   11.25;
  40  814 594 8   11.25;
  41  804 747 8   11.25;
  42  768 936 8   11.25;
  42  872  82 7.7 13.75;
  43  832 232 7.7 13.75;
  44  840 376 7.7 13.75;
  45  812 508 7.7 13.75;
  46  810 692 7.7 13.75;
  47  806 824 7.7 13.75;
  48  758 992 7.7 13.75;
  48  548   0 8   16.25;
  48 1456 823 5.5 13.75;
  49  522 260 8   16.25;
  49 1383 136 5.5 16.25;
  50  586 206 8   16.25;
  50 1332  72 5.5 16.25;
  51  534 490 8   16.25;
  51 1298 286 5.5 16.25;
  52  488 620 8   16.25;
  52 1303 355 5.5 16.25;
  53  424 963 8   16.25;
  53 1276 610 5.5 16.25;
  54 1292 796 5.5 16.25;
  55  826 492 5.5 16.25;
  56  858 799 5.5 16.25;
  57  875 954 5.5 16.25;
  57  718  16 5.5 13.75;
  58  740 250 5.5 13.75;
  58 1342 128 7.7 13.75;
  59  815 326 5.5 13.75;
  60  824 500 5.5 13.75;
  61  830 690 5.5 13.75;
  62  804 972 5.5 13.75;
  62  766  68 5.5 11.25;
  63  719 260 5.5 11.25;
  64  768 486 5.5 11.25;
  65  728 770 5.5 11.25;
  66  683  95 5.5  8.75;
  67  632 291 5.5  8.75;
  67 1478 182 8    8.75;
  68  628 498 5.5  8.75;
  68 1479 372 8    8.75;
  69  616 672 5.5  8.75;
  69 1476 522 8    8.75;
  70  614 874 5.5  8.75;
  71  526 198 5.5  6.25;
  71 1374  82 8    6.25;
  72  532 332 5.5  6.25;
  72 1415 214 8    6.25;
  73  512 580 5.5  6.25;
  73 1464 439 8    6.25;
  74  499 732 5.5  6.25;
  74 1455 554 8    6.25;
  75  510 898 5.5  6.25;
  75 1479 732 8    6.25;
  94  712  72 5.5  6.25;
  94 1478  55 3    6.25;
  95  727 172 5.5  6.25;
  96  684 314 5.5  6.25;
  97  720 516 5.5  6.25;
  98  714 708 5.5  6.25;
  99  666 956 5.5  6.25;
  99  748  86 5.5  8.75;
 100  740 212 5.5  8.75;
 101  710 380 5.5  8.75;
 102  690 539 5.5  8.75;
 103  691 740 5.5  8.75;
 104  724 866 5.5  8.75;
 105  702 999 5.5  8.75;
 105  703 126 5.5 11.25;
 106  707 278 5.5 11.25;
 107  690 444 5.5 11.25;
 108  732 652 5.5 11.25;
 109  668 862 5.5 11.25;
 109  690  66 5.5 13.75;
 109 1487 115 3   13.75;
 109   28 142 7.7 13.75;
 110  686 972 5.5 11.25;
 110  691 158 5.5 13.75;
 110   23 252 7.7 13.75;
 111  659 370 5.5 13.75;
 111 1503 340 3   13.75;
 112  695 528 5.5 13.75;
 113  695 663 5.5 13.75;
 114  675 862 5.5 13.75;
 115  595  67 5.5 16.25;
 115 1470  18 3   16.25;
 116  655 230 5.5 16.25;
 117  656 336 5.5 16.25;
 118  648 496 5.5 16.25;
 119  551 771 5.5 16.25;
 119 1466 700 3   16.25;
 120  712 414 3   16.25;
 121  734 592 3   16.25;
 122  743 756 3   16.25;
 123  720 876 3   16.25;
 124  688 143 3   13.75;
 125  728 319 3   13.75;
 126  712 424 3   13.75;
 127  726 598 3   13.75;
 128  746 822 3   13.75;
 129  747 994 3   13.75;
 129  766 154 3   11.25;
 130  772 278 3   11.25;
 131  771 535 3   11.25;
 132  732 623 3   11.25;
 133  752 776 3   11.25;
 134  748 915 3   11.25;
 134  712  78 3    8.75;
 135  768 999 3   11.25;
 135  738 160 3    8.75;
 136  724 327 3    8.75;
 137  703 510 3    8.75;
 138  758 590 3    8.75;
 139  750 812 3    8.75;
 140  738 942 3    8.75;
 140  791 118 3    6.25;
 141  820 170 3    6.25;
 142  803 274 3    6.25;
 143  791 435 3    6.25;
 144  840 583 3    6.25;
 145  843 799 3    6.25;
 146  803 963 3    6.25;
 165  584 110 3    6.25;
 166  575 216 3    6.25;
 167  587 423 3    6.25;
 168  566 556 3    6.25;
 169  560 670 3    6.25;
 170  522 882 3    6.25;
 171  602 984 3    6.25;
 171  627  95 3    8.75;
 172  604 247 3    8.75;
 173  624 346 3    8.75;
 174  646 472 3    8.75;
 175  622 608 3    8.75;
 176  622 728 3    8.75;
 177  608 864 3    8.75;
 177  644  11 3   11.25;
 178  618 147 3   11.25;
 179  583 300 3   11.25;
 180  590 500 3   11.25;
 181  615 598 3   11.25;
 182  588 794 3   11.25;
 183  576 936 3   11.25;
 183  543  96 3   13.75;
 184  558 208 3   13.75;
 185  566 372 3   13.75;
 186  516 567 3   13.75;
 187  598 624 3   13.75;
 188  594 767 3   13.75;
 189  582 916 3   13.75;
 189  515   2 3   16.25;
 190  515 182 3   16.25;
 191  495 340 3   16.25;
 192  494 507 3   16.25;
 193  536 675 3   16.25 ];

LBLPointData = sortrows ( LBLPointData, 1 );

% --[ Plot Source Trajectory and Mosaic ]----------------------------------
% disp ( 'Displaying Initial Trajectory...' );
% h = figure;
% s = 'Initial Trajectory';
% set ( h, 'Name', s );
% title ( gca ( h ), s );
% axis equal;
% axis ij;
% hold on;
% MosaicTrajectoryAbs ( M, ImgWidth, ImgHeight, -1, -1, [], [], false, false, false );

disp ( 'Displaying Initial Mosaic...' );
h = figure;
s = 'Source Mosaic Preview';
set ( h, 'Name', s );
title ( gca ( h ), s );
axis equal;
axis ij;
hold on;
MosaicPreviewAbs ( M, ImgWidth, ImgHeight, -1, -1, 1, [], true );


% --[ Perform the Global Optimization ]------------------------------------

disp ( 'Running Optimization...' );
% Residual Weights
WMatchPoint = 1 * pixelSizeForMosaic;  % Meters
% WMatchPoint = 1;
WLBLPoint = 1 * pixelSizeForMosaic;    % Meters
WLBLCam = 0;
WAngCam = [];

x0 = [];
JPattern_Ini = [];
DoNormalization = true;
NumIter = 15;

disp ( 'Weight Configuration...' );
fprintf ( '  Point-Match: ' );
if ~isempty ( WMatchPoint ); fprintf ( '%f\n', WMatchPoint ); else fprintf ( 'Not Used\n' ); end;
fprintf ( '  LBL-Point  : ' ); 
if ~isempty ( WLBLPoint ); fprintf ( '%f\n', WLBLPoint ); else fprintf ( 'Not Used\n' ); end;
fprintf ( '  LBL-Camera : ' );
if ~isempty ( WLBLCam ); fprintf ( '%f\n', WLBLCam ); else fprintf ( 'Not Used\n' ); end;
fprintf ( '  Cam Angle  : ' );
if ~isempty ( WAngCam ); fprintf ( '%f %f %f\n', WAngCam(1), WAngCam(2), WAngCam(3) ); else fprintf ( 'Not Used\n' ); end;

tic;
[iHw_Ini wHi_Ini iHw_Opt wHi_Opt x Jacobians JPattern Residual] = ...
     RunOptimization ( K, RTs, NumIter, WMatchPoint, MatchIdxTable, MatchPointData, ...
                       WLBLPoint, LBLPointData, WLBLCam, ...
                       LBLCamData, WAngCam, AngCamData, x0, JPattern_Ini, DoNormalization );
toc

% Continue iterations
NumIt = input ( 'Enter how many iterations more: ' );
NumIter = round ( NumIt );
while NumIter > 0;
  tic;
  [iHw_Ini wHi_Ini iHw_Opt wHi_Opt x Jacobians JPattern Residual] = ...
       RunOptimization ( K, RTs, NumIter, WMatchPoint, MatchIdxTable, MatchPointData, ...
                         WLBLPoint, LBLPointData, WLBLCam, ...
                         LBLCamData, WAngCam, AngCamData, x, JPattern, DoNormalization );
  toc

  NumIt = input ( 'Enter how many iterations more: ' );
  NumIter = round ( NumIt );
end


% Number of Homographies (The same than entries in RTs)
NumHomo = size ( iHw_Opt, 3 );

% Copy the source mosaic
M_Opt = M;
              
if NumHomo ~= size ( M_Opt.nodes, 2 );
  error ( 'MATLAB:Optimization:Input', 'Incorrect number of output homographies' );
end

for i = 1 : NumHomo;
  H = Hs * wHi_Opt(:,:,i);
  M_Opt.nodes(i).homo.matrix = H / H(3,3);
end


% --[ Plot Destination Trajectory and Mosaic ]-----------------------------

% disp ( 'Displaying Final Trajectory...' );
% h = figure;
% s = 'Final Trajectory';
% set ( h, 'Name', s );
% title ( gca ( h ), s );
% axis equal;
% axis ij;
% hold on;
% MosaicTrajectoryAbs ( M_Opt, ImgWidth, ImgHeight, -1, -1, [], [], false, false, false );

disp ( 'Displaying Final Mosaic...' );
h = figure;
s = 'Final Mosaic Preview';
set ( h, 'Name', s );
title ( gca ( h ), s );
axis equal;
axis ij;
hold on;
MosaicPreviewAbs ( M_Opt, ImgWidth, ImgHeight, -1, -1, 1, [], true );


% --[ Save the Optimized final Mosaic ]------------------------------------
disp ( 'Calculating mosaic size...' );
M_Res = mosaic_calc_size ( M_Opt );

disp ( sprintf ( 'Saving Final Optimized Mosaic "%s"...', OptimizedMosaicFilePath ) );
gmml_save ( OptimizedMosaicFilePath, M_Res );

disp ( sprintf ( 'Saving Data (Residual, poses, etc...) "%s"...', ResultingDataFilePath ) );
save ( ResultingDataFilePath );

% --[ Draw Mosaics ]-------------------------------------------------------
disp ( 'Drawing mosaics...' );
%mosaic_draw ( M );
%mosaic_draw ( M_Res );
