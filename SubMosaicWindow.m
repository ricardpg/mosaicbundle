%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Extract the submosaic given a GMML structure and a window.
%
%  File          : SubMosaicWindow.m
%  Date          : 06/03/2006 - 08/03/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  SubMosaicWindow Extract the submosaic given a GMML structure and a window.
%
%      [MR NodeMap, Segments] = SubMosaicWindow ( M, ImgWidth, ImgHeight,
%                                                 First, Last, P1, P2, UseImageCenter )
%
%     Input Parameters:
%      M: GMML Mosaic Structure.
%      ImgWidth: Image Width.
%      ImgHeight: Image Height.
%      First: Initial Image.
%      Last: Final Image.
%      P1: Top-Left corner to define the window.
%      P2: Bottom-Right corner of the window.
%      UseImageCenter: Use the center of the image instead of the corners 
%                      as criterion to check if the images are inside the window.
%
%     Output Parameters:
%      Mw: Resulting cropped GMML Mosaic Structure.
%      NodeMap: 1xn vector mapping the old nodes to the new ones.
%      Segments: 2xn matrix giving at each column the start and end for
%                each connected segment in the mosaic.
%

function [Mw NodeMap, Segments] = SubMosaicWindow ( M, ImgWidth, ImgHeight, First, Last, P1, P2, UseImageCenter )
  % Test the input parameters
  error ( nargchk ( 7, 8, nargin ) );
  error ( nargoutchk ( 3, 3, nargout ) );

  if nargin == 7; UseImageCenter = true; end

  N = size ( M.nodes, 2 );
  % If they are out of range, take the limits
  if First < 1 || First > N; First = 1; end
  if Last > N || Last < First; Last = N; end;

  if length ( P1 ) ~= 2 || length ( P2 ) ~= 2;
    error ( 'MATLAB:SubMosaicWindow:Input', 'P1 and P2 must be two points in (x,y) format!' );
  end

  if P1(1) >= P2(1) || P1(2) >= P2(2);
    error ( 'MATLAB:SubMosaicWindow:Input', 'P1 and P2 points must define a 2D window!' );
  end

  % Image Coordinates
  x1 = 1;
  y1 = 1;
  x2 = ImgWidth;
  y2 = ImgHeight;

  % Edges and Center coordinates
  p1 = [x1; y1; 1];
  p2 = [x1; y2; 1];
  p3 = [x2; y1; 1];
  p4 = [x2; y2; 1];
  pc = [ImgWidth/2; ImgHeight/2; 1];

  % Mapping function from the Child to the Father
  ChildOf = zeros ( 1, N );

  % To detect "Errors" in the mosaic
  EmptyNodes = 0;
  MultipleParent = 0;
  MultipleChild = 0;
  SortError = 0;

  % Compute the first and last node to check later
  FirstNode = +Inf;
  LastNode = -Inf;

  % Number of valid nodes in the specified region
  NumNodes = 0;
  % Scan the Nodes
  for i = First : Last;
    % Homography
    H = M.nodes(i).homo.matrix;

    if UseImageCenter;
      c = H * pc; c = c ./ c(3);
      IsInside = InWindowBounds ( c, P1, P2 );
    else
      % Compute the warping
      r1 = H * p1;  r1 = r1 ./ r1(3,1);
      r2 = H * p2;  r2 = r2 ./ r2(3,1);
      r3 = H * p3;  r3 = r3 ./ r3(3,1);
      r4 = H * p4;  r4 = r4 ./ r4(3,1);
      IsInside = InWindowBounds ( r1, P1, P2 ) && InWindowBounds ( r2, P1, P2 ) && ...
                 InWindowBounds ( r3, P1, P2 ) && InWindowBounds ( r4, P1, P2 );
    end

    % Check if all the corners of the image are in bounds
    if IsInside;
      % Check if the Image has only one edge
      NEdges = length ( M.nodes(i).edges );
      if NEdges == 1;
        % Homography
        Idx = M.nodes(i).edges(1).index;

        if i <= Idx;
           SortError = SortError + 1;
        end
        
        H = M.nodes(Idx).homo.matrix;

        if UseImageCenter;
          c = H * pc; c = c ./ c(3);
          IsInside = InWindowBounds ( c, P1, P2 );
        else
          % Compute the warping
          r1 = H * p1;  r1 = r1 ./ r1(3,1);
          r2 = H * p2;  r2 = r2 ./ r2(3,1);
          r3 = H * p3;  r3 = r3 ./ r3(3,1);
          r4 = H * p4;  r4 = r4 ./ r4(3,1);
          IsInside = InWindowBounds ( r1, P1, P2 ) && InWindowBounds ( r2, P1, P2 ) && ...
                     InWindowBounds ( r3, P1, P2 ) && InWindowBounds ( r4, P1, P2 );
        end

        if IsInside;
          % Build the mappings
          if ChildOf(Idx) == 0;
            % Compute the boundaries
            if Idx < FirstNode; FirstNode = Idx; end;
            if Idx > LastNode; LastNode = Idx; end;

            ChildOf(Idx) = i;
            NumNodes = NumNodes + 1;
          else
            MultipleParent = MultipleParent + 1;
          end
        end
      elseif NEdges == 0;
        % No edges
        EmptyNodes = EmptyNodes + 1;
      else
        % More than one edge
        MultipleChild = MultipleChild + 1;
      end
    end
  end

  % If there is nodes in the window: build the new mosaic
  if NumNodes > 0;
    % Init the Header
    Mw.init = M.init;
    % I don't know if it is worth to change:
    %   M.init.mosaic_size
    %   M.init.mosaic_origin

    % Init the Nodes
    Mw.nodes = repmat ( M.nodes(1), 1, NumNodes );

    % The worst case: anything is connected
    Segments = zeros ( 2, N );
    r = 0;

    % Old Node -> New Node Mapping vector
    NodeMap = zeros ( 1, N );

    i = FirstNode;
    l = 1;
    while i <= LastNode;
      % Look for two: check that there is more than one node
      v1 = i;
      Mw.nodes(l) = M.nodes(v1);
%      Mw.nodes(l).index = l;
      Mw.nodes(l).edges = [];
      NodeMap(v1) = l;
      % Store the start of segment
      r = r + 1;
      Segments(1,r) = v1;

      l = l + 1;

      v2 = ChildOf(v1);
      while v2 ~= 0;
        Mw.nodes(l) = M.nodes(v2);
%        Mw.nodes(l).index = l;
        if ~isempty ( Mw.nodes(l).edges );
          Mw.nodes(l).edges(1).index = l-1;
        end
        NodeMap(v2) = l;
        l = l + 1;
        v1 = v2;
        v2 = ChildOf(v2);
      end
      % Store the end of segment
      Segments(2,r) = v1;

      i = v1 + 1;
      % Skip empty
      while i <= LastNode && ChildOf(i) == 0; i = i + 1; end;
    end

    % Cut the result
    if r > 0; Segments = Segments(:,1:r); else Segments = []; end
  else
    Mw = [];
    NodeMap = [];
    Segments = [];
  end

  % Show warnings
  if EmptyNodes > 0;
    warning ( 'MATLAB:SubMosaicWindow:Input', sprintf ( 'There are %d nodes without any child!', EmptyNodes ) );
  end
  if MultipleChild > 0;
    warning ( 'MATLAB:SubMosaicWindow:Input', sprintf ( 'There are %d nodes with multiple children!', MultipleChild ) );
  end  
  if MultipleParent > 0;
    warning ( 'MATLAB:SubMosaicWindow:Input', sprintf ( 'There are %d nodes with multiple parents!', MultipleParent ) );
  end
  if SortError > 0;
    warning ( 'MATLAB:SubMosaicWindow:Input', sprintf ( 'There are %d parent nodes with lower index than their child!', EmptyNodes ) );
  end
end


% Check if P is in the window defined by P1 and P2
% No check is performed:
%   - P1 and P2 must define a 2D window
%   - P, P2 and P2 must be 1x2 vectors.
function Ok = InWindowBounds ( P, P1, P2 )
  Ok = P(1) >= P1(1) && P(1) <= P2(1) && ...
       P(2) >= P1(2) && P(2) <= P2(2);
end

