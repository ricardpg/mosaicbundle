%Seed = sum(100*clock);
rand ( 'twister', Seed );

Width = 384;
Height = 288;
N = 50;
K = 8;

clear P;
P(:,1) = ceil(Width.*rand(N,1));
P(:,2) = ceil(Height.*rand(N,1));
P(:,3) = ceil(Width.*rand(N,1));
P(:,4) = ceil(Height.*rand(N,1));

[M, I] = SpreadPoints ( P, K, Width, Height );

A = P;
i=1;
Xpi=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);
i=2;
Ypi=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);

i=3;
Xmi=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);
i=4;
Ymi=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);


A = M;
i=1;
Xp=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);
i=2;
Yp=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);

i=3;
Xm=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);
i=4;
Ym=reshape ( [A(:,i)'; repmat([nan],size(A(:,i)))'], size(A(:,i),1)*2,1);


figure;
plot ( Xpi, Ypi, 'x', 'MarkerEdgeColor','k',...
                      'MarkerFaceColor','k',...
                      'MarkerSize', 5 );
hold on;
plot ( Xmi, Ymi, '+', 'MarkerEdgeColor','k',...
                      'MarkerFaceColor','k',...
                      'MarkerSize', 5 );

plot ( Xp, Yp, 'o', 'MarkerEdgeColor','g',...
                    'MarkerFaceColor','g',...
                    'MarkerSize', 6 );
plot ( Xm, Ym, 'o', 'MarkerEdgeColor','r',...
                    'MarkerFaceColor','r',...
                    'MarkerSize', 6 );

