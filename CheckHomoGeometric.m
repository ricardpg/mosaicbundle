%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Check whether a Homography is geometrically possible or not.
%
%  File          : CheckHomoGeometric.m
%  Date          : 13/03/2006 - 13/03/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  CheckHomoGeometric Check whether a Homography is geometrically possible
%                     or not.
%
%      Ok = CheckHomoGeometric ( ImgWidth, ImgHeight, H )
%
%     Input Parameters:
%      ImgWidth: Images Width.
%      ImgHeight: Images Height.
%      H: 3x3 Planar Homography transformation.
%
%     Output Parameters:
%      Ok: True if all the checks are passed.
%

function Ok = CheckHomoGeometric ( ImgWidth, ImgHeight, H )
  % Test the input parameters
  error ( nargchk ( 3, 3, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Check the Thresholds
  [r, c] = size ( H );
  if r ~= 3 || c ~= 3; ...
    error ( 'MATLAB:CheckHomoGeometric:Input', ...
            'H must be a 3x3 planar Homography!' );
  end

  Borders = [ 0   ImgWidth  ImgWidth      0;
              0      0      ImgHeight ImgHeight;
              1      1          1         1      ];

  WrpBorders = H * Borders;
  WrpBorders(1,:) = WrpBorders(1,:) ./ WrpBorders(3,:);
  WrpBorders(2,:) = WrpBorders(2,:) ./ WrpBorders(3,:);

  % The resulting lines cannot intersect!
  IntA = LineIntersect ( [ WrpBorders(1:2,1)' WrpBorders(1:2,2)' ], ...
                         [ WrpBorders(1:2,3)' WrpBorders(1:2,4)' ] );

  IntB = LineIntersect ( [ WrpBorders(1:2,1)' WrpBorders(1:2,4)' ], ...
                         [ WrpBorders(1:2,2)' WrpBorders(1:2,3)' ] );
  
  Ok = ~IntA && ~IntB;
end

