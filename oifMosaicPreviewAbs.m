%
%  Autor(s)      : Arman Elibol & Jordi Ferrer Plana
%  e-mail        : aelibol@eia.udg.edu, jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Preview given Mosaic using Absolute Homographies.
%
%  File          : ioMosaicPreviewAbs.m
%  Date          : 27/02/2006 - 28/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  oifMosaicPreviewAbs Preview given mosaic with use of absolute homographies.
%                      First frame (node) is in green, last in blue and all
%                      others in red.
%
%      oifMosaicPreviewAbs ( FID, M, ImgWidth, ImgHeight, First, Last, Step, List, DrawImCenter )
%
%     Input Parameters:
%      FID: Can be scalar or a vector, if it is scalar, is the handle to an
%           opened file for writting. If it is a vector the first element
%           is the handle to an opened file for writting. If it is vector,
%           nothing is sent to the current figure.
%      M: GMML Mosaic Structure.
%      ImgWidth: Image Width.
%      ImgHeight: Image Height.
%      First: Initial Image.
%      Last: Final Image
%      Step: Step between Images.
%

function oifMosaicPreviewAbs ( FID, M, ImgWidth, ImgHeight, First, Last, Step )
  % Test the input parameters
  error ( nargchk ( 7, 7, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  N = size ( M.nodes, 2 );
  % If they are out of range, take the limits
  if First < 1 || First > N; First = 1; end
  if Last > N || Last < First; Last = N; end;
  
  % Check the Sequence Steps
  if First < 1 || Last > size ( M.nodes, 2 ) || Last < First || Step < 0; ...
    error ( 'MATLAB:ioMosaicPreviewAbs:Input', ...
            [ 'First must be bigger than Last and they must be in the mosaic image frames range\n' ...
              'Step must be bigger that 0!' ] );
  end

  % Get FID
  if ~isscalar ( FID ); FFID = FID(1); end;

  % Image Coordinates
  x1 = 1;
  y1 = 1;
  x2 = ImgWidth;
  y2 = ImgHeight;

  % Edges and Center coordinates
  p = [x1 x2 x2 x1 x1 ImgWidth/2;
       y1 y1 y2 y2 y1 ImgHeight/2;
       1  1  1  1  1      1 ];

  ColorOr = [0 0 0.75];

  % Texture Quality (Small to not smooth bitmaps)
  fprintf ( FFID, 'TextureScalePolicy {\n' );
  fprintf ( FFID, '\tpolicy USE_TEXTURE_QUALITY\n');
  fprintf ( FFID, '\tquality 0.0001\n' );
  fprintf ( FFID, '  }\n' );

  % Depth Interlayer => Trick to avoid overlapping texture effects but
  %                     with messy result.
  DoDepthInterlay = false;
  
  if DoDepthInterlay;
    Zn = 10;   % Number of Steps
    Zr =  1;   % Length of Depth (picture units)
  
    Zs = Zr / ( Zn + 1 );
    Zi = - Zr / 2 : Zs : Zr / 2;
    z = 1;
  end

  % Scan the Nodes
  for i = First : Step : Last
    % Homography
    H = M.nodes(i).homo.matrix;
    % Compute the warping
    r = H * p;
    r(1,:) = r(1,:) ./ r(3,:);
    r(2,:) = r(2,:) ./ r(3,:);
    
    % Compute the right color depending the Image Index
    if ( i == First )
      Color = [ 0 0.75 0 ];
    elseif ( i == Last )
      Color = [ 0.75 0 0 ];
    else 
      Color = [ 0 0 0 ];
    end

    % Plot the Image Plane
    oifPlot3 ( FID, r(1,1:5), r(2,1:5), zeros(1, 5), 'Color', Color );
    oifPlot3 ( FID, r(1,5:6), r(2,5:6), zeros(1, 2), 'Color', ColorOr );

    x1 = r(1,1); y1 = r(2,1);
    x2 = r(1,2); y2 = r(2,2);
    x3 = r(1,3); y3 = r(2,3);
    x4 = r(1,4); y4 = r(2,4);

    if DoDepthInterlay;
      z1 = Zi(z);
      z2 = Zi(z);
      z3 = Zi(z);
      z4 = Zi(z);
      z = mod ( z, size ( Zi, 2 ) ) + 1;
    else
      z1 = 0;
      z2 = 0;
      z3 = 0;
      z4 = 0;
    end

    fprintf ( FFID, 'Separator {\n' );
    fprintf ( FFID, '\tShapeHints { vertexOrdering COUNTERCLOCKWISE }\n' );
    fprintf ( FFID, '\tTexture2 { filename "%s" }\n', M.nodes(i).image );
    % Don't Scale Bitmaps
%    fprintf ( FFID, '\tComplexity { textureQuality 0.001 }\n' );
  
    fprintf ( FFID, '\tTexture2Transform { rotation 4.712388980384690 }\n' ); % 3 * pi / 2
    fprintf ( FFID, '\tCoordinate3 {\n' );
    fprintf ( FFID, '\t\tpoint [ %.6f %.6f %.6f, %.6f %.6f %.6f,\n', x1, y1, z1, x4, y4, z4 );
    fprintf ( FFID, '\t\t        %.6f %.6f %.6f, %.6f %.6f %.6f ]\n', x2, y2, z2, x3, y3, z3 );

    fprintf ( FFID, '\t}\n' );
    fprintf ( FFID, '\tNurbsSurface {\n' );
    fprintf ( FFID, '\t\tnumUControlPoints 2\n' );
    fprintf ( FFID, '\t\tnumVControlPoints 2\n' );
    fprintf ( FFID, '\t\tuKnotVector [ 0.0, 0.0, 1.0, 1.0 ]\n' );
    fprintf ( FFID, '\t\tvKnotVector [ 0.0, 0.0, 1.0, 1.0 ]\n' );
    fprintf ( FFID, '\t}\n' );
    fprintf ( FFID, '   }\n' );
  end
end
