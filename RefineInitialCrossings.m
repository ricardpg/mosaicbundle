%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Refine the Initial set of Crossings.
%
%  File          : RefineInitialCrossings.m
%  Date          : 27/02/2006 - 19/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Toolboxes
% addpath /opt/Matlab/toolbox/...;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Show Mosaic Figures
ShowMosaic = false;
% Filter Initial Crossing Pairs
FilterInitialCrossings = false;

% Data
DataPath = '/mnt/data/MosaicBundle/Data';

% Input Data
MosaicFileName = '/gmml_Similarity.gmml';
MosaicFilePath = [ DataPath MosaicFileName ];
InitCrossingsFileName = '/InitCrossings.mat';
InitCrossingsFilePath = [ DataPath InitCrossingsFileName ];
% Output Results
CrossingsFileName = '/Crossings_3.mat';
CrossingsFilePath = [ DataPath CrossingsFileName ];

% Image sequence Length
NumImages = 3;
n = uint8 ( NumImages );
if NumImages < 1 || n ~= NumImages || ~mod ( n, 2 ); ...
  error ( 'MATLAB:RefineInitialCrossings:Input', ...
          'NumImages must be an odd number in the range [1..255]!' );
end
% Number of images to each direction
ImgOffs = (NumImages - 1) / 2;

% Image Sizes, K, etc...
InitParamLucky;

% Load Initial Crossings
disp ( 'Loading Initial Crossings...' );
load ( InitCrossingsFilePath, 'InitCrossings' );

% Load Mosaic if it is not already loaded
if ~exist ( 'M', 'var' );
  disp ( 'Loading Mosaic...' );
  M = gmml_load ( MosaicFilePath );
end

if FilterInitialCrossings;
  % List construction 
  disp ( 'Filtering Initial Crossings...' );
  FilteredCrossings = FilterCrossings ( M, ImgWidth, ImgHeight, InitCrossings, NumImages, 0.9, 1000 );
else
  FilteredCrossings = InitCrossings;
end

% Final List Construction 
disp ( 'Refining Filtered Crossings...' );
Crossings = RefineCrossings ( FilteredCrossings, ImgOffs );

% Save Results
disp ( 'Saving Results...' );
save ( CrossingsFilePath, 'Crossings' );


% Show the mosaic and the list
if ShowMosaic;
  disp ( 'Plotting Results...' );

  % Display some images
  FirstImage = 1;
  LastImage = size ( FilteredCrossings, 2 );
  if LastImage > 200; LastImage = 200; end;
  StepImage = 1;
  for i = FirstImage:StepImage:LastImage
    % Draw the figure
    Handle = figure;
    hold on; axis equal; axis ij;
    MosaicPreviewAbs ( M, ImgWidth, ImgHeight, FilteredCrossings(1, i)-NumImages, FilteredCrossings(1, i)+NumImages, 1, [], true );
    MosaicPreviewAbs ( M, ImgWidth, ImgHeight, FilteredCrossings(2, i)-NumImages, FilteredCrossings(2, i)+NumImages, 1, [], true );
  end
end

