%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Plot the residuals grouped in different figures.
%
%  File          : PlotResidual.m
%  Date          : 14/07/2007 - 19/07/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  PlotResidual Plot the residuals grouped in different figures.
%
%     Input Parameters:
%      Residual: 1xn vector os residuals.
%      WMatchPoint: Scalar containing the Weight for point and Match Residuals.
%      WLBLPoint: Scalar containing the Weight for LBL Point Readings.
%      WLBLCam: Scalar or Vector containing the Weight/Weights for LBL
%               Camera Residuals.
%      WAngCam: 1x3 vector containing the Weights for Angular Residuals.
%      ResMatchPointFig: Handle to Match-Point figure.
%      ResLBLPointFig: Handle to LBLPoint figure.
%      ResLBLCamFig: Handle to LBLCam figure.
%      ResAngCamFig: Handle to AngCam figure.
%      NumMatchPointFig: Number of Match-Point residuals.
%      NumLBLPointFig: Number of LBLPoint residuals.
%      NumLBLCamFig: Number of LBLCam residuals.
%      NumAngCamFig: Number of AngCam residuals.
%
%     Output Parameters:
%

function PlotResidual ( Residual, WMatchPoint, WLBLPoint, WLBLCam, WAngCam, ...
                        ResMatchPointFig, ResLBLPointFig, ...
                        ResLBLCamFig, ResAngCamFig, ...
                        NumMatchPoint, NumLBLPoint, NumLBLCam, NumAngCam )
  % Test the input parameters
  error ( nargchk ( 13, 13, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  if NumMatchPoint > 0;
    ResMatchPoint = Residual(1:NumMatchPoint) ./ WMatchPoint;
    figure ( ResMatchPointFig );
    ResMatchPointAxes = gca ( ResMatchPointFig );
    cla ( ResMatchPointAxes );

    % Plot in 4 different colors
    Ida = 1:4:size(ResMatchPoint); plot ( ResMatchPoint(Ida), 'color', [0.75 0 0] );
    Ida = 2:4:size(ResMatchPoint); plot ( ResMatchPoint(Ida), 'color', [0 0.75 0] );
    Ida = 3:4:size(ResMatchPoint); plot ( ResMatchPoint(Ida), 'color', [0 0 0.75] );
    Ida = 4:4:size(ResMatchPoint); plot ( ResMatchPoint(Ida), 'color', [0.75 0.75 0] );
    legend ( 'x', 'y', 'x''', 'y''', -1 );
  end

  if NumLBLPoint > 0;
    ResLBLPoint = Residual(1+NumMatchPoint:NumMatchPoint+NumLBLPoint) ./ WLBLPoint;
    figure ( ResLBLPointFig );
    ResLBLPointAxes = gca ( ResLBLPointFig );
    cla ( ResLBLPointAxes );

    % Plot in 2 different colors
    Ida = 1:2:size(ResLBLPoint); plot ( ResLBLPoint(Ida), 'color', [0.75 0 0] );
    Ida = 2:2:size(ResLBLPoint); plot ( ResLBLPoint(Ida), 'color', [0 0.75 0] );
    legend ( 'X', 'Y', -1 );
  end

  if NumLBLCam > 0;
    ResLBLCam = Residual(1+NumMatchPoint+NumLBLPoint:NumMatchPoint+NumLBLPoint+NumLBLCam) ./ WLBLCam;
    figure ( ResLBLCamFig );
    ResLBLCamAxes = gca ( ResLBLCamFig );
    cla ( ResLBLCamAxes );

    % Plot in 3 different colors
    Ida = 1:3:size(ResLBLCam); plot ( ResLBLCam(Ida), 'color', [0.75 0 0] );
    Ida = 2:3:size(ResLBLCam); plot ( ResLBLCam(Ida), 'color', [0 0.75 0] );
    Ida = 3:3:size(ResLBLCam); plot ( ResLBLCam(Ida), 'color', [0 0 0.75] );
    legend ( 'X', 'Y', 'Z', -1 );
  end

  if NumAngCam > 0;
    ResAngCam = Residual(1+NumMatchPoint+NumLBLPoint+NumLBLCam:NumMatchPoint+NumLBLPoint+NumLBLCam+NumAngCam) ./ WAngCam;
    figure ( ResAngCamFig );
    ResAngCamAxes = gca ( ResAngCamFig );
    cla ( ResAngCamAxes );

    % Plot in 3 different colors
    Ida = 1:3:size(ResAngCam); plot ( ResAngCam(Ida), 'color', [0.75 0 0] );
    Ida = 2:3:size(ResAngCam); plot ( ResAngCam(Ida), 'color', [0 0.75 0] );
    Ida = 3:3:size(ResAngCam); plot ( ResAngCam(Ida), 'color', [0 0 0.75] );
    legend ( 'Roll', 'Pitch', 'Yaw', -1 );
  end
end
