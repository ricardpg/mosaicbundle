%
%  Autor(s)      : Arman Elibol & Jordi Ferrer Plana
%  e-mail        : aelibol@eia.udg.edu, jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Preview given Mosaic using Absolute Homographies.
%
%  File          : MosaicPreviewAbs.m
%  Date          : 27/02/2006 - 04/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - See also: mosaic_preview_rel, gmml, mosaic
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  MosaicPreviewAbs Preview given mosaic with use of absolute homographies.
%                   First frame (node) is in green, last in blue and all
%                   others in red.
%
%      MosaicPreviewAbs ( M, a, b, c, List, imcenter )
%
%     Input Parameters:
%      M: GMML Mosaic Structure.
%      ImgWidth: Image Width.
%      ImgHeight: Image Height.
%      First: Initial Image.
%      Last: Final Image
%      Step: Step between Images.
%      List: Empty to plot all the Image Indices or a vector of the Indices
%            to be drawn.
%

function MosaicPreviewAbs ( M, ImgWidth, ImgHeight, First, Last, Step, List, DrawImCenter )
  % Test the input parameters
  error ( nargchk ( 8, 8, nargin ) );
  error ( nargoutchk ( 0, 0, nargout ) );

  N = size ( M.nodes, 2 );
  % If they are out of range, take the limits
  if First < 1 || First > N; First = 1; end
  if Last > N || Last < First; Last = N; end;
  
  % Check the Sequence Steps
  if First < 1 || Last > size ( M.nodes, 2 ) || Last < First || Step < 0; ...
    error ( 'MATLAB:MosaicPreviewAbs:Input', ...
            [ 'First must be bigger than Last and they must be in the mosaic image frames range\n' ...
              'Step must be bigger that 0!' ] );
  end

  % Image Coordinates
  x1 = 1;
  y1 = 1;
  x2 = ImgWidth;
  y2 = ImgHeight;

  % Edges and Center coordinates
  p = [x1 x2 x2 x1 x1 x2/2;
       y1 y1 y2 y2 y1 y2/2;
       1  1  1  1  1   1 ];
    
  ColorOr = [0 0 0.75];

  % Scan the Nodes
  for i = First : Step : Last
    % Homography
    H = M.nodes(i).homo.matrix;
    % Compute the warping
    r = H * p;
    r(1,:) = r(1,:) ./ r(3,:);
    r(2,:) = r(2,:) ./ r(3,:);
    
    % Compute the right color depending the Image Index
    if ( i == First )
      Color = [ 0 0.75 0 ];
    elseif ( i == Last )
      Color = [ 0.75 0 0 ];
    else 
      Color = [ 0 0 0 ];
    end

    % Plot the Image Plane
    plot ( r(1,1:5), r(2,1:5), 'Color', Color );
    plot ( r(1,5:6), r(2,5:6), 'Color', ColorOr );

    % Draw the image Centers if wanted
    if DrawImCenter && ( isempty ( List ) || ~isempty ( find ( List, i ) ) );
       text ( r(1,5), r(2,5), num2str ( M.nodes(i).index ), 'Color', Color );
    end
  end
end
