%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Convert Rotation Matrix R to Angles Roll, Pitch and Yaw.
%
%  File          : RotMat2RPY.m
%  Date          : 07/05/2006 - 19/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%                  - See RotMat2YPR to use Craig (pg. 47) convention.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  RotMat2RPY Decomposition of a 3x3 Rotation Matrix to the 3 Angles that are used
%             to obtain the Rotation Matrix.
%             The used convention is:
%                   Rxyz = Rx(Roll) * Ry(Pitch) * Rz(Yaw)
%             So, first there is a Roll rotation in the X axis, then a Pitch
%             rotation in the Y axis and finally a Yaw rotation in the Z axis
%             allways defined in the mobile axis reference frame.
%             This convention IS NOT the convention in the Craig, pag. 47!
%
%     Input Parameters:
%      R: 3x3 Rotation Matrix.
%
%     Output Parameters:
%      Roll, Pitch, Yaw: 3D Rigid Motion angles.
%

function [Roll Pitch Yaw] = RotMat2RPY ( R )

%  >> R = sym ( 'R', 'real' );
%  >> P = sym ( 'P', 'real' );
%  >> Y = sym ( 'Y', 'real' );
%  >> Rx = [1 0 0 ; 0 cos(R) -sin(R); 0 sin(R) cos(R)];
%  >> Ry = [ cos(P) 0 sin(P); 0 1 0 ; -sin(P) 0 cos(P)];
%  >> Rz = [ cos(Y) -sin(Y) 0 ; sin(Y) cos(Y) 0; 0 0 1];
%  >> Rxyz = Rx * Ry * Rz
%  
%   Rxyz =
%     [                       cos(P)*cos(Y),                      -cos(P)*sin(Y),                              sin(P)]
%     [  sin(R)*sin(P)*cos(Y)+cos(R)*sin(Y), -sin(R)*sin(P)*sin(Y)+cos(R)*cos(Y),                      -sin(R)*cos(P)]
%     [ -cos(R)*sin(P)*cos(Y)+sin(R)*sin(Y),  cos(R)*sin(P)*sin(Y)+sin(R)*cos(Y),                       cos(R)*cos(P)]

  % Wrong solution:
  %
  % Yaw = atan2 ( -R(1,2), R(1,1) );
  % Pitch = atan2 ( R(1,3), R(1,1)/cos(Yaw) );
  % Roll = atan2 ( -R(2,3), R(3,3) );

  Px = sqrt ( R(2,3)*R(2,3) + R(3,3)*R(3,3) );

  % Check Singularity
  if Px > 1e-8;
    Pitch = atan2 ( R(1,3), Px );
    cP = cos ( Pitch );
    Yaw = atan2 ( -R(1,2)/cP, R(1,1)/cP );
    Roll = atan2 ( -R(2,3)/cP, R(3,3)/cP );
  else
    Pitch = pi / 2;
    % cos ( Pitch ) = 0;  Sin ( Pitch ) = 1
    % Only the sum Yaw + Roll can be computed => Convention Yaw = 0
    % cos ( Yaw ) = 1; sin ( Yaw ) = 0
    Yaw = 0;
    Roll = atan2 ( R(3,2), R(2,2) );
  end
end
