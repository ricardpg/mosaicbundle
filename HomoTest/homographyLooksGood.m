function bool = homographyLooksGood(H, rotation_threshold)

% bool = homographyLooksGood(H, rotation_threshold)
%
%       H                  - transformation matrix
%       rotation_threshold - threshold of rotation
%       bool               - say if it's good or not
%
% Analyse a homography to see if it's a coherent homography
%
% Developed by Olivier Delaunoy (delaunoy@eia.udg.edu)
% Dep. of Elctronics, Informatics and Automation, University of Girona



bool=1;

[U,D,V]=svd(H(1:2,1:2));
rotation=U*V';
theta=(atan2(rotation(2,1),rotation(1,1))/pi)*180;

if abs(theta)>rotation_threshold
    bool=0;
else
	if (max(D(1,1),D(2,2))>3 || min(D(1,1),D(2,2))<0.3 || max(D(1,1),D(2,2))/min(D(1,1),D(2,2))>2)
        bool=0;
    else
        if(abs(H(3,1))>0.01 || abs(H(3,2))>0.01)
            bool=0;
        end
	end
end