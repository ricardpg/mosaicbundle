%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : 3D Vehicle Trajectory Parameters Optimization.
%
%  File          : HomoTo3DAngles.m
%  Date          : 18/04/2006 - 20/04/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  HomoTo3DAngles From a 3x3 Planar Homography and a 3x3 Intrinsic
%                 Parameters matrix, estimate the Roll, Pitch and Yaw angles
%                 of the 3D rigid motion that the camera have done to acquire
%                 the two images related with the input homography.
%                 Algorithm H -> Rt from
%                   http://lear.inrialpes.fr/people/triggs/pubs/Triggs-eccv98-long.ps.gz
%                 Approximation of a Rotation Matrix:
%                   http://home.pipeline.com/~hbaker1/quaternion/stanfordaiwp79-salamin.ps.gz
%
%     Input Parameters:
%      M: 3x3 Planar Homography.
%      K: 3x3 Intrinsic parameters matrix.
%
%     Output Parameters:
%      Roll, Pitch, Yaw: 3D Rigid Motion angles.
%

function [Roll, Pitch, Yaw] = HomoTo3DAngles ( H, K ) 

  % !!! REVIEW !!!!
  ikHk = inv(K) * H * K;

  % Extract the two possible configurations
  [R1,t1,n1, R2,t2,n2, zeta] = homog_to_Rt ( ikHk );

  % Use the closest to the fronto-parallell view (Nuno)
  n1 = n1 * sign(n1(3));
  n2 = n2 * sign(n2(3));

  d1 = norm ( n1 - [0; 0; 1] );
  d2 = norm ( n2 - [0; 0; 1] );

  if d1 < d2;
    R = R1;
  else
    R = R2;
  end

  % Determine the Euler angles from the estimated Rotation Matrix

  % Parameters
  EpsilonEig = 0.01;
  EpsilonDet = 0.01;

  AproximateRotation = false;
  
  % The determinant must be 1 -> Otherwise R is degenerated
  if abs ( abs ( det ( R ) ) - 1 ) < EpsilonDet;
    % Eigen analisys
    [V, D] = eig ( R );

    % Only one Real Eigenvalue is allowed -> Otherwise R is degenerated
    I = find (~imag(diag(D)));
    if length ( I ) == 1;
      % Eigenvalue must be 1 -> Otherwise R is degenerated
      if abs ( abs ( I(1) ) - 1 ) > EpsilonEig;
        % Eigenvector corresponding to Eigenvalue = 1 is the Rotation axis
        AproximateRotation = true;
      end
    else
      AproximateRotation = true;
    end
  else
    AproximateRotation = true;
  end

  % If it is degenerated -> Aproximate R to the closest rotation matrix
  % E. Salamin 1979: "Application of quaternions to computation with rotations"
  if AproximateRotation; R = R*(R'*R)^(-1/2); end;

  [Roll Pitch Yaw] = RotMat2RPY ( R );
end
