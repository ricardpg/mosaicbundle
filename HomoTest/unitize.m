function [auni,buni] = unitize(a,b)
  % [auni,buni] = unitize(a,b)
  %
  % Escala 2 variáveis de forma a que tenha soma dos quadrados unitária.
  %

  auni = a / sqrt(a^2 + b^2);
  buni = b / sqrt(a^2 + b^2);
end
