function [angle,vec,R,d1,d2] = checkhomogrot(M,K) 
% [angle,vec,R] = checkhomogrot(M,K) 
%
% Check Homography Rotation
%
% Estimates the rotation between camera frames from a homography M and intrinsic parameter matrix K
% Uses the decomposition by B. Triggs ECCV98 http://www.inrialpes.fr/movi/people/Triggs/publications.html
% to obtain the two possible solution for the rotation matrix, translation up to scale and normal of plane. 
%It selects the  solution with the normal closest to [0 0 1], corresponding to a fronto-parallel configuration.
% It returns the angle and vector representation of the rotation matrix
%

H = inv(K) * M * K;

[R1,t1,n1, R2,t2,n2, zeta] = homog_to_Rt(H);

n1 = n1 * sign(n1(3))
n2 = n2 * sign(n2(3))

d1 = norm(n1 - [0 0 1]')
d2 = norm(n2 - [0 0 1]')

if d1 < d2
    R = R1;
else
    R = R2;
end;
%Approximation to Rotation matrix
%R=R*(R'*R)^(-1/2);

% MISSING conversion of rotation matrix to rotation axis plus rotation angle. 
%The homogrphy test will be based on the rotation angle  implement equations in 
%   http://en.wikipedia.org/wiki/Rotation_representation_(mathematics)
%   section Conversion formulae between representations 
%   subsection DCM -> Euler axis/angle


%If the rotation angle ? is zero, the axis is not uniquely defined.
%R has eigenvalues 1,cos(?)+isin(?),cos(?)-isin(?)
[V,D]=eig(R);%find the eigenvalues and eigenvectors
%Eigenvector corresponding to eigenvalue=1 is the rotation axis
[i j]=find(abs(D-1)<10^-10);
vec1(1,:)=V(1,i);
vec1(2,:)=V(2,i);
vec1(3,:)=V(3,i);
[i1 j1]=find(imag(D)>0); %find the cos(?)+isin(?)
theta=atan2(imag(D(i1,j1)),real(D(i1,j1))); 
%theta=angle(D(i1,j1))
%if ? is very close to zero, all eigenvalues and eigenvectors can become real numbers
%it can be tried to calculated the ? with other formulation
if isempty(theta)
    theta=acos((R(1,1)+R(2,2)+R(3,3)-1)/2);
end;

%if the ? is zero, this calculation will fail because of the axis that is
%not unique
%if the angle is not multiple 180 degrees, following formulations can be
%used
theta1=acos((R(1,1)+R(2,2)+R(3,3)-1)/2);
vec(1,1)=(R(3,2)-R(2,3))/(2*sin(theta1));
vec(2,1)=(R(1,3)-R(3,1))/(2*sin(theta1));
vec(3,1)=(R(2,1)-R(1,2))/(2*sin(theta1));