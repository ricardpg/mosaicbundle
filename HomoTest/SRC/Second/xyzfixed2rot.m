function ROT3 = xyzfixed2rot(ANGVEC)
% ROT3 = xyzfixed2rot(ANGVEC)
%
% X-Y-Z fixed angles to Rotation matrix
%
% Calcula uma matriz de rota��o 3x3 a partir dos �ngulos fixos de rota��o X-Y-Z, ANGVEC = [a b c],
% de acordo com as f�rmulas do Craig 2� Ed. Pag. 47. 
%

a = ANGVEC(1);
b = ANGVEC(2);
c = ANGVEC(3);

PHI1 = [cos(a)*cos(b); sin(a)*cos(b); -sin(b)];
PHI2 = [cos(a)*sin(b)*sin(c)-sin(a)*cos(c); sin(a)*sin(b)*sin(c)+cos(a)*cos(c); cos(b)*sin(c)];
PHI3 = [cos(a)*sin(b)*cos(c)+sin(a)*sin(c); sin(a)*sin(b)*cos(c)-cos(a)*sin(c); cos(b)*cos(c)];

ROT3 = [PHI1 PHI2 PHI3];
