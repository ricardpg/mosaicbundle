function Mim = wpose2moshomog(WPOSE,K,wNm)
% Mim = wpose2moshomog(WPOSE,K,wNm)
% 
% Get image to mosaic homography from pose
%
% (falta acabar o help)
%

% 
% Devolve, para cada imagem, a matriz 4x3 rNi que mapeia os pontos no referencial desse frame directamente no plano do mundo, descrito
% num referencial 3D da camera 1. 
% Necessita da matriz dos par�metros intr�nsecos K, da homografia Mi1 entre a imagem corrente e a imagem de refer�ncia (imagem 1),
% da normal n que define a orienta��o do plano do mundo em rela��o ao referencial da camera 1, e da dist�ncia ao plano medida sobre o 
% eixo zz da camera 1. A normal � definida como sendo a que se afasta da camera, i.e., a �ltima componente � positiva.
%
% [rNi,wNi] = frame2world(K,Mi1,n,wdist)
% Devolve o mapeamento entre um frame de imagem e o referencial 3D associado ao plano do mundo (coordenada ZZ nula).
%
% Usa o sistema (u,v) para as coordenadas 2-D.
%

%warning(['Fun��o ' mfilename ' n�o totalmente testada!']);

iRw = xyzfixed2rot(WPOSE(1:3));

wTi = [iRw' [WPOSE(4); WPOSE(5); WPOSE(6)]; 0 0 0 1];

itw = -iRw * [WPOSE(4); WPOSE(5); WPOSE(6)];

iPw = K * [iRw itw];

Mim = iPw * wNm;

return;

% Definir a matriz que mapeia pontos 2D no referencial do mundo em ponto 3D no plano do mundo
Q = [1 0 0; 0 1 0; 0 0 0; 0 0 1];

% Definition of the world origin, written in the 3D reference frame (1st camera)
rPw = [0 0 wdist]';

rZw = -[n(1) n(2) n(3)]';
rXw = [1 0 -n(1)/n(3)]';
rXw = rXw / norm(rXw);
rYw = cross(rZw,rXw);

rRw = [rXw rYw rZw];

% 3D world to reference projective mapping 
rTw = [rRw rPw; 0 0 0 1];

rNi = rTw * Q * inv(Mi1 * [K zeros(3,1)] * rTw * Q);

wNi = inv(rTw) * rNi;


