% Teste das duas possiveis solucoes para o movimento e estrutura planar de 2 camera frontoparalelas

POSE1 = [0.4898    0.0882   -3.1265    0.4011    1.1992    0.9600];
POSE2 = POSE1 + [0 0 0 0.5 -0.3 0.2];

K = [463.141382937418 0 180;0 458.338461263073 135;0 0 1];
wNm = [0.00215916788445382 -3.3881317890172e-021 -0.730298524024684;-1.52465930505774e-020 -0.00218179377145055 3.06043218043018;-6.82257271128836e-021 -9.03328806970803e-023 1.08797427840892e-018;-6.7762635780344e-021 -1.35525271560688e-020 1.00064412712716];

M1m = wpose2moshomog(POSE1,K,wNm)
M2m = wpose2moshomog(POSE2,K,wNm)

M21 = M2m*inv(M1m);
M21 = M21 / M21(3,3);

M = inv(K) * M21 * K;

% Esta funcao esta' em c:\users\ngracias\mosaicv5\homog_to_Rt.m
[R1,t1,n1, R2,t2,n2, zeta] = homog_to_Rt(M)


[angle,vec,R] = checkhomogrot(M21,K)