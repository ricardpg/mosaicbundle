function [angle,vec,R] = checkhomogrot(M,K) 
% [angle,vec,R] = checkhomogrot(M,K) 
%
% Check Homography Rotation
%
% Estimates the rotation between camera frames from a homography M and intrinsic parameter matrix K
% Uses the decomposition by B. Triggs ECCV98 http://www.inrialpes.fr/movi/people/Triggs/publications.html
% to obtain the two possible solution for the rotation matrix, translation up to scale and normal of plane. It selects the 
% solution with the normal closest to [0 0 1], corresponding to a fronto-parallel configuration.
% It returns the angle and vector representation of the rotation matrix
%

H = inv(K) * M * K;

[R1,t1,n1, R2,t2,n2, zeta] = homog_to_Rt(H);

n1 = n1 * sign(n1(3))
n2 = n2 * sign(n2(3))

d1 = norm(n1 - [0 0 1]')
d2 = norm(n2 - [0 0 1]')

if d1 < d2
    R = R1;
else
    R = R2;
end;

% MISSING conversion of rotation matrix to rotation axis plus rotation angle. The homogrphy test will be based on the rotation angle
% implement equations in 
%   http://en.wikipedia.org/wiki/Rotation_representation_(mathematics)
%   section Conversion formulae between representations 
%   subsection DCM -> Euler axis/angle