%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Find possible crossings between non consecutive images.
%
%  File          : FindInitialCrossings.m
%  Date          : 27/02/2006 - 28/02/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%
%  Usage:
%
%  FindInitialCrossings Given a GMML structure, find the cadidates to be a
%                       crossings in non consecutive frames.
%
%      List = FindInitialCrossings ( M, ImgWidth, ImgHeight [, Threshold] )
%
%     Input Parameters:
%      M: GMML structure representing the mosaic.
%      ImgWidth: Images Width.
%      ImgHeight: Images Height.
%      Threshold: Optional parameter to define the threshold to
%                 discriminate whether two frames are consecutive or not.
%                 When is not specified the distance between frame centers
%                 is used.
%
%     Output Parameters:
%      List: Set of 2xn pairs on indices which correspond to the
%            non-consecutive crossing candidates.
%

function List = FindInitialCrossings ( M, ImgWidth, ImgHeight, Threshold )
  % Test the input parameters
  error ( nargchk ( 3, 4, nargin ) );
  error ( nargoutchk ( 1, 1, nargout ) );

  % Get the CPU Time
  CurTime = cputime;

  % Image Center
  ImageCenter = [ImgWidth/2; ImgHeight/2; 1];

  % If threshold is not defined, use the maximum distance betwen frames
  if nargin == 3;
    Threshold = sqrt ( ImgWidth * ImgWidth + ImgHeight * ImgHeight );
  end

  l = 1;
  % Initialized the list to don't grow arrays
  List = zeros ( 2, size ( M.nodes, 2 ) );
  N = size ( M.nodes, 2 ) - 1;
  for i = 1 : N
    % Check distance between homography translations
    Tempi = M.nodes(i).homo.matrix * ImageCenter;
    Tempi = Tempi ./ Tempi(3,1);
    Tempd = M.nodes(i+1).homo.matrix * ImageCenter;
    Tempd = Tempd ./ Tempd(3,1);
    p = [Tempi(1) Tempi(2) Tempd(1) Tempd(2)];

    % If the Image Centers are closer (is not a big skip in the mosaic)
    if FilterDist ( p(1:2), p(3:4), Threshold );
      % Search for possible Crossing
      for j = i+2 : N
        Tempi = M.nodes(j).homo.matrix * ImageCenter;
        Tempi = Tempi ./ Tempi(3,1);
        Tempd = M.nodes(j+1).homo.matrix * ImageCenter;
        Tempd = Tempd ./ Tempd(3,1);
        k = [Tempi(1) Tempi(2) Tempd(1) Tempd(2)];

        % If the Image Centers are closer (is not a big skip in the mosaic)
        if FilterDist ( k(1:2), k(3:4), Threshold );
          if LineIntersect ( p, k );
             List(1:2, l ) = [j; i];
             l = l + 1;
          end
        end
      end
    end

    % Show Debug each 5 frames
    if mod ( i, 10 ) == 0;
      disp ( sprintf ( 'Node %d / %d. Elapsed time %f', i, N, cputime - CurTime ) );
      CurTime = cputime;
    end
  end
  
  % Erase empty elements in the list
  if l > 1;
    List = List(:,1:l-1);
  else
    List = [];
  end
end

% Function to check if the distance is smaller than the Threshold
%
function Ok = FilterDist ( p, k, Thresh )
  v = p - k;
  Ok = sqrt ( sum ( v .* v ) ) < Thresh;
end
