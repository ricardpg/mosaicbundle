%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Check the Initial set of Crossings.
%
%  File          : CheckCrossings.m
%  Date          : 27/02/2006 - 19/06/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Toolboxes
% addpath /opt/Matlab/toolbox/...;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Number of matchings to accept a crossing candidate
MatchesThreshold = 10;

% Data
DataPath = '/mnt/data/MosaicBundle/Data';

% Input Data
MosaicFileName = '/gmml_Similarity.gmml';
MosaicFilePath = [ DataPath MosaicFileName ];
% CrossingsFileName = '/Crossings_11.mat';
CrossingsFileName = '/Crossings_5.mat';
CrossingsFilePath = [ DataPath CrossingsFileName ];
FinalCrossingsFileName = '/FinalCrossings_5.mat';
FinalCrossingsFilePath = [ DataPath FinalCrossingsFileName ];

PathToFIM = './FIM';
PathToImages =  '/mnt/uvl/DataSets/Lustre96';
PathToResults = [DataPath '/FIM'];
addpath ( PathToFIM ); 
SetPathsFIM ( PathToFIM );

% Image Sizes, K, etc...
InitParamLucky;

% Load Initial Crossings
disp ( 'Loading Crossings...' );
load ( CrossingsFilePath, 'Crossings' );

% Load Mosaic if it is not already loaded
if ~exist ( 'M', 'var' );
  disp ( 'Loading Mosaic...' );
  M = gmml_load ( MosaicFilePath );
end


% Configure FIM

% - Image Processing Parameters --------------------------------------------

ProcessingParameters.SingleChannel = '';              % 0-no, ''-default, % 1-red, 2-green, 3-blue, 4-grayscale, 5-PCA, 6-Y channel of YIQ
ProcessingParameters.Equalization = 1;                % 0-no, ''-default, 1-full, 2-Clahe, 3-CLAHS
% ProcessingParameters = 1;                           % <- to use default Processing parameters
% ProcessingParameters = 0;                           % <- to cancel Preprocessing of images: Images must have single color channel!

% - Detection Parameters ---------------------------------------------------

Detectors   = { 'SIFT', 'SURF', 'Harris', ...         % 1 - 3 
                'Laplacian', 'Hessian', 'MSER' };     % 4 - 6
          
Descriptors = { 'SIFT', 'SURF', 'SURF-128', ...       % 1 - 3
                'SURF-36', 'U-SURF', ...              % 4 - 5
                'U-SURF-128', 'U-SURF-36' };          % 6 - 7

DetectionParameters.DetType = Detectors(2);
DetectionParameters.DescType = Descriptors(2);        % Chose the descriptor.
DetectionParameters.MaxNumber = 7000;                 % max number of features to detect
DetectionParameters.RadiusNonMaximal = 2;             % radius for non-maximal suppression for detector
DetectionParameters.Sigma = 1;                        % sigma of the Gaussian used in Harris and Laplacian detectors
DetectionParameters.Threshold = 0;                    % [0] - default for all detectors except MSER ( threshold to reject poorer features); 
                                                      % [30] (pixels) - default for MSER -> minimum region size. 
% DetectionParameters = 1;                            % <- to use default Detection parameters
% DetectionParameters = 0;                            % <- to cancel Detection; Description, Matching and RANSAC are
                                                      % cancelled then automatically
Mask = [];                                            % Mask

% - Descriptor Parameters --------------------------------------------------

% Select parameters to calculate the descriptor of the keypoins:
if isstruct ( DetectionParameters )
  DescriptionParameters.DescType = DetectionParameters.DescType; 
else 
  DescriptionParameters.DescType = Descriptors(2); 
end
% DescriptionParameters = 1;                          % <- to use default Description parameters
% DescriptionParameters = 0;                          % <- to cancel calculation of descriptors; Matching and 
                                                      % RANSAC are cancelled then automatically
% - Matching Parameters ----------------------------------------------------

MatchingParameters.Matcher = 'Sift';
MatchingParameters.SiftRatio = 1.5;                   % ratio between the next closest and closest nearest neighbour for SIFT matching     
MatchingParameters.Bidirectional = 1;                 % If ~= 0 Intersection between "image1 to image2" and "image2 to image1" matches
% MatchingParameters = 1;                             % <- to use default Matching parameters
% MatchingParameters = 0;                             % <- to cancel Matching, RANSAC is cancelled then by default

% - RANSAC Parameters ------------------------------------------------------

HomographyModels = { 'Euclidean', 'Similarity', ...   % 1 - 2
                     'Affine', 'Projective' };        % 3 - 4
                 
RansacParameters.HomographyModel = HomographyModels(4);
RansacParameters.SpatialStdev = 5;                    % stdev of spatial coord of features, in pixels
% RansacParameters = 1;                               % <- to use default parameters for RANSAC
% RansacParameters = 0;                               % <- to cancel execution of RANSAC

% - Display Parameters -----------------------------------------------------

DisplayParameters.DisplayConfig = 1;                  % 1 -> display FIM configuretion; 0 -> not display
DisplayParameters.DisplayResults = 1;                 % 1 -> display statistics of FIM execution results; 0 -> not display
DisplayParameters.SepLines = 1;                       % 1 -> display separation lines; 0 -> not display
% DisplayParameters = 1;                              % <- to use default display parameters
% DisplayParameters = 0;                              % <- to cancel display output of FIM

% - Plotting Parameters ----------------------------------------------------

PlottingParameters.PlotInitialImages = 1;             % 1 -> initial images are displayed; 0 -> the processed grayscale images are displayed;
PlottingParameters.SaveFigures = 0;             
PlottingParameters.SaveInFormat = 'jpg';              % supported: 'jpg', 'tif', 'eps', 'png', 'ppm', 'wmf' ('wmf' only for Windows)
PlottingParameters.PlotImages = 0;                    % 1 -> display images with keypoints; 0 -> not display
PlottingParameters.PlotInitialMatches = 0;            % 1 -> display initial matches; 0 -> not display
PlottingParameters.PlotRejectedMatches = 0;           % 1 -> display rejected by RANSAC matches; 0 -> not display
PlottingParameters.PlotAcceptedMatches = 1;           % 1 -> display accepted by RANSAC matches; 0 -> not display
PlottingParameters.PlotMotion = 1;                    % 1 -> display matched pairs in the first image; 0 -> not display;
PlottingParameters.PlotMosaic = 1;                    % 1 -> display mosaic of two images; 0 -> not display;
PlottingParameters.WhereToSave = PathToResults; 
% PlottingParameters = 0;                             % suppress plotting

% ========================== FIM Execution ===============================

% Create the Directory with Results
if PlottingParameters.SaveFigures; 
   mkdir ( PathToResults ); 
end

% Total amount of Potential Crossings
N = size ( Crossings, 2 );

% Check whether FinalCrossings list should be computed or not
if ~exist ( 'FinalCrossings', 'var' ) || ~exist ( 'i', 'var' ) || ...
   ~exist ( 'l', 'var' );
  % The Final List of Crossings
  FinalCrossings = zeros ( 2, N );
  FirstCrossing = 1;
  l = 0;
else
  % Continue last work
  FirstCrossing = i + 1;
end

InitTime = cputime;

% Scan the List of crossings
for i = FirstCrossing : N;
  ItTime = cputime;

  % Reference Image
%  IRefName = [PathToImages '/' M.nodes(Crossings(2,i)).image];
  IRefName = [M.nodes(Crossings(2,i)).image];
  IRef = imread ( IRefName );

  % Current Image
%  ICurName = [PathToImages '/' M.nodes(Crossings(1,i)).image];
  ICurName = [M.nodes(Crossings(1,i)).image];
  ICur = imread ( ICurName );

  disp ( sprintf ( 'Trying image %d / %d', i, N ) );
  disp ( sprintf ( '  Ref. Image %d: "%s"', Crossings(2,i), IRefName ) );
  disp ( sprintf ( '  Cur. Image %d: "%s"', Crossings(1,i), ICurName ) );

  [ Img1, Img2, ResultsFIM, ConfigurationFIM ] = FIM ( IRef, ICur, Mask, ...
                                                       ProcessingParameters, ...
                                                       DetectionParameters, ...
                                                       DescriptionParameters, ...
                                                       MatchingParameters, ...
                                                       RansacParameters, ...
                                                       DisplayParameters );

  % Compute how many matches where found
  if isfield ( ResultsFIM, 'AcceptedMatches' );
    MatchesFound = size ( ResultsFIM.AcceptedMatches, 2 );
  else
    MatchesFound = 0;
  end

  if MatchesFound >= MatchesThreshold;
    l = l + 1;
    FinalCrossings(:, l) = Crossings(:, i);
  else
    disp ( sprintf ( 'Not enough matches found ( %d / %d )!!!\n', MatchesFound, MatchesThreshold ) );
  end

  FinalTime = cputime;
  Time = FinalTime - ItTime;
  AllTime = (( FinalTime - InitTime ) * (N - i + 1)) / (i - FirstCrossing + 1);
  disp ( sprintf ( 'Last Iteration time %f s. Expected Time %f s. (%f hours)', Time, AllTime, AllTime / 3600 ) );
  disp ( sprintf ( 'Crossings counter %d\n\n', l ) ); 
end

% Eliminate the non-used columns
if l > 0;
  FinalCrossings = FinalCrossings(:, 1:l);
else
  FinalCrossings = [];
end

% Save Results
disp ( 'Saving FinalCrossings...' );
save ( FinalCrossingsFilePath, 'FinalCrossings' );
