%
%  Autor(s)      : Jordi Ferrer Plana
%  e-mail        : jferrerp@eia.udg.edu
%  Branch        : Computer Vision
%
%  Working Group : Underwater Vision Lab
%  Project       : -
%
%  Homepage      : http://porcsenglar.udg.edu
%
%  Module        : Compute Initial Crossings betwen segments.
%
%  File          : ComputeInitialCrossings.m
%  Date          : 27/02/2006 - 01/08/2007
%
%  Compiler      : MATLAB >= 7.0
%  Libraries     : -
%
%  Notes         : - File written using ISO-8859-1 encoding.
%
% -----------------------------------------------------------------------------
%
%  Copyright (C) 2005-2007 by Jordi Ferrer Plana
%
%  This source code is free software; you can redistribute it and/or
%  modify it under the terms of the GNU General Public License
%  as published by the Free Software Foundation; either version 2
%  of the License, or (at your option) any later version.
%
%  This source code is distributed in the hope that it will be useful,
%  but WITHOUT ANY WARRANTY; without even the implied warranty of
%  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%
%  See GNU licenses at http://www.gnu.org/licenses/licenses.html for
%  more details.
%
% -----------------------------------------------------------------------------
%

% Clear Workspace
% clear;
close all; clc;

% Adding Toolboxes
% addpath /opt/Matlab/toolbox/...;

% Adding Paths
MosaicingPath = '/mnt/mosaic/matlab/mosaicing';
addpath ( [MosaicingPath '/gmml'], ...
          [MosaicingPath '/mosaic'], ...
          [MosaicingPath '/registration'], ...
          '-END' );

% Show Mosaic Figure
ShowMosaic = false;

% Data
% DataPath = '/mnt/mosaic/netfox/Crossings/Data';
DataPath = '/mnt/data/netfox/Crossings/Data';
% Input Data
% MosaicFileName = '/gmml_Similarity.gmml';
% MosaicFileName = '/Sequential_Crossings.gmml';
% MosaicFileName = '/MosaicViewer/Big/Opt/Opt_Sub_Sequential_Crossings.gmml';
MosaicFileName = '/MosaicViewer/Big/NotOpt/Sub_Sequential_Crossings.gmml';

MosaicFilePath = [ DataPath MosaicFileName ];
% Output Results
% InitCrossingsFileName = '/InitCrossings.mat';
% InitCrossingsFileName = '/Sub_InitCrossings.mat';
% InitCrossingsFileName = '/MosaicViewer/Big/mat/Opt_Sub_InitCrossings.mat';
InitCrossingsFileName = '/MosaicViewer/Big/mat/Sub_InitCrossings.mat';
InitCrossingsFilePath = [ DataPath InitCrossingsFileName ];
MosaicPreviewFileName = '/MosaicPreview.fig';
MosaicPreviewFilePath = [ DataPath MosaicPreviewFileName ];

% Image Sizes, K, etc...
InitParamLucky;

% Load Mosaic if it is not already loaded
if ~exist ( 'M', 'var' );
  disp ( 'Loading Mosaic...' );
  M = gmml_load ( MosaicFilePath );
end

% Compute
disp ( 'Computing Initial Crossings...' );
% Allow big warpings, but not jumps in the consecutive frames
DistanceThreshold = 2 * sqrt ( ImgWidth * ImgWidth + ImgHeight * ImgHeight );
InitCrossings = FindInitialCrossings ( M, ImgWidth, ImgHeight, DistanceThreshold );

% Save Results
disp ( 'Saving Results...' );
save ( InitCrossingsFilePath, 'InitCrossings' );


% Show the mosaic and the list
if ShowMosaic;
  disp ( 'Plotting Results...' );
  % Draw the figure
  Handle = figure;
  hold on; axis equal; axis ij;

  MosaicPreviewAbs ( M, ImgWidth, ImgHeight, 1, size ( M.nodes, 2 ), 1, [], true );
  InitCrossings,

  % Save Preview
  saveas ( MosaicPreview, MosaicPreviewFilePath );
end

